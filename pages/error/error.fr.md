---
title: Page not Found
---
{% set emoji = ['¯\\(°_o)/¯', '¯\\_(⊙︿⊙)_/¯', '(⊙_◎)', 'ミ●﹏☉ミ', '(⊙.☉)7']|randomize() %}

<p style="font-size:4em;" aria-label="Emoji triste ou bizarre en ascii art">{{ print_r(emoji[0]) }}</p>
Oups. Il semble que cette page n’existe pas ou plus.

[Retourner à l'accueil](/) - <a href="https://web.archive.org/web/*/{{uri.url}}" target="_blank">Retrouver sur archive.org</a>
<!-- {{ 'PLUGIN_ERROR.ERROR_MESSAGE'|t }} -->


