---
title: 'Nous rencontrer'
body_classes: modular
size: lg
class: rencontrer
---

## __Nous *rencontrer*__

---

<div id="map" style="height:350px;z-index:0;"></div>
<script>
var map = L.map('map').setView([48.57895,7.72832], 16);
var tiles = L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
    maxZoom: 17,
    attribution: '&copy; <a href="/mentions#credits">Crédits</a>'
}).addTo(map);
var marker = L.marker([48.57822,7.72831], {alt:'AUBE 97 avenue de colmar'}).addTo(map);
marker.bindPopup("<b>Hackstub à la Semencerie</b><br>42 Rue du Ban-de-la-Roche<br>67000 Strasbourg").openPopup();
</script>
<p style="display:none"></p>

### En physique, tous les vendredis soir, au Hackstub
Nous tenons une permanence <b>tous les vendredis soir à 20h</b> au [Hackstub](https://hackstub.eu), au _42 Rue du Ban-de-la-Roche_ à la _Semencerie_.
<br>
C'est l'occasion de <b>nous voir en personne</b> pour échanger sur les <b>sujets de notre association</b> ou une première approche pour <b>résoudre vos soucis</b> d'ordinateurs ou smartphones.

<a href="https://pad.sans-nuage.fr/p/charte-hackstub" target="_BLANK" role="button">Lire la charte du Hackstub</a>

---

![](Carte_Ateliers.jpg)
### Lors de nos ateliers, chaque mois :
<b>L'assemblée</b>, la réunion d'avancement : les 14 et 26 de chaque mois, _[en ligne](https://visio.colibris-lemouvement.org/b/als-bfk-obk-sjg)_ 🎧<br>
<b>hop!</b>, Halte à l'Obsolescence et au Pistage : les seconds samedis de chaque mois, _en physique_ 🧑‍<br>
<b>Slash</b>, la soirée de l'infra : les seconds et quatrièmes jeudis de chaque mois, _[en ligne](https://visio.colibris-lemouvement.org/b/als-bfk-obk-sjg)_ 🎧

<a href="https://mobilizon.sans-nuage.fr/@arnfai/events" target="_BLANK" role="button">Notre programme</a>

---
