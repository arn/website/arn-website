---
title: 'Nous contacter'
body_classes: modular
image_align: left
media_order: 'Contact.svg,Forum.svg,Chat.svg'
class: contacter
form:
    name: contact
    fields:
        name:
            id: contact-id
            label: Nom
            autocomplete: 'on'
            placeholder: 'Saisissez votre nom ou pseudo'
            type: text
            validate:
                required: true
        email:
            id: contact-email
            label: Email
            autocomplete: 'on'
            placeholder: 'Adresse Email'
            type: email
            validate:
                required: true
        subject:
            id: contact-subject
            label: Sujet
            type: select
            options:
                info: 'En savoir plus sur ARN ou un de ses services'
                support: 'J''ai besoin d''aide avec un des services ARN'
                educpop: 'Faire venir l''association pour une intervention'
                contribution: 'J''ai des idées, je veux participer'
                autre: 'Tout autre sujet'
        message:
            id: contact-message
            label: Message
            placeholder: 'Saisissez votre message (et Téléphone si pas d''Email)'
            type: textarea
            validate:
                required: true
        ville:
            id: contact-ville
            type: select
            label: 'Cinq fois 7 ?'
            options:
                paris: 1
                marseille: 2
                lyon: 3
                toulouse: 4
                nice: 5
                nantes: 6
                montpellier: 7
                strasbourg: 8
                bordeaux: 9
                lille: 10
                rennes: 11
                reims: 12
                toulon: 13
                saintetienne: 14
                lehavre: 15
                grenoble: 16
                dijon: 17
                angers: 18
                saintdenis: 19
                villeurbanne: 20
                nimes: 21
                clermontferrand: 22
                aixenProvence: 23
                lemans: 24
                brest: 25
                tours: 26
                amiens: 27
                limoges: 28
                annecy: 29
                boulognebillancourt: 30
                perpignan: 31
                metz: 32
                besancon: 33
                orleans: 34
                obernai: 35
                rouen: 36
                montreuil: 37
                argenteuil: 38
                mulhouse: 39
                caen: 40
                nancy: 41
                saintpaul: 42
                roubaix: 43
                tourcoing: 44
                nanterre: 45
                vitrysurseine: 46
                noumea11: 47
                creteil: 48
                avignon: 49
                poitiers: 50
                aubervilliers: 51
            validate:
                required: true
                pattern: ^obernai$
                message: 'Raté, refaites le calcul.'
    buttons:
        submit:
            type: submit
            value: 'Envoyer le message'
    process:
        captcha: false
        email:
            subject: '{{ form.value.subject|e }}'
            body: '{% include ''forms/data.html.twig'' %}'
            cc: '{{ form.value.email|e }}'
        message: 'Merci, votre message a été envoyé!'
        display: /merci
---

## __Nous *contacter*__

---

### ![](Contact.svg)Formulaire de contact

---
### ![](Forum.svg)Forum
N'hésitez pas à venir discuter avec nous sur notre forum / mailing liste.
<br>
<br>

[Forum](https://forum.arn-fai.net/)

---
### ![](Chat.svg)Salons de discussions
Pour un échange direct, vous pouvez utiliser le chat en ligne des bénévoles.

[IRC](https://kiwiirc.com/nextclient/?settings=966344fe1041eb2ea6857cc5a48dba58)
[Avec un compte Matrix](https://matrix.to/#/#arn:sans-nuage.fr)

Ou le chat d'entraide numérique alsacien si vous avez besoin d'aide pour utiliser ou configurer au mieux des outils libres.

[Matrix](https://chat.sans-nuage.fr/#/room/#alsace-entraide-numerique:sans-nuage.fr)
[Signal](https://signal.group/#CjQKIHBnMlCn9SSXYtu9n7_e_aiNztqYUAvcH5_Vl6OzTlzUEhCcw2USv8MENBAyA7Rg1C1-)
[Whatsapp](https://chat.whatsapp.com/Jf2lxTd7cntKhUUgUgHwc8)
<br>

