---
title: 'Devenir membre'
body_classes: modular
---
# __Espace *presse*__

Alsace Réseau Neutre est une association militante à but non lucratif qui s’oppose aux dérives des technologies du numérique.

Ses membres sont pour un numérique soutenable, accessible - y compris aux personnes aveugles ou en zone blanche - et hors des logiques de surveillance qui menacent nos libertés.

A ce titre, Alsace Réseau Neutre propose un bout d’utopie numérique composée de nombreux services en ligne alternatifs à ceux des GAFAM. L'association propose aussi des ateliers et des contenus pour accompagner les personnes qui souhaitent se libérer du pistage et faire durer leurs équipements.

## [safe-email autolink="true" icon="envelope-o"]contact+presse@arn-fai.net[/safe-email]

