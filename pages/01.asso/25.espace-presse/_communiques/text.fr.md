---
title: 'Communiqués de presse'
body_classes: modular
---
## __Nos *communiqués de presse*__

* [31/05/2023 - Les noces d'Étain - 10 ans de hacking](/blog/cp-10-ans)
* [25/04/2022 - Contribatelier sur l’accessibilité numérique](https://framablog.org/2022/06/21/retour-contribatelier-accessibilite-numerique/)
* [07/04/2020 - Lettre ouverte pour des services numériques viables dans l’Éducation nationale et lʼEnseignement supérieur](/blog/lettre-ouverte-pour-des-services-numeriques-viables-dans-leducation-nationale-et-l-enseignement-superieur)
* [09/03/2020 - Dérives sécuritaires et surveillance, jusqu'où irons nous ?](/blog/derives-securitaires-et-surveillance-jusquou-irons-nous)

