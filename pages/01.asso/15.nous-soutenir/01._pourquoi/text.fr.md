---
title: 'Pourquoi nous soutenir'
body_classes: modular
image_align: left
---

## __Pourquoi nous *soutenir ♥* ?__

Alsace Réseau Neutre est une association **à but non lucratif**, fonctionnant à ce jour **sur la base du bénévolat**. Chez ARN, il n’y a pas de clients ou clientes, mais des usagers et des usagères. **Tout le monde est invité à se regrouper et à se mobiliser** pour lutter contre les dérives technologiques et construire un bout d’Internet utopique.

Financièrement, les dons peuvent nous permettre d’avoir une politique plus solidaire, d’ouvrir en accès libre certains services, de défrayer le trajet de nos bénévoles aux évènements FFDN ou CHATONS, de renforcer l’existant et de démarrer de nouveaux projets.

**Si vous souhaitez lutter contre les dérives technologiques, rencontrer des personnes qui comprennent ces enjeux, nous aider à maintenir les outils dont vous avez besoin ou encore apporter de nouvelles idées et façons de faire, notre porte est grande ouverte 😊**
