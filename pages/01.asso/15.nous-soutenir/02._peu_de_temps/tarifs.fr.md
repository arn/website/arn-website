---
title: "Comment\_participer si j’ai peu de temps ?"
body_classes: modular
size: md
---

## __Comment participer si *j’ai peu de temps* ?__

---
### Relayer nos messages

* En parler autour de vous et dans vos associations
* Demander une intervention

<a href="https://wiki.arn-fai.net/benevoles:communication:parler" target="_blank" role="button">Lire le guide</a>

---
### Devenir membre

* Faire grandir l'asso
* Lui donner plus de voix
* Bénéficier de certains services
* un compte @sans-nuage.fr

<a href="/asso/adhérer" target="_blank" role="button">Adhérer</a>

---
### Faire un don

* Dons en euros
* Dons en matériel
* Dons en [monnaie libre ğ1](https://g1.duniter.fr/#/app/wot/9Yi8B3q1GjeYr6XqpBET6xz1B2pGF5PBGK1pAfBy...)<br>
_Important: pas de reçu défiscalisable_

<a href="https://don.arn-fai.net" target="_blank" role="button">Donner</a>

---
### Missions ponctuelles

* Ponctuelle
* Bénévole
* Réalisable en quelques heures
* Sans engagement de régularité

<a href="https://forum.arn-fai.net/tag/appel_%C3%A0_b%C3%A9n%C3%A9vole" target="_blank" role="button">Découvrir les missions</a>

---
