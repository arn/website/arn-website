---
title: 'Nous aider'
body_classes: nous-soutenir
visible: false
published: true
login: {  }
content:
    items: '@self.modular'
page-toc:
  active: false
---

<br><br>
<div class="soutien-title-image">
    <div id="image-title">
        <img src="/images/Soutien.svg" alt=" " class="img-soutenir"/>
        <h1 class="title-soutenir">Pourquoi nous <span class="rose-light">soutenir ?</span></h1>
    </div>
    <img src="/images/sous-lignage-gris.svg" alt=" " class="sous-lignage-gris1"/>
</div>
<br>
 <center>
     <div class="page_soutenir_para_pourquoi">
        <p>
        Alsace Réseau Neutre est une <strong>association à but non lucratif</strong>, fonctionnant à ce jour sur la base du <strong>bénévolat</strong>. Chez ARN, il n’y a pas de client⋅es, mais des usagers et des usagères. Tout le monde est invité à se regrouper et à se mobiliser pour lutter contre les dérives technologiques et construire un bout d’Internet utopique.
        <br><br>
        Côté financier, les <strong>dons</strong> peuvent nous permettre d’avoir une politique plus solidaire, d’ouvrir en accès libre certains services, de défrayer le trajet de nos bénévoles aux évènements FFDN ou CHATONS, de renforcer l’existant et de démarrer de nouveaux projets.
        <br><br>
        Si vous souhaitez lutter contre les dérives technologiques, rencontrer des personnes qui comprennent ces enjeux, nous aider à maintenir les outils dont vous avez besoin ou encore apporter de nouvelles idées et façons de faire, notre porte est grande ouverte :)
        </p>
    </div>
 </center>
<div class="contribuer-title2"> 
    <h2>Comment ?</h2>
    <img src="/images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Faire un don</h3>
            <div class="offre-para">
            <p>
            Vous trouverez sur ce lien le <a href="https://don.arn-fai.net">formulaire pour un don en euros</a>. <b>Attention : nous n’éditons pas de reçu de dons défiscalisables.</b>
            </p>   
            <p class="how_soutenir_para">
            Nous acceptons aussi les dons de matériel (voir la liste du matériel recherché) et les <a href="https://g1.duniter.fr/#/app/wot/9Yi8B3q1GjeYr6XqpBET6xz1B2pGF5PBGK1pAfBy...">dons en monnaie libre ğ1</a>.
            </p>
            </div>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Devenir membre</h3>
            <div class="offre-para">
                <p>
                <strong>Adhérer à Alsace Réseau Neutre</strong> permet de faire grandir l’association et de donner du poids à ses actions et sa prise de position. Chaque adhésion donne également accès à un compte sans-nuage pour vous aider à sortir de l'emprise des GAFAM, et ainsi contribuer à votre échelle.
                </p>
                </div>
                <a href="https://adherents.arn-fai.net/adherer" class="offre-button">Adhérer à l'association</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">
                Contribuer
            </h3>
            <div class="offre-para">
            <p>
            En donnant un peu de votre temps, vous pouvez grandement nous aider. Nous proposons régulièrement des <strong>missions ponctuelles</strong> dans <a href="https://forum.arn-fai.net/tag/appel_%C3%A0_b%C3%A9n%C3%A9vole">l’étiquette « appel à bénévoles » </a>sur notre forum. 
            <br>
            Nous communiquons autant que possible sur les missions ponctuelles réalisables en quelques heures et sans engagement de régularité. 
            </p>
            </div>
            <a href="#" class="offre-button">Venir à nos travateliers</a> 
        </div>
</div>

<div class="contribuer-title2"> 
    <h2>Contribuer, <span class="rose-light">c'est aussi</span></h2>
    <img src="/images/sous-lignage-gris.svg" alt=" " class="sous-lignage-gris3"/>
</div>

<div class="timeline">
    <div class="timeline-back"></div>
            <div class="timeline-item">
                <h3>Se tenir informé</h3>
                <p>Vous pouvez découvrir nos ressources sur notre page <a href="../educpop">Educpop</a>, pour vous informer sur le fonctionnement d’Internet et de nos services, et sur les principes que nous défendons.
                </p>
            </div>
            <div class="timeline-item">
                <h3>Faire part de vos envies</h3>
                <p>Nous vous encourageons à nous contacter, par mail ou sur le chat, pour exprimer vos envies. Ainsi, nous pourrons vous donner accès aux outils dont vous avez besoin pour participer et nous ne vous oublierons pas lors des prochaines actions susceptibles de vous intéresser.
                </p>
            </div>
             <div class="timeline-item">
                <h3>Échanger avec les membres</h3>
                <p>Venez à notre rencontre lors des <a href="https://mobilizon.sans-nuage.fr/@arnfai" >évènements et mobilisations</a> : c' est aussi une façon de nous soutenir. Vous pouvez aussi nous contacter sur le forum mail-liste et les chats indiqués sur la page <a href="../nous-contacter">contact</a>. Nous répondrons à toutes vos questions !
                </p>
            </div>
            <div class="timeline-item">
                <h3>En parler autour de vous</h3>
                <p>Faire relayer nos messages et nos luttes est tout aussi important pour nous. N’hésitez pas à diffuser dans votre entourage les informations sur <strong>nos services et sur les possibilités d’interventions</strong> menées par l'association. Nous avons également besoin de relais locaux et de relais dans les associations qui portent sur des thématiques similaires.
                </p>
            </div>
</div>
