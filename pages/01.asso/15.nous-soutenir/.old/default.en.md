---
title: 'Support us'
body_classes: nous-soutenir
visible: true
published: true
login: {  }
content:
    items: '@self.modular'
page-toc:
  active: false
---

<br><br>
<div class="soutien-title-image">
    <div id="image-title">
        <img src="images/Soutien.svg" alt=" " class="img-soutenir"/>
        <h1 class="title-soutenir">Why should you <span class="rose-light">support</span> us ?</h1>
    </div>
    <img src="images/sous-lignage-gris.svg" alt=" " class="sous-lignage-gris1"/>
</div>
<br>
 <center>
     <div class="page_soutenir_para_pourquoi">
        <p>Alsace Réseau Neutre is a <strong>non-profit association</strong> created and led by <strong>volunteers</strong>. At ARN, you are not a client, but a user, who is invited to fight against the misuse of technology and to build a digital utopia with us.
        <br><br>
        Money wise, <strong>donations</strong> can help us build a more cohesive policy, provide some services in free access, pay for volunteers to go to FFDN or CHATONS events, improve our existing projects and start new ones.
        <br><br>
        If you want to fight against the misuse of technology, meet people who understand these issues, help us maintain the tools you need, or bring new ideas and ways of doing things; then join us, our door is open :)
        </p>
    </div>
 </center>
<div class="contribuer-title2"> 
    <h1><strong>How?</strong></h1>
    <img src="images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Donation</h3>
            <div class="offre-para">
            <p>
            Follow this link to find the <a href="https://don.arn-fai.net">form for making a donation in euro</a>. 
            <b>Important: we do not provide invoices for tax-deductible donations.</b>
            </p>   
            <p>
            We also accept donations of devices and supplies (see the list of accepted devices and supplies) and <a href="https://g1.duniter.fr/#/app/wot/9Yi8B3q1GjeYr6XqpBET6xz1B2pGF5PBGK1pAfBy...">donations in the ğ1 free currency</a>.
            </p>
        </div>
    </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">
                Membership
            </h3>
                <div class="offre-para">
                <p>
                <strong>Joining Alsace Réseau Neutre</strong> means giving importance to the organization's actions and decisions. Members also have access to a sans-nuage - 'cloud-free' - account, that serves as an alternative to GAFAM services, so that you can contribute to our goal.
                </p>
                </div>
                    <a href="https://adherents.arn-fai.net/adherer" class="offre-button">Join us</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">
                Contribution
            </h3>
            <div class="offre-para">
            <p>
            By giving just a little of your time, you can help us greatly. We often post <strong>punctual mission offers</strong> in the <a href="https://forum.arn-fai.net/tag/appel_%C3%A0_b%C3%A9n%C3%A9vole">"call for volunteer" section of our forum</a>. 
            We communicate as much as possible about these missions. They can be completed in just a few hours, and do not involve any commitment.
          </p>
          </div>
                <a href="#" class="offre-button">Contribute</a>
        </div>
</div>

<div class="contribuer-title2"> 
    <h2>Contributing <span class="rose-light">is also...</span></h2>
    <img src="images/sous-lignage-gris.svg" alt=" " class="sous-lignage-gris3"/>
</div>

<div class="timeline">
    <div class="timeline-back"></div>
        <div class="timeline-item">
                <h3>Staying informed</h3>
                <p>You can take a look at our resources on the <a href="../en/educpop">Educpop</a> page to learn more about the way Internet works, our services, and our ethics.
                </p>
            </div>
            <div class="timeline-item">
                <h3>Letting know what you need</h3>
                <p>We encourage you to contact us and talk about your needs. This way, we can give you access to the tools you need to participate and we will not forget you in the next actions that may catch your interest.
                </p>
            </div>
        <div class="timeline-item">
                <h3>Talking with our members</h3>
                <p>Attending our <a href="https://mobilizon.sans-nuage.fr/@arnfai" >events and mobilizations</a> is also a way to support us. You can contact us on the forum mail-list and the chats indicated on the <a href="../en/nous-contacter">contact</a> page. We will be happy to answer your questions!
                </p>
            </div>
            <div class="timeline-item">
                <h3>Talking about us</h3>
                <p>
                Even if you don't have a lot of time available, spreading words about our actions is also important. Do not hesitate to share about our <strong>services, workshops and public interventions</strong>. We also need local relays as well as relays in  associations that deal with similar issues.
                </p>
            </div>
</div>
