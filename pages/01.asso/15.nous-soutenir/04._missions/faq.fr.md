---
title: 'Missions et groupes de travail'
body_classes: modular
tag: 'h4'
---

### Les projets et *missions récurrentes*

!!!! Voir aussi les [missions ponctuelles](https://forum.arn-fai.net/tag/appel_%C3%A0_b%C3%A9n%C3%A9vole) qui ne nécessitent qu'un faible investissement temporelle

Si vous souhaitez prendre en charge l'une de ses tâches, [contactez-nous](/contact#nous-contacter) ou discutez-en avec d'autres membres de l'asso. **Un système de tutorat peut être mis en place pour que vous soyez accompagné⋅e jusqu'à devenir autonome.**


---

#### Éducation populaire & plaidoyer

**Personnes référentes:** Gabjy, scapharnaum

##### Missions réccurentes

   * Participer à la tenue de stands, d'ateliers ou de conférences
     * Organiser et animer les stands de la rentrée ([Village des Associations](https://village-assos.mdas.org), fête de rentrée de l'acrociation)
     * Participer aux permanences accessibilité
     * Tenir une table lors d'un Café HOP (Halte à l'obsolescence et au pistage)
     * Participer aux [Repair Café de l'Acrociation](https://www.facebook.com/acrociation)
     * Co-animer les hack-ver-alli avec [Hackstub](https://hackstub.eu/home/fr)
     * Concevoir la programmation de l'année et répondre aux demandes d'interventions
   * Créer et rassembler des ressources
     * Compléter la bibliographie
     * Alimenter la veille (Topic: Ayez confiance)
     * Concevoir ou rassembler des supports
     * Création d'article sur le blog


##### Projets en cours (09/2023)
 * Campagne d'information concernant le moteur de recherche Lilo

##### Projets à venir

 * Création de campagne de sensibilisation
 * Tutos EducPop et services (ou collaboration wikilibriste)
 * Publier un catalogue des interventions possibles
 * Animer nationnalement les ateliers Network & Magic
 * Plaidoyer
 * Campagne de sensibilisation
 * Observatoire du tracking
 * Constitution d'un annuaire local de professionnel "sans tracking" (médecin hors doctolib? pro du numérique?)

##### Projets passés
 * Contribution sur le décret au sujet du contrôle parental

---

#### Support et provisionnement

[**&rarr; Tâches, compétences et manifestation d'intérêt**](https://forum.arn-fai.net/t/support-et-provisionnement-suivi-des-taches-a-faire-ensemble-et-manifestation-dinteret/7650)

**Personnes référentes:** Gaut

!! **Important:** avant de pouvoir participer à des tâches nécessitant des accès sensibles (infra, administration des membres), il est nécessaire de venir à plus d'une dizaine de rencontre en présentiel.

##### Missions réccurentes


   * Support général: aiguillage des messages
   * Support et provisionnement "Sans-nuage"
   * Support et provisionnement "Tunnels VPN"
   * Support et provisionnement "Serveurs privés virtuels"
   * Support et provisionnement "Accès internet fibre optique"
   * Support et provisionnement "Housing"
   * Support et provisionnement "Reverse DNS"

---

#### Maintenance et amélioration continue des services
[**&rarr; Tâches, compétences et manifestation d'intérêt**](https://forum.arn-fai.net/t/infra-suivi-des-taches-a-faire-ensemble-et-manifestation-dinteret/7662).

&rarr; Voir aussi les [sujets épinglés concernant l'amélioration de sans-nuage.fr](https://forum.arn-fai.net/c/discussions/sans-nuage/8)

**Personnes référentes:** ljf, seb, gaut

!! **Important:** avant de pouvoir participer à des tâches nécessitant des accès sensibles (infra, administration des membres), il est nécessaire de venir à plus d'une dizaine de rencontre en présentiel.

##### Missions réccurentes

De façon transversale, il s'agit de:

   * mettre à jour les serveurs
   * contrôler les sauvegardes
   * configurer le monitoring
   * prévenir les pannes
   * intervenir en cas de panne ou d'alerte
   * agrandir les murs
   * contrôler les accès


Une répartition par machine ou service peut être envisagé:

   * Cluster Ganeti et routeur BGP
   * Cluster Proxmox
   * VM sans-nuage.fr
   * VM public.sans-nuage.fr
   * VM ynh
   * VM forum
   * VM VPN
   * VM Résolveur DNS
   * VM Autorité DNS
   * VM Wiki + Monitoring + slave DNS
   * VM Monitoring Zabbix
   * Smartphone bridge whatsapp

##### Projets en cours (09/2023)
   * Migration vers Proxmox

##### Projets à venir

   * Projet Mobilizon
   * SBG-IX
   * Rénovation routage et switch
   * ROSACE
   * Fibre optique FFDN
   * Statistique
   * Rénovation DNS (DoH)
   * Hébergement de zone DNS (eu.org & co)

---

#### Développement logiciel

**Personnes référentes:** Aleks, ljf, gaut

##### Missions réccurentes

   * Développement Siteweb
   * Développement Libreto
   * Développement ARN-messager
   * Développement COIN
   * Développement des scripts de provisionnement
   * Test d'accessibilité
   * Reporter (voir corriger) les bugs des packages/app liés à nos services

---

#### Communication externe

**Personnes référentes:** Optogram


##### Missions réccurentes

   * Promotion de nos évènements
   * Animation des réseaux sociaux ([https://wiki.arn-fai.net/benevoles:communication:partager))](https://wiki.arn-fai.net/benevoles:communication:partager)))
   * Faire vivre le blog (si il manque d'activité)
   * Publier l'info-lettre
   * Mise à jour des contenus du siteweb et wiki
   * Réseautage
   * Distribution de flyers et affichage


---

#### Animation interne

**Personnes référentes:** Optogram, ljf

##### Missions réccurentes

   * Animer les réunions mensuelles (animation, gestion du temps, compte rendu)
   * Organiser et ranger le forum
   * Gérer l’épinglage des sujets
   * Marquer les sujets résolus comme tel (en cas d'oubli)
   * S'assurer que les sujets sont dans les bonnes catégories et formatés comme il faut
   * Tenir les groupes du forum à jour
   * Tenir les groupes nextcloud à jour
   * Organiser l'AG
   * Organiser les ARN Camp
   * S'assurer que les workflows de l'asso sont fluides

---

#### Accueil, inclusion, médiation et modération

**Personnes référentes:** -

##### Missions réccurentes

   * Envoyer le mail de bienvenu aux nouvelles et nouveaux membres utilisateurices
   * Envoyer le mail de bienvenu aux nouvelles et nouveaux membres bénévoles
   * Faire le suivi des nouvelles et nouveaux membres bénévoles
   * Veiller au respect du code de conduite
   * Prévenir, gérer et apaiser les conflits
   * Modérer le forum
   * Modérer les messageries instantanées


##### Projets à venir
 * Livret d'accueil des bénévoles
 * Rénovation de la documentation bénévoles

---

#### Conseil d'Administration

**Composition 2022-2023:**
 * Optogram (Président)
 * ljf (Vice-président)
 * Sepp (Secrétaire)
 * Scapharnaum (Trésorier)
 * Fgermani (Vice-trésorier)
 * Gautgaut
 * ced117
 * Hackstub
 * Alain

!! **Important:** avant de pouvoir participer à des tâches nécessitant des accès sensibles (infra, administration des membres), il est nécessaire de venir à plus d'une dizaine de rencontre en présentiel.

##### Missions réccurentes

Le conseil d'administration prend toutes les décisions nécessaires à la gestion quotidienne de l’association, mais délègue en général cette fonction aux assemblées des bénévoles. Dans la pratique , il s'agit surtout de:

   * Se répartir la présidence, le secrétariat et la trésorerie
   * Voter concernant les décisions politiques ou financières
   * Prononcer des sanctions contre des membres (exclusion, etc.)
   * Plaidoyer auprés d'instance nationale

###### Présidence
   * Signer les lettres ouvertes
   * Représenter légalement l'association
   * Représenter médiatiquement l'association
   * Veiller au respect des statuts
   * Voter et représenter l'association dans nos collectifs (CHATONS et FFDN)

###### Secrétariat
   * Faire les déclarations administratives
   * Tenir le registre des délibérations de l'AG et du CA
   * Rédiger le pv d'AG et autres documents concernant l'association
   * Veiller à ce qu'il y ai des comptes rendus de chaque réunion et actions
   * Veiller à la compostabilité de l'association

###### Trésorerie
   * S'assurer que les abonnés payent, faire les relances
   * Prononcer la clôture d'un abonnement
   * Tenir une comptabilité probante et régulière, tenir un grand journal (en compta de trésorerie pour l'instant)
   * Rendre compte de sa gestion aux autres membres
   * S'assurer que les fournisseurs et les notes de frais sont payés
   * Importer régulièrement les comptes dans COIN en faisant attention que le matching automatique ne se soit pas trompés (un écran de validation est prévu pour ça dans coin)
   * Faire des projections et alerter avec des éléments pour mesurer les risques (genre savoir quand on aura plus de sous)
   * Récupérer les accès au comptes bancaires pour lui et une ou 2 autres personnes de l'association
   * Éditer un compte de résultat et un bilan (à la date de l'exercice, apparemment c'est 31/12 et pas 30/05 !)
   * Conserver les pièces justificatives de façon ordonnées et numérotées
   * Inventorier le matériel et suivre les prêts de matériel
   * S’assurer que nos politiques tarifaires restent viables et les faire évoluer

##### Projets en cours (09/2023)

   * Création réglement intérieur et code de conduite

##### Projets à venir

   * Rénovation statutaire (officialiser les binômes? collégiale ? écriture inclusive)
   * Trouver comment croitre (si on le souhaite)
   * Amélioration revendications (tag, référence aux actions)


---


!!! Le « Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires » et la « Fédération des FAI associatifs » ont chacun une organisation en groupe de travail. Selon la situation, il peut être pertinent de travailler à l'échelle nationale plutôt que locale.
