---
title: "Et pour s’engager d’avantages\_?"
body_classes: modular
image_align: left
---

## __Et pour s’engager *davantage* ?__

### Se signaler et *indiquer ses souhaits*

Nous vous encourageons à remplir ce formulaire, ainsi nous pourrons vous donner des accès à ce dont vous avez besoin pour participer et nous ne vous oublierons pas lors des prochaines actions susceptibles de vous intéresser.

---
### Se coordonner

Nous utilisons essentiellement le [forum mail-liste](https://forum.arn-fai.net) et les [chats indiqués sur la page contact](/contact). 

---
### Venir aux *rendez-vous*

Ci-dessous, quelques rendez-vous auxquels vous pouvez prendre part:

 * 🎧 [**L'assemblée**, La réunion d'avancement d'Alsace Réseau Neutre](/agenda) : Les 14 et 26 de chaque mois, à 19h30 en ligne à [cette adresse](https://visio.colibris-lemouvement.org/b/als-bfk-obk-sjg)
 * 🧑‍ [Permanences du **Hackstub**](/contact#nous-rencontrer) : Tout les vendredis soirs à partir de 20h à la Semencerie
 * 🧑‍ [**Café HOP!**, Halte à l'Obsolescence et au Pistage](/agenda), notre atelier mensuel tout les seconds samedis du mois à partir de 14h.
 * 🎧 [**/Slash/**, la soirée de l'infra](/agenda) nos moments de maintenance et mise à jour, les 2eme et 4eme jeudi du mois à 20h en ligne à [cette adresse](https://visio.colibris-lemouvement.org/b/als-bfk-obk-sjg)
 * [**ARNCamp**, nos moments de développement](/agenda), à suivre sur notre agenda
 * [La réunion mensuelle des CHATONS](https://forum.chatons.org/t/calendrier-des-reunions-virtuelles-mensuelles-2022/2975)
 * 🎧 La réunion mensuelle de la Fédération FDN : le 10 de chaque mois à 21h en ligne sur BigBlueButton
 * 🎧 La réunion mensuelle de la quadrature du net : le 20 de chaque mois à 20h en ligne sur BigBlueButton

---

### Découvrir *les ressources*

 * [Wiki des bénévoles](https://wiki.arn-fai.net/benevoles:menu)
 * [Le drive](https://sans-nuage.fr/file/)
 * [Le libreto](https://libreto.arn-fai.net/arn)
 * [La forge git](https://code.ffdn.org/arn)

---

