---
title: 'Conditions Générales de Services'
body_classes: asso
visible: false
published: true
login: {  }
content:
    items: '@self.modular'
routes:
  default: '/cgs'
  aliases: 
    - '/asso/cgs'
    - '/cgu'
page-toc:
  active: true
sitemap:
  priority: !!float 0.1
---
# Conditions Générales de Services

04/05/2021

En utilisant les services d'Alsace Réseau Neutre ou de sans-nuage.fr, vous acceptez les conditions suivantes.

## Évolution des conditions générales de services

Alsace Réseau Neutre se réserve le droit de mettre à jour et modifier ces conditions. Dans ce cas, Alsace Réseau Neutre informe les personnes concernées par mail si elle le peut ou par un affichage sur le site.

## Accès aux services

### Services en libre-accès vs services sur adhésion et abonnement

**Certains services sont en accès libres d'autres nécessitent de souscrire un abonnement. Tous les services sur abonnement nécessitent d'adhérer à l'association**

Ci-dessous, sont listés les services en libre-accès :

 * Les pads publiques (etherpad)
 * Les wiki (libreto)
 * Le transfert de fichiers temporaires (lufi)
 * La création de présentation animée (strut)
 * La prise de rendez-vous et les petits votes (opensondage)
 * La création de flux RSS pour des sites qui n'en ont pas (RSS-Bridge)
 * Le résolveur DNS ouvert
 * La consultation d'évènements mobilizon

Pour l'utilisation de tous les autres services, votre groupe ou vous-même devez adhérer à l'association en vous acquittant de la cotisation annuelle. Cela vaut y compris pour les services payants sur abonnement.Certains services comme le gestionnaire de tâches(Wekan), les pads privés, le bridge Matrix/Whatsapp, le cloud ou la visio (Nextcloud) permettent de donner un accès limité à des personnes tierces invitées. le ou la membre, cette personne est en charge de veiller au respects de ces CGS.

### Approbation technique préalable

Les services sur abonnement sont soumis à l'approbation technique préalable de l'équipe technique selon les ressources disponibles.

### Périmètre géographique

Alsace Réseau Neutre s'adresse avant tout aux personnes physiques ou morales résidant en alsace ou ayant un lien avec l'alsace. Toutefois, il est possible d'utiliser les services et d'adhérer en dehors de ce périmètre si aucun service équivalent n'est disponible plus proche de chez-vous.
La langue d'échange pour le support, les ateliers et les réunions est le français.

## Fonctionnement

### Délais de mise en service

**L'association ne s'engage sur aucun délais de mise en service**

Alsace Réseau Neutre propose les services suivants grâce à des bénévoles, de ce fait l'association ne s'engage sur aucun délais de mise en service. Nous essayons toutefois de faire de notre mieux pour les fournir dans les 48h.

### Support et conseil

**Tout problème technique doit être signalé par mail sur les adresses de support.**

Vous pouvez nous proposer vos souhaits sur la création de futur service, mais sachez que nous ne pourrons pas créer un nouveau service en moins de 6 mois. L'association pourra toutefois vous rediriger vers des CHATONS à même de répondre à vos demandes.

Merci d'éviter d'utiliser les adresses de support pour obtenir des conseils techniques en dehors du cadre des services proposés (par exemple sur la façon d'installer tel ou tel logiciel sur votre VPS ARN).

### Transmission sécurisée d’identifiants
**Alsace Réseau Neutre ne vous demandera jamais de communiquer vos mots de passe**

Alsace Réseau Neutre ne vous demandera jamais de communiquer vos mots ou phrases de passe. Lorsque l'association doit vous transmettre un identifiant elle le fera via un lien sécurisé à usage unique disponible dans l'espace membre.
 
### Intervention en cas de panne
**Si un service casse, on fera de notre mieux MAIS nous ne sommes pas obligés de réparer**

Alsace Réseau Neutre propose l'ensemble de ces services grâce à des bénévoles qui feront ce qu'ils et elles peuvent pour résoudre les problèmes techniques qui pourraient subvenir.
 
Si vous avez un abonnement, vous êtes également membre et à ce titre vous avez une part de responsabilité dans le bon fonctionnement de l'association. Si le service est indisponible ou si vos données sont perdus, ce sera donc une faute collective.
 
En cas d'incapacité à résoudre un problème technique, l'association pourra prendre la décision de fermer le service.
 
### Responsabilité d'Alsace Réseau Neutre
**En aucun cas, un utilisateur ou une utilisatrice ne pourra se prévaloir de dommages ou indemnités résultant de problèmes techniques de quelque nature que ce soit.**
 
Alsace Réseau Neutre est assujetti à une obligation de moyens, considérant la haute technicité des technologies mises en œuvre. En cas de défaillance, Alsace Réseau Neutre ne peut être tenu pour responsable des dommages indirects tels que pertes d’exploitation, préjudices commerciaux, perte de Clientèle, de chiffre d’affaires, de bénéfices ou d’économies prévus, ou de tout autre préjudice indirect.
 
### Mésusage des services
**Alsace Réseau Neutre n'est pas là pour vous couvrir et prendre des risques légaux à votre place, même si votre action est légitime, vous êtes entièrement responsables de ce que vous faites.  La consommation de ressources doit respecter le cadre mutualisé de nos services. Tout abus peut entraîner la fermeture d'un compte. Vous devez par ailleurs tenir à jour votre adresse mail de contact.**
 
Toute personne utilisatrice doit respecter les lois et réglementations en vigueur lors de l’usage des services proposés que ce soit en matière de respect de la vie privée, d’envoi de mails en grande quantité, de propriété intellectuelle, de propos discriminatoires, d’appel à la haine, de harcèlement, d’atteinte aux libertés fondamentales de personnes, etc.
 
En cas d’usage prohibé, Alsace Réseau Neutre peut se trouver dans l’obligation de déclencher la suspension totale ou partielle du service, le retrait de contenu, ou toute autre mesure que les lois et réglementations lui imposent.
 
Par ailleurs, si un ou une utilisatrice abuse du service, par exemple en monopolisant des ressources machine partagées, son contenu ou son accès pourra être supprimé, si nécessaire sans avertissement ni négociation. Alsace Réseau Neutre reste seul juge de cette notion « d’abus » dans le but de fournir le meilleur service possible à l’ensemble des usagers et usagères.
 
### Devenir des services
**Rien n’est éternel**

L'association peut par ailleurs choisir de résilier des abonnements ou d'arréter des services si elle estime ne plus être en mesure de fournir les dits services. Si elle en a la possibilité, elle fera de son mieux pour laisser un délais suffisant pour permettre à tout le monde de migrer sereinement.
 
## Nos engagements
**Respect de vos données personnelles, votre vie privée, votre propriété intellectuelle, vos droits ainsi que le principe de neutralité du net**

Alsace Réseau Neutre n’exploitera vos données personnelles que dans le cadre de ces 5 finalités:

 * fournir les services pour lesquels vous avez transmis vos données
 * produire d'éventuelles statistiques anonymisées et agrégées
 * vous prévenir d’un changement important sur le service (panne, notification d'intrusion et de vol de données, changement d'interface, date d'arrêt du service...)
 * obtenir votre avis sur les services et l'action de l'association
 * vous inviter à participer à un évènement d'Alsace Réseau Neutre
 * répondre à des demandes légales

 
Alsace Réseau Neutre ne transmettra ni ne revendra vos données personnelles (votre vie privée nous tient - vraiment - à cœur). Votre contenu vous appartient tout autant, toutefois, nous vous encourageons à le publier sous licence libre si c'est pertinent.
 
### Chartes et collectifs
**Nous respectons les chartes de la FFDN et des CHATONS.**
 
Alsace Réseau Neutre s’engage à : 

 * respecter la charte du Collectif des Hébergeurs, Alternatifs, Transparents, Ouverts, Neutres et Solidaires dans le cadre de son activité d'hébergeur et de fourniture de services en ligne
 * à respecter la charte de la Fédération FDN dans le cadre de son activité de fournisseur d'accès internet

Alsace Réseau Neutre est membre de ces deux collectifs. 
 
Remarque : concernant la fonctionnalité qui permet de suivre une conversation whatsapp depuis matrix celle-ci est de facto dépendante du logiciel propriétaire Whatsapp (bien isolé sur notre infrastructure). Nous estimons toutefois que notre action visant à permettre au maximum de monde de migrer vers Matrix et à empécher Whatsapp d'établir des profils individuels, justifie cette entorse à la charte C.H.A.T.O.N.S. Le collectif a été consulté via son forum et n'a pu se positionner clairement pour ou contre.
 
 * la [charte C.H.A.T.O.N.S.](https://chatons.org/fr/charte)
 * la [charte de la Fédération FDN](https://www.ffdn.org/fr/charte)
 
### Localisation des données
**Vos données sont en Alsace**

Alsace Réseau Neutre héberge ses services et son infrastructure en France. Les serveurs sont actuellement situés à Schiltigheim et sont la propriété d'Alsace Réseau Neutre. Les sauvegardes quotidiennes sont stockées,de façon chiffrée, à Strasbourg chez des membres de l'association.
 
### Devenir des données 
Une fois la résiliation effective, l'association peut procéder à la suppression des données ou décider de les conserver jusqu'à 2 mois en cas de résiliation suite à un retard de paiement.
 
Certains services en libre accès permettent de configurer la péremption des données, d'autres les conservent de façon permanente, mais vous pouvez demander leur retrait si vous pouvez prouver que vous en êtes l'auteur ou autrice.
 
Concernant les services de VPN, de VPS, d'Housing ou de FAI, ARN tient un fichier log des attributions des adresses IP concernées au cours des 365 derniers jours.
 
### Exercice de vos droits
Vous pouvez exercer les droits suivant par mail au CA de l'association (droits d’accès, de rectification, d’effacement et d’opposition, droit à la limitation du traitement, droit à la portabilité des données.Droit de ne pas faire l’objet d’une décision individuelle automatisée, par le biais d'algorithmes appliqués à vos données personnelles.
 
## Conditions financières
### Tarifs
**Certains services sont accessibles en G1**

Pour les services payants, les prix sont mentionnés en euros ou en G1 sur le site. Les prix sont indiqués en franchise de TVA (art 293b du CGI).
 
### Modification des prix
**En cas de difficultés financiaires le prix de votre abonnement peut évoluer**

En règle général, les prix initiaux sont maintenus et les augmentations discutées en Assemblée Générale. Cependant, la personne abonnée est encouragée à demander la mise à jour de son prix vers le nouveau pour aider financièrement l'association.
 
Enfin, en cas de difficulté financière, le CA peut choisir d'imposer la nouvelle tarification dès le mois suivant.
 
### Tacite reconduction
**Les abonnements sont sans engagements mais sont reconduits tacitement (ainsi que l'adhésion si vous avez un abonnement actif)**
 
Tout abonnement est reconduit chaque mois tacitement. L'adhésion annuelle est elle aussi reconduite tacitement si la personne adhérente possède encore un abonnement actif, par exemple un compte sans-nuage.fr.
 
### Facturation
**Les abonnements payants sont facturés au pro-rata**
 
Les factures sont disponibles en début de mois sur l'espace adhérent ou adhérente.
La facturation des abonnements est faite au pro-rata des jours d'abonnements. Par exemple si un VPN est commencé le 15 avril, la facture pour la période du 15 au 30 avril sera le prix d'un mois divisé par 2. Nous procédons de la même façon pour la cloture.
Attention: les adhésions annuelles ne sont pas facturées au pro rata.
 
### Retard de paiement
**La mise en place d'un virement mensuel automatique est recommandée
Tout impayé ayant fait l'objet d'une relance depuis 30 jours ou plus entraînera la coupure immédiate du service.**
 
L'espace adhérent calcule un solde pour chaque compte. Tout solde négatif de 15€ ou plus fera l'objet de relance automatique à chaque import des comptes (environ toutes les 2 semaines). 
 
Les usagers qui le souhaitent peuvent payer plusieurs mois à l'avance, le solde sera alors positif. L'association préfère toutefois la mise en place d'un virement mensuel automatique.
 
Sauf exception décidée par le trésorier ou le CA, tout impayé ayant fait l'objet d'une relance depuis plus de 30 jours entraînera la coupure immédiate du service.Aucune indemnité ou dommage ne pourra être revendiqué et les sommes facturées non payées resteront dues par l'abonné ou abonnée.
 
### Résiliation d'un abonnement par l'abonné ou abonnée
**Si vous souhaitez résilier un abonnement, vous devez le signaler à l'association et ne pas attendre que des bénévoles passent du temps à se demander ce que signifie l'arrêt de vos paiements**
 
Il est du devoir de la personne adhérente d'avertir l'association de son souhait d’arrêter un abonnement 15 jours avant la date de fin d'abonnement souhaité. Si les abonnements à résilier représentent plus de 100€ / mois le délais de pré-avis est porté à 3 mois.Une décision contraire du CA peut autoriser un pré-avis exceptionnellement plus court.
 
## Litige et juridiction compétente                         
Le droit applicable aux présentes dispositions est le droit français. En cas de différent, les parties recherchent une solution amiable. Si la démarche échoue, le litige sera tranché par le Tribunal de Grande Instance de STRASBOURG.
 
Le fait que l'usager ou Alsace Réseau Neutre ne se prévale pas à un moment donné de l’une des présentes conditions générales et/ou tolère un manquement par l’autre partie ne peut être interprété comme valant renonciation par l'usager ou Alsace Réseau Neutre à se prévaloir ultérieurement de ces conditions.
 
La nullité d’une des clauses de ces conditions en application d’une loi, d’une réglementation ou d’une décision de justice n’implique pas la nullité de l’ensemble des autres clauses. Par ailleurs l’esprit général de la clause sera à conserver en accord avec le droit applicable.


# Conditions spécifiques de services
## Forum / Mailing liste

Chaque personne est invitée dans ses échanges à faire de son mieux pour éviter qui pro quo, joute, trolls, pertes de temps et d’énergie, comportements excluants ou discriminants, etc.

N’oubliez pas d’écrire des vrais textes alternatifs pour vos images, il y a au moins une personne active dans l’association qui s’en servira avec son lecteur d’écran. Pour la même raison, évitez d’utiliser les formes contractées de l’écriture inclusive « doublets abrégés » (même le point médian). A la place, optez plutôt pour la règle de proximité, les mots épicènes, les formules englobantes, les doublets complets, faite appel à votre imagination pour que votre texte soit accessible ET inclusif.

Même si en France l’informatique est socialement assimilée à un milieu masculin, évitez de présumer des compétences de vos interlocutrices (mansplaining).

## VPN & Accès Internet (fibre, radio)

 
## VPS
Outre l'espace disque, la RAM et les IPs, aucune ressource (débit, CPU, vitesse du disque dur) n'est garantie dans ce service.
 
## Housing
La consommation électrique de la machine doit être raisonnable et validée au préalable par l'équipe technique. Le matériel hébergé ne doit pas nuire au bon fonctionnement de l'infrastructure de l'association.

## Sans-nuage
### Email, XMPP et Matrix
Merci de ne pas utiliser nos services de mail et de messageries instantannées pour spammer des personnes. 

Le fait qu'il y ai des limites hautes ne signifie pas que vous pouvez faire n'importe quoi et envoyer 1 mail à 500 personnes. N'hésitez pas à demander à un ou une admins si vous avez un doute.

### Mobilizon
Le mobilizon accepte pour le moment tout évènement organisé par un membre ARN. Cette politique pourrait être revu, nous ne savons pas très bien où nous allons avec ce service.

### Lufi
Merci de ne pas abuser du service. C'est un bien commun tout le monde devrait pouvoir en profiter.



















