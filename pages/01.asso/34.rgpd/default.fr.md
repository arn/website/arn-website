---
title: 'RGPD'
body_classes: asso
visible: false
published: true
login: {  }
content:
    items: '@self.modular'
page-toc:
  active: true
sitemap:
  ignore: true
  priority: !!float 0.1
---
# __ARN et *le RGPD*__

**Alsace Réseau Neutre n'est pas en conformité avec le RGPD.** 

Au moins nous sommes honnêtes; ce qui n'est pas le cas de nombreux géants du net qui sont en totale contradiction avec le RGPD et **l'esprit** de cette réglementation. Si autour de nous de nombreuses petites structures ont éffectivement pris du temps pour essayer de se mettre en conformité et ont donc réfléchis sur le sujet, la plupart des grosses entreprises continuent de faire n'importe quoi.

De notre côté, nous suivons l'esprit qui a poussé à la création de cette législation. Nous faisons même parties des structures qui oeuvrent pour que ce genre de législation soit mise en place. Ici pas de bandeau cookies puisque nous ne permettons pas la récolte d'informations à l'insu de la compréhension des utilisateurices de ce site (ou d'un de nos outils).

Si nous ne sommes pas en conformité, c'est parce que le RGPD inclut des articles très bureaucratiques pour faire semblant d'encadrer ce qui devrait selon nous être interdit. Ainsi, il nous faudrait par exemple créer un registre des données personnelles (difficile à tenir vu le nombre de service que nous gérons).

Nous préférons allouer du temps pour rendre notre site et nos outils accessibles, légers et faire nos mises à jour. Mais qui sait peut être qu'un jour, un un ou une bénévole se penchera sur notre conformité.

**Toujours est-il, qu'aujourd'hui, en 2022, nous constatons que le RGPD ne tient pas ses promesses et échoue dans sa mission de protection et de régulation[^1].**

Nous **réclamons l'interdiction**:

 * de la collecte de données personnelles (hors formulaire validé),
 * de la surveillance de masse
 * des algorithmes visant à induire un comportement chez une personne ou une population
 * et bien d'autres choses

<a href="/asso/revendications">Découvrir nos revendications</a>


[^1]: Un petit exemple d'échec du RGPD que [SirData ai raison ou non sur son analyse des textes](https://invidious.fdn.fr/watch?v=gkP9ORGBm-U)
