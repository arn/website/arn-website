---
title: Communautés
body_classes: asso
visible: true
published: true
login: {  }
content:
    items: '@self.modular'
page-toc:
    active: true
---

## __Collectifs dont *nous sommes membres*__

### ● Le CHATONS

<a href="https://chatons.org" target="_blank">CHATONS</a> est le **Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires**. Ce collectif regroupe une centaine de structures proposant des **services en ligne libres, éthiques et décentralisés** afin de permettre aux utilisateurices de <a href="https://www.chatons.org/search/by-service" target="_blank">trouver des alternatives respectueuses de leurs données</a> et de leur vie privée aux services proposés par les GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

### ● La Fédération des Fournisseurs d'Accès Internet Associatifs (FFDN)

La <a href="https://ffdn.org" target="_blank">Fédération des Fournisseurs d'Accès Internet Associatifs</a> regroupe des associations et SCIC se reconnaissant dans des valeurs communes : bénévolat, solidarité, fonctionnement démocratique et à but non lucratif ; défense et promotion de la neutralité du Net.
À ce titre, la Fédération FDN se donne comme mission de porter la  voix de ses membres dans les débats concernant la liberté d'expression  et la neutralité du Net.

### ● Hackstub

<a href="https://hackstub.eu" target="_blank">Hackstub</a> est un hackerspace strasbourgeois actuellement situé à la Semencerie, au 42 rue du Ban de la roche à Strasbourg, ouvert (presque) tous les vendredi soirs et parfois en semaine.

L'association sensibilise le public local à la culture et l'informatique Libre et mène un travail d'éducation populaire, à travers des ateliers, projections, podcasts, débats, etc.

L'accès au hackerspace et la participation aux activités proposées sont soumis.e.s au respect d'une <a href="https://pad.sans-nuage.fr/p/charte-hackstub" target="_blank">charte</a>, adressée tant aux usager⋅ères qu'aux membres de l'association. Merci d'en prendre connaissance avant de vous y présenter !

## __Autres initiatives *d'ici ou d'ailleurs*__

### ● Desclicks

L’association « <a href="https://desclicks.net" target="_blank">L’Informatique Solidaire – Desclicks</a> », basée au 3 rue Saint Paul à Schiltigheim,   a pour objet de  rendre les technologies de l’information et de la communication  accessibles au plus grand nombre. Afin de répondre aux différents  besoins et de réduire la fracture numérique, l’association  propose  plusieurs services à ses membres : acquisition d’ordinateur sous GNU/Linux, impressions, formations, maintenance, cybercafé...

### ● Résistance à l’Agression Publicitaire Strasbourg

<a href="https://antipub.org/category/groupe-locaux/strasbourg/" target="_blank">Résistance à l’Agression Publicitaire</a> (R.A.P.) est une association nationale qui lutte contre les effets néfastes de la publicité. Actions ludiques, créatives et non-violentes, actions juridiques, interpellation des élus, réflexions et sensibilisation des  médias et du public, etc. ses modes d’actions sont diversifiées, Alsace Réseau Neutre étant très actif sur la question du pistage en ligne qui sert majoritairement dans la publicité,  soutient les actions des activistes de l’antenne locale et quelques liens avec l’antenne locale de Strasbourg et sommes en accord avec leurs initiatives.

### ● YunoHost

<a href="https://yunohost.org" target="_blank">YunoHost</a> est un système d’exploitation qui vise à simplifier autant que possible l'administration d'un serveur pour ainsi démocratiser <a href="https://yunohost.org/fr/selfhosting" target="_blank">l’auto-hébergement</a> tout en restant fiable, sécurisé, éthique et léger. Nous sommes plusieurs bénévoles strasbourgeois à contribuer au projet et plusieurs des services proposés par ARN sont construit avec ce système.

### ● La monnaie Libre

<a href="https://monnaie-libre.fr" target="_blank">La Ğ1</a> (la "June") est la première monnaie libre. Conçue sur une  blockchain écologique, c'est une expérience citoyenne, solidaire... et  peut-être subversive ! Alsace Réseau Neutre propose des Tunnels VPN payables en Ğ1 et plusieurs membres font partie du réseau et peuvent vous aider à y entrer.

### ● L’Echap

<a href="https://echap.eu.org" target="_blank">Echap</a> est une association loi 1901 qui s’est donné pour objectif de  lutter contre l’utilisation de la technologie dans les violences faites aux femmes. L’Echap est un collectif hacker féministe qui souhaite  apporter des ressources et de l’accompagnement aux associations luttant  contre les violences faites aux femmes sur les questions de technologie.

### ● La Quadrature du net

<a href="https://www.laquadrature.net/" target="_blank">La Quadrature du Net</a> promeut et défend les libertés  fondamentales dans l’environnement numérique. L’association lutte contre la censure et la surveillance, que celles-ci viennent des États ou des entreprises privées. Elle questionne la façon dont le numérique et la société s’influencent mutuellement. Elle œuvre pour un Internet libre, décentralisé et émancipateur. LQDN est à l'origine de <a href="https://technopolice.fr/" target="_blank">la campagne technopolice</a>.

### ● Lavallee

<a href="https://lavallee.ynh.fr/blog/" target="_blank">Lavallee</a> est un chaton situé à Solbach dont le créateur est membre d’Alsace Réseau Neutre. Ce chaton propose essentiellement du Nextcloud.

### ● C’cité

La <a href="https://www.ccite.fr/" target="_blank">Fédération des Aveugles Alsace Lorraine Grand Est</a>, agit pour  l’inclusion des personnes aveugles et malvoyantes dans le Grand Est.  Alsace Réseau Neutre et Hackstub essaie depuis peu de faire bouger les lignes sur l’accessibilité numérique.

### ● Le réseau d’inclusion numérique de Strasbourg

La ville de Strasbourg a monté <a href="https://www.strasbourg.eu/documents/976405/1013614/0/bed7ea35-cd78-6fa3-096a-d2b86e377c82" target="_blank">une initiative visant à lutter contre la fracture numérique</a>. Ce réseau rassemble des structures engagées sur cette thématique (large). Il est notamment possible d’obtenir un accompagnement pour les démarches auprès des services publics, notamment pour les personnes ne maîtrisant pas bien le français.

NB : Notre association reste critique sur 2 points :

 * À propos de la suppression des guichets de services publics en les remplaçant par des sites internet. Le fait qu’il soit nécessaire de créer des dispositifs comme Aidant Connect peu rémunéré, créant des emplois précaires et un accompagnement moins qualitatifs que les guichets pose de nombreuses questions sur l’idéologie de la « dématérialisation ».
 * Au sujet de l’idée que Windows resterait plus facile d’utilisation que Linux Mint pour les néophites

## __Professionnel⋅les du numérique *en Alsace et d’ARN*__

Plusieurs pros engagé⋅es sur les questions de logiciel libre, de libertés et d’accessibilités sont membres de notre association, voici une liste non exhaustives :

 * <a href="https://reflexlibre.net" target="_blank">ReflexLibre</a>, hébergeur membre du collectif CHATONS
 * <a href="https://www.cli.coop/" target="_blank">Coopérative Libre Informatique</a> : association de travailleuses et travailleurs du numérique
 * <a href="https://marjorieober.com" target="_blank">Marjorie Ober</a>, graphiste et programmeuse libriste
