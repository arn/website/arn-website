---
title: 'Devenir membre'
body_classes: modular
image_align: left
---

# __Devenir *membre*__

**L'adhésion vous permet :**
 * de **renforcer la légitimité d'ARN** pour faire entendre [ses revendications](/asso/revendications)
 * d'obtenir un **compte sans-nuage.fr** offert avec l'adhésion
 * de **souscrire à certains services** que nous proposons
 * de **participer à la vie de l’association**, aux réunions, à l’organisation d’évènements ou à son administration
