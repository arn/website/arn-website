---
title: 'Becoming a member'
body_classes: modular
image_align: left
---

# __Becoming a *member*__

**Membership allows you to :**
 * to **reinforce ARN's legitimacy ** to make its demands heard.
 * to obtain a **account sans-nuage.fr** wich is for free if you take a membership.
 * to **subscribe to certain services* that we offer.
 * to **participate in the life of the association**, in meetings, in the organisation of events or in its administration.
