---
title: 'Mentions légales'
body_classes: asso
visible: false
published: true
login: {  }
content:
    items: '@self.modular'
routes:
  default: '/mentions'
  aliases: 
    - '/asso/legal'
page-toc:
  active: true
sitemap:
  priority: !!float 0.1
---

# Mentions légales

Le présent site web est un service de communication au public en ligne édité à titre non professionnel au sens de l’article 6, III, 2° de la loi 2004-575 du 21 juin 2004 par l'association de droit alsacien-mosellan Alsace Réseau Neutre inscrite au registre des associations du tribunal de Strasbourg.

## Contact

En cas de réclamation ou de remarque sur le contenu de ce site web, plusieurs moyens sont à votre disposition pour [contacter l'éditeur du présent site web](/contact).

## Hébergeur

L'hébergeur de ce site web est l'association elle-même dont le siège social est situé à l'adresse suivante :

> Alsace Réseau Neutre
> 16 route du Belfort
> 67100 STRASBOURG


Néanmoins, nous nous permettons de rappeler à votre mémoire l'article 6, I, 4° de la loi 2004-575 du 21 juin 2004 qui dispose ce qui suit :

> Le fait, pour toute personne, de présenter aux [hébergeurs du site] un contenu ou une activité comme étant illicite dans le but d’en obtenir le retrait ou d’en faire cesser la diffusion, alors qu’elle sait cette information inexacte, est puni d’une peine d’un an d’emprisonnement et de 15 000 EUR d’amende.

## Données personnelles

Comme pour tout service de communication au public en ligne et conformément à la législation française, l'éditeur de ce blog conserve, pendant un an, un historique des connexions (comprenant la date, l’heure, l’adresse IP, la provenance, la signature du logiciel client (user-agent) et la ressource ajoutée, modifiée ou supprimée) des visiteurs. Ces données ne sont en aucun cas exploitées pour vous identifier ou vous tracer et ne sont ni diffusées ni vendues ni cédées à des tiers mais peuvent être réclamées par une autorité judiciaire.

