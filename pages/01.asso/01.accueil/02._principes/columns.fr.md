---
title: 'Nos principes'
body_classes: modular
size: sm
class: principes
---

## __Le *concept*__

---
![](/images/illustrations/manifestation.svg)

**Alsace Réseau Neutre** est une association militante…

---
![](/images/illustrations/benevoles.svg) 

… animée par des **bénévoles** enthousiastes.

---
![](/images/illustrations/icon3.svg)

Nous nous opposons aux **dérives de la technologie**…

---
![](/images/illustrations/icon4.svg)

… et sommes engagés pour un numérique **accessible**, **soutenable** et **libérateur**.

---
![Logos barrés de Google, Amazon, Facebook, Apple et Microsoft](/images/illustrations/icon5.svg)

Pour cela, nous vous proposons des **outils et des contenus alternatifs**…

---
![](/images/illustrations/icon6.svg)

… afin de vivre ensemble **une utopie numérique**.

---
