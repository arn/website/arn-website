---
title: 'Our ethics'
body_classes: modular
size: sm
class: principes
---

## __Our *Ethics*__

---
![](/images/illustrations/manifestation.svg)

**Alsace Réseau Neutre (ARN)** is an **activist organization** …

---
![](/images/illustrations/benevoles.svg) 

… led by enthusiastic and benevolent **volunteers**.

---
![](/images/illustrations/icon3.svg)

We oppose the misuse of technology…

---
![](/images/illustrations/icon4.svg)

…and are committed to **accessible**, **sustainable** and **liberating** digital technology.

---
![Striked logos of Google, Amazon, Facebook, Apple and Microsoft](/images/illustrations/icon5.svg)

For this, we provide **alternative tools** and contents...

---
![](/images/illustrations/icon6.svg)

…to achieve our goal to live together in a **digital utopia**.

---
