---
title: Présentation générale
media_order: 'logoaccueil.png,accueillogo.svg,icon1.png,icon1.svg,icon2.svg,icon3.svg,icon4.svg,icon5.svg,icon6.svg,fleche.svg,SANS-NUAGE 02.svg,NATTA Titre.svg,EDUCPOP 01.svg,ECOLOGIE 01.svg,SANS-NUAGE Titre.svg,NATTA 01.svg,INFRA Titre.svg,INFRA 01.svg,EDUCPOP Titre.svg'
body_classes: accueil asso
visible: true
published: true
page-toc:
  active: false
---

<!--Accueil-->

<div class="flottaison">
    <img src="images/accueillogo.svg" alt="logo Alsace Réseau Neutre" class="logoaccueil"/>  
    <a href="#to_content">
        <img src="images/fleche.svg" alt=" " class="fleche duhaut"/>
    </a>
</div>

<!--Principes-->

<h1 class="underline">Nos <span class="bleu">principes</span></h1>


  <img src="images/icon1.svg" alt=" " class="iconesmobile"/> 
<br>
<p class="textemobile">
    Alsace Réseau Neutre est une <strong>association militante</strong> gérée par des bénévoles enthousiastes. Nous nous opposons aux dérives de la technologie et sommes engagés pour la <strong>Neutralité du Net</strong> et un <strong>numérique soutenable</strong>. Pour cela, nous vous proposons des <strong>outils et des contenus alternatifs</strong>, afin vivre ensemble dans une utopie numérique.
</p>

 <center>
            <form action="#">
                <button class = "bouton bbleu boutonsmobile" type="submit">
                    Plus d'infos ici
                </button>
            </form>
</center>


<div class="principeboxes">
   <div class="rangee">
       <div class="accueilboxes"> 
             <img src="images/manifestation.svg" alt=" " class="iconaccueil"/> 
              <br><br>
              <p><strong>Alsace Réseau Neutre</strong> est une association militante</p>
        </div>
       <div class="accueilboxes">
            <img src="images/icon2.svg" alt=" " class="iconaccueil"/> 
             <br><br>
             <p>Animée par des bénévoles enthousiastes</p>
       </div>
        <div class="accueilboxes">
            <img src="images/icon3.svg" alt=" " class="iconaccueil"/>
             <br><br>
             <p>Nous nous opposons aux dérives de la technologie</p>
        </div>
    </div>
    <div class="rangee">
        <div class="accueilboxes">
            <img src="images/icon4.svg" alt=" " class="iconaccueil"/>
             <br><br>
            <p> Et sommes engagés pour la <strong>Neutralité du Net</strong> et un <strong>numérique soutenable</strong></p> 
        </div>
        <div class="accueilboxes">
            <img src="images/icon5.svg" alt=" " class="iconaccueildeux"/>
             <br><br>
             <p> Pour cela, nous proposons des <b>outils et contenus alternatifs</b> </p>
        </div>
        <div class="accueilboxes">
            <img src="images/icon6.svg" alt=" " class="iconaccueildeux"/>
             <br><br>
             <p>Pour vivre ensemble <b>une utopie numérique</b></p>
        </div>
    </div>
</div>


<!--Objectifs-->
<h2 class="underline">Nos <span class="bleu">objectifs</span></h2>

<div class="offres-container">
        <div class="offre-box-accueil offre-3-col"> 
                    <h3>
                    <img src="images/EDUCPOP 01.svg" alt=" " class="objicon"/>
                    <span class="rose">Informer </span>  sur les enjeux du numérique
                    </h3>
                <p>
                    <br>
                    La mutation numérique de notre société dévoile des dérives majeures
                    en termes de libertés fondamentales, d'accès à l'informatoon et de démocratie. 
                    <br><br>
                    Nous souhaitons <b>comprendre et expliquer</b> ces enjeux afin de promouvoir les solutions
                    les plus adéquates.
                </p>
        </div>
        <div class="offre-box-accueil offre-3-col">
                 <h3>
                 <img src="images/INFRA 01.svg" alt=" " class="objicon"/>
                 <span class="violet">Concevoir et maintenir </span> des alternatives viables
                 </h3>
            <p>
                <br>
                Face à l'avènement du capitalisme de surveillance, nous proposons de créer ensemble un morceau
                d'internet utopique fondé sur <strong>l'autonomie des utilisateurs, l'absence de pistage commercial</strong>,
                ainsi que les concepts de <strong>décentralisation et de Neutralité du Net</strong>.
            </p>
        </div>
         <div class="offre-box-accueil offre-3-col">
                <h3>
                <img src="images/ECOLOGIE 01.svg" alt="" class="objicon"/>
                <span class="vert">Lutter </span> contre l'obsolescence <br>et le gaspillage
                </h3>
               <p>
                <br>
                Une part très importante de la pollution numérique est liée à la production de masse et à l'obsolescence
                des appareils électroniques. Nous luttons pour promouvoir des <strong>usages et des technologies plus soutenables</strong>, et pour la prolongation de la durée de vie de nos dispositifs. Ceci, à l'aide de logiciels libres.
            </p>
        </div>   
   
</div>


<!--Réponses-->

<h2 class="underline">Nos <span class="bleu">réponses</span></h2>

<div class="reponsesboxes">
    <div class="rangee">
        <div class="repboxes"> 
            <br>
           <img src="images/Edutitre.svg" alt="educpop" class="edutitre">
            <br><br>
             <p>
                Nous animons régulièrement des <strong>ateliers de sensibilisation</strong> au monde du numérique
                et des <strong>ateliers pratiques</strong> pour vous apprendre à reprendre le contrôle de vos données.
            </p>
            <center>
            <form action="../educpop">
                <button class = "bouton-accueil brose" type="submit">
                    Plus d'infos
                </button>
            </form>
            </center>
        </div>
        <div class="repboxes">
            <br>
             <img src="images/SNTitre.svg" alt="sans nuage" class="sntitre">
            <br><br>
             <p>
                Notre association vous propose des services en ligne respectants la Charte du Collectif des Hébergeurs
                Alternatifs Transparents Ouverts Neutres et Solidaires (CHATONS).
            </p>
            <center>
            <form action="../sans-nuage">
                <button class = "bouton-accueil bbleu" type="submit">
                    Plus d'infos
                </button>
            </form>
            </center>
        </div>
    </div>
    <div class="rangee">
        <div class="repboxes">
            <br>
             <img src="images/Infratitre.svg" alt="infra hébergements alternatifs" class="infratitre">
            <br><br>
            <p>
                L'association fournit, depuis Schiltigheim, différents supports et outils pour <strong>héberger vos propres services en ligne</strong>.
            <br><br>
            </p> 
        </div>
        <div class="repboxes">
            <br>
             <img src="images/Nattatitre.svg" alt="Natta accès internet alternatif" class="nattatitre">
            <br><br>
           <p> 
                En tant que fournisseurs d'accès à Internet associatifs, nous vous garantissons un accès à Internet respectant la Neutralité du Net.
            <br><br>
           </p>
    </div>
</div>
</section>
