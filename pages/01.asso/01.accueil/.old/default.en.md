---
title: Home
media_order: 'logoaccueil.png,accueillogo.svg,icon1.png,icon1.svg,icon2.svg,icon3.svg,icon4.svg,icon5.svg,icon6.svg,fleche.svg,SANS-NUAGE 02.svg,NATTA Titre.svg,EDUCPOP 01.svg,ECOLOGIE 01.svg,SANS-NUAGE Titre.svg,NATTA 01.svg,INFRA Titre.svg,INFRA 01.svg,EDUCPOP Titre.svg,sous-lignage.svg'
body_classes: accueil
visible: false
page-toc:
  active: false
---

<!--Accueil-->
<div class="flottaison">
    <img src="images/accueillogo.svg" alt="logo Alsace Réseau Neutre" class="logoaccueil"/> 
    <a href="#to_content">
        <img src="images/fleche.svg" alt=" " class="fleche"/>
    </a>
</div>

<section class="contenuaccueil" id="to_content">
    <p class="texteaccueil">
    Alsace Réseau Neutre (ARN) is a <strong>non-profit association</strong> founded in 2012. All of our volunteers are striving to create an access to an Internet that respects our individual liberties and the environment. Therefore, we have built a set of alternative tools labelled by <a href="https://www.chatons.org/">CHATONS</a>, a collective we participate in and that shares the same ethics as our organization. We offer an <strong>alternative Internet access</strong> that respects the principles of Net neutrality, also defended by the <a href="https://www.ffdn.org/">FFDN</a>, the "Fournisseurs d'Accès à Internet" (Internet providers) federation.
    </p>

<!--Principes-->

<h1 class="underline">Our <span class="bleu">Ethics</span></h1>

  <img src="images/icon1.svg" alt=" " class="iconesmobile"/> 
<br>
<p class="textemobile">
    Alsace Réseau Neutre is an <strong>activist organization</strong> led by enthusiastic and benevolant volunteers. We are opposed to the misuse of technology and are committed to the respect of <strong> Net neutrality and sustainable uses of technologies</strong>.  Therefore, to achieve our goal to live in a digital utopia, we provide <strong>alternative tools</strong> and contents.
 </p>

 <center>
            <form action="#">
                <button class = "bouton bbleu boutonsmobile" type="submit">
                   More info
                </button>
            </form>
</center>


<div class="principeboxes">
   <div class="rangee">
       <div class="accueilboxes"> 
             <img src="images/icon1.svg" alt=" " class="iconaccueil"/>
              <br><br>
             <p>Alsace Réseau Neutre is an <strong>activist organization</strong></p>
        </div>
       <div class="accueilboxes">
            <img src="images/icon2.svg" alt=" " class="iconaccueil"/>
             <br><br>
            <p>Led by enthusiastic and benevolant volunteers </p>
       </div>
        <div class="accueilboxes">
            <img src="images/icon3.svg" alt=" " class="iconaccueil"/> 
             <br><br><br>
            <p>We are opposed to the <strong>misuse of technology</strong></p>
        </div>
    </div>
    <div class="rangee">
        <div class="accueilboxes">
            <img src="images/icon4.svg" alt=" " class="iconaccueil"/>
             <br><br>
            <p>We are committed to the respect of<strong> Net neutrality and sustainable uses of technologies</strong></p> 
        </div>
        <div class="accueilboxes">
            <img src="images/icon5.svg" alt=" " class="iconaccueildeux"/>
             <br><br>
             <p> Therefore, we provide <strong>alternative tools</strong> and contents </p>
        </div>
        <div class="accueilboxes">
            <img src="images/icon6.svg" alt=" " class="iconaccueildeux"/>
             <br><br>
            <p>To achieve our goal to live in a digital utopia.</p>
        </div>
    </div>
</div>


<!--Objectifs-->

<h2 class="underline">Our <span class="bleu">Goals</span></h2>

<div class="offres-container">
        <div class="offre-box-accueil offre-3-col"> 
                    <h3>
                    <img src="images/EDUCPOP 01.svg" alt=" " class="objicon"/>
                    <span class="rose">Informing </span>about digital technology issues
                    </h3>
                <br>
                <p>
                    The migration towards a digital society highlights a great number of threats for our fundamental <strong>freedom of access to information and democracy</strong>. 
                    <br><br>
                    We want to understand and explain these issues to find more virtuous answers.
                </p>
        </div>
        <div class="offre-box-accueil offre-3-col">
                <h3>
                <img src="images/INFRA 01.svg" alt=" " class="objicon"/>
                <span class="violet">Creating and using </span> viable alternatives
                </h3>
            <p>
                <br>
                In reaction to the advent of surveillance capitalism, we planned to create a safe online place with no <strong>commercial tracking</strong>. We encourage user autonomy and promote the concepts of <strong>decentralization and Net neutrality</strong>
            </p>
        </div>
         <div class="offre-box-accueil offre-3-col">
               <h3>
               <img src="images/ECOLOGIE 01.svg" alt=" " class="objicon"/><span class="vert">Fighting </span>against obsolescence <br> and wastage</h3>
               <p>
                <br>
                A great part of digital pollution is caused by mass production and the obsolescence of technological devices. We are fighting for more <strong>sustainable uses and technologies</strong>, and for the extension of the life span of our devices with free software.
             </p>
        </div>   
   
</div>

<!--Réponses-->
<h2 class="underline">Our <span class="bleu">Solutions</span></h2>

<div class="reponsesboxes">
    <div class="rangee">
        <div class="repboxes"> 
            <br>
             <img src="images/Edutitre.svg" alt="educpop" class="edutitre">
            <br><br>
             <p>
                We regulary host workshops and events about digital awareness to help individuals to <strong>regain full control over their data</strong>.
            </p>
             <center>
            <form action="../en/educpop">
                <button class = "bouton-accueil brose" type="submit">
                    More info
                </button>
            </form>
            </center>
        </div>
        <div class="repboxes">
            <br>
            <img src="images/SNTitre.svg" alt="sans nuage" class="sntitre">
            <br><br>
             <p>
               Our organization provides online services in respect with the Alternative Transparent Open Neutral Supportive Hosts Collective (CHATONS)  charter.
            </p>
            <center>
            <form action="../en/sans-nuage">
                <button class = "bouton-accueil bbleu" type="submit">
                    More info
                </button>
            </form>
            </center>
        </div>
    </div>
    <div class="rangee">
        <div class="repboxes">
            <br>
            <img src="images/Infratitre.svg" alt="Infra alternative hosting" class="infratitre">
            <br><br>
            <p>
                The association provides various services from the city of Schiltigheim to <strong>host your online services</strong>. 
               </p> 
        </div>
        <div class="repboxes">
            <br>
             <img src="images/Nattatitre.svg" alt="Natta alternative internet access" class="nattatitre">
             <br><br>
           <p> 
                As an associative internet service provider, we can assure you that the internet access we offer follows the principles of Net Neutrality.
           </p>
    </div>
</div>
</section>
