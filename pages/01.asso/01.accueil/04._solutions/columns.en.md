---
title: 'Our Solutions'
body_classes: modular
class: solutions
size: lg
---

## __Our Concrete *Solutions*__

---

### ![EducPop](/user/pages/images/logos/poles/Logo_titre_Educpop.svg)
We regularly host **workshops and events** about digital awareness to help individuals **regain full control over their data**.

 * [Workshops and Events](/agenda)
 * [Self-Help Tutorials](https://wiki.arn-fai.net/)
 * [Blog](/blog)

---

### ![Sans-nuage](/user/pages/images/logos/poles/Logo_titre_Sans-nuage.svg)
Our organization provides online services respecting the Alternative Transparent Open Neutral Supportive Hosts Collective <a href="https://www.chatons.org/charte" target="_blank">(CHATONS) charter</a>.

 * [Open-access Services (8)](https://sans-nuage.fr/fr/)
 * [Membership-based Services (11)](https://sans-nuage.fr/fr/#services-member)
 * Solutions for organizations: description soon.

---

### ![Infra](/user/pages/images/logos/poles/Logo_titre_Infra.svg)
The association provides, from the city of Schiltigheim, various supports and tools to **host your own online services**.

 * [Virtual Private Servers](/hébergement-alternatif/vps)
 * [Plug & Play Self-hosting](/hébergement-alternatif/brique-internet)
 * [Racked Hosting](/hébergement-alternatif/hébergement-en-baie)

---

### ![Natta](/user/pages/images/logos/poles/Logo_titre_Natta.svg)
As an <a href="https://ffdn.org" target="_blank">associative internet service provider member of the FFDN</a>, we can assure you that the internet access we offer follows the principles of **Net Neutrality**.

 * [Internet Access](/internet-alternatif/acces-internet)
 * [DNS Resolver](/internet-alternatif/dns)
 * [VPN Tunnels](/internet-alternatif/tunnel-vpn)

---
