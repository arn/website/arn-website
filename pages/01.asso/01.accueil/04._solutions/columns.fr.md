---
title: 'Nos réponses'
body_classes: modular
class: solutions
size: lg
---

## __Nos *réponses* concrètes__

---

### ![EducPop](/user/pages/images/logos/poles/Logo_titre_Educpop.svg)
Nous animons régulièrement des **ateliers de sensibilisation** au monde du numérique et des **ateliers pratiques** pour vous apprendre à reprendre le contrôle de vos données.

 * [Ateliers et conférences](/agenda)
 * [Tutoriels en autonomie](https://wiki.arn-fai.net/)
 * [Blog](/blog)

---

### ![Sans-nuage](/user/pages/images/logos/poles/Logo_titre_Sans-nuage.svg)
Notre association vous propose des services en ligne respectants la <a href="https://www.chatons.org/charte" target="_blank">Charte du CHATONS</a>, le Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires.

 * [Services en accès libres (8)](https://sans-nuage.fr/fr/)
 * [Services sur adhésion (11)](https://sans-nuage.fr/fr/#services-member)
 * Solutions pour les assos : description à venir.

---

### ![Infra](/user/pages/images/logos/poles/Logo_titre_Infra.svg)
L'association fournit, depuis Schiltigheim, différents supports et outils pour **héberger vos propres services en ligne**.

 * [Serveurs privés virtuels](/hébergement-alternatif/vps)
 * [Auto-hébergement plug & play](/hébergement-alternatif/brique-internet)
 * [Hébergement en baie](/hébergement-alternatif/hébergement-en-baie)

---

### ![Natta](/user/pages/images/logos/poles/Logo_titre_Natta.svg)
Nous sommes membres de la <a href="https://ffdn.org" target="_blank">fédération des fournisseurs d'accès internet associatifs</a> et vous proposons des accès à Internet respectant la Neutralité du Net.

 * [Accès Internet](/internet-alternatif/acces-internet)
 * [Résolveur DNS](/internet-alternatif/dns)
 * [Tunnels VPN](/internet-alternatif/tunnel-vpn)

---
