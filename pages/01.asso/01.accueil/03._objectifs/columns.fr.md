---
title: 'Nos objectifs'
body_classes: modular
size: sm
class: objectifs
---

## __Nos *actions*__
---
### ![](/images/logos/poles/educpop_icon.svg) *Informer* sur les enjeux du numérique
La mutation numérique de notre société dévoile des dérives majeures en termes de libertés fondamentales, d'accès à l'information et de démocratie.

Nous souhaitons **comprendre et expliquer** ces enjeux afin de promouvoir des solutions plus adéquates.

---
### ![](/images/logos/poles/infra_icon.svg) *Concevoir et maintenir* des alternatives viables
Face à l'avènement du capitalisme de surveillance, nous proposons de créer ensemble un morceau d'internet utopique fondé sur **l'autonomie des utilisateurices, l'absence de pistage commercial**, ainsi que les concepts de **décentralisation et de Neutralité du Net**.

---
### ![](/images/logos/poles/ecologie_icon.svg) *Lutter* contre l'obsolescence
Une part très importante de la pollution numérique est liée à la production de masse et à l'obsolescence des appareils électroniques.

Nous luttons pour promouvoir des **usages et des technologies soutenables** ainsi que la prolongation de la durée de vie de nos dispositifs à l'aide des logiciels libres.

---