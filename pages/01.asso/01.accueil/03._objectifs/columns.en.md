---
title: 'Our Goals'
body_classes: modular
size: sm
class: objectifs
---

## __Our *Goals*__
---
### ![](/images/logos/poles/educpop_icon.svg) *Inform* on digital technology issues
The migration towards a digital society reveals major drifts in terms of fundamental freedoms, access to information and democracy.
                    
We want to **understand and explain** these issues in order to find more virtuous answers.

---
### ![](/images/logos/poles/infra_icon.svg) *Create and support* viable alternatives
In reaction to the advent of surveillance capitalism, we plan to create a safe online utopia with no **commercial tracking**.

We encourage **user autonomy** and promote the concepts of **decentralization and Net neutrality**.

---
### ![](/images/logos/poles/ecologie_icon.svg) *Fight* obsolescence and waste
A great part of digital pollution is caused by mass production and the obsolescence of technological devices.

We fight for more **sustainable uses and technologies**, and for extending the life span of our devices with the help of free software.

---