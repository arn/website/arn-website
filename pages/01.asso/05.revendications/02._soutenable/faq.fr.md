---
title: 'Créer des technologies soutenables'
body_classes: modular
---

## __Créer des technologies *soutenables*__
Sous sa forme actuelle, le numérique n'est pas soutenable sur le long terme, de par l'**effet de serre** produit[^1], de la pollution engendrée par l'**extractivisme**[^2] et de la mauvaise gestion des **déchets**[^3]. Au contraire des technologies _low-tech_, les technologies _high-tech_, matérielles ou logicielles, **nécessitent une coopération mondiale**[^4] qui n'est pas garantie dans un monde de plus en plus impacté par les effets du réchauffement climatique. La boulimie numérique est d'ailleurs responsable de nombreux **conflits armés et de conditions de travail désastreuses**, par exemple le scandale des **minerais de sang** [^5].

Nous pensons que le numérique doit muter pour faire face à ces enjeux majeurs. Pour commencer, nous recommandons d'adopter les mesures de sobriété suivantes.

---

### Faire fonctionner plus de 10 ans nos équipements

* Lever la taxe _Copie Privée_ sur les équipements reconditionnés
* Obliger les constructeurs à fournir des mises à jour et des pièces de rechange
* Taxer les équipements neufs pour financer la réparation des anciens
* Interdire la vente d'équipements neufs avec batteries non amovibles
* Interdire la vente d'équipements neufs ne permettant pas l'installation d'autres systèmes d'exploitation
* Normaliser des composants modulaires et ouverts
* Contrôler la durabilité des équipements numériques

---

### Questionner nos usages controversés

* Favoriser les produits sans high tech
* Refuser l'équipement des pré-ados en produits numériques personnels
* Interdire les blockchains énergivores
* Réévaluer nos exigences sur la disponibilité des services
* Visibiliser la matérialité du numérique et parler de numérisation plutôt que de dématérialisation

[^1]: Le modèle de 2021 du Shift Project évalue que le numérique en 2022 sera responsable de 3 à 4,5% des émissions de gaz à effet de serre mondiales.
[^2]: D'après Françoise Berthoud https://tube.piweb.be/w/3fb56d1e-dc15-43d3-98a7-a359694f746a
[^3]: En France, en 2019, 52% des DEEE ménagers sont collectés par des éco-organismes agréés (source: [ADEME - Equipements électriques & électroniques Données 2019](https://librairie.ademe.fr/cadic/4967/equipements-electriques-electroniques_deee_donnees2019_rapport2021.pdf?modal=false)). Ceci n'implique pas que 48% des DEEE ménagers sont mal réemployés, recyclés ou valorisés, mais on sait aussi que certains finissent dans des décharges illégales par exemple la ville décharge d'Agbogbloshie au Ghana (source: [documentaire Welcome to sodom](https://invidious.fdn.fr/watch?v=ihv0eZ3uGCY&local=true&subtitles=fr&autoplay=0))
[^4]: que ce soit pour créer les équipements matériels ou maintenir les logiciels
[^5]: [Documentaire "Du sang dans nos portables"](https://wikiless.org/wiki/Blood_in_the_Mobile)