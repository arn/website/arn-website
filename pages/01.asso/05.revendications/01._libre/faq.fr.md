---
title: 'Vivre libre'
body_classes: modular
---

# __Nos *revendications*__

_A valider à l'AG du 23 septembre 2023_


Alsace Réseau Neutre revendique un numérique **libérateur, soutenable et accessible** et s'oppose ainsi aux dérives de la technologie. Nous expliquons ici ce que l'association entend par ces trois revendications.

## __Vivre *libre*__

Dans les années 90 et 2000, le numérique a révolutionné le partage de l'information et des connaissances, certains y voyait le moteur d'un renouveau démocratique de nos sociétés. Ce rêve utopique ressemble aujourd'hui à un cauchemar. Années après années, nous découvrons un numérique au cœur des mutations politico-économiques de nos sociétés. Ces mutations sont menées main-dans-la-main par de grandes multinationales et les états dans ce que Shoshana Zubroff appelle le **capitalisme de surveillance**. Ce dernier à l'affut, **s'immisce dans nos pensées**, **contrôle nos corps** et **menace nos libertés individuelles et collectives**.

Alsace Réseau Neutre défend l'avènement d'un autre numérique capable de **préserver nos libertés fondamentales**. Pour y parvenir nous œuvrons aux côtés d'autres acteurs puissants pour les mesures suivantes.


---

### Garantir notre capacité à penser et à nous autodéterminer

* Stopper la collecte de données personnelles (hors formulaire validé)
* Refuser tout algorithme qui vise à induire un comportement chez une personne ou une population
* Interdire les systèmes de crédit social et autres systèmes de notation similaires

---

### Rétablir la confiance et sortir de la société de contrôle

* Interdire la surveillance de masse
* Conserver des élections sans numérique
* Assurer la possibilité de falsifier nos identités
* Mettre en place un moratoire sur les armes autonomes
* Retirer les technologies d'espionnage par des proches (enfants, femmes, etc.)

---

### Se réapproprier la technologie

* Rester maîtres et maîtresses de nos outils
* "Smart things make us dumb"
* Simplifier la technologie d'internet, sa compréhension, face au règne de l'"expérience utilisateur" facile
* Décentraliser les outils et les données
* Promouvoir et protéger les communs numériques
* Passer du fordisme 2.0 aux coopératives numériques
* Démanteler les géants du net

---
