---
title: 'Rendre le numérique accessible et optionnel'
body_classes: modular
---

## __Rendre le numérique *accessible et optionnel*__

Si le numérique est dévenu **incontournable en France**, pour autant **tout le monde ne peut pas y accéder**. Que ce soit du fait d'**interfaces inadaptées** aux situations de handicap, d'**exclusions culturelles ou financiaires**, de **problèmes de connectivité** ou encore du fait d'un **manque de pratique numérique**. Les politiques « **tout numérique** » génèrent et agravent, de facto, des **discriminations intolérables**.

Nous pensons qu'il faut inverser la tendance et proposons les mesures ci-dessous pour y parvenir.

---

### Rendre le numérique accessible pour les personnes en situation de handicap

* Inclure des modules sur l'accessibilité dans toutes les formations du numérique
* Mettre en place une formation courte obligatoire pour les professionnels du web
* Rendre obligatoire l'accessibilité pour les nouveaux sites web et logiciels
* Augmenter les sanctions en cas de défaut de mise en conformité (cf IV art 47 Loi n° 2005-102 du 11 février 2005)
* Subventionner la mise en conformité des briques logicielles libres ou Open Source utilisées majoritairement
* Forcer les moteurs de recherche à pénaliser dans leurs classements les sites non-accessibles


---

### Déconstruire les stéréotypes dans le numérique

* Abolir les comportements oppressifs dans la tech
* Reconstruire dans la culture numérique des représentations différentes des identités blanches, valides et cis-masculines
* Adopter des mesures pour atteindre une égalité de résultat en formation et au travail (double quotas, encadrer les rémunérations, etc.)
* Interdire l'usage d'IA susceptibles de développer des biais discriminants

---

### Raccorder les foyers isolés et fiabiliser les réseaux existant

* Financer le raccordement des zones peu denses grâce aux zones denses plus rentables
* Pour les foyers vraiment isolés, envisager des solutions très haut débit par antennes wifi longue-portée pour accélérer l'accès à une connexion de qualité
* Encadrer le raccordement fibre optique pour rendre le réseau plus fiable

---

### Donner la possibilité de choisir de ne pas utiliser le numérique

* Créer un droit de ne pas utiliser le numérique
* Remettre en place des guichets humains pour permettre une alternative à celleux qui ne peuvent ou ne veulent pas accèder aux sites internet
* Appliquer des rémunérations décentes pour l'accompagnement numérique des publics
* Offrir l'équipement numérique aux personnes en situation de précarité (a minima tant que le numérique est incontournable)
* Interdire la réponse "Le logiciel ne sait pas gérer votre situation" comme explication aux refus d'accéder à un droit

---
