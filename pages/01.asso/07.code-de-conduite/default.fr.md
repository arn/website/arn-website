---
title: 'Code de conduite'
body_classes: asso article
visible: true
published: true
login: {  }
content:
    items: '@self.modular'
page-toc:
  active: true
---


# __Code de *conduite*__

_Validé lors de l'AG du 23 septembre 2023_

!!! Ce code de conduite ne s'appliquera jamais exactement à toutes les situations, mais nous avons collectivement la responsabilité d'instaurer de bonnes pratiques au sein de l'association, ne le prenons pas à la légère. Appliquons-le et faisons-le appliquer. Chaque contexte appelle des fonctionnements différents. C'est pourquoi nous prendrons le temps de définir ensemble les règles à respecter au début de ces échanges (prise de parole, temps de réunion, notes à prendre, liste de choses à faire,…) et de prendre connaissance des besoins et handicaps de chacun⋅e.
!!! 
!!! Ce code de conduite s'applique au quotidien de la vie de l'association, sans exception : dans les moments collectifs, les prises de parole, les réponses directes, les tribunes publiées, etc. mais aussi lors des moments de pause ou encore les événements festifs liés à la vie d'ARN.
!!! 
!!! Ce Code de conduite doit être accessible sur les plateformes d'échanges reliées à ARN et internet, IRC, Matrix et autres tchat, Forum, BBB, Mumble, ateliers HOP!, réunions et AG. Au besoin, les règles ci-dessous seront lues en début d'événement et affichées afin d'être visible et connu de tou⋅tes.



[size=20]**Collectivement et individuellement, chacun et chacune d'entre nous, vous compris, s'engage à**:[/size]



## __*Respecter* les autres__
_... leurs expériences et leurs points de vue_

   * **rester autant que possible dans une attitude respectueuse**, un désaccord ne justifie pas un comportement injurieux ou déplacé, même si parfois, ressentir de la colère est légitime et nécessaire.
   * **respecter les limites physiques et émotionnelles des autres**, quelles que soient ses intentions. Cela implique de porter une attention au langage non verbal, de s'assurer du consentement des personnes et de modifier notre comportement si une personne nous signifie que nous la mettons mal à l'aise.
   * **garder en tête qu'à l'écrit, il est difficile de percevoir le ton des messages d'autrui et inversement.**
   * **assumer ses propos et la manière dont ils peuvent être perçus.** *Tout le monde fait des erreurs. Mais si une personne concernée est blessée par nos propos, écoutons-la, comprenons son point de vue et changeons de comportement (cf "[En finir avec les rock stars](https://repeindre.info/2019/03/10/en-finir-aves-les-rock-stars)")
   * **éviter de préjuger des compétences de ses interlocuteurices** notamment lorsqu'on échange des connaissances
   * **Échanges oraux**
       * Les réunions sont tenus par un maitre du temps, distribuant la parole à toutes les personnes le souhaitant. Il a la possibilité de vous demander de vous arrêter lorsqu'il estime que le sujet est clos que les arguments des différents partis ont été exprimés et compris.
       * Les tours de parole doivent être respectés pour que chacun.e puisse s'exprimer librement et être entendu.e.
   * **Échanges écrits (tchat, forum, mail, etc.) :**
       * Les échanges sont parfois rapides. Pensez à inclure les nouvelles personnes. N'hésitez pas à citer la personne à qui vous répondez lorsque les réponses s'enchaînent.
       * Prenez le temps de réfléchir à ce que vous écrivez. Gardez en tête un format accessible, vous êtes lu⋅es par de nombreuses personnes.
       * En résumé, vous n'êtes pas là pour vous écouter parler/écrire. Évitez de digresser.


## __Adopter une attitude *solidaire et inclusive*__

   * **faire attention aux autres pour que tout le monde puisse se sentir bienvenu.** Si il n'y a pas eu d'accueil réclamons-le.
   * **ne pas occuper l'espace physique et temporelle au détriement des autres personnes**. Interrogeons nos privilèges et nos préjugés.
   * **être solidaires des autres personnes présentes**, sans pour autant empêcher une personne de se défendre ou d'agir comme elle le souhaite. Intervenir à la place d'une personne n'est pas toujours lui rendre service.
   * **essayer, durant les discussions, de se mettre à la place de l'autre.** Nous sommes des personnes différentes, nous n'avons pas les mêmes compétences techniques, la même conscience politique, les mêmes choix technologiques, ni le même environnement culturel, ou le même langage.
   * **ne discriminer personne d'aucune façon** (handicap, sexe, couleur, genre, origine, statut économique, âge, orientation sexuelle, romantique, politique ou religieuse, compétences techniques, etc…)
   * **ne pas poser de question personnelle pouvant provoquer une gène** (ancien prénom, santé, intimité, coming out, travail, etc.) sans y avoir été invité.


## __Ne mettre *personne en danger*__
_... ni soi-même, ni nos collectifs ou leurs membres_

   * **ne pas enregistrer les réunions, ni photographier tout élément reconnaissable sans l'accord des personnes concernées**. Cela implique les discussions, les tatouages, les écrans d'ordinateurs, les plans larges, etc.
   * **ne pas interagir avec les équipements numériques des autres sans leur accord** (même pour recharger son smartphone via usb).
   * **ne pas divulguer d'informations personnelles** sur d'autres personnes sans leur accord (outing, autre pseudo sans rapport, identités ou données des utilisateurices, etc.)
   * **garder en tête que ses interventions sur les chats et forums sont en général publiques** et peuvent être lues par des centaines de personnes
   * **ne pratiquer aucune activité illégale dans les lieux où vous êtes accueillis ou avec nos services.** Ne nous rendez pas complices. L'association n'est pas là pour vous couvrir ou prendre des risques légaux à votre place.
   * **respecter le voisinage du lieu dans lequel nous nous trouvons**


**Si une personne ne respecte pas ce code de conduite, vous pouvez le signaler à mediation [at] arn-fai.net ou à l'une des personnes de l'équipe médiation.**



**Toute personne ayant des propositions ou des comportements allant à l'encontre de ce code de conduite pourra être exclue de tout ou partie des activités et services de l'association à la discrétion du CA.**


