---
title: 'Statuts'
body_classes: asso article
visible: false
published: true
login: {  }
content:
    items: '@self.modular'
page-toc:
  active: true
---

# __Statuts de l’*association*__

Version du 17 juin 2018

## ARTICLE 1 : Nom et siège

Entre toutes les personnes qui adhèrent aux présents statuts, il est formé une association dénommée Alsace Réseau Neutre.

Cette association est régie par les articles 21 à 79-III du code civil local, maintenus en vigueur dans les départements du Bas-Rhin, Haut-Rhin et Moselle, ainsi que par les présents statuts.

Le siège de l'association est fixé à Strasbourg.

L'association est inscrite au registre des associations du tribunal de Strasbourg.

## ARTICLE 2 : Objet et but

L'association a pour objet : la promotion d'internet et le développement de tous ses usages, dans le respect des libertés individuelles (en particulier la vie privée) et de la neutralité des réseaux.

La neutralité des réseaux pose le principe de l'égalité de traitement de tous les flux de données par les réseaux qui les transportent, notamment s'agissant d'internet.

L'association poursuit un but non lucratif.

## ARTICLE 3 : Durée

L'association est constituée pour une durée illimitée.

## ARTICLE 4 : Les membres

Peut devenir membre toute personne physique ou morale intéressée par l'objet de l'association.

Chaque membre prend l'engagement de respecter les présents statuts.

## ARTICLE 5 : Procédure d'adhésion

L'admission des membres est prononcée par un membre du CA. En cas de refus, le refus doit être confirmé et motivé par le Conseil d'Administration. Un recours peut être envisagé devant l'assemblée générale.

## ARTICLE 6 : La perte de qualité de membre

La qualité de membre se perd par :

 * Le décès de la personne physique ou la dissolution de la personne morale
 * Démission adressée par écrit au président
 * Radiation prononcée par le président pour non-paiement de la cotisation
 * Exclusion prononcée par l'assemblée générale pour faute grave

## ARTICLE 7 : L'assemblée générale : convocation et organisation

L'assemblée générale est composée de l'ensemble des membres de l'association. Elle se réunit au moins une fois par an et chaque fois que l'intérêt de l'association l'exige.

Modalités de convocation :

 * Sur décision du Conseil d'Administration
 * Sur proposition d'au moins 25% des membres de l'association

Les convocations contiennent l'ordre du jour et sont adressées par écrit au moins 7 jours à l'avance.
Procédures et conditions de vote

Pour que l'assemblée générale puisse valablement délibérer, le respect d'un quorum portant sur les la présence des membres (présents ou représentés) disposant d'une voix délibérative est nécessaire. Ce quorum est définis par la formule suivante:

Nombre de membres (présents ou représentés) nécessaire pour délibérer = ARRONDI(RACINE( MEMBRES )*2)

Si cette proportion n'est pas atteinte, une seconde assemblée générale sera convoquée dans un délai d'un mois, elle pourra alors délibérer quelque soit le nombre de membres présents ou représentés.

Les résolutions de l'assemblée générale sont prises à la majorité des suffrages exprimés. Ne pourront prendre part au vote que les membres à jour de cotisation. Par ailleurs, les dites délibérations sont prises à main levée.

Le vote par procuration est autorisé mais limité à 3 procurations par membre disposant d'un droit de vote délibératif.
Organisation

L'ordre du jour est fixé par le président. La présidence de l'assemblée appartient au président. Toutes les délibérations et résolutions de l'assemblée générale font l'objet d'un procès-verbal et sont consignées dans le registre des délibérations des assemblées générales signé par le président et le secrétaire. Il est également tenu une feuille de présence qui est signée par chaque membre.

## ARTICLE 8 : Pouvoirs de l'assemblée générale

Dans la limite des pouvoirs qui leur sont conférés par le code civil local et par les présents statuts, L'assemblée oblige par ses décisions tous les membres, y compris les absents. L'assemblée délibère sur toutes les questions figurant à l'ordre du jour. Elle pourvoit à la nomination et au renouvellement des dirigeants dans les conditions prévues à l'article 9 des présents statuts. Elle fixe le montant de la cotisation annuelle à verser par les membres de l'association. Elle est compétente pour la modification des statuts (cf. article 12) et pour la dissolution de l'association (cf. article 14). Enfin, elle est compétente pour prononcer l'exclusion d'un membre pour tout motif grave portant préjudice à l'association ou à ses membres. L'assemblée générale est généralement compétente pour examiner tous les points qui dépassent les compétences individuelles des dirigeants ou en cas de litige.

## ARTICLE 9 : Conseil d'administration

L'association est administrée par un conseil d'administration composé de 6 à 8 personnes. Ses membres sont élus pour 1 an, par l'assemblée générale et choisis en son sein.

Le conseil d'administration prend toutes les décisions nécessaires à la gestion quotidienne de l’association qui ne sont pas de la compétence de l’assemblée générale. Il assure le secrétariat de l’assemblée générale et veille à ce que toutes les mentions à inscrire sur le registre des associations soient effectuées dans un délai de 3 mois. Il décide de tous actes, contrats, marchés, investissement, achats, ventes, etc, nécessaires au fonctionnement de l'association. Il fait ouvrir tout compte bancaire auprès de tout établissement de crédit, effectue tout emploi de fonds, contracte tout emprunt. Il est également compétent pour les contrats de travail et fixe les rémunérations des salariés de l’association.

Le conseil d'administration se réunit au moins 3 fois par an et chaque fois qu’il est convoqué par son président ou à la demande d'au moins 3 de ses membres. Les convocations écrites devront être adressées au moins 7 jours avant la réunion. L’ordre du jour est fixé collaborativement les jours précédent la réunion. Seuls pourront être débattus les points inscrits à l’ordre du jour. La présence d’au moins 50% de ses membres est nécessaire pour que le conseil d'administration puisse valablement délibérer. Les résolutions sont prises à la majorité des membres présents, après avoir cherché un consensus. Par ailleurs, les dites délibérations sont prises à main levée. Toutes les délibérations et résolutions du conseil d'administration font l’objet d'un compte rendu publié. Le Conseil d'Administration peut être ouvert à l'ensemble des membres de l'association, si les membres du conseil d'administration sont d'accord.

## ARTICLE 10 : Accès au conseil d'administration

Ne peuvent accéder au conseil d'administration que les membres à jour de cotisation. Les membres qui sont adhérents depuis moins d'un an doivent être élus aux deux tiers des voix pour accéder au conseil d'administration.

## ARTICLE 11 : Direction

Le conseil d'administration désigne les dirigeants suivants :

 * Président
 * Trésorier
 * Secrétaire

Si un poste venait à être vacant, le président pourvoit provisoirement au remplacement. Il est procédé au remplacement définitif par le plus proche conseil d'administration.

### Président

Il veille au respect des statuts et à la sauvegarde des intérêts moraux de l'association. Il supervise la conduite des affaires de l'association et s'occupe de sa gestion quotidienne. Il assume les fonctions de représentation légale, judiciaire et extra-judiciaire de l'association dans tous les actes de la vie civile.

### Trésorier

Il veille à la régularité des comptes et tient une comptabilité probante. Il rend compte de sa gestion aux autres membres de l'association.

### Secrétaire

Il tient le registre des délibérations des assemblées générales et le registre des délibérations du conseil d'administration. Il est également chargé de tout ce qui concerne la correspondance de l'association et de la rédaction des procès verbaux des assemblées générales et autres documents nécessaires au fonctionnement de l'association.

## ARTICLE 12 : Modification des statuts

La modification des statuts de l'association doit être décidée par l'assemblée générale à la majorité de 2/3 des membres présents. Les délibérations ne peuvent porter que sur l'adoption ou le rejet des propositions mentionnées à l'ordre du jour. Les modifications feront l'objet d'un procès verbal, signé par le président et le secrétaire et qui sera transmis au tribunal au plus vite.

## ARTICLE 13 : Règlement intérieur

Le conseil d'administration pourra établir [un règlement intérieur](/asso/reglement-interieur) fixant les modalités d’exécution des présents statuts et d’organisation interne et pratique de l’association. Ce règlement intérieur sera soumis à l’approbation de l’assemblée générale ordinaire ainsi que ses modifications ultérieures.

## ARTICLE 14 : Dissolution de l'association

La dissolution de l'association doit être décidée par l'assemblée générale à la majorité de 2/3 des membres présents.

L'assemblée désigne une ou plusieurs personnes membres ou non de l'association qui seront chargées de la liquidation des biens de celle-ci.

L'actif net sera attribué à une association poursuivant des buts similaires choisie par l'assemblée générale.

La dissolution fera l'objet d'un procès verbal signé par le président et le secrétaire et qui sera transmis au tribunal au plus vite.

## ARTICLE 15 : Ressources

Les ressources de l’association sont constituées par :

 * les cotisations des membres ;
 * les subventions émanant d’organismes publics ou privés ;
 * les dons et les legs ;
 * toutes ressources qui ne sont pas interdites par les lois et règlements en vigueur.

## ARTICLE 16 : Adoption des statuts

Les présents statuts ont été adoptés par l'assemblée générale qui s'est tenue à Strasbourg, le 24/11/2012 et modifié lors des l'assemblées générales qui se sont tenue à Strasbourg le 25/06/2016 et le 17/06/2018.

