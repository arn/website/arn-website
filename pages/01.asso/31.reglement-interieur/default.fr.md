---
title: 'Règlement intérieur'
body_classes: asso article
visible: false
published: true
login: {  }
content:
    items: '@self.modular'
page-toc:
  active: true
---

# __Réglement intérieur de l’*association*__

_Validé lors de l'AG du 23 septembre 2023_


Ce document vise à compléter les statuts de l'association, en décrivant les éventuelles décisions du conseil d'administration en matière de fonctionnement de l'association. 

### Code de *conduite*

**L'association s'est doté d'[un code de conduite](/asso/code-de-conduite) qui s'applique au quotidien de la vie de l'association**, sans exception : dans les moments collectifs, les prises de parole, les réponses directes, les tribunes publiées, etc. mais aussi lors des moments de pause ou encore les événements festifs liés à la vie d'ARN.

Ce code de conduite n'est pas à confondre avec une politique de modération des services que nous mettrons en place si nos instances matrix ou mobilizon prennent de l'ampleur.


### Revendications
**Si vous êtes en désaccord profond avec [les revendications de l'association](https://arn-fai.net/fr/asso/revendications), vous ne devriez probablement pas adhérer ou utiliser les services numériques proposés.**

Alsace Réseau Neutre est une association militante qui fournit des services dans ce cadre. Elle publie sur son site une liste de revendications qui peuvent être portées au nom de l'association, de ses membres et utilisateurices dans des actions diverses (plaidoyer, sensibilisation, évènement, création d'alternative, etc.). Vous ne devriez probablement pas adhérer ou utiliser les services numériques proposés si vous êtes en désaccord profond avec ces revendications.

Toutefois, le texte n'est pas figé dans le marbre, au contraire, il est destiné à être mis à jour régulièrement (nouvelles propositions, évolution du numérique, évolution de notre compréhension des enjeux, référence, etc.), toute proposition est la bienvenue.

Même si cette liste n'est probablement pas exhaustive, celle-ci reste longue, et il est probable que vous vous questionnez ou soyez en désaccord avec une minorité de points listés. Si tel est le cas, vous pouvez initier une discussion (forum, chat ou lors d'un stammtisch) afin d'obtenir des explications et d'en débattre.

Si votre question ou votre idée n'éveille que peu d'intérêt ou du scepticisme auprès de plusieurs membres, il est probable que votre thématique ne soit pas alignée avec les idées de l'association. Plutôt que d'insister auprès d'une ou plusieurs personnes, nous vous invitons à écouter et chercher à comprendre leur point de vue. Vous n'êtes peut-être pas la première personne à aborder le sujet, et il se peut que des bénévoles soient fatigués de mener le même débat à la longue.

Les bénévoles peuvent vous indiquer d'autres structures plus à même de répondre à vos attentes. Rien ne vous empêche de travailler sur votre idée/projet hors du cadre de l'association et de voir si vos résultats éveillent plus d'intérêt au bout d'un moment.

Plus concrètement, vous n'êtes pas au bon endroit si:
   * vous cherchez des conseils sur des crypto-monaies énergivores ou spéculatives
   * vous êtes convaincus de l'intérêt du trans-humanisme


### Conditions *générales et spécifiques* de services

**L'accès et l'usage des services proposés par l'association est régit par des [conditions générales et spécifiques de services]([https://arn-fai.net/fr/cgs) qui s'appliquent selon le type de service.**

Nous encourageons fortement les bénévoles (ainsi que les utilisateurices) à lire ces textes pour connaître le cadre définit pour proposer ces services.



### Rencontres et *convivialité*

**Nous sommes une association, vous êtes encouragé⋅es à [rencontrer les autres membres](https://mobilizon.sans-nuage.fr/@arnfai).**

Ceci peut se faire lors des [évènements réccurents proposés par l'association](/contact#nous-rencontrer) ou lors d'autres événements plus ponctuels (comme l'AG ou les ARN Camp). Par ailleurs, des discussions asynchrones ont lieu sur [le forum](https://forum.arn-fai.net) et des discussions temps réelles via [irc/xmpp/matrix](/contact#nous-contacter).

Tous nos évènements sont disponibles sur l'[agenda](https://mobilizon.sans-nuage.fr/@arnfai).





### Bénévolat et *répartition de la « charge mentale »*

**Toute notre activité fonctionne actuellement au bénévolat, [nous avons besoin de vous](/asso/nous-soutenir) :)**


Bien que l'activité de l'association soit conséquente, à ce jour personne n'est rémunéré⋅e au sein de l'association. Pour y parvenir sans prendre de risque pour la pérennité de l'association, cela suppose que suffisamment de membres s'engagent bénévolement pour que l'on puisse éclater le travail au maximum.

[Filer un coup de main](/asso/nous-soutenir)



### Gestion *des accès*

!! **Important: Avant de pouvoir participer à des tâches nécessitant des accès sensibles (infra, administration des membres), il est nécessaire de venir à plus d'une dizaine de rencontres en présentielles. Celà ne constitue pas pour autant un droit pour les obtenir.**

**La transmission d'accès est régit par notre [politique de gestion des accès](https://wiki.arn-fai.net/benevoles:acces).**



### Charte de l'*administration des services*

**Toute personne amenée à intervenir sur l'infrastructure d'ARN, dans l'équipe support ou dans le Conseil d'Administration doit accepter la [charte de l'administration des services](https://wiki.arn-fai.net/benevoles:charte\_adminsys).**


### Modification *des documents*

Le Conseil d'Administration peut à tout moment modifier les documents listés ci-dessous:
 - le code de conduite
 - les revendications
 - les conditions générales et spécifiques des services
 - la charte de l'administration des services
 - la politique de sécurité

Ces modifications prendront alors effet à la date souhaitée par le Conseil d'Administration. Elles devront être présentées et validées lors de la prochaine Assemblée Générale de l'association.
