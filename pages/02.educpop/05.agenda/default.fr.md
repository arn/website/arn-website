---
title: Agenda
body_classes: agenda modular-iframe
visible: false
taxonomy:
    tag:
        - agenda
routes:
  default: '/agenda'
  aliases: 
    - '/educpop/agenda'
sitemap:
  priority: !!float 0.9
---
<div class="iframe-responsive" height="1500">
    <iframe class="iframe-responsive" src="https://mobilizon.sans-nuage.fr/@arnfai/events?future=true" frameborder="0" height="1500" title="Agenda des évènements de l'association"></iframe>
</div>
