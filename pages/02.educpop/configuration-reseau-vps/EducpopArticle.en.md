---
title: 'VPS Network Configuration'
published: false
body_classes: educpop-article
process:
    markdown: true
    twig: true
page-toc:
  active: true
---

# VPS network configuration

Our setup is a bit special: we use **local link addresses** to avoid waste and allow two hypervisors and /32 routing. For more information about that go to the [addressing page](https://wiki.arn-fai.net/benevoles:technique:adressage#topologie_arn) (in French only).
Because of this setup, the **network configuration inside the VPS** is also a bit special. The /etc/network/interfaces file should look like this with a Debian GNU/Linux or one of the derivative :
<kbd>auto lo
iface lo inet loopback</kbd>

<kbd>auto eth0
iface eth0 inet static
  pre-up  /sbin/ip link set $IFACE up
  pre-up  /sbin/ip route add 169.254.42.1 dev $IFACE
  address <your_IPv4>/32
  gateway 169.254.42.1</kbd>

<kbd>iface eth0 inet6 static
  address <an_IPv6_in_your_block_d'IPv6>
  gateway fe80::42:1
  accept_ra 0
  autoconf 0
  dad-attempts 0
  </kbd>

! Caution: since the release of the Debian "Stretch" version, the interface name is no longer eth0 but depends on the driver used by the network interface controller (example: "ens5").

## During the installation of Debian
### Easy way
The simplest solution is to not modify the settings network during the installation and to only set up the "**usual system utilities**" (especially if it is a netinstall). You need to connect to your VPS after the installation and set it up (protocole above this paragraph). Then use the command "ifup <interface_name>" and install an SSH server with the command "apt-get update && apt-get install openssh-server".

### Hard way
Another solution is to set up the network from the **command line** during the installation.
If you are in a **graphical installation**, the only way to access a **shell** is to click the "back" button at the right time and then choose "Run Shell".
If you are in a non-graphical installation, you can press the "escape" key at the right moment and choose "Run a shell". Another possibility: change the console with ctrl+alt+f2 (or alt+right or left arrow).

Process :
* Go to "configure network". Let pass (or cancel) the automatic configuration IPv6 then DHCP;
* Choose "configure the network yourself". Enter an almost-true network configuration: IPv4 = <IP_of_your_VPS>/24 then gateway = 89.234.141.131 then dns = 89.234.141.66 ;
* Let the connection be detected;
* Enter the name of the machine and the domain (if necessary);
* When you arrive at the choice of the super-user password, go to a shell (see above) and type and execute the following commands (none of them should display an error!):
* Identify your network interface with the command "ip link show". Here we will assume it is "ens5";
    * ip addr del <IP_of_VPS>/24 dev ens5
    * ip addr add <IP_of_your_VPS>/32 dev ens5
    * ip route add 169.254.42.1/32 dev ens5
    * ip route add default via 169.254.42.1 dev ens5
    * echo "nameserver 89.234.141.66" > /etc/resolv.conf
* Exit the shell by typing the "exit" command or by clicking on the "back" button;
* Return to the installation at "Create users and choose passwords".
* When the system is rebooted, you will have to connect again with VNC and write the file /etc/network/interfaces as communicated above then use the command "ifup ens5".

! Caution: the Debian installer may destroy the network configuration before you get to the step of selecting a download source... If this is the case, you must return to the shell and execute the commands in points 2 to 5 above again.

## Network configuration for OpenBSD
The principle is the same as for Linux: add a default route to a gateway IP directly on an interface (vio0 under OpenBSD). The configuration is done in the usual file: 

<kbd>/etc/hostname.vio0 inet alias <votre_IPv4> 255.255.255.255
!route -n add -host 169.254.42.1 -llinfo -link -static -iface vio0
inet6 <an_IPv6_in_your_block_IPv6> 56</kbd>
 
Next, you need to configure the gateways (IPv4 and IPv6), as usual in /etc/mygate (don't forget the %vio0):
<kbd>169.254.42.1
fe80::42:1%vio0</kbd>

## Network configuration for Netifrc (Gentoo)
How to configure the network of your VPS under Gentoo, or any other distribution using netifrc:
Create a /etc/conf.d/net file with the following content (replace <ifname> with the name of your interface to the exterior, and <your_IPv4> or <your_IPv6> with the IPs provided by ARN);

<kbd>config_<ifname>="<your_IPv4>/32<your_IPv6>/56"routes_<ifname>="default via 169.254.42.1"
dns_servers_<ifname>="89.234.141.66"</kbd>

<kbd>preup() {
  [ ${IFACE} == "<ifname>" ] && ip route add 169.254.42.1 dev <ifname>
  return 0
}
</kbd>

In this file, you can configure any other interface independently, and thanks to the init system based on symbolic links and bash scripts, you can "reconfigure" one interface or the other, without having to restart all the interfaces. If you are not using OpenRC as an init system, the rest of these instructions do not apply to you.
To activate the card, create a symbolic link from /etc/init.d/net.lo changing "lo" to the name of your interface, to do so:
<kbd>ln -s /etc/init.d/net.lo /etc/init.d/net.<ifname></kbd>

And activate it in the runlevel that fits you, as usual with OpenRC :
<kbd>rc-update add net.<ifname> default</kbd> 

To test, there is nothing easier:
<kbd>rc-service start net.<ifname></kbd> 

If you can ping perdu.com, you are no longer in the interstellar fog of network troubles!

## Advanced configuration
By default, IPs (v4 and v6) are routed directly to the VM, without next-hop. This means that our hypervisors send ARP/NDP requests directly to the tap and the VM must respond before receiving traffic. This simplistic way of working does not allow (without ARP/NDP proxy type tricks) IPv6 subnetting or the use of the VM as a VPN endpoint, for example, because then the VM will not be able to respond to ARP/NDP requests from our hypervisors.

For these uses to be possible, you need to request this from the ARN admins and change the contents of your /etc/network/interfaces file to this:

<kbd>auto lo
iface lo inet loopback</kbd>

<kbd>auto eth0
iface eth0 inet static
  pre-up  /sbin/ip link set $IFACE up
  pre-up  /sbin/ip route add 169.254.42.1 dev $IFACE
  address <your_IPv4>/32
  post-up /sbin/ip addr add 169.254.42.2/24 dev $IFACE scope link
  gateway 169.254.42.1</kbd>

<kbd>iface eth0 inet6 static
  address <an_IPv6_in_youre_block_d'IPv6>
  post-up /sbin/ip -6 addr add fe80::42:2 dev $IFACE
  gateway fe80::42:1
  accept_ra 0
  autoconf 0
  dad-attempts 0</kbd>


