---
title: 'Configuration réseau VPS'
body_classes: educpop-article
process:
    markdown: true
    twig: true
page-toc:
  active: true
published: false
---

# Configuration réseau d'un VPS

Notre installation est un peu spéciale : nous utilisons des **adresses locales au lien** pour éviter le gaspillage et permettre la redondance (deux hyperviseurs) + routage en /32. Pour plus d'infos, voir la page [adressage](https://wiki.arn-fai.net/benevoles:technique:adressage#topologie_arn). En conséquence, la **configuration réseau à l'intérieur du VPS** est un peu spéciale. Avec une Debian GNU/Linux ou dérivée, le fichier /etc/network/interfaces ressemble à ça :
<kbd>auto lo
iface lo inet loopback</kbd>

<kbd>auto eth0
iface eth0 inet static
  pre-up  /sbin/ip link set $IFACE up
  pre-up  /sbin/ip route add 169.254.42.1 dev $IFACE
  address <votre_IPv4>/32
  gateway 169.254.42.1</kbd>

<kbd>
iface eth0 inet6 static
  address <une_IPv6_dans_votre_bloc_d'IPv6>
  gateway fe80::42:1
  accept_ra 0
  autoconf 0
  dad-attempts 0
</kbd>

! Attention : depuis Debian Stretch, le nom de l'interface n'est plus eth0 mais dépend du pilote utilisé par la carte réseau (exemple : « ens5 »)…

## Durant l'installation de Debian
### Simple
La solution la plus simple est de ne pas configurer le réseau durant l'installation. Mais d'installer uniquement les **« utilitaires usuels du système »** (notamment s'il s'agit d'une netinstall) et de te connecter à ton VPS après l'installation et de réaliser la conf' ci-dessus. Puis, utiliser la commande « ifup <nom_de_l'interface> » et installer un serveur SSH avec la commande « apt-get update && apt-get install openssh-server ».

### Compliqué
La solution la moins simple consiste à effectuer la **configuration réseau en ligne de commande** durant l'installation.
* Si t'es dans une installation graphique, la seule possibilité d'accéder à un shell est de cliquer sur le bouton « revenir en arrière » au bon moment puis de choisir « Exécuter un shell ».
* Si t'es dans une installation non graphique, tu peux appuyer sur la touche « échap » au bon moment puis choisir « Exécuter un shell ». Autre possibilité : changer de console avec ctrl+alt+f2 (ou alt+fléches droite ou gauche).

Déroulé :
* Arriver jusqu'à « configurer le réseau ». Laisser passer (ou annuler) la configuration automatique IPv6 puis DHCP ;
* Choisir « configurez vous-même le réseau ». Saisir une presque-vraie configuration réseau : IPv4 = <IP_de_ton_VPS>/24 puis gateway = 89.234.141.131 puis dns = 89.234.141.66 ;
* Laisser faire la détection de la connexion ;
* Saisir le nom de la machine et le domaine (si besoin) ;
* Quand t'arrives au choix du mot de passe du super-utilisateur, va dans un shell (voir ci-dessus) et taper puis exécute les commandes suivantes (aucune ne doit afficher une erreur !) :
  * Identifier ton interface réseau avec la commande avec la commande « ip link show ». Ici, nous supposerons qu'il s'agit de « ens5 » ;
  * ip addr del <IP_de_ton_VPS>/24 dev ens5
  * ip addr add <IP_de_ton_VPS>/32 dev ens5
  * ip route add 169.254.42.1/32 dev ens5
  * ip route add default via 169.254.42.1 dev ens5
  * echo “nameserver 89.234.141.66” > /etc/resolv.conf
* Sortir du shell en tapant la commande « exit » ou en cliquant sur le bouton « revenir en arrière » ;
* Reprendre l'installation à « Créer les utilisateurs et choisir les mots de passe »
* Au redémarrage du système, il faudra à nouveau se connecter en VNC et écrire le fichier /etc/network/interfaces tel qu'il est communiqué ci-dessus puis utiliser la commande « ifup ens5 ».

! Attention : l'installateur Debian peut détruire la configuration réseau avant que t'arrives à l'étape de sélection d'une source de téléchargement… Si c'est le cas, il faut revenir en shell et exécuter à nouveau les commandes des points 2 à 5 ci-dessus.

## Configuration réseau pour OpenBSD
Le principe est le même que pour Linux : ajouter une route par défaut vers une IP passerelle directement sur une interface (vio0 sous OpenBSD). La configuration se fait dans le fichier habituel : 
<kbd>/etc/hostname.vio0
inet alias <votre_IPv4> 255.255.255.255
!route -n add -host 169.254.42.1 -llinfo -link -static -iface vio0
inet6 <une_IPv6_dans_votre_bloc_IPv6> 56</kbd>

Ensuite il faut configurer les passerelles (IPv4 et IPv6), comme d'habitude dans /etc/mygate (n'oubliez pas le %vio0) :
<kbd>169.254.42.1
fe80::42:1%vio0</kbd>

## Configuration réseau pour FreeBSD
Dans /etc/rc.conf :  

<kbd>ifconfig_vtnet0="inet <votre_adresse_IPv4> netmask 255.255.255.255"
static_routes="gate"
route_gate="-host 169.254.42.1 -link -static -iface vtnet0"
defaultrouter="169.254.42.1"</kbd>

<kbd>ifconfig_vtnet0_ipv6="inet6 <une_IP_dans_votre_réseau_IPv6> prefixlen 56"
ipv6_defaultrouter="fe80::42:1%vtnet0"</kbd>

## Configuration réseau pour Netifrc (Gentoo)
Comment configurer le réseau de son VPS sous Gentoo, ou toute autre distribution utilisation netifrc :
Créez un fichier /etc/conf.d/net avec le contenu suivant (remplacez <ifname> par le nom de votre interface vers l'extérieur, et <votre_IPv4> ou <votre_IPv6> par les IP fournies par ARN) :

<kbd>config_<ifname>="<votre_IPv4>/32<votre_IPv6>/56"</kbd><kbd>routes_<ifname>="default via 169.254.42.1"dns_servers_<ifname>="89.234.141.66"</kbd>

<kbd>preup() {
  [ ${IFACE} == "<ifname>" ] && ip route add 169.254.42.1 dev <ifname>
  return 0
}</kbd>  


Dans ce fichier, vous pourrez configurer n'importe quelle autre interface indépendamment, et grâce au système d'init basé sur des liens symboliques et scripts bash, pourrez “reconfigurer” une interface ou l'autre, sans avoir à redémarrer toutes les interfaces. Si vous n'utilisez pas OpenRC comme système d'init, la suite de ces instructions ne vous concerne pas.  


Pour activer la carte, créer un lien symbolique de /etc/init.d/net.lo en changeant “lo” par le nom de votre interface, pour se faire :
<kbd>ln -s /etc/init.d/net.lo /etc/init.d/net.<ifname></kbd>  


Et l'activer dans le runlevel qui vous sied, comme d'habitude avec OpenRC :
<kbd>rc-update add net.<ifname> default</kbd>  


Pour tester, rien de plus simple :
<kbd>rc-service start net.<ifname></kbd>  



Si vous pouvez pinger [perdu.com](https://wiki.arn-fai.net/atrier:perdu.com), c'est que vous n'êtes plus dans le flou intersidéral des déboires du réseau !  


## Configuration avancée
Par défaut, les IPs (v4 et v6) sont routées directement sur la VM, sans next-hop. Cela signifie que nos hyperviseurs envoient directement des demandes ARP/NDP sur la tap et que la VM doit y répondre avant de recevoir du trafic. Ce fonctionnement simpliste ne permet pas (sans bidouille type ARP/NDP proxy) le subnetting IPv6 ou l'utilisation de la VM comme un endpoint VPN, par exemple, car alors, la VM ne sera pas en mesure de répondre aux demandes ARP/NDP de nos hyperviseurs.  



Pour que ces usages soient possibles, vous devez en faire la demande auprès des admins ARN et changer le contenu de votre fichier /etc/network/interfaces pour celui-ci :  



<kbd>auto lo
iface lo inet loopback</kbd>

<kbd>auto eth0
iface eth0 inet static
  pre-up  /sbin/ip link set $IFACE up
  pre-up  /sbin/ip route add 169.254.42.1 dev $IFACE
  address <votre_IPv4>/32
  post-up /sbin/ip addr add 169.254.42.2/24 dev $IFACE scope link
  gateway 169.254.42.1</kbd>

<kbd>iface eth0 inet6 static
  address <une_IPv6_dans_votre_bloc_d'IPv6>
  post-up /sbin/ip -6 addr add fe80::42:2 dev $IFACE
  gateway fe80::42:1
  accept_ra 0
  autoconf 0
  dad-attempts 0</kbd>

 
