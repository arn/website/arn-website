---
title: 'Configuration réseau VPS'
body_classes: educpop-article
process:
    markdown: true
page-toc:
  active: true
published: false
---

# Interopérabilité des messageries instantanées
La messagerie instantanée Matrix/Element
Voir La Litière, le Wiki des Chatons
Passerelle WhatsApp
Création de salon-portail WhatsApp-Matrix
Méthode 1 - depuis Whatsapp:
Créer groupe WhatsApp
Inviter arn-fai.net (+33 six vingt-huit six trois deux fois le deux trente-deux) dans un groupe existant
Attendre message
Écrire pseudo : !am @pseudo:sans-nuage.fr
Ajouter des contacts WhatsApp ou leur envoyer le lien d'invitation
Méthode 2 - depuis Matrix:
Créer salon Matrix privé et non-chiffré
Inviter @arnmessager:sans-nuage.fr
Taper !am whatsapp
Envoyer le lien d'invitation WhatsApp à vos contacts qui rejoignent le groupe en rentrant le lien dans leur appli
