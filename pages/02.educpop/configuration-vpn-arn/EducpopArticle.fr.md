---
title: 'Configuration réseau VPS'
body_classes: educpop-article
process:
    markdown: true
page-toc:
  active: true
published: false
---

# Configuration VPN ARN

## Configuration sur un système GNU/Linux
1. Installer openvpn
1. Extraire l'archive idvpn.zip téléchargé sur votre espace adhérent
1. Copier le fichier idvpn.conf dans /etc/openvpn/
1. sudo service openvpn restart 
1. ip a → si l'interface réseau « tun0 » n'apparait pas au bout de quelques minutes, **écrivez à infra-support@arn-fai.net ou sur le chat**.

Nota : idvpn est votre **identifiant VPN** (format 1ère lettre du prénom + nom)
Instructions plus détaillées/avancées.
Il faut installer **OpenVPN** de la manière dont vous installez un logiciel d'habitude : apt-get, aptitude, logithèque,… Exemple avec apt-get :
</kbd>sudo apt-get install openvpn</kbd>

Il faut télécharger [ce fichier de configuration](https://sans-nuage.fr/file/s/4ATtH4HZiHgxZH6?download) et le stocker dans /etc/openvpn/ . Il faut évidemment les droits root pour faire ça. Vous pouvez le renommer mais le nom doit obligatoirement terminer par « .conf ». À l'intérieur du fichier, il faut remplacer « $login » par votre login personnel sur le VPN.


Il faut également déplacer les fichiers que nous vous fournissons (login.{crt,key} et ca.crt) dans /etc/openvpn/. Il faut évidemment les droits root pour faire ça.

Pour démarrer l'application, un simple sudo service Openvpn restart devrait suffire.
Sur un système équipé de systemd (comme Debian Jessie), la commande ci-dessus n'est pas bonne. Il faut faire :
<kbd>systemctl enable openvpn@openvpn.service</kbd>
<kbd>systemctl restart openvpn@openvpn.service</kbd>

! Attention, si vous avez changé le nom du fichier de configuration (openvpn.conf par défaut), il faudra adapter les deux commandes ci-dessus en remplaçant le « openvpn » après le « @ » par le nom que vous avez donné. Exemple : <kbd>systemctl enable openvpn@arn.service</kbd> <kbd>systemctl restart openvpn@arn.service</kbd>

## Note pour Mageia
Contrairement à Debian, la distribution Mageia d'openpvn ne contient apparemment pas de méthode pour mettre à jour la liste des **résolveurs de noms** (/etc/resolv.conf); le plus simple est d'ajouter un petit script qui insère le resolveur de noms d'ARN (89.234.141.66) au moment du démarrage du VPN. Pour cela, créer (avec les droits d'administrateur) un fichier /etc/openvpn/arn-up.sh, qui contient:

<kbd>#!/bin/sh
echo "nameserver  89.234.141.66" > /run/resolvconf/interface/tun0
resolvconf -u
</kbd>

puis donner à ce fichier les droits d'exécution:
<kbd>sudo chmod +x /etc/openvpn/arn-up.sh</kbd>

et indiquer ce script dans le fichier /etc/openvpn/openvpn.conf, en décommentant/modifiant les 2 lignes à la fin du paragraphe commençant par ; DNS:
<kbd>; script-security 2 ; pour autoriser les exécutions de programmes externes
; route-up /etc/openvpn/client-handler</kbd>

pour les remplacer par:
<kbd>script-security 2
route-up "/etc/openvpn/arn-up.sh"
</kbd>

## Tutoriel pour Mac OS X
Télécharger TunnelBlick (Alternative OpenVPN pour Mac) ici : [https://tunnelblick.net](https://tunnelblick.net/) et l'installer.

Créer un dossier qui contiendra les fichiers de configuration.
Télécharger le fichier de configuration (clic droit sur le lien, « Enregistrer la cible du lien sous » ou équivalent) : [https://sans-nuage.fr/file/s/N688PAB3kYtiAQK&download](https://sans-nuage.fr/file/s/N688PAB3kYtiAQK&download) et le placer dans le dossier créé précédemment.
Copier les fichiers personnels fournis dans l'espace adhérent.
Éditer le fichier openvpn-win.ovpn. Lignes 15 et 16, remplacer « $login » par votre login. Exemple :
* ca ca.crt
* cert prenomnom.crt
* key prenomnom.key
Enregistrer le fichier.

Faire glisser le fichier modifié sur l'icône de TunnelBlick dans la barre de menu.
Enjoy !

## Tutoriel pour Windows
Télécharger OpenVPN ici : [https://openvpn.net/index.php/open-source/downloads.html](https://openvpn.net/index.php/open-source/downloads.html) et l'installer.

Ici, on considère qu'il est installé dans C:\Program Files\OpenVPN
Récupérer le fichier de configuration (clic droit sur le lien, « Enregistrer la cible du lien sous » ou équivalent) :[https://sans-nuage.fr/file/s/N688PAB3kYtiAQK&download](https://sans-nuage.fr/file/s/N688PAB3kYtiAQK&download)
Copier ce fichier dans C:\Program Files\OpenVPN\config
Toujours dans ce dossier, il faut également copier vos fichiers personnels téléchargés dans votre espace adhérent.

Dans le menu démarrer (ou sur le bureau), clic droit sur l'icône “OpenVPN GUI” → Exécuter en tant qu'administrateur.

En bas à droite, dans la zone de notification de la barre des tâches, clic droit sur la nouvelle icône → Editer la configuration.

Aux lignes 15 et 16, remplacer « $login » par votre login. Exemple :
* ca ca.crt
* cert glucas.crt
* key glucas.key
Enregistrer vos modifications.

Clic droit sur l'icône OpenVPN dans la zone de notification de la barre des tâches → Connecter.

Merci à TeSla d'avoir documenté cette section. :)

## Tutoriel pour YunoHost ou une Brique Internet
Il faut d'abord installer l'app vpnclient de YunoHost (rien à voir avec vpnc, Cisco).

Il faut ensuite accéder à l'interface web de configuration du VPN à l'URL suivante : [https://IP_de_la_Brique/vpnadmin/](https://IP_de_la_Brique/vpnadmin/) et charger le fichier .cube dans l'onglet “Automatique”. Pour récupérer le fichier .cube il faut télécharger le matériel cryptographique depuis l'interface adherents.arn-fai.net qui contient un dossier tmp contant le fichier “USER.cube”.

Puis il ne faut pas oublier de valider sur l'interface de la page d'admin de vpnclient avec le bouton en bas.

! Le fichier .cube inclut votre certificat et votre clé privée. Attention donc de ne pas le stocker n'importe où, à la légère. 

! Attention également : nos certificats ont une durée de vie limitée (max un an). Votre .cube sera donc obsolète passé ce délai.

Plus d'information :
* [Installation de l'app vpnclient de Yunohost](https://install-app.yunohost.org/?app=vpnclient)
* [Installation de la Brique Internet](https://install.labriqueinter.net/)
* [Détails sur le .cube](http://labriqueinter.net/dotcubefiles.html)

## Pistes de debug
* Sous GENTOO : activer le useflag down-root sinon on a des erreurs dans /var/log/messages tels que :

<kbd>
Oct 28 12:03:27 localhost ovpn-arn[27909]: PLUGIN_INIT: could not load plugin shared object 
/usr/lib/openvpn/openvpn-plugin-down-root.so: /usr/lib/openvpn/openvpn-plugin-down-root.so: 
cannot open shared object file: No such file or directory
Oct 28 12:03:27 localhost ovpn-arn[27909]: Exiting due to fatal error
Oct 28 12:03:27 localhost /etc/init.d/openvpn.arn[27908]: start-stop-daemon: failed to start `/usr/sbin/openvpn'
Oct 28 12:03:27 localhost /etc/init.d/openvpn.arn[27879]: WARNING: openvpn.arn has started, but is inactive
</kbd>

* Sous un système GNU/Linux (Debian, Ubuntu,…), si vous n'arrivez pas à comprendre l'origine du problème, vous pouvez regarder les logs pour les transmettre à un admin ARN.
    * Si vous avez un système équipé de systemd (si vous avez changé le nom du fichier de configuration dans /etc/openvpn, c'est ce nom-là qu'il faut indiquer après « @ » et avant « .service » au lieu de « openvpn » : sudo journalctl -u openvpn@openvpn.service -xn 50. Montrer tout ce qu'OpenVPN raconte à un admin ARN.
    * Si vous n'avez pas un système équipé de systemd, le plus simple reste encore de stopper le VPN sudo service openvpn stop puis de modifier le fichier de configuration (/etc/openvpn/openvpn.conf par défaut) pour faire précéder par un point-virgule (“;”) la ligne qui commence par « daemon ». Il faut avoir les droits root pour faire ça. ;) Enfin, exécutez OpenVPN dans un terminal : sudo openvpn –config /etc/openvpn/openvpn.conf. Montrer tout ce qu'OpenVPN raconte à un admin ARN.

À compléter avec l'expérience. ;)
