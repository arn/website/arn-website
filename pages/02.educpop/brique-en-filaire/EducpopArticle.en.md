---
title: 'Internet Brick in wired mode'
body_classes: educpop-article

page-toc:
  active: true
published: false
---
# Connect to the Internet brick's VPN  in wired mode
If you **use the Internet brick with a VPN** and the WiFi hotspot application, devices connected to this WiFi necessarily go through the VPN to connect to the Internet.

But what if you have **wired devices connected to your Internet Box** and want it to be connected to the VPN too?


## Configuration protocol for a GNU/Linux connection system
All the operations in this section (and sub-sections) must be performed on the device you want to be connected to the Internet brick.


## IPv4
In most cases:
* With IP, you must use the usual IP assigned to your router. To find out your IP, either right-click on the Network-Manager icon (GNOME) and then "Connection information", or use the following command in a terminal:
<kbd>ip -4 addr show dev eth0</kbd>
 
* With a default gateway, you should use the IP obtained by your internet brick on its eth0 interface. This IP has been assigned by your Internet Box. To find it, you have to connect to your brick in SSH and use the same command indicated above.

Here is an example of configuration protocol for **network-manager** (for example, /etc/NetworkManager/system-connections/eth0):
<kbd>
[ipv4]
method=manual
dns=89.234.141.66;
address1=192.168.XXX.YYY/24,192.168.XXX.ZZZ</kbd>

192.168.XXX.YYY is the common IP address of your machine. 192.168.XXX.ZZZ is the IP address of your Internet brick.

! Caution:
* The file /etc/NetworkManager/system-connections/eth0 must not be empty, otherwise it means it is not the right file, check the name.
* A block "[ipv4]" must already exist in the configuration file, it must be replaced by the one indicated above.


## IPv6
In most cases:
* You should assign an IPv6 address from the IPv6 Prefix Delegation given by your VPN provider connected with your internet brick.
* With a default gateway, you should use the local IPv6 address of your Internet brick: fe80::42:babe .

Here is an example of configuration protocol for network-manager (for example, /etc/NetworkManager/system-connections/eth0):

<kbd>[ipv6]
method=manual
dns=2a00:5881:8100:1000::3;
address1=2a00:5881:8118:xxx::yyyy/64,fe80::42:babe
ip6-privacy=2</kbd>

2a00:5881:8118:xxx::yyyy/64 is an IPv6 in the prefix delegation given by your VPN provider. fe80::42:babe is the  IPv6 default local address of your Internet Brick.

! Caution:
* The file /etc/NetworkManager/system-connections/eth0 must not be empty, otherwise it means it is not the right file, check the name.
* A block "[ipv4]" must already exist in the configuration file, it must be replaced by the one indicated above.

It is possible that your device requests and saves automatically your router's IPv6 address, if so it is better to delete it. 

If your ISP is Free it would be: <kbd>ip -6 addr del 2a01:e35:xxx:xxx::/64 dev eth0</kbd>

## Changes implementation
Once you have made changes in the "IPv4" and "IPv6" sections, you need to ask Network-Manager to reload your connection profile:
<kbd>sudo nmcli connection reload</kbd>

Then you need to disconnect and reconnect from the network.

## Configuration protocol to connect the Internet brick (must be done in any case)
In your Internet brick's settings, you must add a route to make sure your Internet traffic goes through the Ethernet interface. You can do it by adding the following line to the /etc/network/interfaces Internet brick file:
<bkd>post-up ip -6 route add 2a00:5881:8118:xxx::yyyy dev eth0</kbd>

You must add it to the "block" related to the eth0 interface. The end product should be like this:
<kbd>allow-hotplu be/128 dev eth0
  post-up ip -6 route add 2a00:5881:8118:xxx::yyyy dev eth0</kbd>
