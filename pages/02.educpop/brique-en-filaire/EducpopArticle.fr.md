---
title: 'Brique en filaire'
body_classes: educpop-article
page-toc:
  active: true
published: false
---


# Mutualiser la connexion VPN de la Brique en filaire
Si vous **utilisez la Brique avec un VPN** et l'application de hotspot WiFi, les appareils connectés au hotspot émis par la Brique Internet passent automatiquement par le VPN pour sortir sur Internet.

Mais comment faire si vous avez des **machines connectées en filaire à votre Box Internet** et que souhaitez que leur connexion passe également par la Brique ?


## Configuration sur un système GNU/Linux
Toutes les manipulations de cette section (et de ses sous-sections) sont à effectuer sur la machine dont vous voulez que sa connexion passe par la Brique.


## IPv4
De manière générale :
* En adresse IP, il faut utiliser l'IP habituelle que vous attribue votre Box Internet. Pour en prendre connaissance, soit cliquez droit sur l'icône Network-Manager (GNOME) puis « Informations sur la connexion », soit utilisez la commande suivante dans un terminal : <kbd>ip -4 addr show dev eth0</kbd>

* En passerelle par défaut (gateway),il faut utiliser l'IP obtenue par votre Brique sur son interface eth0. Cette IP lui a été attribuée par votre Box Internet. Pour en prendre connaissance, il faut se connecter à la Brique en SSH et utiliser la commande indiquée ci-dessus.

Voici un exemple de configuration pour **network-manager** (/etc/NetworkManager/system-connections/eth0 , par exemple) :
<kbd>
[ipv4]
method=manual
dns=89.234.141.66;
address1=192.168.XXX.YYY/24,192.168.XXX.ZZZ</kbd>

192.168.XXX.YYY est l'adresse IP habituelle de votre machine. 192.168.XXX.ZZZ est l'adresse IP de votre Brique Internet.

! Attention :
* Le fichier /etc/NetworkManager/system-connections/eth0 ne doit pas être vide, sinon c'est que ce n'est pas le fichier qui correspond à votre connexion, vérifiez le nom.
* Un bloc « [ipv4] » doit déjà exister dans le fichier de configuration, il faut le remplacer par celui indiqué ci-dessus.


## IPv6
De manière générale :
* Il faut lui attribuer une adresse IPv6 qui fait partie du préfixe délégué alloué par le fournisseur du VPN configuré sur la Brique.
* En passerelle par défaut (gateway), il faut utiliser l'adresse IPv6 locale de votre Brique : fe80::42:babe .

Voici un exemple de configuration pour network-manager (/etc/NetworkManager/system-connections/eth0, par exemple) :

<kbd>
[ipv6]
method=manual
dns=2a00:5881:8100:1000::3;
address1=2a00:5881:8118:xxx::yyyy/64,fe80::42:babe
ip6-privacy=2</kbd>

2a00:5881:8118:xxx::yyyy/64 est une IPv6 dans le préfixe délégué par le fournisseur de VPN. fe80::42:babe est l'adresse IPv6 locale par défaut de la Brique Internet.

! Attention :
* Le fichier /etc/NetworkManager/system-connections/eth0 ne doit pas être vide, sinon c'est que ce n'est pas le fichier qui correspond à votre connexion, vérifiez le nom.
* Un bloc « [ipv6] » doit déjà exister dans le fichier de configuration, il faut le remplacer par celui indiqué ci-dessus.

Il est possible que votre machine récupère automatiquement une adresse IPv6 fournie par votre Box Internet, dans ce cas il vaut mieux la supprimer. Exemple en étant chez Free :
<kbd>ip -6 addr del 2a01:e35:xxx:xxx::/64 dev eth0</kbd>


## Application des modifications
Une fois les modification des sections « IPv4 et « IPv6 » effectuées, il faut indiquer à Network-Manager de recharger le profile de votre connexion :
<kbd>sudo nmcli connection reload</kbd>

Puis, il faut vous déconnecter-reconnecter du réseau.


## Configuration à effectuer sur la Brique Internet dans tous les cas
Dans la config' de votre Brique, Il faut ajouter une route pour que le trafic à destination de votre machine en Ethernet passe bien par l'**interface Ethernet**. Cela se fait en ajoutant la ligne suivante dans le fichier /etc/network/interfaces de la Brique :
<kbd>post-up ip -6 route add 2a00:5881:8118:xxx::yyyy dev eth0</kbd>
Cette ligne doit être ajoutée au bloc concernant l'interface eth0. 

Au final, celui-ci doit ressembler à ça :
<kbd>
allow-hotplu be/128 dev eth0
  post-up ip -6 route add 2a00:5881:8118:xxx::yyyy dev eth0
</kbd>



