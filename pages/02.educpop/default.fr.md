---
title: 'Se mobiliser'
media_order: 'EDUCPOP 01.svg,EDUCPOP Titre.svg,Logo_Noir.svg,Logo_Rose.svg,EDUCPOP title.svg,Educpop_menu.svg'
body_classes: educpop
visible: true
process:
    markdown: true
    twig: true
page-toc:
    active: false
published: false
sitemap:
  ignore: true
---

<img src="images/EDUCPOP title.svg" alt="EDUCPOP" class="logoeducpop"/>

<div class="documentation">
    <img src="images/EDUCPOP 01.svg" alt="documentation en ligne"/>
    <h1>Documentation</h1>
    <p>
    Educpop est une page entièrement dédiée à la <strong>documentation</strong> sur des thèmes variés tels que la <strong>collecte des données personnelles</strong> ou encore les <strong>services et outils alternatifs aux GAFAM</strong>. Elle est à votre disposition afin que vous puissiez vous informer et vous former à tout moment sur ces sujets. </p>
</div>

{% include 'partials/tntsearch.html.twig' with { in_page: true } %}
  

<div class="div_educpop">
    <div class="titre_educpop">
        <img src="images/Logo_Noir.svg" alt=" "/>
        <h2>Documentation utilisateurs</h2>
    </div>
    <ul>
        <li><a href="#">Incidents et maintenance des services</a></li>
        <li><a href="#">Voir les infos de son service</a></li>
    </ul>
</div>


<div class="div_educpop">
    <div class="titre_educpop">
        <img src="images/Logo_Noir.svg" alt=" "/>
        <h2>Pour se libérer du pistage</h2>
    </div>
    <ul>
        <li><a href="#">Parcours « se libérer du pistage »</a></li>
        <li><a href="#">Pourquoi nous libérer du pistage ?</a></li>
    </ul>
</div>



<div class="div_educpop atelier">
    <div class="titre_educpop">
        <img src="images/Logo_Noir.svg" alt=" "/>
        <h2>Ateliers</h2>
    </div>
    <ul>
        <div class="li_educpop">
            <li><a href="#">Configurer son navigateur</a></li>
            <li><a href="#">Passer à Linux</a></li>
            <li><a href="#">Configurer Linux</a></li>
        </div>
        <div class="li_educpop">
            <li><a href="#">Passer son smartphone <br>sur un système libre</a></li>
            <li><a href="#">Configurer son smartphone</a></li>
            <li><a href="#">Sécuriser son smartphone <br>(avancé)</a></li>
        </div>
        <div class="li_educpop">
            <li><a href="#">S'auto-héberger</a></li>
            <li><a href="#">Réduire son empreinte écologique</a></li>
        </div>
    </ul>
</div>



<div class="div_educpop">
   <div class="titre_educpop">
        <img src="images/Logo_Noir.svg" alt=" "/>
        <h2>Sans-nuage.fr</h2>
    </div>
    <ul>
        <div class="li_educpop">
            <li><a href="#">Synchroniser ses mails</a></li>
            <li><a href="#">S'interconnecter avec <br>un groupe WhatsApp</a></li>
        </div>
        <div class="li_educpop">
             <li><a href="#">Synchroniser son agenda <br>et ses contacts</a></li>
        </div>
        <div class="li_educpop">
             <li><a href="#">Synchroniser ses fichiers</a></li>
        </div>
    </ul>
</div>


<div class="div_educpop">
    <div class="titre_educpop">
        <img src="images/Logo_Noir.svg" alt=" "/>
        <h2>Accès Internet</h2>
    </div>
    <ul>
        <div class="li_educpop">
            <li><a href="#">Configurer ses <br>résolveurs DNS</a></li>
            <li><a href="#">Dépannage wifi de quartier</a></li>
        </div>
        <div class="li_educpop">
            <li><a href="educpop/configuration-vpn-arn">Configurer son VPN</a></li>
        </div>
        <div class="li_educpop">
            <li><a href="educpop/brique-en-filaire">Brique Internet en filaire</a></li>
        </div>
    </ul>
</div>

<div class="div_educpop">
    <div class="titre_educpop">
        <img src="images/Logo_Noir.svg" alt=" "/>
        <h2>Hébergement</h2>
    </div>
    <ul>
        <li><a href="#">Configuration réseau Housing</a></li>
        <li><a href="educpop/configuration-reseau-vps">Configuration réseau VPS</a></li>
        <li><a href="#">Se connecter en VNC</a></li>
    </ul>
</div>

<div class="div_educpop">
    <div class="titre_educpop">
        <img src="images/Logo_Noir.svg" alt=" "/>
        <h2>Documentation bénévoles</h2>
    </div>
    <ul>
        <div class="li_educpop">
            <li><a href="#">Liste des choses <br>à faire</a></li>
            <li><a href="#">Comptes-rendus</a></li>
            <li><a href="#">Infos administratives</a></li>
        </div>
        <div class="li_educpop">
            <li><a href="#">Les coûts du <br>fonctionnement d'ARN</a></li>
            <li><a href="#">Procédures pour l'AG</a></li>
            <li><a href="#">Infos techniques</a></li>
        </div>
        <div class="li_educpop">
            <li><a href="#">Procédures techniques <br>(admin)</a></li>
            <li><a href="#">Gestion des accès</a></li>
            <li><a href="#">Charte adminsys</a></li>
        </div>
    </ul>
</div>
<section class="filler">
</section>

