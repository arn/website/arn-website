---
title: Newsletter
body_classes: modular
class: contacter
form:
    name: newsletter
    fields:
        email:
            id: newsletter-email
            label: 'Courriel'
            autocomplete: on
            placeholder: 'Saisissez votre mail'
            type: email
            validate:
                required: true
        news:
            id: newsletter-news
            type: checkbox
            label: Le télégramme: 3 à 4 mails par an de nouvelles de l'association
            default: true
        tuto:
            id: newsletter-tuto
            type: checkbox
            label: 2 mails par mois pour changer ses pratiques numériques
        events:
            id: newsletter-events
            type: checkbox
            label: 1 mail avant chaque atelier ou conférence
        ville:
            id: newsletter-ville
            type: select
            label: Cinq fois 7 ?
            options:
                agen: 9
                nevers: 12
                paris: 18
                nancy: 20
                metz: 23
                belfort: 27
                strasbourg: 32
                toulouse: 35
                lille: 40
                marseille: 42
                lyon: 48
                valence: 50
                brest: 55
                nantes: 57
            validate:
                required: true
                pattern: "^toulouse$"
                message: Oups, mauvaise réponse pour le calcul
    buttons:
        submit:
            type: submit
            value: "S'inscrire"
    process:
        captcha: false
        save:
            filename: newsletter.csv
            body: '{% include ''forms/newsletter.csv.twig'' %}'
            operation: add
        email:
            subject: '[Newsletter][add] {{ form.value.email|e }}'
            body: '{% include ''forms/data.html.twig'' %}'
        message: 'Merci, votre inscription a bien été transmise!'
---

# __S'inscrire aux *info-lettres*__


