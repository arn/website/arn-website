---
title: 'DNS Resolver'
body_classes: modular
class: dns_intro_bold
---

# __Our free and open *DNS Resolver*__

89.234.141.66<br>
2a00:5881:8100:1000::3

_UDP and TCP, port 53 (standard), DNSSEC validation<br>
DoH coming soon_
