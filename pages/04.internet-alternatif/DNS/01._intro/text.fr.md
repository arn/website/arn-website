---
title: 'Résolveur DNS'
body_classes: modular
class: dns_intro_bold
---

# __Notre *résolveur DNS* ouvert et gratuit__

89.234.141.66<br>
2a00:5881:8100:1000::3

_UDP et TCP, port 53 (standard), validation DNSSEC<br>
DoH à venir_
