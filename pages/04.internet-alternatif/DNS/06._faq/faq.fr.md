---
title: 'Foire aux questions'
body_classes: modular
---
## __Foire aux *questions*__

---

### Est-ce payant ?

Non. Si vous l'utilisez pour un projet libres n'hésitez pas à nous le signaler et prévoyez de la redondance au cas où ce résolveur est en panne.

---

### Pourquoi ne pas utiliser ceux de Google ou Cloudflare, les IPs sont plus simples ?

Parce que ces résolveurs mentent et/ou enregistrent les sites que l'on consulte avec.

---

### Est-ce qu'il existe des cas concrets de comportements qui nous semblent abusifs ?

Oui :
 * Les Fournisseurs d'Accès à Internet commerciaux français d'envergure nationale ont tous eu la tentation d'envoyer de la publicité quand un site web n'existe pas (exemple : vous avez tapé « arnfai.net » au lieu d'« arn-fai.net » dans la barre d'adresse de votre navigateur web). Certains l'ont mise en pratique comme SFR ou Alice. <br>
            <a href="https://www.opendns.com/">D'autres récursifs DNS « mentent »</a> pour des raisons de sécurité (pour vous empêcher d'aller sur un site contenant des virus et autres joyeusetés, pour corriger automatiquement vos fautes de frappe d'un nom pour éviter de tomber sur un mauvais site web, etc.) qui ne sont pas forcément légitimes (autonomie des personnes, erreur d'appréciation de la qualité d'un site web, etc.).
            
 * C'est sur le DNS que repose le blocage judiciaire des sites web de jeux d'argent qui ne payent pas leur licence d'exploitation à l'État (loi 2010-476 de 2010), ou de tout autre site web (exemples : t411 et Copwatch). Mais aussi tout le blocage administratif (hors procédure judiciaire), sur liste secrète du ministère de l'Intérieur, des sites web pédopornographiques (loi LOPPSI 2 de 2011) ou faisant l'apologie du terrorisme (<a href="https://www.laquadrature.net/2014/10/29/pjl-terrorisme-le-parlement-peut-encore-sopposer-a-la-derive-securitaire/">loi Cazeneuve de 2014</a>). Ces types de blocage sont insidieux puisqu'ils ne font pas disparaître les problèmes, ni ne viennent au secours des victimes, ni ne les dédommagent : les infractions à la loi et les atteintes aux personnes continuent… mais en silence, derrière le rideau, puisque leurs auteur·-e·-s ne sont pas arrêté·-e·-s. Bloquer des sites web, c'est fermer les yeux sur des problèmes plutôt que de s'attaquer à leurs racines. De plus, comment contrôler la légitimité de l'action du gouvernement dans le cas du blocage administratif afin d'éviter les dérives ? Un des premiers sites web bloqués dans ce contexte, islamic-news, l'a été sans véritables arguments de droit ou de fait de la part du gouvernement.
            
 * Du point précédent découle une inégalité entre les citoyens. Un blocage judiciaire est opposable aux seuls Fournisseurs d'Accès à Internet qui font l'objet d'une décision définitive de la justice. Dans le cadre du blocage administratif, la liste secrète est communiquée par le Ministère de l'Intérieur aux Fournisseurs d'Accès à Internet sélectionnés par le gouvernement. En pratique, dans les deux cas, il s'avère que seuls les plus gros FAI commerciaux de France d'envergure nationale sont contraints à prendre des mesures de blocage. Quid du citoyen qui a recours aux prestations d'un autre FAI (université, travail, association, etc.) ? Sans compétences particulières, les contenus consultables en ligne par l'ensemble des citoyens français sont différents. Est-ce que cette différence d'accès à l'information est acceptable ?

---

### Quelle est la politique de conservation des données de connexions (logs) d’ARN pour ce service ?
        
Nous n’enregistrons aucune donnée de connexion car nous ne sommes légalement pas contraint de le faire.

---
    
### Puis-je monter un tunnel DNS à travers ce résolveur ?
        
Non, nous ne souhaitons pas que ce résolveur génère du trafic inhabituel pour un résolveur DNS.

---
    
### Jusqu’où porte la validation DNSSEC ?
        
Notre résolveur vérifie cryptographiquement la validité d’une réponse DNS. Toutefois, à moins que vous n’ayez mis en place une solution pour valider cette info sur votre équipement, vous restez vulnérable à une manipulation située entre vous et le résolveur d’ARN.

De fait, la validation DNSSEC sur notre résolveur, n'apporte réellement toutes ses garanties qu'aux <a href="https://www.bortzmeyer.org/ou-valider-dnsssec.html">abonné⋅es ayant un service avec une IP ARN</a>.
    
---
    
### Pourquoi ne pas proposer DNS-over-HTTPS ?
        
Parce qu’on a pas encore eu le temps. N’hésitez pas à venir nous aider afin que nous puissions le mettre en place :)

---
    
### Comment reproduire ce service ?
        
La mise en place de notre service de résolveur DNS est documentée sur  <a href="https://wiki.arn-fai.net/benevoles:technique:recursif">la page wiki</a> des bénévoles comment monter un recursif DNS ouvert.

Avant d'ouvrir un récursif DNS ouvert au public, il y a quelques précautions à prendre afin que celui-ci ne soit pas utilisé pour conduire des attaques DDoS de grande envergure.

Voir en détail les <a href="http://www.guiguishow.info/2014/08/23/comment-mettre-en-place-un-serveur-dns-recursif-cache-ouvert-dans-de-bonnes-conditions/">réflexions pour protéger un résolveur DNS ouvert</a>.
        

---

