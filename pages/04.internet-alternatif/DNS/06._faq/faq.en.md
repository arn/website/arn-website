---
title: 'Frequently Asked Questions'
body_classes: modular
---

## __Frequently asked *questions*__

---
### Do we have to pay for it?

No. If you are using it for a free project, please let us know and plan for redundancy in case the resolver goes down.

---
### Why not use those of Google or Cloudflare, the IPs are simpler?

Because these resolvers lie and/or record the sites you visit with them.

---
### Are there concrete cases of what ARN considers as abusive uses?

Yes:
 * French commercial ISPs have all been tempted to send the user advertisements when a website does not exist (example: you have typed "arnfai.net" instead of "arn- fai.net" in the address bar of your browser). Some have done it like SFR or Alice. <br>
<a href="https://www.opendns.com/">Other DNS resolvers "lie" for security reasons</a> (to prevent access to a site that contains viruses or to automatically correct your typos) which are not necessarily legitimate (individual's autonomy, error in assessing the quality of a website, etc.).
            
 * Judicial blocking is relying on the DNS for gambling websites that do not pay their business licence (law 2010-476 of 2010) or any other website (examples: t411 and Copwatch)... But also all the administrative blocking (outside legal proceedings) on the secret list of the Ministry of the Interior, child pornography websites (LOPPSI 2 law of 2011), or glorifying terrorism (<a href="https://www.laquadrature.net/2014/10/29/pjl-terrorisme-le-parlement-peut-encore-sopposer-a-la-derive-securitaire/">Cazeneuve law of 2014</a>).<br>
These types of blockages are insidious since they do not make the problems go away, nor do they come to the rescue of the victims, nor do they compensate them: violations of the law and personal injury continue...  but in silence, since criminals are not arrested. <br>
Blocking websites is like turning a blind eye to problems, rather than tackling them. Moreover, how could we control the legitimacy of the government's action in the case of the administrative blockage to avoid those issues? One of the first websites blocked in this context, islamic-news, was blocked without any real legal or factual arguments from the government.

* The previous point also highlights inequality between citizens. Judicial blockage can only be applied to ISPs who are subject of a final court decision. As part of the administrative blocking, the secret list is communicated by the Ministry of the Interior to the ISPs selected by the government.<br>
In both cases, it turns out that only the largest commercial and national ISPs in France have to take blocking measures. So what about citizens who use the services of another ISP (university, work, association, etc.)? This means that, without specific skills, online content is different for all French citizens. Is this difference in access to information acceptable?

---
### What is ARN's logging policy for DNS resolver services?
        
We do not save any connection data because we legally do not  have to.

---
### Can I create a DNS tunnel through this resolver?
        
No, we don't want this resolver to generate unusual traffic for a DNS resolver.

---
### How useful is DNSSEC validation?
        
Our resolver cryptographically verifies the validity of a DNS response. However, unless your equipment contains a solution to check this, you are still vulnerable to manipulation between you and the DNS resolver.
In fact, DNSSEC validation on our resolver is only truly effective for <a href="https://www.bortzmeyer.org/ou-valider-dnsssec.html">subscribers who access to a service with an ARN IP address</a>.
    
---
### Why not providing DNS-over-HTTPS?
        
Because we did not have enough time to work on this yet. Do not hesitate to come and help us so that we can set it up :)

---
### How to replicate this service?
Documentation on how to set up our open DNS resolver service is available on <a href="https://wiki.arn-fai.net/benevoles:technique:recursif">the volunteers wiki page</a> (page in French).
           
Before opening a DNS resolver to the public, there are some precautions to take to ensure the DNS will not be used to conduct large-scale DDoS attacks.

See in detail <a href="http://www.guiguishow.info/2014/08/23/comment-mettre-en-place-un-serveur-dns-recursif-cache-ouvert-dans-de-bonnes-conditions/">the considerations for protecting an open DNS resolver</a> (page in French).

---

