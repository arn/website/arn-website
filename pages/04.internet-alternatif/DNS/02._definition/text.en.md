---
title: 'What is an Open DNS Resolver?'
body_classes: modular
size: lg
---

## __What is an *Open DNS Resolver*?__

A DNS resolver is a kind of directory that allows your equipment to transform a domain name into an IP address, for example _wikipedia.org_ into _185.15.58.224_, thus giving you access to the content of the requested site.

Our solver is open and uncensored. To learn more: we organize from time to time a [Network & Magic educational game](https://beta.arn-fai.net/fr/agenda) to learn how the internet works in a fun way, without any digital equipment.
