---
title: 'Qu''est ce qu''un résolveur DNS ?'
body_classes: modular
size: lg
---

## __Qu'est ce qu'un *résolveur DNS* ?__

Un résolveur DNS est une sorte d’annuaire qui permet à vos équipements de transformer un nom de domaine en une adresse IP, par exemple _wikipedia.org_ en _185.15.58.224_, vous offrant ainsi l'accès au contenu du site demandé. 

Notre résolveur est ouvert et non censuré. Pour en savoir plus : nous organisons de temps en temps un [atelier ludique Network & Magic](https://beta.arn-fai.net/fr/agenda) destiné à faire découvrir de façon ludique le fonctionnement d’Internet, sans aucun équipement numérique.
