---
title: 'A quoi sert ce résolveur DNS ?'
body_classes: modular
size: lg
---

## __A quoi sert ce *résolveur DNS* ?__

---
### Éviter le pistage DNS
Ce résolveur DNS **n’enregistre pas** la liste des sites que vous consultez et la loi ne nous y contraint pas car elle s’applique uniquement aux opérations de modification de contenu. 

---
### Rendre service
Ce résolveur DNS est **ouvert**, c'est à dire qu'il peut être utilisé depuis n'importe quelle IP. Cette caractéristique est éssentielle pour le fonctionnement de certains programmes. 

---
### Connaitre LA vérité
Ce résolveur DNS **ne censure aucun site web** : le gouvernement ne nous transmet pas la liste des sites à censurer, nous ne faisons pas de pub avec, ni de contrôle parental ou un quelconque filtrage.

---
