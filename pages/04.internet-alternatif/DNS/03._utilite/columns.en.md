---
title: 'What is the purpose of this DNS Resolver?'
body_classes: modular
size: lg
---

## __What is the purpose of this *DNS Resolver*?__

---
### Avoiding tracking
This DNS resolver **does not record** the list of sites you visit and we are not required by law to do so as it only applies to content modification operations.

---
### Provide a service
This DNS resolver is **open**, which means that it can be used from any IP. This feature is essential for the operation of some programs.

---
### Knowing *THE* truth
This DNS resolver **does not censor any web site**: the government does not send us the list of sites to censor, we do not advertise with it, nor do we use parental controls or any other filtering.

---
