---
title: 'Résolveurs de secours'
body_classes: modular
---
## __Résolveurs de *secours*__

FAI associatif | IPv4 | IPv6
-------------- | ---- | ----
Aquilenet | 185.233.100.100 | 2a0c:e300::100
Aquilenet | 185.233.100.101 | 2a0c:e300::101
FDN | 80.67.169.12 | 2001:910:800::12
FDN | 80.67.169.40 | 2001:910:800::40
