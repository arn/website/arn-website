---
title: 'How to change your DNS resolvers'
body_classes: modular
image_align: left
media_order: 'champs_dns.png,Windows_04.jpg,Windows_03.jpg,Windows_02.jpg,Windows_01.jpg,MacOS_04.jpg,MacOS_03.jpg,MacOS_02.jpg,MacOS_01.jpg,MacOS_05.jpg'
---

## __How to *configure* your DNS resolvers__

Before going any further in the process, you must know if you want to change your DNS resolver for a single computer or for all the equipment connected to the Internet in your home (computers, tablets, smartphones, etc.). In the first case, modifications can be made on your computer according to your operating system. In the second case, it is faster and easier to make modifications on your Internet router (your box).

[ui-tabs position="top-left" active="0" theme="lite"] 
[ui-tab title="(Recommended) On your Box"]

!! Ci-dessous un exemple avec une box d'un BOFS.[^1] Il est fort possible qu'il y ait des différences de procédure avec votre Box, voir même que votre FAI vous empèche de faire la modification.[^2]

### 1. Se rendre sur l'interface de la box
Pour vous connecter, essayer d'afficher les pages web suivantes, jusqu'à ce que l'une d'entre-elles affichent quelques choses:
* [192.168.0.1](http://192.168.0.1)
* [192.168.0.254](http://192.168.0.254)
* [192.168.1.1](http://192.168.1.1)
* [192.168.1.254](http://192.168.1.254)
* [172.16.0.1](http://172.16.0.1)
* [10.0.0.1](http://10.0.0.1)

### 2. Se connecter
En général, si vous ne les avez pas changé, les identifiants se trouvent sur une étiquette sur votre box ou accessible via un petit écran de 5cm.

!!! Astuce : si c'est un vieux routeur, vous pouvez essayer avec `admin` ou `root` en identifiant et `admin` ou `1234` en mot de passe.

### 3. Fouiller l'interface
Il faut ensuite fouiller l'interface pour trouver les champs qui permettent de configurer le résolveur DNS primaire et le secondaire. 

![Capture d'écran du formulaire pour changer les résolveurs primaire et secondaire](champs_dns.png) {.illustration} 
*Capture d'écran de l'interface d'une box Red by SFR  pour changer les résolveurs DNS*

Ci-dessous, quelques chemins qui peuvent vous y mener:

 * Red by SFR (Box: `F@st3686 V1b`): `Réseau` > `Paramètres de base` > `Configuration réseau wan` > `Utiliser l'adresse de serveur DNS suivante`


[/ui-tab][ui-tab title="Ordinateur"]
[ui-tabs position="top-left" active="0" theme="lite"]

[ui-tab title="Linux"]
Le plus simple est de configurer <a href="https://doc.ubuntu-fr.org/dns#par_interface_graphique">Network-manager</a>. Si vous n'utilisez pas Network-manager, vous pouvez configurer dhclient ;
[/ui-tab]

[ui-tab title="Windows"]
* Depuis le menu démarrer, tapez _Réseau_
* Cliquez sur le paramètre _État du réseau_

![Capture d'écran du menu démarrer avec le paramètre Etat du réseau](Windows_01.jpg){.illustration}
* Une fois la fenêtre ouverte, identifiez votre moyen de connexion, ici par câble ethernet, et cliquez sur _Propriétés_
  * Le résolveur DNS est à définir pour chacun de vos moyens de connexion, en Wi-fi, en ethernet, ou carte SIM pour les plus nomades d'entre-vous.

![Capture d'écran du menu paramètres réseau](Windows_02.jpg){.illustration}
* Dans la fenêtre, cliquez sur le bouton _Modifier_ de la section _Paramètres IP_

![Capture d'écran du menu paramètres réseau](Windows_03.jpg){.illustration}

* Puis, copiez-coller l'adresse du résolveur DNS dans le champ _DNS préféré_. Vous pouvez aussi ajouter un second DNS dans le champ _Autre DNS_, permettant d'éviter des soucis d'accès à internet en cas de panne du premier.
  * Vous pouvez effectuer la même opération pour les IPv6, en scrollant au bas de la fenêtre. Attention, Windows n'accepte parfois qu'un seul résolveur DNS en IPv6.

![Capture d'écran du menu paramètres de connexion](Windows_04.jpg){.illustration}
[/ui-tab]

[ui-tab title="Mac OS"]
* Depuis la barre d'application, cliquez sur  _Paramètres systèmes_

![Capture d'écran de la barre d'applications](MacOS_01.jpg){.illustration}

* Une fois la fenêtre ouverte, cliquez sur _Réseau_

![Capture d'écran de la fenêtre de paramètres](MacOS_02.jpg){.illustration}

* Dans la fenêtre _Réseau_, cliquez sur _Avancé..._

![Capture d'écran de la fenêtre de paramètres](MacOS_03.jpg){.illustration}

* Pour ajoutez des résolveurs DNS, cliquez sur le bouton _+_

![Capture d'écran de la fenêtre de paramètres](MacOS_04.jpg){.illustration}

* Puis saisissez l'adresse du résolveur avant de valider en cliquant sur le bouton _OK_

![Capture d'écran de la fenêtre de paramètres](MacOS_05.jpg){.illustration}
[/ui-tab]

[/ui-tabs]
[/ui-tab][ui-tab title="Smartphone / Tablette"]
[ui-tabs position="top-left" active="0" theme="lite"]

[ui-tab title="Android"]
**/!\ Cette manipulation ne fonctionne que pour les réseaux Wi-fi et doit être effectuée pour chacun des réseaux. Alsace Réseau Neutre ne propose pas encore un résolveur DNS capable de s'appliquer à tout le système /!\**
* Ouvrez l'applications _Paramètres_ de votre téléphone
* Appuyez sur le paramètre _Réseau et internet_

![Capture d'écran de la fenêtre de paramètres](Android_01.jpg){.illustration}

* Appuyez sur le paramètre _Wi-fi_

![Capture d'écran de la fenêtre de paramètres](Android_02.jpg){.illustration}

* Sélectionnez les options du réseau Wi-fi concerné

![Capture d'écran de la fenêtre de paramètres](Android_03.jpg){.illustration}

* Puis appuyez sur le bouton _modifier_

![Capture d'écran de la fenêtre de paramètres](Android_04.jpg){.illustration}

* Afficher les paramètres avancés.
* Sous le paramètre _Paramètres IP_, passez le paramètre en _Statiques_.
* Puis, modifier les valeurs DNS maintenant disponibles plus bas.

![Capture d'écran de la fenêtre de paramètres](Android_05.jpg){.illustration}

[/ui-tab]

[ui-tab title="iOS"]
<a href="https://support.hidemyass.com/hc/fr/articles/202720776-Comment-changer-des-param%C3%A8tres-DNS-sur-votre-Windows-Mac-Android-iOS-ou-Linux#tabs-4">iPhone or iPad</a>. <b>Attention</b>: the change applies only for the currently used Wi-Fi access point. It does not apply to all Wi-Fi access points nor to mobile data Internet (3G/4G) ;</li>
[/ui-tab]

[/ui-tabs]
[/ui-tab]
[/ui-tabs]
        
!!! To verify that your change is effective, you can use the <a href="https://ipleak.net/">IPLeak</a> website. « 89.234.141.66 » needs to be filled below the « DNS Address detection » mention.


[^1]: Bouygues, Orange, Free, SFR
[^2]: <a href="https://communaute.orange.fr/t5/prot%C3%A9ger-mes-donn%C3%A9es-et-mon/OpenDNS-et-livebox/td-p/342585">OpenDNS and livebox</a>
