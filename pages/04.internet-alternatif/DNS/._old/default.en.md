---
title: DNS resolver
body_classes: natta
visible: true
published: true
login: {  }
content:
    items: '@self.modular'
routable: true
page-toc:
  active: false
---

<div class="logo-natta">
        <img src="../../images/NATTA_Titre.svg" alt="Natta alternative internet access" class="logo-natta-img"/>
</div>
<div class="natta-title-image">
            <h1>Our free and open<span class="orange"> DNS Resolver </span></h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <p class="dns_intro_bold">recursif.arn-fai.net<br>
               89.234.141.66<br>
               2a00:5881:8100:1000::3<br>
               UDP and TCP, port 53 (standard), DNSSEC validation<br>
               DoH coming soon
            <p>

</div>

<div class="natta-title-image">
            <h2>What is an<span class="orange"> Open DNS Resolver?</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <p>
            A <strong>DNS resolver</strong> is like a directory that allows your equipment to transform a domain name into an IP address (for example, "wikipedia.org" into "185.15.58.224"), which corresponds to the device location. Indeed, on the Internet, each device has a unique IP address that can be used by other devices to establish communication with it.
            <br>
            A DNS resolver is "open" if there is <strong>no limitation on the origin of the request</strong> by which it can be queried.
            <br><br>
            If you want to learn more about DNS resolvers, <a href="../../../agenda">we organize an educational game</a> called Network & Magic (accessible without a computer) to learn how the internet works in a fun way.
            </p>
</div>
<div class="natta-title-image">
            <h2>What is a 
            <span class="orange"> DNS resolver</span> used for?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <h3>Goodbye lying resolvers</h3>
            <p>
            By default, it is common to have a "lying" DNS resolver  (via DHCP). In other words, it consists in a resolver that returns an IP that differs from the one associated with the domain name. This may be the case for (non-exhaustive list):
            <ul>
                <li>parental control system</li>
                <li>commercial ISPs that return an IP with ads or a search engine when you use the wrong domain name. Even if that can create bugs with some software</li>
                <li>ISPs who legally have to censor some websites. Since December 2014, France has set up a secret list of censored sites. 
                </li>
            </ul>
            ARN's DNS resolver is not "lying" and does not involve any control system, advertising or censorship.
            </p>
            <h3>Avoid data tracking</h3>
            <p>
            A DNS resolver stocks the names of all the sites visited by an IP address. Therefore, it is the best way to track the navigation data of a person or the entire population. For instance, this is how the <a href="https://developers.google.com/speed/public-dns/privacy">Google resolver collects your personal information</a>.
            Our DNS resolver does not track logs, as we are not legally obliged to do so (it only applies to content modification operations).
            </p>          
</div>
<div class="natta-title-image">
            <h2>How to change your <span class="orange">DNS resolvers</span>?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <p>
           Before going any further in the process, you must know if you want to change your DNS resolver for a single computer or for all the equipment connected to the Internet in your home (computers, tablets, smartphones, etc.). In the first case, modifications can be made on your computer according to your operating system. In the second case, it is faster and easier to make modifications on your Internet router.
            </p>
        <ul>
            <li>Router: it depends on your router and <a href="https://communaute.orange.fr/t5/prot%C3%A9ger-mes-donn%C3%A9es-et-mon/OpenDNS-et-livebox/td-p/342585">whether the change is authorized</a> (page in French);</li>
            <li>GNU/Linux: the easiest way is to configure <a href="https://doc.ubuntu-fr.org/dns#par_interface_graphique">Network-manager</a> (page in French). If you do not use Network-manager, you can configure dhclient;</li>
            <li><a href="https://forums.cnetfrance.fr/tutoriels-reseaux-et-internet/252583-comment-changer-ses-dns-manuellement-windows-mac-ios-android">Windows</a> (page in French);</li>
            <li><a href="https://support.apple.com/kb/PH6373?locale=en_US&viewlocale=fr_FR">Mac OS X</a> ;</li>
            <li><a href="https://support.hidemyass.com/hc/fr/articles/202720776-Comment-changer-des-param%C3%A8tres-DNS-sur-votre-Windows-Mac-Android-iOS-ou-Linux#tabs-4">iPhone or iPad</a> (page in French). <b>Short notice</b>: modifications only apply to the current WiFi access point. They do not apply to all WiFi access points or mobile Internet (3G/4G);</li>
            <li>Android phone or tablet: there does not seem to be an easy, sustainable and free software way to change the recursive DNS.</li>
        </ul>
        <p>
        <b>Warning</b>: some tutorials advise using Google or OpenDNS DNS resolvers. The first one is a moot topic... because Google's business model is based on the massive exploitation of our personal data. The second is a "lying" DNS resolver. We advise you to only use their tutorials on how to change recursive/DNS resolver.<br>
        To check if your modification is effective, you can use <a href="https://ipleak.net/">IPLeak</a> website. “89.234.141.66” must be indicated below the mention “DNS Address detection”.
        </p>
</div>

<div class="natta-title-image">
            <h2><span class="orange">Emergency</span> resolvers</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
           <table>
                <tr>
                    <th>Associative ISP</th>
                    <th>IPv4</th>
                    <th>IPv6</th>
                </tr>
                <tr>
                    <th>Aquilenet</th>
                    <td>185.233.100.100</td>
                    <td>2a0c:e300::100</td>
                </tr>
                 <tr>
                    <th>Aquilenet</th>
                    <td>185.233.100.101</td>
                    <td>2a0c:e300::101</td>
                </tr>
                 <tr>
                    <th>FDN</th>
                    <td>80.67.169.12</td>
                    <td>2001:910:800::12</td>
                </tr>
                 <tr>
                    <th>FDN</th>
                    <td>80.67.169.40</td>
                    <td>2001:910:800::40</td>
                </tr>
           </table>
</div>

<div class="natta-title-image">
            <h2>Frequently asked <span class="orange"> questions</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="natta-question-container">
    <div class="natta-question-box">
        <h3>Are there concrete cases of what ARN considers as abusive uses?</h3>
        <p>
        Yes: 
        </p>
        <ul>
            <li>French commercial ISPs have all been tempted to send the user advertisements when a website does not exist (example: you have typed "arnfai.net" instead of "arn- fai.net" in the address bar of your browser). Some have done it like SFR or Alice. <br>
            <a href="https://www.opendns.com/">Other DNS resolvers "lie" for security reasons</a> (to prevent access to a site that contains viruses or to automatically correct your typos) which are not necessarily legitimate (individual's autonomy, error in assessing the quality of a website, etc.).
            </li>
            <li>
            Judicial blocking is relying on the DNS for gambling websites that do not pay their business licence (law 2010-476 of 2010) or any other website (examples: t411 and Copwatch)... But also all the administrative blocking (outside legal proceedings) on the secret list of the Ministry of the Interior, child pornography websites (LOPPSI 2 law of 2011), or glorifying terrorism (Cazeneuve law of 2014).<br><br>
            These types of blockages are insidious since they do not make the problems go away, nor do they come to the rescue of the victims, nor do they compensate them: violations of the law and personal injury continue...  but in silence, since criminals are not arrested. <br>
            Blocking websites is like turning a blind eye to problems, rather than tackling them. Moreover, how could we control the legitimacy of the government's action in the case of the administrative blockage to avoid those issues? One of the first websites blocked in this context, islamic-news, was blocked without any real legal or factual arguments from the government.
            </li>
            <li>
            The previous point also highlights inequality between citizens. Judicial blockage can only be applied to ISPs who are subject of a final court decision. As part of the administrative blocking, the secret list is communicated by the Ministry of the Interior to the ISPs selected by the government. <br><br>
            In both cases, it turns out that only the largest commercial and national ISPs in France have to take blocking measures. So what about citizens who use the services of another ISP (university, work, association, etc.)? This means that, without specific skills, online content is different for all French citizens. Is this difference in access to information acceptable? 
             </li>
        </ul>
    </div>
    <div class="natta-question-box">
        <h3>What is ARN's logging policy for DNS resolver services?</h3>
        <p>
            We do not save any connection data because we legally do not  have to.
        </p>
    </div><div class="natta-question-box">
        <h3>Can I create a DNS tunnel through this resolver?</h3>
        <p>
            No, we don't want this resolver to generate unusual traffic for a DNS resolver.
        </p>
    </div><div class="natta-question-box">
        <h3>How useful DNSSEC validation is?</h3>
        <p>
            Our resolver cryptographically verifies the validity of a DNS response. However, unless your equipment contains a solution to check this, you are still vulnerable to manipulation between you and the DNS resolver.
            In fact, DNSSEC validation on our resolver is trully effective for <a href="https://www.bortzmeyer.org/ou-valider-dnsssec.html">subscribers who access to a service with an ARN IP address</a>.
        </p>
    </div><div class="natta-question-box">
        <h3>Why not providing DNS-over-HTTPS?</h3>
        <p>
         Because we did not have enough time to work on this yet. Do not hesitate to come and help us so that we can set it up :)
         </p>
    </div><div class="natta-question-box">
        <h3>How to replicate this service?</h3>
        <p>
           Documentation on how to set up our open DNS resolver service is available on the volunteers <a href="https://wiki.arn-fai.net/benevoles:technique:recursif">wiki page</a> (page in French).
           <br>
           Before opening a DNS resolver to the public, there are some precautions to take to ensure the DNS will not be used to conduct large-scale DDoS attacks. See in detail the <a href="http://www.guiguishow.info/2014/08/23/comment-mettre-en-place-un-serveur-dns-recursif-cache-ouvert-dans-de-bonnes-conditions/">considerations for protecting an open DNS resolver</a> (page in French).
        </p>
</div>
<br><br>
<div class="natta-help">
            <p class="aidez_nous">Did you find this helpful? You can help us too :)</p>
            <p><b>Come to our next volunteer meeting</b></p>
</div>
