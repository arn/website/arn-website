---
title: Résolveur DNS
body_classes: natta
visible: true
published: true
login: {  }
content:
    items: '@self.modular'
routable: true
page-toc:
    active: false
---

<div class="natta-title-image">
            <h2>Comment changer de <span class="orange">résolveur DNS</span> ?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <p>
            Avant d'aller plus loin, il faut définir si les changements concerneront un seul ordinateur ou bien tous vos équipements connectés à Internet (tous les ordinateurs, tablettes, smartphones, etc.). Dans le premier cas, la manipulation est à effectuer sur l'ordinateur en question et varie en fonction de votre système d'exploitation. Dans le second cas, il est plus rapide et plus simple d'effectuer le changement sur votre Box Internet.
            </p>
        <ul>
            <li>Box : cela dépend de votre box et de si ce changement est encore autorisé ou <a href="https://communaute.orange.fr/t5/prot%C3%A9ger-mes-donn%C3%A9es-et-mon/OpenDNS-et-livebox/td-p/342585">non</a> ;</li>
            <li>GNU/Linux : le plus simple est de configurer <a href="https://doc.ubuntu-fr.org/dns#par_interface_graphique">Network-manager</a>. Si vous n'utilisez pas Network-manager, vous pouvez configurer dhclient ;</li>
            <li><a href="https://forums.cnetfrance.fr/tutoriels-reseaux-et-internet/252583-comment-changer-ses-dns-manuellement-windows-mac-ios-android">Windows</a> ;</li>
            <li><a href="https://support.apple.com/kb/PH6373?locale=en_US&viewlocale=fr_FR">Mac OS X</a> ;</li>
            <li><a href="https://support.hidemyass.com/hc/fr/articles/202720776-Comment-changer-des-param%C3%A8tres-DNS-sur-votre-Windows-Mac-Android-iOS-ou-Linux#tabs-4">iPhone ou iPad</a>. <b>Attention</b> : la modification s'applique uniquement au point d'accès WiFi en cours. Elle ne s'applique ni à l'ensemble des points d'accès WiFi ni à l'Internet mobile (3G/4G) ;</li>
            <li>Téléphone ou tablette Android : il ne semble pas exister de manière facile, durable et en logiciel libre pour changer le récursive DNS utilisé.</li>
        </ul>
        <p>
        <b>Attention</b> : certains de ces tutoriels conseillent d'utiliser les récursifs/résolveurs DNS de Google ou d'OpenDNS. Le premier est discutable quant au respect de la vie privée, car le modèle économique de Google est basé sur l'exploitation massive de nos données personnelles, et le second est menteur. Nous vous dirigeons vers eux uniquement pour leurs explications sur la démarche à suivre pour changer de récursif/résolveur DNS.<br>
        Pour vérifier que votre modification est effective, vous pouvez utiliser le site web <a href="https://ipleak.net/">IPLeak</a>. « 89.234.141.66 » doit être indiqué en dessous de la mention « DNS Address detection ».
        </p>
</div>

