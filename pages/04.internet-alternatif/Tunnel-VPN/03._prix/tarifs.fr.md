---
title: 'Nos offres'
body_classes: modular
---

## __Combien *ça coûte* ?__
---
### Tunnel VPN

__*4€* / mois__

1 IPv4 publique<br>
1 préfixe IPv6 publique<br>
Reverse DNS personnalisable<br>
Compatible PC et smartphone<br>
Mono-équipement<br>
Jusqu'à 80 Mbps symétrique<br>
Technologie OpenVPN<br>

---
### Brique Internet

__*4€* / mois + 70€__

1 IPv4 publique<br>
1 préfixe IPv6 publique<br>
Reverse DNS personnalisable<br>
Plug&Play avec hotspot wifi<br>
Multi-équipements<br>
Jusqu'à 40 Mbps symétrique<br>
Technologie OpenVPN<br><br>

<a href="/hébergement-alternatif/brique-internet" role="button">En savoir plus</a>

---
*Prix en franchise de TVA (art 293B du CGI), c'est à dire TTC. Sans engagement. L’accès à ces services requiert une [adhésion à l’association](/asso/adhérer). Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction. Abonnement payable en Ğ1 (4DU/mois).*


