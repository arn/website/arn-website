---
title: 'Our Deals'
body_classes: modular
---

## __Our pricing__
---
### VPN Tunnel

__*€4* / month__

1 public IPv4<br>
1 public IPv6 prefix<br>
Customizable reverse DNS (IPv4 & IPv6)<br>
Compatible with PC and smartphone<br>
Single-equipment<br>
Up to symmetric 80 Mbps<br>
OpenVPN technology

---
### Internet Cube

__*€4* / month + €60__

1 public IPv4<br>
1 public IPv6 prefix<br>
Customizable reverse DNS (IPv4 & IPv6)<br>
Compatible with PC and smartphone<br>
Multi-equipment<br>
Up to symmetric 40 Mbps<br>
OpenVPN technology<br><br>

<a href="/hébergement-alternatif/brique-internet" role="button">Know more</a>

---
*Tax free price (art 293B of the CGI). No commitment. Access to these services requires a [membership to the association](/asso/adhérer). Renewal of subscription and annual membership by tacit agreement. Subscription payable in Ğ1 (4DU/month).*



