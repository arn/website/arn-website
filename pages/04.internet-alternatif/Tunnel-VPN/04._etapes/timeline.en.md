---
title: 'How to subscribe'
body_classes: modular
---

## __How to subscribe__

---
### 1. Create an account in the member space and register for the service

Most of our services are reserved for the members of the association. You have to join ARN to use our VPN service. Create an account on the [member space](https://adherents.arn-fai.net/members/register/), then " [Request a new subscription](https://adherents.arn-fai.net/members/login/?next=/members/request_subscriptions/step1) ".

---
### 2. Validation and payment

The volunteer team verifies that there are sufficient resources left to provision the service, validates your request and starts the service.

---
### 3. Access your service and payment

Once your service is set up, you can access information and documentation about it on the " [My subscriptions](https://adherents.arn-fai.net/members/subscriptions/) ". Please proceed to the payment of the membership if you have not already done so, and to the setting up of a standing order, or else to pay several months in advance in one go.

---

