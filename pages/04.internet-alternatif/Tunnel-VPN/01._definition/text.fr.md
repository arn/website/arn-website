---
title: 'Tunnel VPN'
body_classes: modular
image_align: left
---

!! Nos **VPN sont actuellement sur liste d'attente**, car notre plage d'adresses IP consécutives est pleine. 
!!
!! Il faut probablement un délai de **quelques semaines à plus d'un mois** pour qu'une addresse se libère.
!!
!! Les **VPN payables en Ğ1 sont en rupture de stock** jusqu'à effacement de ce message.

## __Qu'est-ce qu'un *tunnel VPN* ?__

Un **tunnel de réseau virtuel privé (VPN)** permet de relier virtuellement votre ordinateur à un autre réseau. Dans le cas des tunnels proposés par ARN, c’est un peu comme si votre équipement était relié avec un câble branché directement dans la baie serveur de notre association.

Les sites web que vous consultez, les mails que vous recevez, les messages que vous envoyez, les recherches que vous effectuez… En bref : toutes vos activités en ligne seront vues comme émanant  des **serveurs d'ARN**.