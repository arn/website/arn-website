---
title: 'VPN Tunnel'
body_classes: modular
---

![Natta](/images/logos/poles/natta_title.svg) {.logo-categorie}

## __What is a *VPN tunnel*?__
A **Virtual Private Network (VPN) tunnel** virtually connects your computer to another network. Regarding ARN's VPN tunnels, it looks as if your equipment was connected with a cable plugged directly into the server cabinet of our association.

The websites you visit, the emails you receive, the messages you send, and basically all your online activities, will be seen as coming out of **ARN's servers**.

