---
title: 'Quand utiliser ces VPN ?'
body_classes: modular
size: lg
---

## __Quand utiliser ces *VPN* ?__

!!! Les VPN ARN ne sont pas une solution d’anonymat ou de géolocalisation en dehors de la France. Pour cela, nous vous conseillons de regarder plutôt du côté de [Tor Browser](https://www.torproject.org/fr/).

---
### Sécuriser sa connexion

Pour **éviter le « Sniffing Wi-Fi »**, c'est-à -dire la mise en écoute de sa connexion par son Fournisseur d'accès à Internet (FAI), mais aussi pour se protéger sur les wifis publics.

---
### S’auto-héberger

Ces VPN sont pensés pour <strong>l’auto-hébergement</strong> (y compris des mails) et sont compatibles avec YunoHost et les Briques Internet.

---
### Débrider sa connexion

Empêcher les atteintes à la Neutralité du Net par son FAI, retour du bon vieil internet aimé…

---
### Se géolocaliser en France

Consulter depuis l'étranger les contenus accessibles uniquement en France en utilisant notre point de sortie à Schiltigheim.

---
### Accéder à l’IPv6

Les VPN ARN sont **compatibles IPv6**, même si votre réseau initial ne supporte qu’IPv4.

---