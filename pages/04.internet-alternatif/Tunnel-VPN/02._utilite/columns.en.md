---
title: Eligibility
body_classes: modular
size: lg
---

## __When should you use a *VPN*?__

!!! Short notice : ARN VPNs are not a solution for anonymity or geolocation outside of France. For this, take a look at [Tor Browser](https://www.torproject.org/fr/) instead.

---

### Secure your connection
To **avoid "Wi-Fi sniffing"**,  i.e. the tapping of your connection by your Internet Service Provider (ISP), and also to protect your data when you use a public Wi-Fi.

---

### Self-hosting

These VPNs are designed for **self-hosting** (including emails) and are compatible with YunoHost and Internet bricks.

---

### Unbridle your connection

Prevent Net Neutrality violations by your ISP, return of the beloved good old Internet…

---

### Geolocate in France

Access contents available only in France from anywhere in the world, by using our connection based in Schiltigheim.

---

### Access to IPv6

ARN VPNs are **IPv6 compatible**, even if your own network only supports IPv4.

---
