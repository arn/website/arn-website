---
title: 'Frequently Asked Questions'
body_classes: modular
---

## __Frequently asked  *questions*__

---
### Why do I need to join?

At Alsace Réseau Neutre, a non-profit association without employees, you are not a customer of a provider. You are a member of a collective that is organizing to build a utopian piece of the internet and influence digital policies towards more virtuous schemes. Memberships allow us to offer these services and to finance them.

Moreover, we wish that through the general assembly, the subscribers have a right to vote concerning the future of their services.

---
### What does "VAT free prices" mean?

The VAT exemption means that ARN is not subject to VAT at this time. The amount "before tax" is therefore equal to the amount "including tax".

---
### Is there a commitment?

No, but the services are renewed tacitly for a period of 30 days every 1st of the month.

If you don't use the service anymore, don't forget to save your data and to cancel the subscription.

---
### How do I pay?

Every 1st of the month, an invoice is issued and you are expected to pay it within 15 days.

If you wish, you can pay up to 12 months in advance in one go.

Payment is to be made by bank transfer. The bank references can be found in the member area. **Don't forget to indicate your "ID" + the ID number that has been assigned to you.**

If you have no choice, we accept cash payments to be sent to the people in charge of the ARN treasury.

---
### What is Ğ1?

The Ğ1 (pronounced "june") is a free currency the association accepts to pay for your subscription. 

---
### Where is the datacenter that hosts ARN's servers?

It is the Cogent datacenter, located 46 route de Bischwiller in Schiltigheim, Alsace, France.

---
### What is ARN's logging policy for VPN services?

We log:
* your identity
* the ARN IPs you are assigned to
* the allocation period

Please read the "[Misuse of Services](/asso/cgs)" CGU clause. In the case of an investigation, we may be legally obliged to allow a wiretap of your VPN.

---
### How to get help in case of problems?
Help can be obtained by contacting us on the contact form, the forum, the support chat and even in person at the AUBE office. It is imperative that you take the time to describe your problem.

ARN volunteers will try to solve your problem by making a diagnosis and potentially accompanying you by email or chat (exceptionally by phone). 

---
### Can I host my own mail server on this connection?
Yes. Since the connection is neutral, ports are not restricted (even port 25) and you can define a reverse DNS. Moreover, we do not list our IPs as residential, unlike many ISPs.

Be careful not to tarnish the reputation of the IPs.

---
### Can my IP change? Is it shared?

IPs are fixed and we do not plan to share port ranges in order to encourage self-hosting.

---
### Is IPv6 available?

Yes.

---
### Can this VPN work on limited networks?

It is possible to connect to any port, including port 443 (in UDP or TCP), which allows you to configure  the VPN for various networks.
However, it is likely that ARN's VPN does not work in some countries, such as China. You can try it (without putting yourself at risk), do not hesitate to give us feedback.

Il est possible de se connecter sur n’importe quel port, y compris le 443 (en UDP ou TCP). Ce qui permet bien souvent de configurer le VPN dans de nombreux réseaux. Toutefois, il est probable que le VPN ARN ne fonctionne pas dans certains pays tels que la Chine. À tester (sans vous mettre en danger), n’hésitez pas à nous faire des retours.

---
### How many devices can I connect to the VPN?

In IPv4, it is only possible to connect one device at a time, unless you use a Neutribox and a wifi connection. In "IPv6 only", it is possible to connect several devices, but "IPv6 only" browsing is very limited, hence most websites and applications will not work.

---
### What is a Neutribox?

The Neutribox ARN is a box with a Wifi antenna you can link to the connection box with a wire. The Neutribox generates a wifi hotspot. Communications will pass through the VPN between the Neutribox and ARN's VPN server.
To create this Neutribox, we are currently using an orange pi pc+ card, an antenna, and YunoHost.

---
### Which operating systems are supported?

Almost every recent OS is supported: Linux, Windows, Mac OS, BSD, Android and free alternatives, and iOS.

---
### What is the authentication type?
Authentication is to be done with a private/public key pair provided in the member space. N.-B.: we do not provide CSR.

---
### How to replicate this service?

Some helpful documentation :      

* Get public IPs
* Route the IPs to a server
* Set up a DNS authority
* Set up the VPN server

Did you find this helpful? You can help us too :)

---

