---
title: 'Foire aux questions'
body_classes: modular
---

## __Foire aux *questions*__

---
### Pourquoi dois-je adhérer ?

Chez Alsace Réseau Neutre, association à but non lucratif sans salariés, vous n’êtes pas client⋅es d’un fournisseur. Vous êtes membre d’un collectif qui s’organise pour construire un bout d’internet utopique et influencer les politiques numériques vers des schémas plus vertueux. Les adhésions nous permettent de proposer ces services et de les financer.

Par ailleurs, nous souhaitons qu’au travers de l’assemblée générale, les personnes abonnées aient un droit de vote concernant l’avenir de leurs services.

---
### Que signifie « Prix en franchise de TVA » ?

La franchise de TVA signifie qu’à ce jour ARN n’est pas soumis à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».

---

### Y-a-t’il un engagement ?

Non, en revanche les services sont renouvelés tacitement pour une durée de 30 jours chaque 1er du mois.

Si vous ne vous servez plus du service, n’oubliez pas de sauvegarder vos données et de résilier l’abonnement.

---

### Comment payer ?

Tous les 1er du mois, une facture est éditée et il est attendu que vous la régliez sous 15 jours.

Si vous le souhaitez vous avez la possibilité de régler jusqu'à 12 mois d'avance en une seule fois.

Le règlement est à effectuer par virement bancaire. Les références bancaires se trouvent dans l'espace membre. **N'oubliez pas d'indiquer votre identifiant « ID » + le numéro d'identifiant qui vous a été attribué.**

Si vous n'avez pas le choix, nous acceptons les paiements en liquide à transmettre aux personnes en charges de la trésorerie d'ARN.

---
### Qu’est-ce que la Ğ1 ?

La Ğ1 (prononcée “ june”) est une monnaie libre que l’association accepte pour le règlement de votre abonnement

---

### Où se trouve le datacenter qui héberge les serveurs d'ARN ?

Il s'agit du datacenter Cogent, situé 46 route de Bischwiller à Schiltigheim, Alsace, France.

---
### Quelle est la politique de sauvegarde des données de connexions (logs) d’ARN pour ce service ?

Nous enregistrons :

 * votre identité
 * les IPs ARN qui vous sont attribuées
 * la période d’attribution

Toutefois, nous vous invitons à lire la clause « [Mésusage des services](/asso/cgs) » de nos CGS. Notez que dans le cadre d’une enquête, nous pourrions être contraints légalement de permettre une mise sur écoute de votre VPN.

---
### Comment obtenir de l'aide en cas de problème ?
L'aide peut être obtenue en nous contactant sur le formulaire de contact, le forum, le chat d'entraide et même en présentiel lors des permanences du vendredi soir au hackstub. Il est impératif que vous preniez le temps de décrire votre problème.

Les bénévoles d'ARN essaierons de résoudre votre problème en faisant un diagnostique et potentiellement en vous accompagnant par mail ou chat (exceptionnellement téléphone). 


---
### Puis-je héberger mon propre serveur mail sur cette connexion ?
Oui. La connexion étant neutre, les ports ne sont pas bridés (même le port 25) et vous pouvez définir un reverse DNS. En plus, nous ne listons pas nos IPs comme résidentiels, contrairement à de nombreux FAI.

Attention tout de même à ne pas ternir la réputation des IPs.

---
### Mon IP peut-elle changer ? Est-elle partagée ?
Les IPs sont fixes et nous ne prévoyons pas de partager les plages de ports afin de favoriser la pratique de l'auto-hébergement.

---
### L'IPv6 est-elle disponible ?
Oui.

---
### Ce VPN peut-il fonctionner sur des réseaux restreints ?

Il est possible de se connecter sur n’importe quel port, y compris le 443 (en UDP ou TCP). Ce qui permet bien souvent de configurer le VPN dans de nombreux réseaux. Toutefois, il est probable que le VPN ARN ne fonctionne pas dans certains pays tels que la Chine. À tester (sans vous mettre en danger), n’hésitez pas à nous faire des retours.
Combien d’équipements puis-je connecter au VPN ?

En IPv4, il est possible de connecter un seul équipement à la fois, à moins d’utiliser une Neutribox et de s’y connecter en wifi. En revanche, il est possible de connecter plusieurs appareils en « IPv6 only ». Mais la navigation « IPv6 only » étant très limitée, la plupart des sites et applications ne fonctionneront pas.

---
### Qu’est-ce qu’une Neutribox ?

La Neutribox ARN est un boîtier doté d’une antenne Wifi à brancher sur la box en filaire. La Neutribox génère un hotspot wifi dont les communications passeront au travers du VPN entre la Neutribox et le serveur VPN d’ARN. Actuellement, nous utilisons une carte orange pi pc+, une antenne et YunoHost pour fabriquer la Neutribox.

---
### Quels sont les OS supportés ?

Presque tous les SE dans leur version récente sont supportés : Linux, Windows, Mac OS, BSD, Android et dérivés libres, iOS.

---
### Quel est le type d’authentification ?

Authentification par clé privée/clé publique fournie via l’espace membre. NB : nous ne proposons pas de mécanisme de CSR.



---
### Comment reproduire ce service ?
 Quelques documentation utiles :

 * obtenir des IPs publiques
 * être en mesure de les router sur un serveur
 * mettre en place une autorité DNS
 * configurer le serveur VPN

---

