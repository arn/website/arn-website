---
title: Tunnel VPN
body_classes: natta
visible: true
published: true
login: {  }
content:
    items: '@self.modular'
routable: true
page-toc:
  active: false
---

<div class="logo-natta">
        <img src="../../images/NATTA_Titre.svg" alt="Natta alternative internet access" class="logo-natta-img"/>
</div>
<div class="natta-title-image">
            <h1>What is a <span class="orange">VPN tunnel</span>?</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <p>
            A <strong>Virtual Private Network (VPN) tunnel </strong>  virtually connects your computer to another network. Regarding ARN's VPN tunnels, it is as if your equipment was connected with a cable plugged directly into our association's server cabinet. <br>
            The websites you visit, the emails you receive, the messages you send, and basically all your online activities, will be seen as coming from   <strong>ARN's servers</strong>. 
        </p>
</div>
<div class="natta-title-image">
            <h2>When should you use a <span class="orange">VPN</span>?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <p>
            Short notice : ARN VPNs are not a solution for anonymity or geolocation outside of France. For this, take alook at [Tor Browser] instead.
            </p>
            <h3>Secure your connection</h3>
            <p>
            To <strong>avoid " Wi-Fi sniffing "</strong>,  i.e. the tapping of your connection by your Internet Service Provider (ISP), but also to protect your data when you use a public Wi-Fi.
            </p>
            <h3>Self-hosting</h3>
            <p>
            These VPNs are designed for <strong>self-hosting</strong> (including emails) and are compatible with YunoHost and Internet bricks.
            </p>
            <h3>Unbridle your connection</h3>
            <p>
            Prevent Net Neutrality violations by your ISP, return of the beloved old Internet...
            </p>
            <h3>Geolocate in France</h3>
            <p>
            Consult the contents accessible only in France from anywhere in the world, by using our connection based in Schiltigheim.
            </p>
            <h3>Access to IPv6</h3>
            <p>
            ARN VPNs are <strong>IPv6 compatible</strong>, even if your initial network only supports IPv4.
            </p>
</div>
<div class="natta-title-image">
            <h2>Our <span class="orange"> deals</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-vpn">
            <h3 class="offre-title">VPN Tunnel</h3>
            <div class="offre-para">
                <p class="offre-title">€4/month + membership</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p>
                1 public IPv4<br>
                1 public IPv6 prefix<br>
                Reverse DNS and customizable IPv4&v6<br>
                compatible with PC and smartphone<br>
                Single equipement<br>
                Up to symmetric 80 Mbps<br>
                OpenVPN technology
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
        <div class="offre-box offre-vpn">
            <h3 class="offre-title">Neutribox</h3>
            <div class="offre-para">
                <p class="offre-title">€4/month + €60 + membership</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p>
                1 public IPv4<br>
                1 public IPv6 prefix<br>
                Reverse DNS, customizable IPv4&v6<br>
                Plug & Play with wifi hotspot<br>
                Multi-equipement<br>
                Up to 40 Mbps symmetric<br>
                OpenVPN technology
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
</div>
<p>
Tax free price (art 293B of the CGI) and not time-limited subscription. Subscription renewal and annual membership renewal is done automatically. Subscriptions can be paid in G1 (4DU/month).
</p>
<div class="natta-title-image">
            <h2>How to <span class="orange">subscribe</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="timeline">
        <div class="timeline-back-natta"></div>
            <div class="timeline-item">
                    <h3>1. Join the association</h3>
                    <p>
                    Most of our services are reserved for the <strong>members of the association</strong>. You have to join ARN to use our VPN service.
                    </p>
                </div>
                <div class="timeline-item">
                    <h3>2. Fill in the application form</h3>
                    <p>
                    Once your membership is validated, you can request a VPN access by answering the questions in our dedicated form.
                    </p>
                </div>
                <div class="timeline-item">
                     <h3>3. Validation and payment</h3>
                    <p>
                    Our team of volunteers will validate your subscription. Make sure to set up a standing order or cover several months in advance.
                    </p>
                </div>
                <div class="timeline-item">
                    <h3>4. Configure your service with our tutorials</h3>
                    <p>
                    Once your VPN is open, you can access its information in the member interface and configure it by using <a href="../../educpop">our tutorials</a>.
                    </p>
                </div>
</div>

<div class="natta-title-image">
            <h2>Frequently asked <span class="orange"> questions</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="natta-question-container">
    <div class="natta-question-box">
        <h3>Can this VPN work on limited networks?</h3>
        <p>
           It is possible to connect to any port, including port 443 (in UDP or TCP), which allows you to configure  the VPN for various networks.
            However, it is likely that ARN's VPN does not work in some countries, such as China. You can try it (without putting yourself at risk), do not hesitate to give us feedback.
        </p>
    </div>
    <div class="natta-question-box">
        <h3>How many devices can I connect to the VPN?</h3>
        <p>
            In IPv4, it is only possible to connect one device at a time, unless you use a Neutribox and a wifi connection.
            In "IPv6 only", it is possible to connect several devices, but "IPv6 only" browsing is very limited, hence most websites and applications will not work.
        </p>
    </div><div class="natta-question-box">
        <h3>What is a Neutribox?</h3>
        <p>
            The Neutribox ARN is a box with a Wifi antenna you can link to the connection box with a wire. The Neutribox generates a wifi hotspot. Communications will pass through the VPN between the Neutribox and the ARN's VPN server.
            To create this Neutribox, we are currently using an orange pi pc+ card, an antenna, and YunoHost.
        </p>
    </div><div class="natta-question-box">
        <h3>What is Ğ1?</h3>
        <p>
            The Ğ1 (pronounced "june") is a free currency the association accepts to pay for its subscription. 
        </p>
    </div><div class="natta-question-box">
        <h3>What is ARN's logging policy for VPN services?</h3>
        <p>
            We log:
        </p>
        <ul>
            <li>your identity</li>
            <li>the ARN IPs you are assigned to</li>
            <li>the allocation period</li>
        </ul>
        <p>
            Please read the "Misuse of Services" CGU clause. In the case of an investigation, we may be legally obliged to allow a wiretap of your VPN.
        </p>
    </div>
    <div class="natta-question-box">
        <h3>Which operating system  is supported?</h3>
        <p>
            Almost every recent OS is supported: Linux, Windows, Mac OS, BSD, Android and free alternatives, and iOS.
        </p>
    </div><div class="natta-question-box">
        <h3>What is the authentication type?</h3>
        <p>
            Authentication is to be done with  a private/public key provided in the member login space. N.-B.: we do not provide CSR.
        </p>
    </div><div class="natta-question-box">
        <h3>How to replicate this service?</h3>
        <p>
            Some helpful documentation :
        </p>
        <ul>
            <li>Get public IPs</li>
            <li>Root the IPs to a server</li>
            <li>Set up a DNS authority</li>
            <li>Set up the VPN server</li>
        </ul>
    </div>
</div>
<div class="natta-help" style="text-align:center">
            <p class="aidez_nous">Did you find this helpful? You can help us too :)</p>
            <p><b>Come to our next volunteer meeting</b></p>
</div>