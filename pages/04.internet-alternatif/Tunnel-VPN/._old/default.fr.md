---
title: Tunnel VPN
body_classes: natta
visible: true
published: true
login: {  }
content:
    items: '@self.modular'
routable: true
page-toc:
  active: false
---

<!--Principes-->
<div class="logo-natta">
        <img src="../../images/NATTA_Titre.svg" alt="Natta - accès internet alternatif" class="logo-natta-img"/>
</div>
<div class="natta-title-image">
            <h1>Qu'est-ce qu'un <span class="orange">tunnel VPN</span> ?</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <p>
            Un <strong>tunnel de réseau virtuel privé (VPN)</strong> permet de relier virtuellement votre ordinateur à un autre réseau. Dans le cas des tunnels proposés par ARN, c’est un peu comme si votre équipement était relié avec un câble branché directement dans la baie serveur de notre association. <br>
            Les sites web que vous consultez, les mails que vous recevez, les messages que vous envoyez, les recherches que vous effectuez… En bref : toutes vos activités en ligne seront vues comme émanant  des <strong>serveurs d'ARN</strong>. 
        </p>
</div>
<div class="natta-title-image">
            <h2>Quand utiliser les <span class="orange">VPN</span> ?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
            <p>
            N.B. : Les VPN ARN ne sont pas une solution d’anonymat ou de géolocalisation en dehors de la France. Pour cela, nous vous conseillons de regarder plutôt du côté de [Tor Browser].
            </p>
            <h3>Sécuriser sa connexion</h3>
            <p>
            Pour <strong>éviter le « Sniffing Wi-Fi »</strong>, c'est-à -dire la mise en écoute de sa connexion par son Fournisseur d'accès à Internet (FAI), mais aussi pour se protéger sur les wifis publics.
            </p>
            <h3>S’auto-héberger</h3>
            <p>
            Ces VPN sont pensés pour <strong>l’auto-hébergement</strong> (y compris des mails) et sont compatibles avec YunoHost et les Briques Internet.
            </p>
            <h3>Débrider sa connexion</h3>
            <p>
            Empêcher les atteintes à la Neutralité du Net par son FAI, retour de l’internet aimé...
            </p>
            <h3>Se géolocaliser en France</h3>
            <p>
            Consulter depuis l'étranger les contenus accessibles uniquement en France en utilisant notre point de sortie à Schiltigheim.
            </p>
            <h3>Accéder à l’IPv6</h3>
            <p>
            Les VPN ARN sont <strong>compatibles IPv6</strong>, même si votre réseau initial ne supporte qu’IPv4.
        </p>
</div>
<div class="natta-title-image">
            <h2>Nos <span class="orange">offres</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-vpn">
            <h3 class="offre-title">Tunnel VPN</h3>
            <div class="offre-para">
                <p class="offre-title">4€/mois + adhésion</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p>
                1 IPv4 publique<br>
                1 préfixe IPv6 publique<br>
                Reverse DNS personnalisable<br>
                IPV4 & V6<br>
                Compatible PC et smartphone<br>
                Mono-équipement<br>
                Jusqu'à 80 Mbps symétrique<br>
                Technologie Open VPN
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
        <div class="offre-box offre-vpn">
            <h3 class="offre-title">Neutribox</h3>
            <div class="offre-para">
                <p class="offre-title">4€/mois + 60€ + adhésion</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p>
                1 IPv4 publique<br>
                1 préfixe IPv6 publique<br>
                Reverse DNS personnalisable<br>
                IPV4 & V6<br>
                Plug&Play avec hotspot wifi<br>
                Multi-equipement<br>
                Jusqu'à 40 Mbps symétrique<br>
                Technologie Open VPN
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
</div>
<p>
Prix en franchise de TVA (art 293B du CGI) et sans engagement. Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction. Abonnement payable en G1 (4DU/mois).
</p>
<div class="natta-title-image">
            <h2>Comment <span class="orange">procéder</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="timeline">
        <div class="timeline-back-natta"></div>
            <div class="timeline-item">
                    <h3>1. Adhérez à l’association</h3>
                    <p>Comme la plupart de nos services, celui-ci est réservé aux <strong>membres de l'association</strong>. Il faut donc adhérer à l'association pour profiter du service VPN.
                    </p>
                </div>
                <div class="timeline-item">
                    <h3>2. Remplissez le formulaire de demande</h3>
                    <p>Une fois que vous êtes membre ARN, il est possible de demander un service en répondant aux questions posées dans notre formulaire.
                    </p>
                </div>
                <div class="timeline-item">
                    <h3>3. Validation et paiement</h3>
                    <p>L’équipe de bénévoles validera votre service, merci de procéder dès lors à la mise en place d’un virement permanent, ou à défaut de régler plusieurs mois d’avance en une fois.
                    </p>
                </div>
                <div class="timeline-item">
                    <h3>4. Configurez votre service avec nos tutoriels</h3>
                    <p>Une fois votre service livré, vous pouvez accéder aux informations le concernant dans l’interface membre et le configurer à l’aide de <a href="../../educpop">nos tutoriels</a>.
                    </p>
                </div>
</div>


<div class="natta-title-image">
            <h2>Foire aux <span class="orange">questions</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="natta-question-container">
    <div class="natta-question-box">
        <h3>Ce VPN peut-il fonctionner sur des réseaux restreints ?</h3>
        <p>
            Il est possible de se connecter sur n’importe quel port, y compris le 443 (en UDP ou TCP). Ce qui permet bien souvent de configurer le VPN dans de nombreux réseaux.
            Toutefois, il est probable que le VPN ARN ne fonctionne pas dans certains pays tels que la Chine. À tester (sans vous mettre en danger), n’hésitez pas à nous faire des retours.
        </p>
    </div>
    <div class="natta-question-box">
        <h3>Combien d’équipements puis-je connecter au VPN ?</h3>
        <p>
            En IPv4, il est possible de connecter un seul équipement à la fois, à moins d’utiliser une Neutribox et de s’y connecter en wifi.
            En revanche, il est possible de connecter plusieurs appareils en « IPv6 only ». Mais la navigation « IPv6 only » étant très limitée, la plupart des sites et applications ne fonctionneront pas.
        </p>
    </div><div class="natta-question-box">
        <h3>Qu’est-ce qu’une Neutribox ?</h3>
        <p>
            La Neutribox ARN est un boîtier doté d’une antenne Wifi à brancher sur la box en filaire. La Neutribox génère un hotspot wifi dont les communications passeront au travers du VPN entre la Neutribox et le serveur VPN d’ARN.
            Actuellement, nous utilisons une carte orange pi pc+, une antenne et YunoHost pour fabriquer la Neutribox.
        </p>
    </div><div class="natta-question-box">
        <h3>Qu’est-ce que la Ğ1 ?</h3>
        <p>
            La Ğ1 (prononcée “ june”) est une monnaie libre que l’association accepte pour le règlement de votre abonnement.
        </p>
    </div><div class="natta-question-box">
        <h3>Quelle est la politique de log d’ARN pour ce service ?</h3>
        <p>
            Nous enregistrons :
        </p>
        <ul>
            <li>votre identité</li>
            <li>les IPs ARN qui vous sont attribuées</li>
            <li>la période d’attribution</li>
        </ul>
            <p>Toutefois, nous vous invitons à lire la clause « Mésusage des services » de nos CGS. Notez que dans le cadre d’une enquête, nous pourrions être contraints légalement de permettre une mise sur écoute de votre VPN.
            </p>
    </div><div class="natta-question-box">
        <h3>Quels sont les OS supportés ?</h3>
        <p>
            Presque tous les SE dans leur version récente sont supportés : Linux, Windows, Mac OS, BSD, Android et dérivés libres, iOS.
        </p>
    </div><div class="natta-question-box">
        <h3>Quel est le type d’authentification ?</h3>
        <p>
            Authentification par clé privée/clé publique fournie via l’espace membre. NB : nous ne proposons pas de mécanisme de CSR.
        </p>
    </div><div class="natta-question-box">
        <h3>Comment reproduire ce service ?</h3>
        <p>
            Quelques documentation utiles :
        </p>
        <ul>
            <li>obtenir des IPs publiques</li>
            <li>être en mesure de les router sur un serveur</li>
            <li>mettre en place une autorité DNS</li>
            <li>configurer le serveur VPN</li>
        </ul>
    </div>
</div>
<div class="natta-help" style="text-align:center">
            <p class="aidez_nous">Nos actions vous plaisent ? Aidez-nous :)</p>
            <p><b>Venez à la prochaine réunion d’accueil des bénévoles</b></p>
</div>