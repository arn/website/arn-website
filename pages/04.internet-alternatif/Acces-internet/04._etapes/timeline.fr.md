---
title: 'Comment souscrire'
body_classes: modular
---
## __Comment *souscrire* ?__

---
### 1. Créez un compte sur l’espace membre et demandez le service

Comme la plupart de nos services, celui-ci est réservé aux membres de l'association, il faut donc créer un compte sur l'[espace membre](https://adherents.arn-fai.net/members/register/), puis « [Demander un nouvel abonnement](https://adherents.arn-fai.net/members/login/?next=/members/request_subscriptions/step1) ». Vous êtes libre de régler l’adhésion qu’à la réception du service.

---
### 2. Validation, paiement des frais d’accès au service et raccordement

L’équipe bénévole vous appelle ou vous contacte par mail pour vérifier la faisabilité du raccordement (dans certains cas, un rendez-vous sur place peut être nécessaire) et valide votre demande. Vous réglez les frais d’accès au service et les bénévoles prennent rendez-vous avec vous pour le raccordement.

---
### 3. Vérification du bon fonctionnement et règlement

Une fois votre service mis en place, vous pouvez accéder aux informations et à la documentation le concernant sur la page « [Mes abonnements](https://adherents.arn-fai.net/members/subscriptions/) ». Merci de procéder ensuite au règlement de l’adhésion si ce n’est pas déjà fait, et à la mise en place d’un virement permanent, ou à défaut de régler plusieurs mois d’avance en une fois.

---

