---
title: 'Accès Internet'
body_classes: natta
visible: true
published: true
login: {  }
routable: true
page-toc:
    active: false
---

<div class="logo-natta">
        <img src="../../images/NATTA_Titre.svg" alt="Natta accès internet alternatif" class="logo-natta-img"/>
</div>
# __Éligibilité à un *accès à Internet ARN*__


<div class="acces-intro-text">
    <p>
    <iframe id="acces-eligible" title="éligibilité fibre et antennes" src="https://arn-fai.net/eligibilité/">
    </iframe>
    <br>
    Testez ici votre éligibilité à la fibre ou au réseau d'antennes ARN.
    </p>
</div>
<div class="natta-title-image">
            <h2>Fonctionnalités</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
           <h3>IPv4 & IPv6 public fixe</h3>
           <p>
            Nous fournissons un <strong>VPN compatible IPv6</strong>, si la connexion n’est pas nativement en IPv6.
            </p>
            <h3>Neutralité du net</h3>
            <p>
            Afin de respecter le concept de la <strong>neutralité du Net</strong>, nous nous engageons à ne fermer aucun port de logiciel. De même, nous ne changeons pas votre résolveur DNS et nous ne regardons ni ne modifions votre trafic Internet.
            </p>
            <h3>Avec ou sans box</h3>
            <p>
            Ces abonnements vous sont fournis avec ou sans box (routeur), pour vous permettre d’utiliser l’équipement adapté à votre logement et vos besoins. Nous possédons également des routeurs recyclés par nos soins pour une <a href="https://tube.aquilenet.fr/w/9fcc676e-e62a-45de-b848-3bedd630fde6">empreinte environnementale numérique réduite</a> :)
            </p>
            <h3>Possibilité d’auto-hébergement</h3>
            <p>
            Aucun port logiciel n’est fermé et il est possible de <strong> personnaliser le reverse DNS en IPv4 et en IPv6</strong>.
            </p>
            <h3>Partage avec les voisins</h3>
            <p>
            Avec notre contrat, vous avez le droit de partager votre connexion Internet avec vos voisins. Vous pouvez aussi n’autoriser que la connexion via un <strong>VPN ARN</strong> pour séparer la responsabilité légale.
            </p>
            <h3>Et BFMTV ?</h3>
            <p>
            Nous ne proposons pas la télé. Après tout, votre « temps de cerveau disponible » vous appartient :)
            </p>
</div>
<div class="natta-title-image">
            <h2>Nos<span class="orange"> services</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-acces">
            <h3 class="offre-title">Fibre optique</h3>
            <div class="offre-para">
                <p class="offre-title">30€/mois</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p class="condition-acces">
                Sur réseau <a href="https://www.rosace-fibre.fr/">Rosace</a><br>
                Frais d'accès : 100€<br>
                Sans engagement
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
        <div class="offre-box offre-acces">
            <h3 class="offre-title">Fibre optique / xDSL</h3>
            <div class="offre-para">
                <p class="offre-title">À partir de 42€/mois</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p class="condition-acces">
                Sur réseau <a href="https://www.fdn.fr/">FDN</a><br>
                Frais d’accès : 66€ à 156€<br>
                Engagement : 1 an
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
          <div class="offre-box offre-acces">
            <h3 class="offre-title">Antennes radio</h3>
            <div class="offre-para">
                <p class="offre-title">15€/mois</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p class="condition-acces">
                Frais d’accès au service : en fonction du raccordement<br>
                Sans engagement
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
</div>
<p>Prix en franchise de TVA (art 293B du CGI). L’accès à ces services requiert une adhésion à l’association (15€/an). Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction. 
</p>
<div class="natta-title-image">
            <h2>Comment <span class="orange">souscrire</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="timeline">
        <div class="timeline-back-natta"></div>
                <div class="timeline-item">
                    <h3>1. Créez un compte sur l’espace membre et demandez le service</h3>
                    <p>
                    Comme la plupart de nos services, celui-ci est réservé aux membres de l'association, il faut donc créer un compte sur l'<a href="https://adherents.arn-fai.net/members/register/">espace membre</a>, puis « <a href="https://adherents.arn-fai.net/members/login/?next=/members/request_subscriptions/step1">Demander un nouvel abonnement</a> ». Vous êtes libre de régler l’adhésion qu’à la réception du service. 
                    </p>
                </div>
                <div class="timeline-item">
                    <h3>2. Validation, paiement des frais d’accès au service et raccordement</h3>
                    <p>L’équipe bénévole vous appelle ou vous contacte par mail pour vérifier la faisabilité du raccordement (dans certains cas, un rendez-vous sur place peut être nécessaire) et valide votre demande. Vous réglez les frais d’accès au service et les bénévoles prennent rendez-vous avec vous pour le raccordement.
                    </p>
                </div>
            <div class="timeline-item">
                    <h3>3. Vérification du bon fonctionnement et règlement</h3>
                    <p>
                    Une fois votre service mis en place, vous pouvez accéder aux informations et à la documentation le concernant sur la page « <a href="https://adherents.arn-fai.net/members/subscriptions/">Mes abonnements </a> ». Merci de procéder ensuite au règlement  de l’adhésion si ce n’est pas déjà fait, et à la mise en place d’un virement permanent, ou à défaut de régler plusieurs mois d’avance en une fois.
                    </p>
                </div>
</div>
<div class="natta-title-image">
            <h2>Foire aux <span class="orange">questions</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="natta-question-container">
    <div class="natta-question-box">
        <h3>Pourquoi dois-je adhérer ?</h3>
        <p>
            Chez Alsace Réseau Neutre, association à but non lucratif sans salariés, vous n’êtes pas client⋅es d’un fournisseur. Vous êtres membre d’un collectif qui s’organise pour construire un bout d’internet utopique et influencer les politiques numériques vers des schémas plus vertueux. Les adhésions nous permettent de proposer ses services et de les financer.
            <br><br>
            Par ailleurs, nous souhaitons qu’au travers de l’assemblée générale, les personnes abonnées aient un droit de vote concernant l’avenir de leurs services.  
        </p>
    </div>
    <div class="natta-question-box">
        <h3>Que signifie « Prix en franchise de TVA » ?</h3>
        <p>
            La franchise de TVA signifie qu’à ce jour ARN n’est pas soumis à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».
        </p>
    </div><div class="natta-question-box">
        <h3>Quel est le débit ?</h3>
        <p>
            Le débit dépend du service choisi. Ci-dessous, un ordre d’idées :
            <ul>
                <li>Fibre optique ROSACE : jusqu’à 1Gbps</li>
                <li>Fibre optique via FDN : jusqu’à 1Gbps descendant et 500Mbps montants selon le prix choisi</li>
                <li>VDSL via FDN : jusqu’à 18 Mbps (upload jusque 1 Mbps)</li>
                <li>ADSL via FDN : jusqu’à 50 Mbps (upload jusque 8 Mbps)</li>
                <li>Réseau d’antennes radio : maximum 90Mbps symétriques</li>
            </ul>
            Si vous utilisez un VPN ARN par-dessus ce service, vous ne pourrez pas atteindre plus de 90Mbps. <a href="https://arn-fai.net/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-">En savoir plus sur : Est-ce je pénalise l'association en téléchargeant beaucoup ?</a>
        </p>
    </div><div class="natta-question-box">
        <h3>Est-ce que la fibre arrive jusque dans le logement ?</h3>
        <p>
            Oui, il s’agit bien d’un service FTTH « Fiber To The Home ». C’est à dire que la fibre arrivera jusque dans votre logement. 
        </p>
    </div><div class="natta-question-box">
        <h3>Quelle est la politique de sauvegarde des données de connexions (logs) d’ARN pour ce service ?</h3>
        <p>
           Pour le réseau radio, la politique de sauvegarde des données est la même que pour les VPN. 
        <br>
        Pour la fibre, l’ADSL et le VDSL, nous sommes en cours de discussion avec FDN et SCANI pour en connaître les modalités. Nous vous tiendrons au courant une fois qu’elle sera définie.
        </p>
    </div><div class="natta-question-box">
        <h3>Proposez-vous des abonnements 4G ?</h3>
        <p>
            Nous ne proposons pas ce service !
        </p>
    </div><div class="natta-question-box">
        <h3>Puis-je choisir ma box ?</h3>
        <p>
            Oui. Ces abonnements sont fournis sans box (routeur). Vous devez donc vous procurer le routeur, sauf  si vous souhaitez vous connecter à un seul ordinateur par câble ethernet. Vous aurez ainsi la main sur les fonctionnalités et la consommation de votre équipement.
            <br><br>Si vous n'avez pas déjà un routeur, n'hésitez pas à nous demander de l'aide pour faire votre choix. Nous sommes aussi en mesure d'en trouver d'occasions et bon marché (via Emmaüs Strasbourg principalement). C’est également un petit geste pour l’environnement!
            <br>Si vous souhaitez des fonctionnalités serveur, n'hésitez pas à venir à notre <a href="https://mobilizon.sans-nuage.fr/@arnfai/events?future=true">atelier mensuel sur l'auto-hébergement</a>.
        </p>
    </div>
    <div class="natta-question-box">
        <h3>Y-a-t'il un service télévision ou téléphone inclu ?</h3>
        <p>
           Non, mais...
            <br>Pour la télévision, vous pouvez en général  recevoir la TNT gratuitement via le signal radio. Pour cela, il vous faut un téléviseur compatible ou un décodeur. Il existe également des offres IPTV.<br><br>
            Pour le téléphone, il existe en France des offres de  téléphonie fixe sur IP (à brancher sur un routeur/box) à moins de 2€  TTC/mois. Par exemple, celles d'OVH.
        </p>
    </div>
     <div class="natta-question-box">
        <h3>Comment obtenir de l'aide en cas de problème avec ma connexion ?</h3>
        <p>
           Les bénévoles d'ARN essaierons de résoudre votre problème en faisant un diagnostique de la connexion, potentiellement en vous accompagnant par téléphone. Si cela ne suffit pas, les informations seront transmises à SCANI ou FDN, qui regarderont de leur côté ou feront remonter à leurs prestataires.
            <br>L'aide peut être obtenue en nous contactant sur le <a href="../../nous-contacter">formulaire de contact</a>, le forum, le chat d'entraide et même en présentiel lors des permanences du vendredi soir au hackstub.
        </p>
    </div>
     <div class="natta-question-box">
        <h3>Puis-je héberger mon propre serveur mail sur cette connexion ?</h3>
        <p>
           Oui. La connexion étant neutre, les ports ne sont pas bridés (même le port 25) et vous pouvez définir un reverse DNS. En plus, nous ne listons pas nos IPs comme résidentiels, contrairement à de nombreux FAI.
<br>Attention tout de même à ne pas ternir la réputation des IPs.
        </p>
    </div>
     <div class="natta-question-box">
        <h3>Mon IP peut-elle changer ? Est-elle partagée ?</h3>
        <p>
           Les IPs sont fixes et nous ne prévoyons pas de partager les plages de ports afin de favoriser la pratique de l'auto-hébergement. 
            <br>En revanche, en ce qui concerne la connexion fibre optique, des IPs SCANI ou FDN seront attribuées pour le moment. Il est donc possible qu’un jour, l’association décide de renuméroter avec des IPs ARN.
        </p>
    </div>
     <div class="natta-question-box">
        <h3>L'IPv6 est-elle disponible ?</h3>
        <p>
           Oui (au minimum grâce un VPN inclut avec).
        </p>
    </div>
     <div class="natta-question-box">
        <h3>Comment reproduire ce service ?</h3>
        <p>
           Pour la fibre optique ou le xDSL, il s’agit pour l’instant de marque blanche SCANI ou FDN. 
        <br>Pour les antennes radio, tout est expliqué dans la page dédiée aux <a href="https://wiki.arn-fai.net/benevoles:technique:natta">antennes radio</a> sur le wiki des bénévoles.
        </p>
    </div>
</div>
<div class="natta-help" style="text-align:center">
            <p class="aidez_nous">Nos actions vous plaisent ? Aidez-nous :)</p>
            <p><b>Venez à la prochaine réunion d’accueil des bénévoles</b></p>
</div>
