---
title: 'Les fonctionnalités en plus'
body_classes: modular
size: sm
---

## __Les fonctionnalités *en plus*__

---
### IPv4 & IPv6 public fixe

Nous fournissons un **VPN compatible IPv6**, si la connexion n’est pas nativement en IPv6.

---
### Neutralité du net

Nous ne regardons ni ne modifions votre trafic et nous respectons votre choix de résolveur DNS.

---
### Partager votre accès
Vous avez **contractuellement le droit** de partager votre connexion Internet avec vos voisins. 

---
### Avec ou sans box

Utilisez la vôtre ou encore un **routeur reconditionné** par nos soins avec une <a href="https://tube.aquilenet.fr/w/9fcc676e-e62a-45de-b848-3bedd630fde6" target="_blank">**empreinte environnementale réduite**</a>.

---
### Auto-hébergement

Aucun port logiciel n’est fermé et il est possible de **personnaliser le reverse DNS en IPv4 et en IPv6**.

---
### Et BFMTV ?
**Nous ne proposons pas la télé**. Après tout, votre « temps de cerveau disponible » vous appartient 😉

---