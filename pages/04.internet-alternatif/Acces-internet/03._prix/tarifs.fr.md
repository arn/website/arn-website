---
title: 'Nos services'
body_classes: modular
---

## __Combien *ça coûte* ?__
---
### Fibre optique

__*35€* / mois__

Via réseau [Rosace](https://www.rosace-fibre.fr/)<br>
Frais d'accès : 60€<br>
_Sans engagement_

---
### Fibre optique / xDSL

__*35€ à 60€* / mois__

Via réseau [FDN](https://www.fdn.fr/)<br>
Frais d’accès : 66€ à 156€<br>
_Engagement : 1 an_

---
### Antennes radio

__*15€* / mois__

Via réseau ARN<br>
Frais d’accès : sur demande<br>
_Sans engagement_

---
_Prix en franchise de TVA (art 293B du CGI), c'est à dire TTC. L’accès à ces services requiert une [adhésion à l’association](/asso/adhérer). Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction._