---
title: Éligibilité
body_classes: modular
iframe:
    src: '/user/themes/arn/js/eligibility/index.html#9/48.4128/8.1656'
    full: false
    height: 550
    title: "Outils d'éligibilité"
---

# __Éligibilité à un *accès à Internet ARN*__

<p style="text-align: center;">Testez ici <b>votre éligibilité à la fibre</b> ou au réseau d'antennes ARN.</p>
