---
title: 'Foire aux questions'
body_classes: modular
---

## __Foire aux *questions*__

---
### Pourquoi dois-je adhérer ?
Chez Alsace Réseau Neutre, association à but non lucratif sans salariés, vous n’êtes pas client⋅es d’un fournisseur. Vous êtes membre d’un collectif qui s’organise pour construire un bout d’internet utopique et influencer les politiques numériques vers des schémas plus vertueux. Les adhésions nous permettent de proposer ses services et de les financer.

Par ailleurs, nous souhaitons qu’au travers de l’assemblée générale, les personnes abonnées aient un droit de vote concernant l’avenir de leurs services.

---
### Que signifie « Prix en franchise de TVA » ?

La franchise de TVA signifie qu’à ce jour ARN n’est pas soumis à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».

---
### Quel est le débit ?
Le débit dépend du service choisi. Ci-dessous, un ordre d’idées : 

 * Fibre optique ROSACE : jusqu’à 1Gbps
 * Fibre optique via FDN : jusqu’à 1Gbps descendant et 500Mbps montants selon le prix choisi
 * VDSL via FDN : jusqu’à 50 Mbps (upload jusque 8 Mbps)
 * ADSL via FDN : jusqu’à 18 Mbps (upload jusque 1 Mbps)
 * Réseau d’antennes radio : maximum 90Mbps symétriques

Si vous utilisez un VPN ARN par-dessus ce service, vous ne pourrez pas atteindre plus de 90Mbps.

[Est-ce que je pénalise l'association en téléchargeant beaucoup ?](/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-)

---
### Est-ce que la fibre arrive jusque dans le logement ?
Oui, il s’agit bien d’un service FTTH « Fiber To The Home ». C’est à dire que la fibre arrivera jusque dans votre logement.

---
### Quelle est la politique de sauvegarde des données de connexions (logs) d’ARN pour ce service ?
Pour le réseau radio, la politique de sauvegarde des données est la même que pour les VPN.

Pour la fibre, l’ADSL et le VDSL, nous sommes en cours de discussion avec FDN et SCANI pour en connaître les modalités. Nous vous tiendrons au courant une fois qu’elle sera définie.

---
### Proposez-vous des abonnements 4G/5G ?
Pas encore, mais vous pouvez vous signaler si vous le souhaitez. Nos ami⋅es d'Aquilenet sont en phase d'expérimentation sur ce sujet.

---
### Puis-je choisir ma box ?
Oui. Ces abonnements sont fournis sans box (routeur). Vous devez donc vous procurer le routeur, sauf si vous souhaitez vous connecter à un seul ordinateur par câble ethernet. Vous aurez ainsi la main sur les fonctionnalités et la consommation de votre équipement.

Si vous n'avez pas déjà un routeur, n'hésitez pas à nous demander de l'aide pour faire votre choix. Nous sommes aussi en mesure d'en trouver d'occasions et bon marché (via Emmaüs Strasbourg principalement). C’est également un petit geste pour l’environnement!
Si vous souhaitez des fonctionnalités serveur, n'hésitez pas à venir à notre [atelier mensuel sur l'auto-hébergement](https://mobilizon.sans-nuage.fr/@arnfai/events?future=true).

---
### Y-a-t'il un service télévision ou téléphone inclu ?
Non, mais...

Pour la télévision, vous pouvez en général recevoir la TNT gratuitement via le signal radio. Pour cela, il vous faut un téléviseur compatible ou un décodeur. Il existe également des offres IPTV.

Pour le téléphone, il existe en France des offres de téléphonie fixe sur IP (à brancher sur un routeur/box) à moins de 2€ TTC/mois. Par exemple, celles d'OVH.

---
### Comment obtenir de l'aide en cas de problème avec ma connexion ?
Les bénévoles d'ARN essaierons de résoudre votre problème en faisant un diagnostique de la connexion, potentiellement en vous accompagnant par téléphone. Si cela ne suffit pas, les informations seront transmises à SCANI ou FDN, qui regarderont de leur côté ou feront remonter à leurs prestataires.

L'aide peut être obtenue en nous contactant sur le formulaire de contact, le forum, le chat d'entraide et même en présentiel lors des permanences du vendredi soir au hackstub.

---
### Puis-je héberger mon propre serveur mail sur cette connexion ?
Oui. La connexion étant neutre, les ports ne sont pas bridés (même le port 25) et vous pouvez définir un reverse DNS. En plus, nous ne listons pas nos IPs comme résidentiels, contrairement à de nombreux FAI.

Attention tout de même à ne pas ternir la réputation des IPs.

---
### Mon IP peut-elle changer ? Est-elle partagée ?
Les IPs sont fixes et nous ne prévoyons pas de partager les plages de ports afin de favoriser la pratique de l'auto-hébergement.

En revanche, en ce qui concerne la connexion fibre optique, des IPs SCANI ou FDN seront attribuées pour le moment. Il est donc possible qu’un jour, l’association décide de renuméroter avec des IPs ARN. 

---
### L'IPv6 est-elle disponible ?
Oui (au minimum grâce à un VPN inclut avec).

---
### Comment reproduire ce service ?
Pour la fibre optique ou le xDSL, il s’agit pour l’instant de marque blanche SCANI ou FDN.

Pour les antennes radio, tout est expliqué dans la page dédiée aux [antennes radio](https://wiki.arn-fai.net/benevoles:technique:natta) sur le wiki des bénévoles.

---

