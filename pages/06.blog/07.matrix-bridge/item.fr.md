---
title: "Des membres d'ARN dans Open Tech will save us"
visible: false
published: true
summary:
    size: 200
date: '03-03-2022'
---

Entretien avec Alexandre et Gauthier à propos de YunoHost et des bridges Matrix.

<iframe title="Open Tech Will Save Us #13 - Nextcloud, Yunohost/CHATONS, Matrix P2P" src="https://tube.fdn.fr/videos/embed/1f8072f9-b4a5-4920-bcaf-e00adfe23c11?warningTitle=0&amp;p2p=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>


