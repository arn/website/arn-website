---
title: 'Dérives sécuritaires et surveillance, jusqu''où irons nous ?'
visible: false
published: true
media_order: panoptique.JPG
date: '07-03-2020 12:58'
---

![Ancienne prison panoptique en ruine. Une tour centrale permet d'observer chaque cellule.](panoptique.JPG)
*CC BY-SA 3.0 I, Friman*

**Le 10 mars à Strasbourg se tiendra le procès de Jules, militant strasbourgeois. Il risque 1 an de prison et 15 000€ d'amende pour avoir refusé de donner son ADN et ses empreintes lors d'une garde à vue dont il est ressorti libre. Les associations Alsace Réseau Neutre et Hackstub, espaces de critique et de construction du numérique, soutiennent Jules dans son choix du refus et demandent l'arrêt de la surenchère sécuritaire, particulièrement forte avec le développement des nouveaux outils numériques.**


Depuis sa création en 1998, le fichier national automatisé des empreintes génétiques (FNAEG) ne cesse d'enfler. En ce début d'année, le fichier contient 2,9 millions de profils ADN. Si on estime que chaque profil permet par filiation d'identifier environ 2 à 3 autres personnes ceci porte le nombre de personnes identifiables à environ 10 millions. A l'origine, conçu pour identifier les auteurs des crimes sexuels, ce fichier, étendu à la plupart des crimes et délits, fait aujourd'hui l'objet d'une politique visant à l'identification générale de la population par le recueil d'un maximum de profils.

Mais ce fichier n'est de loin pas le seul dispositif numérique dédié à la sécurité, mis en place dans un cadre et un contexte précis, et qui au fil du temps a dérivé de son usage initial.

Par ailleurs, au niveau local, plusieurs villes sont en train de déployer des dispositifs de détection de comportements et d'évènements basés sur la reconnaissance faciale, certaines rêvent de drones dans leurs projets. La région PACA a même déployé dans des lycées des systèmes de suivis inspirés de la gestion des mouvements des détenus! L'Eurométropole de Strasbourg n'est pas en reste, avec plus de 600 caméras, une cartographie en temps réel de suivi des foules, ainsi que plusieurs micros audio dans nos rues.

Des fichiers plus larges, plus facilement consultables, des dispositifs toujours plus massifs et intrusifs. Est-ce là les signes d'une société dans laquelle tout le monde deviendrait suspect ?

**ARN & Hackstub dénoncent le fichage généralisé**, que ce soit par les institutions ou les entreprises du numérique. Ces méthodes font peser une menace disproportionnée sur la protection de la vie privée et sont incompatibles avec les principes d'une société démocratique.

> **_1984 de George Orwell n'était pas destiné à devenir un mode d'emploi._**

## Autres dispositifs, histoires semblables :

* Le fichier de Traitement d'antécédents judiciaires (TAJ). Ce fichier, créé en 2011, contient aujourd'hui 18,9 millions de fiches de personnes, qu'elles soient victimes, suspectes ou déclarées non coupables. Il est depuis utilisé pour alimenter des algorithmes de reconnaissance faciale.
* Le fichier des cartes d'identité et des passeports qui a suivi la même trajectoire. Initialement, la police et la gendarmerie avaient le droit d'y accéder dans le cadre de l'anti-terrorisme ( la loi du 23 janvier 2006). Plusieurs lois, dont celle sur le renseignement, sont venues étendre les motifs d'accès si bien qu'aujourd'hui la simple suspicion de trouble à l'ordre public suffit.
* La dérive des fichiers porte aussi sur les informations qu'ils contiennent. Un décret publié le 22 février dernier autorise les gendarmes à consigner, dans une application de prises de notes, des photos mais aussi des données relatives «à la prétendue origine raciale ou ethnique, aux opinions politiques, philosophiques ou religieuses, à l’appartenance syndicale, à la santé ou à la vie sexuelle ou l’orientation sexuelle». Ces données peuvent ensuite être exportées vers d'autres fichiers.
* La loi sur le renseignement de 2015 a permis la pose d'équipements destinés à écouter l'ensemble des communications internet de la population.
* Depuis un an, plusieurs allocataires du RSA et utilisateurs du navigateur Tor Browser favorisant l'anonymat, nous ont rapporté que la CAF effectue des contrôles à domicile basés sur la provenance des communications.
* L’article 154 de la loi de finance 2020 autorise les administrations fiscales à analyser automatiquement les réseaux et médias sociaux, en vue de détecter des fraudeurs.

## À propos d'Alsace Réseau Neutre et Hackstub
Alsace Réseau Neutre est une association à but non lucratif qui participe à la construction d'un internet qui respecte les libertés fondamentales de ses usagers. L'association propose depuis 3 ans les ateliers "Libérons-nous du pistage" permettant aux personnes participantes de limiter les données transmises à leur insu lors de leur utilisation quotidienne d'internet. ARN est également fournisseur d'accès internet et de services en ligne, il est ainsi possible d'être raccordé à internet sur Strasbourg, mais également d'utiliser divers services proposés par l'association comme sans-nuage.fr.

[Hackstub](https://hackstub.eu/home/fr), le Hackerspace de Strasbourg, est un espace dédié à la critique de la technologie et à la création. L'association propose notamment un évènement mensuel autour des enjeux du numérique, de la culture du logiciel libre ou de l'éthique Hacker.
