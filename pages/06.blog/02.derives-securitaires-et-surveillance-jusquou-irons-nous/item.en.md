---
title: 'Security drift, computer and network surveillance, where is the limit ?'
visible: false
published: true
media_order: 'welcome to sodom.jpg'
date: '07-03-2020 12:58'
---

![Old panoptic prison in ruins. A central tower which observes each cell](panoptique.JPG "panoptique")

On the 10th of March, will be held the trial of Jules, an activist from Strasbourg.for refusing to give his DNA and fingerprints while in police custody from which he has been released for refusing to give his DNA and fingerprints while in police custody from which he has been released. The associations Alsace Réseau Neutre and Hackstub, digital world’s critique and construction hot spots, support Jules and ask to stop the overuse of the security argument, increasingly used with the emergence of new digital tools.

Since the FNAEG (Automated National File of Genetic Prints) creation in 1998, they collect and accumulate DNA datas. Early this year, this file had 2.9 millions DNA profil registered. As you might know, each profil can identify 2 or 3 people by descent, meaning more than 10 millions people can be identified. This file was originally designed to identify sexual perpetrators. However, today it has been extended to most crimes and is now the subject of a policy to identify the population by collecting as many profiles as possible.

Nevertheless, the FNAEG is not the only digital device set up for safety purposes within a specific context and whose purpose has been derived from its primary use.

Moreover, at the local level, several cities are deploying  facial recognition-based behavior and event detection devices, and some even dream of using drones for this project.

The Provence-Alps-French Riviera (PACA) administrative region has rolled out monitoring systems in high schools, inspired by the prison management and detainee movements! In the Strasbourg metropolitan area more than 600 cameras and several audio microphones seize control of the streets for a real-time mapping of crowd monitoring.

Files growing increasingly voluminous and accessible, the use of ever more massive and intrusive devices. Could this be the beginning of a society where each individual arouses mistrust?

ARN & Hackstub denounce the banalization of individual's personal data storage by institutions and digital companies. The different means used for data collection represent a disproportionate threat to privacy and are incompatible with the principles of a democratic society.
> George Orwell's 1984 was not created to be used as an operating instructions.

## Other devices and similar stories:

* Created in 2011, the French Home secretary's processing criminal records (TAJ) file now contains 18.9 million records of people. They are either victims, suspects or found not guilty. The file is today used to feed facial recognition algorithms.
* The identity card and passport file are used the same way today. Initially, the police and the gendarmerie were allow to access only for anti-terrorism purposes (the law of January 23, 2006). Several laws, including the intelligence bill, have extended the grounds for access so that today the simple suspicion of disturbance of public order is sufficient.
* The information the files contain also address the same issues. A decree published on February 22 allows gendarmes to record, in a note-taking application, not only photos but also data relating to "racial or ethnic origin, political, philosophical or religious opinions, trade union membership, health or sex life or sexual orientation". Datas can then be exported to other files.
* The 2015 intelligence bill allowed the installation of equipment designed to listen to the entire population's internet communications.
* For the past year, minimum social benefits recipients and users of the Tor Browser (give an anonymous internet access), have reported that the CAF carries out home checks based on the origin of communications.
* Article 154 of the 2020 Finance bill allows tax authorities to automatically analyze social networks and media in order to detect fraudsters.
## About Alsace Neutral Network and Hackstub
Alsace Réseau Neutre is a non-profit organization. 

It participates in the creation of an Internet space that respects the fundamental freedoms of its users. Three years ago, the association has organized it's first "Libérons-nous du pistage" workshop to educate about  unconscious data transmission in the daily use of the internet. ARN is also an internet service and online services provider. You can have a secured internet connection in Strasbourg and have access to various services offered by the association such as sans-nuage.fr.

Hackstub, the Hackerspace of Strasbourg, is a space dedicated to technology criticism and creation. The association proposes a monthly event about digital issues, free software culture or Hacker ethics.
