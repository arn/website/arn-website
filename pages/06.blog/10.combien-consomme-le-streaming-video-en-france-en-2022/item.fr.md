---
title: 'Combien consomme le streaming vidéo en France en 2022 ?'
visible: false
classes: lowtech-list-item
summary:
    size: 100
date: '28-04-2022 00:00'
---

_**Sans compter la fabrication des équipements numérique, la fabrication des contenus et la gestion des déchets.**
_
<div class="lowtech" markdown="1">
![Image compressée de la centrale de Fessenheim](fessenheim_compressed.png)
</div>
*CC BY-SA 3.0 - ljf - image dérivée de celle de wikipedia, florival et César* 

Nous avons récemment été sollicités au sujet d'un exemple de calcul sur le coût énergétique de Netflix que nous avons cité dans notre conférence "[Quelques pistes pour diminuer notre empreinte numérique](/blog/empreinte-numérique-écologique)".

Il est suggéré d'après ce calcul rapporté d'une discussion avec une association amie lors de la préparation de cette conférence que:

> *L'utilisation de Netflix en France représenterait en 2021: 1 jour et 14h de centrale nucléaire.* 

Après avoir refait le calcul, en actualisant les données avec le contexte de 2022, nous n'arrivons pas au même résultat ! Nous proposons donc dans cet article un détail de ce nouveau calcul pour caractériser l'usage de Netflix en regard d'un dispositif de production nucléaire.

! Garder en tête que nous ne sommes pas chercheurs ou chercheuses dans le domaine. Toutefois, nous pensons que la démarche n'est pas dénuée d'intérêt et permet de se rendre compte des difficultés pour réaliser ce genre de vulgarisation.

## Centrale vs réacteur

La première chose qui pose question c'est évidemment le terme de centrale, une centrale contient plusieurs réacteurs. En France, nous avons 18 centrales nucléaires pour 56 réacteurs[^1].

Il s'agit donc d'être précis sur le terme utilisé. Dans le cas du chiffre donné dans la conférence, il est possible qu'il y ait eu confusion.

## Combien produit en moyenne un réacteur par jour ?

Les réacteurs ont une puissance nette entre 900MWe et 1,5 GWe. Mais attention à ne pas confondre puissance nette (MWe) et énergie (MWh)[^1].

Par ailleurs, en ce mois d'Avril 2022, 27 réacteurs sur 56 sont à l'arrêt essentiellement pour des problèmes de risques et de maintenance. En février 2022, conscient de ces problèmes, EDF a communiqué une estimation de production nucléaire en 2022 entre 295-315TWh[^2] et quasiment la même chose en 2023[^3]. 

Nous partons donc de l'idée qu'un réacteur en 2022 produit en moyenne par jour :

!!! 305 000 GWh / 56 / 365 = **14,9 GWh**

## La fabrication, la gestion des déchets, la production des contenus et les équipements finaux pour regarder, sont-ils inclus ?

Dans la conférence, la slide contestée au début de cet article n'est pas clair sur ce qui est inclus ou non dans cette estimation. Toutefois, en bas de la slide un article "[The carbon footprint of streaming](https://www.iea.org/commentaries/the-carbon-footprint-of-streaming-video-fact-checking-the-headlines)" de l'Agence International de l'Energie est cité comme source.

Cet article compare les estimations du Shift Project avec celles de l'Agence Internationale de l'Énergie. Dedans on peut notamment trouver un graphique, détaillant l'estimation de la consommation électrique (mise à jour) de l'AIE, pour 1h de streaming visionné:
* Data centres: 0.0037 kWh
* Réseaux acheminant le signal: 0.0177 kWh
* Terminaux pour la visualisation: 0.0555 kWh (55Wh)
* Total: **0,1102 kWh pour 1h de streaming Netflix**

Cependant, au regard de l'article, il apparaît que ce chiffre **ne contient pas** les consommations électriques liées à la fabrication ou à la gestion des déchets informatiques dans ces 3 domaines (datacenter, réseaux, terminaux). On pourrait aussi se poser la question de la consommation liée à la gestion des déchets nucléaires :p . Sans parler de l'impact CO<sub>2</sub> ou en termes de ressources.

De même, la consommation associée à la production des contenus n'est pas analysée ici.

A noter que l'impact environnemental du numérique se trouve essentiellement dans la fabrication et les déchets. 
**Du coup, l'absence de cette donnée peut facilement amener à une compréhension du résultat biaisée.**

## Combien d'heures de vidéos Netflix sont vues par jour ? Et combien pour toutes les enseignes ?

Il apparaît que Netflix a aujourd'hui 9 millions d'abonnements, il est par ailleurs estimés que chaque abonnement correspond à environ 2h/jour.

Ceci nous porte donc à **18 millions d'heures par jour pour Netflix**.

Si on cherche la même chose en totalisant l'ensemble des fournisseurs de streaming vidéo (chaînes de télé, Youtube, Twitch, Netflix, etc.), d'après eMarketer[^5], en 2021 une personne en France de plus de 18 ans consommait 4:30 de vidéo par jour. Si on considère 68 millions de français et qu'on extrapole le même chiffre au moins de 18 ans, ça nous donne: **305 millions d'heures / jour**


## Résultats (hors fabrication & déchets)


!! Il est important de noter que de nombreuses données cités plus haut comportent des marges d'erreurs probablement importantes. Il s'agit donc ici d'une estimation nous donnant un ordre de grandeur.

La consommation électrique liée au visionnage Netflix représenterait environ **3h d'un réacteur nucléaire / jour**.

18 millions d'heures × 0,1102 kWh / 14,9 millions de kWh × 24 heures = 0,13 réacteurs

!!! SI on regarde pour le streaming vidéo en général, nous serions autour de:<br> **2 réacteurs nucléaires / jour**.
!!!
!!! 305 millions d'heures × 0,1102 kWh / 14,9 millions de kWh = 2,26 réacteurs

## Interprétation

Vu qu'il ne s'agit ici que de la consommation électrique liée à l'action de visionnage, sans compter la fabrication des équipements, des contenus ou la gestion des déchets, on peut se demander comment interpréter ce chiffre.

En imaginant que la population française remplace chaque jour 4h30 de visionnage vidéo par 4h30 de méditation ou de marche pendant quelques mois, **ce chiffre suggère que nous pourrions peut être mettre en maintenance 1 ou plusieurs réacteurs nucléaires en plus des 27**... 

Ce serait autant de déchets et de risques nucléaires évités dans un contexte délicat[^6] où la France pourrait avoir des difficultés à mettre en maintenance autant de réacteurs qu'elle le souhaite.

## Conclusion

Quoi qu'il en soit nous tenons à terminer cet article en rappelant la conclusion de la conférence: au regard de nos recherches sur le sujet, après la non-utilisation totale du numérique, le geste numérique individuel **le plus efficace semble être de faire durer son équipement le plus longtemps possible**.

Par ailleurs, l'impact et la responsabilité environnementale des puissants est sans équivoque. Nous ne pouvons pas accepter que ces derniers se défaussent de leurs responsabilités (train de vie, décisions politiques) en pointant le manque d'acte écologique individuel de la population.

[^1]: [Liste des réacteurs Français](https://fr.wikipedia.org/wiki/Liste_des_r%C3%A9acteurs_nucl%C3%A9aires_en_France)
[^2]: [EDF ajuste son estimation de production pour 2022](https://www.edf.fr/groupe-edf/espaces-dedies/journalistes/tous-les-communiques-de-presse/edf-ajuste-son-estimation-de-production-nucleaire-en-france-pour-2022)
[^3]: [EDF ajuste son estimation de production pour 2023](https://www.edf.fr/groupe-edf/espaces-dedies/journalistes/tous-les-communiques-de-presse/edf-ajuste-son-estimation-de-production-nucleaire-en-france-pour-2023)
[^5]: [Temps passés à regarder des vidéos en streaming en France](https://www.emarketer.com/content/time-spent-on-digital-video-in-france-now-20-of-daily-viewing-time)
[^6]: [Nucléaire: les centrales d'EDF inquiètent de plus en plus](https://www.lesechos.fr/industrie-services/energie-environnement/nucleaire-les-centrales-dedf-inquietent-de-plus-en-plus-1401865)
