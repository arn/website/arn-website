---
title: 'Quelques pistes pour diminuer notre empreinte numérique'
visible: false
published: true
summary:
    size: 200
date: '20-11-2021 23:43'
---

Merci à l'APRIL qui nous a fournit une [transcription textuelle de la conférence](https://wiki.april.org/w/Quelques_pistes_pour_diminuer_notre_empreinte_num%C3%A9rique).

<iframe title="Quelques pistes pour diminuer notre empreinte numérique" src="https://tube.fdn.fr/videos/embed/7019ea40-7a39-4d92-83ba-423449d7864e?warningTitle=0&amp;p2p=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>


