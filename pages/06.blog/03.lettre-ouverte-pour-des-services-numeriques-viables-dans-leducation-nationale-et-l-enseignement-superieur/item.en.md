---
title: 'Open letter for sustainable digital services for the National Education and Higher Education'
visible: false
published: true
media_order: 'welcome to sodom.jpg'
date: '07-04-2020 13:05'
---

![La Quadrature Du Net campaign against GAFAM](lqdn-gafam.png "lqdn-gafam")

The letter is foster by somes of the Collective of the Transparent Open Neutral and Solidary Alternative Hosters (CHATONS). Signatories: 42L, Alolise, ARN, ARTCODE.re, Colibris Outils Libres, Défis, Devloprog, Hadoly, Leprette.fr, Le Samarien, siick, Sleto, Sud-Ouest.org, Zici.

From the quarantine outset our organizations have noticed dysfunctions with regrettable consequences in the National Education and Higher Education.The government and the CNED claimed to have set up measures so that everyone can continue to attend classes, even if the reality seems quite different. Testimonies show the overloading of the virtual classrooms servers and the Digital Workspaces, and realized how  teachers have to make up alone for the national institutions ' technical failures.

Aware of these shortcomings, the General Direction of Higher Education even published a document encouraging the use of services created by the associative sector (e.g.: Framasoft).

The government has repeatedly ignored the calls of associations to massively use services under Free Software and has signed a partnership with Microsoft in 2015. The Government continues to lower public service's funds. Manifestly, this strategy is not a solution to deal with the current crisis.

Civil society is an important contribution to overcome this difficult position, but it can possibly not be an alternative to the National Education and Higher Education's missions. These institutions must guarantee course stability for all. The associative world, given its underfunding and the disadvantageous employment policy, cannot guarantee sufficient resources to meet the needs of 870,000 teachers and 12,800,000 students.

Without good information and guidance, teachers and students steer massively towards the GAFAM (Google, Amazon, Facebook, Apple, Microsoft) online services. These companies' economic model is based on gathering personal data, digital confinement, legislation, extraterritoriality and the opacity of their practices. These practices represent great threats to the privacy, security of personal data and common good.

**We urge the National Education and Higher Education to take reformative measures** to reinforce local and national digital services capacity.

With the technical team's support and digital advisers, the government undoubtedly has the ability to increase technical means and offer alternative services using Free Software.They have proven their effectiveness to share resources with a varied public, collaborative document editing and videoconferencing. 

Therefore, to communicate with the educational team about the RDPD (The General Data Protection Regulation nᵒ 2016/679), digital tools and their incompatibility with the teaching legal system is crucial. It is even more important to redirect teachers and students to use open source and decentralized services to guarantee their autonomy and their personal data safety.

These measures implementation must be different to what is currently done and
in line with the following guideline :

* Give priority to Free Software use and support its development
* Ensure in-house services management
* Rely on field teams
* Prohibit the obligation to use services (especially GAFAM one's) exploiting students' privacy or not respecting the RGPD
* Inform about the regulatory requirements teachers and students must follow.

**EDIT from Alsace Réseau Neutre**: Since we wrote this open letter, we are happy to see that software like "peertube" has been set up in academies. 