---
title: 'Lettre ouverte du Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires'
visible: false
published: true
media_order: lqdn-gafam.png
date: '07-04-2020 13:05'
---

![Campagne de 5 affiches de La Quadrature Du Net contre les GAFAM. Les slogans sont : Google filtre ta pensée, Apple sait où est ta mère, Facebook contrôle ce que tu peux lire, Amazon sait quels cadeaux tu auras, Microsoft formate tes enfants.](lqdn-gafam.png)

La présente lettre est soutenue par une partie du Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires (CHATONS). Les organisations signataires : 42L, Alolise, ARN, ARTCODE.re, Colibris Outils Libres, Défis, Devloprog, Hadoly, Leprette.fr, Le Samarien, siick, Sleto, Sud-Ouest.org, Zici.

Depuis le début de l'épisode de confinement, nos organisations ont constaté des dysfonctionnements aux conséquences regrettables dans l'Éducation nationale et l'Enseignement supérieur.

Alors que le gouvernement et le CNED soutiennent qu'ils ont mis en place un système permettant la continuité des cours pour tous, la réalité semble bien différente. Les témoignages montrent la surcharge des serveurs de classes virtuelles et des Espaces Numériques de Travail, et montrent combien les enseignants sont laissés seuls pour pallier les manquements techniques des institutions nationales.

Conscient de ces manquements, la Direction Générale de l'Enseignement Supérieur est même allée jusqu'à publier un document incitant à l'usage de services mis à disposition par le monde associatif (exemple : Framasoft).

L'État a ignoré à de nombreuses reprises les appels des associations à utiliser massivement des services sous Logiciel Libre et a préféré signer un partenariat avec Microsoft en 2015. L'État a continué à dégrader les moyens disponibles dans la fonction publique. Force est de constater que cette stratégie ne permet pas de faire face à la crise actuelle.

La société civile a un rôle essentiel à tenir dans cette période difficile mais elle n'a pas vocation à être une alternative aux missions de l'Éducation nationale et de l'Enseignement supérieur. Ces institutions se doivent de garantir la continuité de la formation pour tous. Le monde associatif, vu son sous-financement et la politique désavantageuse de l'emploi à son égard, ne peut pas garantir une mobilisation suffisante pour répondre aux besoins de 870 000 enseignants et 12 800 000 élèves.

À défaut d'être correctement informés et orientés, c'est en masse qu'enseignants et élèves se tournent vers les services en ligne des GAFAM (Google, Amazon, Facebook, Apple, Microsoft). Le modèle économique de ces entreprises repose sur la collecte de données personnelles, l'enfermement numérique, l'extraterritorialité de la loi et l'opacité de leurs pratiques. Autant de menaces mettant en danger la vie privée, les données personnelles et l'intérêt général. 

**Nous appelons l'Éducation nationale et l'Enseignement supérieur à prendre des mesures correctives et curatives.**

En renforçant les capacités de leurs services numériques, tant nationaux que locaux. C'est réalisable avec le soutien des équipes techniques et des référents numériques, l'augmentation des moyens techniques et de l'offre de services alternatifs basés sur du Logiciel Libre.

Ces derniers ont montré leur efficacité auprès d'un public varié aussi bien pour le partage de ressources, l'édition collaborative de documents que pour la visioconférence.

En informant davantage les équipes pédagogiques sur le cadre légal de l'utilisation des outils numériques et de l'incompatibilité des conditions générales d'utilisation de certains services avec le contexte de l'enseignement et sur le RGPD (Le Règlement Général sur la Protection des Données nᵒ 2016/679).

En orientant les enseignants et les élèves sur l'utilisation de services libres et décentralisés, pour garantir leur autonomie et la sécurité de leurs données personnelles.

La mise en place de ces mesures doit se faire dans le respect de certains principes, en tirant les enseignements de la situation actuelle :
* Donner la priorité au Logiciel Libre et soutenir son développement.
* Assurer la gestion des services en interne.
* S'appuyer sur les équipes de terrain.
* Interdire toute obligation d'usage de services exploitant la vie privée des élèves ou ne respectant pas le RGPD, notamment ceux des GAFAM.
* Informer correctement sur les contraintes réglementaires à respecter par les enseignants et les élèves. 

**EDIT d'Alsace Réseau Neutre** : Depuis l'écriture de cet appel, nous constatons avec joie que des logiciels comme peertube ont été déployés par les académies. C'est un bon début, nous encourageons les équipes concernées à continuer dans ce sens.
