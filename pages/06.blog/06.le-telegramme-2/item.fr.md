---
title: 'Le Télégramme #2'
visible: false
published: true
media_order: 'welcome to sodom.jpg'
date: '21-02-2022 13:04'
---

La newsletter d’Alsace Réseau Neutre. Première publication de l'année 2022. les sujets principaux seront une recherche de volontaires pour tester les accès internets, la recherche de bénévole pour prendre part au fonctionnement de l'association et un retour sur la conférence zéro déchet de Strasbourg. 

## 🎆 Bienvenue en 2022 ! 🎆 
De la part de tout les membres, Alsace Réseau Neutre vous souhaite une libre et heureuse année !
 
## Accès internet
A la recherche de volontaires pour la fibre internet d'Alsace Réseau Neutre
Nous recherchons des volontaires afin d'essayer le nouvel accès à internet par fibre optique bientôt proposé par Alsace Réseau neutre.

Nous recherchons des bêta-testeurs dans les environs de Strasbourg, pouvant se permettre des coupures et souhaitant nous aider dans cette nouvelle aventure 🙂

Évidemment, nous prenons en charge tout les coûts de ces essais.

[Plus de détails ici →](https://forum.arn-fai.net/t/devenir-betatesteur-fibre-optique-pour-arn/5008/15)

Les caractéristiques de la fibre ARN
* Une IP V4 Fullstack
* Débit montant et descendant fixe
* DNS non filtrés
* Ports non filtrés
* Fourniture de modems pré-configurés prenant en charge des fonctions d'auto-hébergement
* En bonus, un VPN Alsace Réseau Neutre

## Adhérents
A la recherche du bénévole perdu

Nous avons des membres mais nous avons besoin de bénévoles. Vous êtes maintenant nombreux à utiliser nos services et nous sommes à court de bras pour continuer à les maintenir en service aussi efficacement que nous le souhaitons.

Si vous souhaitez nous aider et/ou avez des compétences en hébergement et maintenance de services web, ne cherchez plus et contactez-nous sur le forum ou par courriel à contact@arn-fai.net !

[Un petit coup de main ? →](https://forum.arn-fai.net/t/soutenez-arn-en-apprenant-la-technique-a-tout-niveau/5250)

## 📰 Que s'est-t-il passé récemment ?
### Conférence pour Zéro Déchet Strasbourg
Notre nouvel axe de travail autour de la sobriété numérique a été l'occasion de répondre à l'invitation de ZdS pour une petite conférence à ce sujet. Vous pouvez retrouver le lien vers la vidéo ci-dessous.

[Par ici la vidéo →](https://tube.fdn.fr/w/eQSYgM9gEWETcJEEhLuZhb)

![Image du documentaire "welcome to sodom" montrant un africain penché sur un tas d'ordinateurs fixes en vrac sur un sol de terre en plein air](welcome%20to%20sodom.jpg "welcome%20to%20sodom")

Illustration : Documentaire, Welcome to Sodom


### Le libre à l'eurométropole de Strasbourg
ARN & Hackstub se sont réunis avec le directeur du service informatique afin d'engager une discussion autour du logiciel libre pour la ville et ses usager.
