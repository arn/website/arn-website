---
title: 'Les Noces d Étain - 10 ans de hacking'
visible: false
date: '29-05-2023 23:30'
media_order: 'nocesetain.jpg'
---

![10 ans de hacking - Noces d'étain - 2 juin atelier à la semencerie - 3 juin soirée au molodoï](nocesetain.jpg)

**Notre association fêtera, vendredi et samedi, ses 10 ans à la Semencerie et au Molodoï en compagnie du [Hackstub](https://hackstub.eu/home/fr/schedule/nocesdetain) et de [YunoHost](https://forum.yunohost.org/t/celebrating-yunohosts-10-years-birthday-in-strasbourg-with-hackstub-arn-on-june-2nd-and-3rd/22011) (qui fêtent leurs 10 ans également).**

Vous êtes tous et toutes conviées que ce soit pour profiter et/ou pour nous aider.

## Informations pratiques
**Ateliers >** vendredi 2 et samedi 3 juin de 14h à 18h @La Semencerie, 42 rue du Ban de la roche. Ateliers à prix libre

**Soirée >** samedi 3 juin de 20h à 4h @Molodoï, 16 rue du Ban de la roche, Prix: 6€

Paiements uniquement en espèces (pas de CB !).

Aucun comportement déplacé (injures, violences, gestes non consentis etc..) ne saura être toléré à l'égard de toute personne présente dans les deux espaces qui nous acceuillerons.
Nos 5 associations et les lieux qui nous acceuillent sont constitués de bénévoles et se réservent le droit de renvoyer toute personne ne respectant pas ces règles qu'on ne saurait jamais assez rappeler. Un stand de réduction des risques sera disponible lors de la soirée.

En attente de nouveaux WC, seul le Molodoï est en capacité d'accueillir les personnes à mobilités réduites, la Semencerie sera cependant bientôt pleinement accessible à l'étage de plein-pied.

## Programmation


### Du mardi 30 mai au jeudi 1er juin
Nous proposons en après-midi et en soirée, une résidence scénographique à la Semencerie (42 rue du ban de la roche) pour créer les décors de la soirée. Au programme, création d'un mur d'écrans, décorations cyber, etc. Vous pouvez également venir participer à l'aménagement du Hackstub ou prendre des renseignements si vous souhaitez nous aider vendredi ou samedi.

[Plus d'informations sur la résidence](https://pad.sans-nuage.fr/p/libreto+hackstub+sc%C3%A9no+10+ans)

### Vendredi 2 juin (A la Semencerie, 42 rue du ban de la roche)
#### 9h - 12h : Mise en place des ateliers

Si vous êtes disponible, n'hésitez pas à venir nous aider à installer les ateliers.


#### 14h - 18h: Accueil et stand de présentation

Un stand d'accueil et de présentation de nos associations où il sera également possible de consulter de la documentation. Il nous manque encore du monde pour tenir des créneaux.


#### 14h - 18h: Expo interactive

A découvrir en libre accès sur place: 

 - une borne d'arcade de retrogaming
 - des jeux ludo éducatif: GreenBot, L'école Matrice, Terminus, Hex Invaders, CSS dinner, SilentTeacher, Ktouch
 - une zadahost
 - un ordinateur à démonter
 - de l'art et des alternatives numérique


#### 14h - 15h: Network & Magic

Atelier pour découvrir le fonctionnement d'internet.  Prenez chacun et chacune le rôle d'une machine sur internet et affichez ensemble une page web.
A partir de 10 ans.


#### 14h - 17h: Escape Game "Le plan IAccess" (Étape de création)

Venez beta-tester des parties de l'Escape Game créé par Marjorie et Irina sur le thème de l'accessibilité.

L’avènement de lunettes connectées conçue par la société Meta confèrent les pleins pouvoirs et assure une présence hégémonique sur l’ensemble des réseaux au géant du numérique en désuétude. Dans cette société dystopique ultra virtualisée marquée par le règne des métavers, vous vous retrouvez dans de mystérieux locaux et entrez en communication avec une intelligence artificielle dont vous ignorez le dessein. Quelles sont les intentions de cette IA ? Pourquoi a-t-on fait appel à vous ? À vous de le découvrir en recourant à l’esprit d’équipe !


#### 15h - 16h: Création de fakenews

Découvrez à l'aide du debbuger Firefox comment détourner facilement une page web pour en faire une impression-écran.


#### 15h - 16h30: VJing

Trouver des visuels sur des thématiques numériques et Lude, notre Vidéo-Jockey, les mixera sur l'écran.


#### 16h30 - 18h: Livecoding

Initiation au Livecoding par Seb SVDK


#### 16h - 18h: Atelier HOP! Halte à l'Obsolescence et au Pistage

Nos bénévoles sont là pour vous aider au choix à: réparer un équipement, configurer votre navigateur web pour éviter la surveillance commerciale, découvrir des alternatives libres, passer à Linux, libérer votre smartphone Android, découvrir l'auto-hébergement, etc. N'oubliez pas d'amener votre équipement.


#### 14h - 18h: Espace informel

Pimper son vélo ? Découvrir le hacking informatique ? Apprendre à crocheter des serrures ? Créer des décorations pour la soirée du Molodoï ? Tout est possible dans cet espace informel !


#### A partir de 18h: La Groos Stammtisch des 10 ans

A partir de 18h, c'est le repas prévu pour l'ensemble des bénévoles, artistes, etc. On se pose pour manger, boire et communier ses 10 ans en beauté entre nous. Le repas est à prix libre pour les non-bénévoles.

On en profitera probablement pour régler des détails de dernières minutes.


### Samedi 3 juin (A la Semencerie, 42 rue du ban de la roche)

#### 12h - 20h : Mise en place de la scénographie au Molodoï

Nous avons besoin d'aide pour déplacer la scénographie de la Semencerie vers le Molodoï et pour la mettre en place là-bas.


#### 14h - 00h: Accueil et stand de présentation

Un stand d'accueil et de présentation de nos associations où il sera également possible de consulter de la documentation. Il nous manque encore du monde pour tenir des créneaux.


#### 14h - 18h: Expo interactive

A découvrir en libre accès sur place: 
 - une borne d'arcade de retrogaming
 - des jeux ludo éducatif: GreenBot, L'école Matrice, Terminus, Hex Invaders, CSS dinner, SilentTeacher, Ktouch
 - une zadahost
 - un ordinateur à démonter
 - de l'art et des alternatives numérique


#### 14h - 17h: Maquillage Dazzle

Atelier de maquillage pour échapper à la reconnaissance faciale.


#### 14h - 15h: Network & Magic

Atelier pour découvrir le fonctionnement d'internet. Prenez chacun et chacune le rôle d'une machine sur internet et afficher ensemble une page web.

A partir de 10 ans. 


#### 14h - 18h: Atelier HOP! Halte à l'Obsolescence et au Pistage

Nos bénévoles sont là pour vous aider au choix à: réparer un équipement, configurer votre navigateur web pour éviter la surveillance commerciale, découvrir des alternatives libres, passer à Linux, libérer votre smartphone Android, découvrir l'auto-hébergement, etc. N'oubliez pas d'amener votre équipement.


#### 14h - 18h: Espace informel

Pimper son vélo ? Découvrir la sécurité informatique ? Apprendre à crocheter des serrures (lockpicking) ? Créer des décorations pour la soirée du Molodoï ? Tout est possible dans cet espace informel !


#### 18h - 20h: Vin d'honneur

L'heure pour les bénévoles de se sustenter autour de discours célébrant nos 10 ans.


#### 20h à 4h : Soirée Noces d'Étain au Molodoï (19 rue du ban de la roche)
Co-organisée avec Ultratech Records

Attention ! Ce 3 juin au Molodoï, une faille entre cyberespaces sera dévoilée par un équipage de techno-sorcièr⋅e⋅s. 


Cet exploit célébrera les 10 ans d'existence de la triade pirate Hackstub/ARN/Yunohost : une soirée de noces où se mêleront 4 organisations et 8 représentant⋅e⋅s du cyberpunk à l'hyperpop.

 - 20h30 - 21h00 : Papírové Houby
 - 21h30 - 22h00 : Azertype
 - 22h00 - 23h00 : Bisou Magique
 - 23h00 - 00h00 : 1Ka
 - 00h00 - 01h00 : Strass
 - 01h00 - 02h00 : Evil Pony
 - 02h00 - 03h00 : Crash Server
 - 03h00 - 04h00 : Veine

[PLus d'informations sur la page du Hackstub](https://hackstub.eu/home/fr/schedule/nocesdetain)


#### A partir de 4h : Netw'Oh-Yé

Toute aide pour nettoyer le molo et les unités de la Semencerie sont les bienvenues :)
