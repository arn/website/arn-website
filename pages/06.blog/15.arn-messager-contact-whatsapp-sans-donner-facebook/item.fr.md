---
title: 'ARN Messager : Garder contact sur WhatsApp sans donner son téléphone à Facebook (Matrix 3/3)'
visible: false
date: '26-02-2023 17:00'
feed:
    limit: 10
media_order: '2022-12-06_Header_Plan de travail 1.jpg,comment-gafam-generent-des-milliards.jpg,Edward_Snowden-2.jpg,microsoft_graph.png'
---

La solution ARN-Messager permet de **faciliter l'échappée de WhatsApp et Facebook**, tout en limitant les données personnelles que nous y laissons.

## Pourquoi ARN-Messager ?

Notre précédent article [Pourquoi quitter WhatsApp et migrer vers Element+Matrix ?](https://arn-fai.net/fr/blog/pourquoi-whatsapp-vers-matrix) présente l'usage généralisé de WhatsApp by Facebook et explique en quoi cette dérive menace nos libertés individuelles et collectives.

![Logo de WhatsApp versus logo de Element+Matrix stylisés avec la charte graphique ARN](2022-12-06_Header_Plan%20de%20travail%201.jpg "Logo de WhatsApp versus logo de Element+Matrix")
*Logo de WhatsApp versus logo de Element/Matrix*

Devant le mur que représente l'effet de réseau _"tout le monde est sur WhatsApp"_, l'expérience nous a montré que la seule volonté individuelle de changement de réseau de messagerie ne suffit pas. 
Le militant armé de sa détermination, migre un groupe de fidèles de WhatsApp à Signal. Bientôt, remarquant que 90% de l'activité a toujours lieu sur WhatsApp, les fidèles abandonnent peu-à-peu Signal _"j'ai trop d'applis de tchat"_. De plus, les appels à retrouver WhatsApp sont constants : Groupes du boulot, pour organiser les vacances, pour préparer les 30 ans de la frangine, etc. Quitter le _réseau social_ WhatsApp, c'est laisser tomber une part importante de sa vie sociale, devenir dépendant de proches qui y sont, voire passer pour l'associal de service.

_Au-delà de la blagu'ounette tout droit sortie de notre contexte sanitaire récent_, **ARN-Messager** permet à un.e utilisateur.ice d'un réseau, disons Signal, de communiquer sur un autre réseau, disons WhatsApp, sans y avoir de compte ; C'est l'**Interopérabilité** des réseaux. Par exemple Gmail et vos mail pros sont interopérables. Ni WhatsApp ni Signal ne sont interopérables, donc nous avons recours à des passerelles pour enjamber le mur. Ainsi plus personne n'est obligé de donner son numéro de téléphone et celui de ses contacts à Facebook pour pouvoir communiquer avec un groupe social.


## /!\ Avertissement important sur l'usage des Passerelles WhatsApp et Signal /!\

_Avant d'utiliser notre solution, et si vous n'arrivez pas à la fin de cet article, merci de lire a minima cet avertissement._

Notre solution **privilégie la liberté à la sécurité**. Il appartient aux utilisateurs des passerelles de ne pas y mettre de contenus sensibles. Ce n'est pas un service pour lanceur d'alerte ou militant, mais un _réseau social_ rendu interopérable par nos soins.

Notre objectif est de **faciliter la migration des utilisateurs de Whatsapp vers Element+Matrix**, en proposant ARN-Messager comme catalyseur de transition. Le service est mis à l'épreuve depuis plus de deux ans avec succès, mais peut être rendu obsolète par Whatsapp à tout moment. Nous espérons alors que la volonté d'interopérabilité des systèmes de communications annoncée par le législateur européen finira par contraindre les plateformes à une réelle interopérabilité.

**Pour l'utilisateur individuel** : Nous préconisons d'utiliser en **priorité Element+Matrix/Signal**, les passerelles pour quelques groupes Whatsapp récalcitrants, et éventuellement les SMS/MMS (chiffrés avec Silence) pour les messages individuels.

**Pour un collectif :** Ne pas contraindre les membres à l'utilisation de réseaux enfermant leurs utilisateur.ices. Tel qu'expliqué dans la [Charte pour une utilisation modérée des médias sociaux publicitaires](https://arn-fai.net/fr/blog/charte) _"toute information postée sur Facebook ou Twitter doit être accessible par un moyen éthique, par exemple Mobilizon ou Mastodon"_, de la même manière tout groupe de discussion sur un réseau GAFAM tel WhatsApp devrait être accessible sur Element+Matrix / Signal / XMPP / RocketChat / Mattermost, etc. Il nous semble **indispensable** **de travailler de manière active à la migration des membres**. Par exemple en ouvrant des salons exclusivement sur Matrix pour certaines discussions critiques des administrateurs d'une association.

## Principe de fonctionnement

Chaque réseau de messagerie définit d'une manière propre le format et la méthode d'échange des messages, appelés **protocole de communication**. Lorsque ce protocole n'est pas publié, et le code source du logiciel est propriétaire, il est très difficile de développer une application capable de communiquer à la fois sur ce réseau et un autre. Dans le cas de WhatsApp cela est même interdit par les CGU. Il est alors nécessaire d'y créer un compte, et d'utiliser l'application de WhatsApp pour parler le protocole WhatsApp. Cela nous paraîtrait inacceptable de devoir installer Gmail pour envoyer un mail professionnel ou perso, pourquoi devoir installer WhatsApp pour envoyer un MMS ?

**ARN-Messager** est une **solution d’interopérabilité entre** les messageries instantanées **Element+Matrix** (chat.sans-nuage.fr), **WhatsApp** (Facebook/Meta) et **Signal**. Plutôt que d'avoir votre propre compte WhatsApp, vous partagez le compte (numéro de téléphone) détenu par Alsace Réseau Neutre avec les autres utilisateurs de sans-nuage.fr. La solution repose sur un **logiciel de passerelle**, capable de faire passer des messages entre 2 réseaux qui n'utilisent pas le même protocole de communication. Voici [la liste des passerelles existantes](https://matrix.org/bridges/), nous ne proposons pour l'instant que Signal et Whatsapp. Le principe est simple :

* Vous demandez à échanger depuis Matrix vers un groupe WhatsApp.
* Un **_groupe WhatsApp_ est synchronisé avec un _salon Matrix_** par notre logiciel de paserelle. Un tel salon est appelé [**_salon-raccordé Matrix_**](https://wiki.chatons.org/doku.php/services/messagerie_instantanee/matrix?s[]=matrix#salons_raccords_plumbed_rooms)
* La passerelle envoie dans le groupe WhatsApp tout message écrit dans le salon-raccordé. Côté WhatsApp tout message est vu comme provenant du compte/numéro ARN, et préfixé par le pseudo Matrix du rédacteur.
* Inversement un message écrit dans le groupe WhatsApp est envoyé dans le salon-raccordé. Côté Matrix un compte fictif “marionnette” est créé pour chaque compte WhatsApp, donc chaque rédacteur est bien identifié par son pseudo.

**La passerelle ARN-Messager permet une utilisation pseudonymisée de WhatsApp via un compte mutualisé :**

* Les utilisateurs sans-nuage ne donnent plus leur numéro de téléphone ni ceux de leur carnet d'adresses. Cela enlève au groupe Facebook/Meta un moyen de lier leur profil à leur identité civile
* La mutualisation brouille le graphe social : les messages du compte ARN venant de plusieurs comptes Matrix pseudonymisés ils ne peuvent être ajoutés au graphe social de WhatsApp/Facebook.

## Mise en pratique de d'ARN-Messager

Voici à quoi ressemble un salon-raccordé à Signal depuis Element+Matrix :

![Deux bulles d'un salon raccordé entre Signal et Matrix. Le premier messager est envoyé depuis Signal par Nico H : Hello ayant supprimé WhatsApp depuis 1 an j'aimerais utiliser matrix pour rejoindre quelques groupes. Faut-il que je créé un compte matrix sans-nuage ou n'importe quel serveur peut fonctionner ? La réponse est envoyée par Optogram depuis Element+Matrix : Nous proposons notre bridge Whatsapp - Matrix à nos adhérents uniquement. Il faut donc un compte adhérent à notre association et avoir une cotisation à jour pour bénéficier d'un compte sans-nuage.](arn_messager_signal.png "Un salon raccordé à Signal par ARN-Messager.")

Les contacts sur Signal apparaissent suffixés par `(SG)`. Pour WhatsApp c'est `(WA)`.

Si ce n'est pas déjà fait, [créez un compte sans-nuage.fr](https://arn-fai.net/fr/sans-nuage/individuel#comment-proc%C3%A9der) et [prenez en main Element+Matrix](https://pad.lescommuns.org/s/zcqChGuTe#Tutoriel-Matrix). La passerelle est alors très simple d'utilisation, il faut suivre les [instructions données au lien suivant](https://wiki.arn-fai.net/documentation:sans-nuage:whatsapp#passerelle_whatsapp) :

1. Créer un groupe WhatsApp ou Signal
2. Inviter le compte WhatsApp d'arn-fai.net dans ce groupe existant
3. Attendre le message de arn-fai.net
4. Écrire le pseudo à inviter : `!am @pseudo:sans-nuage.fr`
5. Envoyer le lien d'invitation à vos contacts WhatsApp

Félicitations ! Votre salon-raccordé Matrix et le groupe WhatsApp sont maintenant synchronisés par la passerelle :

* Ajoutez-y d’autres comptes Matrix (sans-nuage.fr ou autres instances).
* Tous peuvent converser avec WhatsApp et ajouter d’autres comptes.

## Les protections d'ARN-Messager

[Dans la dernière section de notre premier article](https://arn-fai.net/fr/blog/pourquoi-whatsapp-vers-matrix#notre-solution-avec-element-matrix-est-elle-meilleure-a-vous-de) nous comparions les protections offertes par Element+Matrix vs Signal vs WhatsApp en termes de données personnelles. En résumé, du côté clair, tous les messages qui restent sur Element+Matrix sont chiffrés de bout-en-bout **ET** le graphe social est protégé par la possibilité du pseudonymat. Du côté obscur, les messages sur WhatsApp sont chiffrés de bout-en-bout **MAIS** le graphe social comprenant l'identité civile et le carnet d'adresses est exploité et revendu. Nous essayons de faire reculer le côté obscur. Pour l'utilisateur côté Element+Matrix d'un salon-raccordé WhatsApp, voilà le niveau de protection offert :

- ❌ Juridiction aux Etats-Unis via le compte WhatsApp d'ARN
- ❌/✅ Chiffrement de bout-en-bout levé au niveau du serveur de la passerelle (sans-nuage.fr) et dans l'application WhatsApp sur Android (partition chiffrée)
- ✅ Inscription pseudonymisée sur Matrix et pseudonymat sur Whatsapp via le numéro ARN
- ✅ Pas besoin de donner son numéro de téléphone et l'accès à ses contacts sur Android
- ❌ Les numéros de téléphone d'un groupe WhatsApp sont exposés sur WhatsApp ET dans le salon-raccordé Element+Matrix en cas d’intrusion

Ce bilan mitigé explique notre encouragement à ne pas voir ARN-Messager comme une solution pérenne, mais comme un outil de transition vers Element+Matrix. On remarquera que le chiffrement de bout-en-bout ne sera pas possible dans un salon-raccordé tant que WhatsApp ne sera pas forcé à s'interopérer. Pour modérer cet état, précisons que le chiffrement de bout-en-bout est principalement utile lorsque l'utilisateur ne fait pas confiance à l'administrateur de son serveur :

- pour ne pas regarder et revendre soi-même les données des messages.
- pour protéger ses données d'un attaquant tiers

Dans le cas d'**Alsace Réseau Neutre**, nous considérons que :

- les administrateurs, bénévoles militant pour la défense de la vie privée sur internet, n'ont ni le temps ni l'envie de regarder et revendre les données
- notre serveur est suffisamment petit et fondu dans la masse de serveurs équivalents pour ne pas attirer l'attention de hackers malveillants.

## Pistes d'amélioration

- Activer le chiffrement "de l'utilisateur à la passerelle" --> fait, à tester
- Supprimer automatiquement l'historique des messages --> [en cours](https://forum.arn-fai.net/t/protection-des-donnees-sur-element-matrix/7508)
- Utiliser plusieurs numéros de téléphones pour éviter de se faire blacklister par WhatsApp --> en cours
- Limiter la fédération des salons-raccord pour éviter une intrusion --> [en discussion](https://forum.arn-fai.net/t/amelioration-de-lexperience-utilisateur-element-matrix/7533/2)
- Conserver les messages un temps minimum sur la VM WhatsApp, de l'ordre de quelques minutes
- Diminuer les métadonnées conservées sur Matrix
- Des suggestions ?

## Préconisations techniques d'interopérabilité pour le législateur européen
A la lueur de notre compréhension acquise par la pratique, nous préconisons au législateur européen et français de rendre obligatoire l'interopérabilité des très grandes plateformes (45 millions d'utilisateurs, c’est-à-dire un nombre équivalent à 10 % de la population de l’UE). Cette interopérabilité doit être implémentée par la possibilité, même pour de petits acteurs, de mettre-en-place une [passerelle de type serveur-à-serveur](https://wiki.chatons.org/doku.php/services/messagerie_instantanee/matrix?s[]=matrix#passerelle_serveur-a-serveur) tel que c'est déjà le cas entre les réseaux IRC (libera.chat), et Matrix (matrix.org). Et les protocoles doivent être suffisamment ouverts pour permettre le chiffrement de bout-en-bout à travers les passerelles. Ainsi vous n'aurez plus besoin ni d'ARN, ni de faire confiance à ses administrateurs, et nous pourrons retourner à des activités normales, à ciao bon dimanche !

### Pour réagir à cet article

**Rendez-vous sur le** [**sujet dédié de notre forum**](https://forum.arn-fai.net/t/libre-communication-trilogie-matrix-episode-3-arn-messager-garder-contact-sur-whatsapp-sans-donner-son-telephone-a-facebook/7582)