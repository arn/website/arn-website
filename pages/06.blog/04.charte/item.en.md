---
title: 'Charter for a moderate use of social media advertising'
visible: false
published: true
summary:
    size: 300
date: '14-06-2021 18:43'
---

**Full credit to Résistance à l’Agression Publicitaire**

Advertising on social media is not an unbiased tool. Like other advertising mediums, their use implies receipt of unsolicited commercial advertising on the user. As a matter of fact, these media portray users and trade their personal information for advertising targeting purposes. Therefore, we recommend you comply with the following principles to prevent misapplications:

## 1 - Do not promote advertising mediums
Using these media to target their users seems clever, but you can use them without promoting these social media. As an example, avoid putting them forth in your own communication support like your flyers or websites.

## 2 - Avoid exclusive content on advertising mediums
Publishing exclusive content on advertising social networks is an indirect way to encourage people to use them. When you post on advertising social networks, make sure that the information is accessible through other mediums like your website or free and non-advertising social networks. Exceptions are public comments and appeals.

## 3 - Do not use sponsored links
Paying to show one's publications off means playing a part in the advertising system and in the attention economy.

Source: https://antipub.org/resistance-a-l-agression-publicitaire-s-infiltre-dans-les-reseaux-sociaux/

**Share with organizations that feel bound to use private social media.
