---
title: 'Charte pour une utilisation modérée des médias sociaux publicitaires'
visible: false
published: true
media_order: RAP.jpg
summary:
    size: 200
date: '14-06-2021 18:43'
---

**Charte proposée par Résistance à l’Agression Publicitaire**

[Texte original sur le site du RAP](https://antipub.org/resistance-a-l-agression-publicitaire-s-infiltre-dans-les-reseaux-sociaux/)

Les médias sociaux publicitaires ne sont pas des outils neutres. Comme d’autres médias publicitaires, la contrepartie pour leur utilisateur est la réception de publicités mais pas seulement. En effet, ces réseaux profilent les utilisateurs et font commerce de leurs informations personnelles à des fins de ciblage publicitaires. En cela, leur utilisation n’est pas anodine et nous recommandons de la cadrer par quelques principes : 

## 1 – Ne pas faire la promotion des réseaux sociaux publicitaires
S’il semble stratégique d’utiliser ces médias pour s’adresser aux gens qui les utilisent, ce n’est pas une raison pour les mettre en avant ou les légitimer. Il est possible de les utiliser et d’y être présent sans appeler à s’y rendre dans ses propres supports de communication (tracts, sites web, etc.).

## 2 – Éviter de mettre de contenu exclusif sur les réseaux sociaux publicitaires
Publier du contenu exclusif sur les réseaux sociaux publicitaires est une manière indirecte d’inciter les gens à leur utilisation. Quand vous publiez sur les réseaux sociaux publicitaires, assurez-vous que l’information soit accessible par d’autres moyens (site, réseau social libre et non publicitaire, etc.). Les commentaires et interpellations publiques constituent des exceptions.

## 3 – Ne pas recourir aux liens sponsorisés
Payer pour mettre en avant ses publications, c’est financer et participer au système publicitaire et à la logique de marché du temps de cerveau disponible.


**A diffuser aux organisations qui se sentent contraintes d'aller sur des médias sociaux privateurs.**
