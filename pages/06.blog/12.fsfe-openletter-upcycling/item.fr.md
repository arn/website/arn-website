---
title: "Installer n'importe quel logiciel sur n'importe quel appareil"
visible: false
date: '23-11-2022 17:00'
published: true
---

![Les 4 logos de la campagne de la FSFE](logo-upcycling.jpg)
*Logo créé par la FSFE*

**C'est la [semaine européenne de réduction des déchets](https://ewwr.eu/fra/). Comme Alsace Réseau Neutre, réclamez le droit d'installer les logiciels de vos choix sur vos appareils.**

Fin mai, Alsace Réseau Neutre a décidé de signer la lettre ouverte de La Free Software Foundation Europe pour le « [Le droit universel d'installer n'importe quel logiciel sur n'importe quel appareil](https://fsfe.org/activities/upcyclingandroid/openletter.fr.html) ». Depuis hier, vous pouvez en faire de même.

<a class="action-btn" href="https://fsfe.org/activities/upcyclingandroid/openletter.fr.html#sign-the-letter" target="_BLANK" role="button">Signer la lettre ouverte</a>

## Pourquoi *nous avons signé*
La création d'un tel droit serait tout à fait en adéquation avec nos revendications pour [vivre libre](/revendications#vivre-libre) et [créer des technologies soutenables](/fr/asso/revendications#créer-des-technologies-soutenables). Plus spécifiquement, ce droit est une condition nécessaire pour « faire fonctionner plus de 10 ans nos équipements numériques ». En outre, il favoriserait grandement « notre réappropriation des technologies ».

Fin 2021, nous tenions une conférence de vulgarisation sur des [pistes pour diminuer notre empreinte écologique dans le numérique](https://tube.fdn.fr/w/eQSYgM9gEWETcJEEhLuZhb) ». SPOILER: augmenter la durée de vie des terminaux, c'est à dire des télévisions, des ordinateurs, des smartphones, etc. est LA priorité. Donc oui, si vous en doutiez encore, il vaut mieux continuer d'utiliser votre vieux coucou qui consomme un peu plus d'électricité que d'être responsable de la fabrication d'un équipement neuf et de la création d'un déchet.

Fier de notre constat, une personne démunie nous a interpellé à la fin de cette conférence en expliquant que malgré toute sa bonne volonté de ne pas renouveler son téléphone, celui-ci ne fonctionnait tout simplement plus par manque de mise à jour constructeur.

Par ailleurs, lors de nos ateliers « [Halte à l'Obsolescence et au Pistage](/agenda) », nous sommes régulièrement confrontés à des smartphones impossibles à réinstaller ou configurer, ainsi qu'à des stores d'applications qui interdisent l'installation dans leur version complète d'outils pour éviter le pistage comme Blokada.

![Bulle de bédé où une personne de l'atelier HOP annonce à un participant "Désolé, on ne pourra pas réinstaller ce smartphone"](le-drame-de-atelier-hop.png)

Enfin, si les écosystèmes étaient plus ouverts, nous serions en mesure d'améliorer le réemploi en trouvant de nouveaux usages aux équipements. Des smartphones avec un écran cassé pourraient par exemple faire de très bon petit serveur d'auto-hébergement, à condition de pouvoir les réinstaller facilement.

## Comment *garantir* ce droit ?

La FSFE détaille dans sa lettre que pour garantir ce droit il faut :
 * que les utilisateurs aient le droit de choisir librement les systèmes d’exploitation et les logiciels tournant sur leurs appareils
 * que les utilisateurs aient le droit de choisir librement les fournisseurs de services auxquels connecter leurs appareils
 * que les appareils soient interopérables et compatibles avec les standards ouverts.
 * que le code source des pilotes, des outils et des interfaces soit publié sous licence libre

## A vous de jouer !
Mais, ce droit ne verra pas le jour si personne ne le réclame et c'est là que vous pouvez entrer en action.

<a class="action-btn" href="https://fsfe.org/activities/upcyclingandroid/openletter.fr.html#sign-the-letter" target="_BLANK" role="button">Signer la lettre ouverte</a>
