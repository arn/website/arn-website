---
title: 'Comprendre la facturation de la connectivité à Internet'
visible: false
media_order: Capture.PNG
summary:
    size: 100
date: '01-01-2016 00:00'
---

Parmi les questions que les personnes qui découvrent les Fournisseurs d'Accès à Internet associatifs se posent, il y a les suivantes :
* Comment est facturée la connectivité à Internet ? 
* Est-ce que je peux effectuer de gros téléchargements ou bien cela compromet l'équilibre financier de l'association ?

Cette page se propose de répondre à ces questions.

## Pas de facturation au volume, mais au débit

Sur Internet, sauf exception, les opérateurs réseau ne se facturent pas entre eux au volume de données échangé. Autrement dit : un-e abonné-e ARN qui télécharge 500 Go (500 gigaoctets) cumulés en un mois ne coûte pas plus cher à l'association qu'un-e abonné-e qui télécharge 500 Mo (500 mégaoctets) cumulés, soit 1000 fois moins, en un mois.

La facturation entre opérateurs porte sur le débit, c'est-à-dire la quantité de données échangée à un instant précis. Ce qui compte, ce n'est donc pas la taille des fichiers téléchargés mais la vitesse à laquelle vous les téléchargez. Exemple : je télécharge ce fichier à 2,5 Mo/s (mégaoctets par seconde) soit 20 Mb/s (mégabits par seconde, couramment nommé « 20 mégas »).

En généralisant : ce qui est facturé à ARN, c'est le débit consommé cumulé de tous-toutes les abonné-e-s à au moins un service de l'association. À chaque intervalle de temps défini (5 minutes, généralement), les prestataires d'ARN mesurent le débit émis et reçu par l'ensemble du réseau d'ARN sur l'intervalle de temps écoulé. Toutes ces valeurs sont stockées et, à la fin du mois, la valeur la plus élevée sera facturée.

Là, vous devriez être abasourdi-e-s : la définition précédente signifie que, si le trafic d'ARN est monté à 80 Mb/s sur 5 minutes une seule fois dans tout un mois mais qu'ensuite, il n'a jamais dépassé 10 Mb/s, alors le débit facturé sera quand même de 80 Mb/s ! Heureusement, la facturation entre opérateurs prévoit un abattement.

Complément d'information : il y a deux débits : un pour la réception, l'autre pour l'émission et plusieurs méthodes cohabitent pour en tenir compte dans le calcul. Par exemple, il serait possible de stocker émission et réception dans deux séries distinctes et de prendre le maximum des deux séries ou encore de faire la somme des débits émis et reçus sur chaque intervalle de temps. Chaque opérateur-fournisseur choisit la méthode qu'il désire. Dans le cas d'ARN, un de nos prestataires prend le max(émission, réception) sur chaque intervalle de temps : si ARN a émis plus qu'elle a reçu, c'est le débit en émission qui est conservé sinon c'est le débit en réception. L'autre prestataire de l'association ne communique pas sur ce point.
 
## Un abattement de 5 %

En réalité, pour lisser la facturation, les pics de consommation sont tronqués en considérant qu'ils ne sont pas représentatifs de l'usage habituel et utile du réseau. Le standard du marché est nommé facturation au 95e percentile : 5 % des mesures du débit les plus élevées sur un mois seront ignorées. La nouvelle mesure la plus élevée après cet abattement sera celle qui sera facturée. Ainsi, si un prestataire mesure le débit consommé toutes les 5 minutes pendant un mois et applique une facturation au 95e percentile, les 36 heures durant lesquelles la consommation a été la plus haute ne seront pas facturées. Comment calcule-t-on ce 36 heures ? Étape par étape :

1. Combien y a-t-il de minutes dans un mois comptable (un mois comptable dure toujours 30 jours) : 60 minutes dans une heure * 24 heures par jour * 30 jours par mois = 43200 minutes dans un mois.
2. Combien y a-t-il d'intervalles de 5 minutes dans un mois comptable ? 5 fois moins qu'il y a de minutes donc 43200 / 5 = 8640 intervalles de 5 minutes dans un mois comptable. C'est le nombre de mesures de débit réalisées par l'opérateur.
3. Combien d'intervalles de 5 minutes seront ignorés dans une facturation au 95e percentile ? 5 % des 8640 mesures réalisées soit 432. Les 432 intervalles de 5 minutes pour lesquels le débit est le plus élevé seront donc ignorés par la facturation. Ces intervalles représentent 432 * 5 = 2160 minutes, soit 36 heures.
4. Attention : il ne s'agit pas de 36 heures consécutives mais de l'addition de 432 intervalles épars de 5 minutes chacun. ;)

Certains très rares opérateurs réseau appliquent d'autres modèles comme le 90e percentile qui consiste à ignorer les 10 % des mesures du débit les plus élevées.
 
## Illustration des concepts présentés

Voici le graphe du trafic réseau émis et reçu par ARN, sur l'un de nos deux prestataires, en mai 2016 : 

![Graphique représentant le transit](Capture.PNG)

On voit clairement que des échanges de 10, 30 ou 37 Mb/s ont eu lieu plusieurs fois dans le mois, qu'ils ont duré entre 20 minutes et plusieurs heures, mais, qu'au final, ce prestataire a facturé 4,1 Mb/s à ARN. C'est ça, l'effet du 95e percentile.

Note : les zones au fond blanc correspondent aux heures ouvrées/travaillées alors que les zones au fond gris correspondent aux nuits & week-end. Ce n'est pas pris en compte dans le calcul du 95e percentile.

## Une histoire de forfait

Une fois que l'on a le débit au 95e percentile, on le multiplie par le prix du mégabit qui a été négocié lors de la signature du contrat, on ajoute la TVA, et l'on obtient le prix final. ARN a signé avec deux prestataires et un mégabit consommé nous revient en moyenne à 5,6 € TTC. C'est une moyenne pas forcément représentative : nous avons deux prestataires dont l'un avec des frais fixes d'acheminement en supplément et le trafic ne s'écoule pas de manière uniforme (50/50) entre nos deux prestataires. Exemple : en mai 2016, 65 % du trafic a été émis ou reçu par le prestataire avec le coût de revient le plus faible.

Si l'on reprend l'exemple illustré ci-dessus, cela nous fait donc : 5,6 € * 4,1 Mb = 22, 96 €. ARN a dû payer 22,96 € à ce prestataire pour le mois de mai 2016.

En réalité, sauf exception, les contrats entre opérateurs prévoient un engagement minimum (commitment en anglais, abrégé commit). Ainsi, ARN a souscrit pour 50 Mb/s de commit auprès de ses deux prestataires. Si l'association consomme mensuellement moins que 50 Mb/s au 95e percentile, elle devra quand même payer pour 50 Mb/s. Si elle consomme plus, elle devra payer le forfait de 50 Mb/s ainsi que le dépassement. Le prix du mégabit sur-consommé (burst en anglais) est généralement facturé plus cher que le mégabit forfaitaire.

La logique derrière ça est que le forfait (commit) permet au prestataire de prévoir l'évolution de son réseau, de réserver / provisionner une partie de la capacité de son réseau pour son client et que si ce client utilise plus que cette réservation, il doit payer le surplus car il y a eu un usage exceptionnel du réseau. Quand l'usage exceptionnel devient la norme, il est plus que temps de renégocier le commit à la hausse.

En mai 2016, chez ARN, nous avons collectivement consommé 4,1 Mb/s au 95e percentile avec un prestataire et 7,3 Mb/s avec le second prestataire. Nous utilisons donc seulement 8 % et 15 % de nos forfaits.
 
## Résumé de la facturation entre opérateurs réseau
Toutes les 5 minutes pendant un mois, on mesure le débit consommé en émission et en réception par un réseau comme celui d'ARN. Chaque opérateur-fournisseur choisit la manière dont il prend en compte l'émission et la réception dans son calcul : dans le cas d'ARN, sur chaque intervalle de temps, un de nos prestataires prend le max(émission, réception), c'est-à-dire la valeur la plus haute entre émission et réception, et l'autre ne communique pas sur ce sujet. À la fin du mois, on trie toutes ces valeurs conservées par ordre croissant. On supprime 5 % des mesures les plus élevées. La mesure la plus élevée restante constitue le débit qui sera facturé. Ce débit est multiplié par le prix au mégabit convenu dans le contrat entre les deux opérateurs. De manière générale, un engagement de consommation minimale est prévu dans ce même contrat et sera facturé même si la consommation réelle est inférieure à ce seuil. Au-delà, une pénalité pour chaque mégabit par seconde sur-consommé est appliquée.

Évidemment, c'est également comme cela que sont facturés les plus gros Fournisseurs d'Accès à Internet commerciaux français d'envergure nationale.
 
## Est-ce qu'une personne qui télécharge beaucoup pénalise l'association ?
De manière générale, non. Pour les raisons suivantes :

* Tout le monde n'a pas encore [une capacité réseau de 100 Mb/s](https://www.numerama.com/tech/155543-la-france-au-44e-rang-mondial-des-vitesses-dacces-a-internet.html) ou plus à son domicile ;
* Plus on télécharge vite, moins longtemps dure le téléchargement. Donc, un gros téléchargement (genre 10 Go) mais rapide constituera un pic qui sera ignoré par la méthode de facturation au 95e percentile ;
* Il est assez difficile de télécharger ou d'émettre de manière continue au débit maximal permis par votre connexion à Internet : engorgement du réseau, vous n'utilisez pas H24 votre connexion à Internet, etc. Même en BitTorrent, il y a des périodes d'inactivité pendant lesquelles vous n'envoyez ni ne recevez quelque chose ou à un débit très inférieur au débit maximal permis par votre connexion ;
* Plus il y a d'abonné-e-s, plus les "excès" sont lissés et noyés dans la masse : tous-toutes les abonné-e-s ne téléchargent pas au même moment mais payent quand même leur service à l'association ;
* Comme nous l'avons vu, les forfaits souscrits par l'association sont très peu utilisés à l'heure actuelle (mi-juin 2016), ce qui laisse de la marge.
 
## Graphes du trafic réseau de l'association

L'association dispose de plusieurs graphes de son trafic global qui sont accessibles à tous-toutes, abonné-e à ARN ou non :

* [Trafic ARN échangé via Cogent](https://zabbix.arn-fai.net/charts.php?form_refresh=1&fullscreen=0&groupid=6&hostid=10085&graphid=914), notre premier prestataire ;
* [Trafic ARN échangé via Interoute](https://zabbix.arn-fai.net/charts.php?form_refresh=1&fullscreen=0&groupid=6&hostid=10085&graphid=1011), notre second prestataire ;
* [Trafic cumulé d'ARN](https://zabbix.arn-fai.net/charts.php?form_refresh=1&fullscreen=0&groupid=6&hostid=10085&graphid=1014) sur nos deux prestataires ;
* [Proportion de trafic sur chaque prestataire, en entrée](https://zabbix.arn-fai.net/charts.php?form_refresh=1&fullscreen=0&groupid=6&hostid=10085&graphid=1015) (reste des Internets vers ARN) ;
* [Proportion de trafic sur chaque prestataire, en sortie](https://zabbix.arn-fai.net/charts.php?form_refresh=1&fullscreen=0&groupid=6&hostid=10085&graphid=1016) (ARN vers le reste des Internets).

!!! Note : les zones blanches correspondent aux heures ouvrées/travaillées alors que les zones grises correspondent aux nuits & week-end. Ce n'est pas pris en compte dans le calcul du 95e percentile.

