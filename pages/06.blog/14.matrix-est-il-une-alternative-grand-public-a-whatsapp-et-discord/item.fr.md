---
title: 'En quoi Matrix est-il une alternative grand public à WhatsApp ET Discord ? (Matrix 2/3)'
feed:
    limit: 10
media_order: 'element.png,messenger_fr.png,nextcloud_talk.png,rocketchat.png,Signal-Logo.svg.png,WhatsApp.svg.webp'
date: '22-05-2023 12:27'
visible: false
---

Dans un premier article, nous expliquons [pourquoi toute personne soucieuse de sa vie privée devrait quitter WhatsApp et pourquoi nous pensons qu'il vaut mieux migrer vers Matrix que Signal](https://arn-fai.net/fr/blog/pourquoi-whatsapp-vers-matrix). 

Dans cet article, nous ne nous attachons pas à [expliquer le client Matrix Element et son utilisation, des tutoriels existants font ça très bien](https://wiki.chatons.org/doku.php/services/messagerie_instantanee/matrix#tutoriels_detailles). Nous insistons plutôt sur les points qui nous sont retournés comme rendant l'utilisation moins intuitive. Et proposons notre stratégie et des actions pour faciliter l'adoption de Matrix.

Dans un troisième article, nous présentons une [innovation développée par notre association Alsace Réseau Neutre pour faciliter la transition de WhatsApp vers Matrix : ARN-Messager](https://arn-fai.net/fr/blog/arn-messager-contact-whatsapp-sans-donner-facebook).

# Les atouts de WhatsApp et Signal

Whatsapp a remplacé les sms/mms/appels dans de nombreux pays. D'abord il y a 10 ans, comme en Espagne où les opérateurs téléphoniques pratiquaient des tarifs prohibitifs sur les sms/mms (forfaits non illimités). Puis, dans les années qui ont suivi, son avance technique, sa large base d'utilisateurs et une expérience utilisateur extrêmement fluide l'ont imposé comme substitut à l’ancien service public de télécommunications.

Quelles sont les fonctionnalités de WhatsApp, généralement vues comme des atouts?

- Plus large base d’utilisateurs « tout le monde a whatsapp »
- Création de compte et ajout de contacts via numéros de téléphone
- Messages personnels et de groupes "toujours chiffrés"
- Appels audio/visio jusqu’à plus d'une dizaine de personnes. Les appels audio sont très
    robustes, rendant whatsapp très populaire dans les pays où les débits
    internet sont faibles comme en Afrique (et en Inde).
- Messages vocaux
- Lecture photo / vidéo dans le fil de la conversation
- Utilisation possible sur ordinateur (WhatsApp web). (Requiert toutefois que le téléphone soit allumé et connecté tous les 14 jours).

Certaines de ces fonctionnalités nous semblent indispensables, d'autres doivent être critiquées. Pour représenter une alternative libre adoptable par le grand public, une application doit reprendre les bonnes fonctionnalités de WhatsApp et combattre ses dérives :

- Expérience utilisateur intuitive pour l'inscription et pour l'ajout de contacts
    - inscription/sauvegarde/restauration facile
- Expérience utilisateur intuitive à l'usage comme tchat et réseau social privé
    - Gestion des clés de chiffrement
    - Ajout de contacts via téléphone ou mail
    - interface épurée et attractive
- Bonne protection des données personnelles
- Choix du fournisseur du service
  - Architecture décentralisée

Les applications populaires de tchat les plus protectrice des données personnelles sont Matrix,
Conversation/XMPP, Signal (mais centralisé), Telegram (mais sources fermées),
threema (mais sources fermées) et wire (mais centralisé) selon le site [securemessagingapps.com](https://www.securemessagingapps.com/).
Il nous semble que Matrix représente le meilleur compromis entre tous les critères. 
De plus, une migration en douceur depuis d’autres systèmes de messagerie doit être possible. Pour cela l'application doit créer/promouvoir l’interopérabilité. L’écosystème Matrix propose un nombre important de passerelles (facebook messenger, whatsapp, telegram…) permettant de connecter entre eux les systèmes de messagerie les plus populaires.

Néanmoins, Matrix est un système de tchat relativement jeune, qui souffre de limites régulièrement critiquées. Nous avons fait le choix de Matrix (face à XMPP) il y a plusieurs années pour l'interopérabilité qu'elle offrait. Et surtout car le code source est entièrement libre et auto-hébergeable. Plusieurs structures membres du collectif CHATONS ont fait le même choix. Nous sommes donc plusieurs à pouvoir travailler ensemble à faire de la solution Matrix ce que nous voulons qu'elle soit. Dans la suite de l'article, nous expliquons les limites actuelles de Matrix et proposons des solutions pour améliorer notre solution de tchat :
- Les problèmes qui doivent être résolus par du développement **TECH**
- Les problèmes qui peuvent être résolus par l'éducation populaire (**EDUCPOP**)
- Les problèmes qui peuvent être résolus par le collectif **CHATONS**

# Problèmes et pistes de solutions
## La complexité de l'inscription à l'App Element et le choix d'une "instance"

WhatsApp et Matrix sont des services de tchat qui fonctionnent selon une architecture client-serveur. Le service offert est constitué d'un couple de logiciels : un _logiciel serveur_ installé sur le serveur d'un hébergeur de services web (on appelle cela une "_instance_") ; et un _logiciel client_, installé sur votre appareil. Votre **App** Element et celle de vos proches communiquent via le protocole Matrix à travers un serveur (Synapse). Lorsque vous installez Element pour la première fois, vous devez choisir :

- un pseudo
- un mot de passe
- une "instance" / un fournisseur de service
- un serveur d'identité (optionnel)
    - numéro de téléphone
    - email

Une complexité vient du fait que plusieurs fournisseurs de service et plusieurs logiciels client existent. Nous conseillons l'application Android Schildichat, et Element sur ordinateur ou appareil Apple. Dans le cadre des services sans-nuage.fr, votre App Element est le client de notre instance Matrix installée sur matrix.sans-nuage.fr. La procédure d'inscription est simplifiée ; Vous utilisez les identifiants uniques de votre compte et le client chat.sans-nuage.fr est préconfiguré sur notre instance matrix.sans-nuage.fr. Il s'agit d'une solution **TECH**.

## L'ajout de contacts

### Annuaire des salons publics

Par défaut, ni numéro de téléphone, ni email ne sont stockés sur notre serveur Matrix. Il est possible de ne pas partager son carnet d'adresses et son numéro de téléphone pour s'inscrire ou ajouter des contacts sur Matrix. Si vous faites ce choix, il est néanmoins impossible d'ajouter directement un contact depuis votre carnet d'adresses. Il faut lui demander son IDentifiant MatriX _MXID_ de la forme _@contact:instance.fr_. A la manière d'un réseau social, il est également possible de découvrir de nouveaux contacts en rejoignant des salons traitant de thématiques qui vous intéressent. Pour cela, chaque instance peut publier un annuaire des salons publics.

### Les serveurs d'identité

Il est possible de découvrir ses contacts étant sur Matrix sans leur demander leur MXID. Pour cela il faut donner accès à son carnet de contacts Android et ses propres coordonnées à un _serveur d'identité_. Matrix.org et Vector.im sont en 2023 les deux seuls serveurs d'identité utilisés, et tout contact enregistré sur l'un est dupliqué sur l'autre. Ils permettent aux gens connaissant votre email ou téléphone de trouver votre MXID. Il est en revanche impossible d'obtenir votre email ou téléphone en donnant votre MXID, sauf pour les employés et "contractors" de New Vector Ltd qui sont en charge de la maintenance de la base de donnée. Si Matrix est un système de tchat décentralisé, on voit donc que l'identité des personnes souhaitant une expérience utilisateur plus fluide est centralisée sur un serveur appartenant à une société.

### Serveurs d'identités pour les CHATONS

Les personnes utilisant Matrix cherchent à l'évidence à protéger leur vie privée. Étant donné la quantité de métadonnées stockées sur la fédération de serveurs Matrix, le pseudonymat y est important. On ne souhaite pas que notre numéro de téléphone ou Email permette à n'importe qui de retrouver notre identité sur Matrix. Et surtout on souhaite éviter que quantité de données personnelles soient stockées sur un serveur d'identité centralisé.

Une solution **TECH** serait de déployer un serveur d'identité pour les serveurs Matrix de confiance du collectif **CHATONS**. Seuls les serveurs de Confiance doivent pouvoir faire des requêtes au serveur d'identité.

Peut-être que la promotion de la découverte par adresse email serait plus appropriée à la préservation du pseudonymat plutôt que la découverte par numéro de téléphone. On pourrait dès lors expliquer simplement que **"Votre adresse email sans-nuage est votre identifiant Matrix"**. Une solution **EDUCPOP**.

## Qualité des appels audio/video

Actuellement, la qualité des appels audio/vidéo dépend fortement du serveur Matrix et de sa bonne configuration. Il est possible de faire des appels 1-à-1 en connexion directe "pair-à-pair", moyennant la divulgation de votre adresse IP à votre interlocuteur. En supposant que votre opérateur téléphonique soit de confiance, une solution **EDUCPOP** est de privilégier les téléphoniques. Pour les adictes à l'image, ou pour les appels de groupe, il faut privilégier des Visio en wifi avec un logiciel de visioconférence. Une  solution **TECH** et valable aussi pour les appels de groupe est d'installer le logiciel serveur Element Call. 

A long terme il sera peut-être possible de faire des appels audio dans un salon Matrix via un [module intégrant le logiciel mumble](https://github.com/vector-im/element-web/issues/7487#issuecomment-1149772657) qui est très léger et performant.

## La difficulté indispensable de la gestion des clés de chiffrement

L'aspect fédéré et réellement multi-appareils de Matrix augmente la complexité de la gestion des clés de chiffrement. Vos clés ne peuvent être stockées uniquement sur votre téléphone mais doivent être partagées entre vos différents appareils via le chiffrement croisé. Dans le cas où vous perdiez la connexion à tous vos appareils, une clé de sécurité peut permettre de déchiffrer les anciennes conversations. C'est pourquoi lors de votre première connexion, il faut sauvegarder la clé de sécurité dans un endroit sûr, comme une base de mots-de-passe. Nous ne pouvons le faire pour vous, sans quoi nous serions en mesure de déchiffrer vos messages. Le grand public est habitué à avoir une méthode de récupération de mot-de-passe lorsqu'il en égare un. Une solution **TECH** consisterait à utiliser le mot de passe du compte sans-nuage pour sécuriser le chiffrement. Mais cela diminuerait fortement la sécurité et implique que le mot de passe soit assez fort et que la personne ait de bonnes pratiques en terme de gestion de mots de passe.

Nous pensons donc plutôt qu'une **solution EDUCPOP** est nécessaire, pour populariser la bonne gestion des mots de passe et des clés de chiffrement. C'est le pari pris par certaines institutions en Allemagne. Le service des impôts Allemand "Elster" propose une connexion par clé de chiffrement plutôt qu'avec une smartphone.

## Les métadonnées 
Signal est le champion du chiffrement des métadonnées, et cela est indispensable car l'application ne permet pas l'inscription anonyme. Matrix à l'inverse stocke un nombre assez important de métadonnées "en clair" sur les serveurs fédérés. Les métadonnées ne peuvent être chiffrées comme sur Signal car elles doivent être échangées entre serveurs pour que les conversations soient correctement fédérées. Il faut dès lors compter sur la confiance en les serveurs fédérés pour préserver nos métadonnées de personnes hostiles, et privilégier l'anonymat pour que ces métadonnées aient une moindre valeur. Pour diminuer l'exposition des métadonnées, une solution est de mettre en place une politique de suppression régulière des données. Celle-ci nécessite une coordination entre serveurs de confiance, par exemple ceux du collectif **CHATONS**. On peut imaginer une commande pour supprimer un salon en cas d'intrusion, ou simplement dans le cas où son usage n'est plus d'actualité.

# Expérience utilisateur Matrix vs Signal, WhatsApp, RocketChat et Nextcloud Talk
On peut distinguer deux cas d'usage principaux pour un système de tchat : le tchat personnel type WhatsApp/Signal, et le tchat d'équipe type Discord/Slack. Le protocole Matrix a été développé en prenant en compte tous les cas d'usage possibles, dans le but de pouvoir se connecter à tous les systèmes de tchat via des passerelles. Le client de référence Element se doit dès lors de couvrir tous les cas d'usage. Element est donc un client hybride qui veut tout faire de WhatsApp à Discord, et il le fait plutôt bien, comme on voit dans le comparatif ci-dessous. 

![Aperçu rapide des systèmes de messagerie instantanée, 01/02/2023](messenger_fr.png "Un tableau synthétisant les systèmes de messagerie instantanée pour conversations personnelles ou d'équipe. Un classement est fait du système le plus sûr et protecteur de la vie privée au système le plus privateur de libertés. Les critères sont la structure du réseau (distribué, fédéral, centralisé), l'ouverture du code source côté client et serveur, le chiffrement et la législation qui s'applique selon la localisation du serveur. Du meilleur système de tchat personnel au pire : Briar, Jami, Tox, XMPP, Matrix, E-mail, Wire, Signal, Threema, Telegram, WhatsApp, Skype, WeChat. Pour les tchats d'équipe : Matrix, Rocket.Chat, Mattermost, Zulip, Nextcloud Talk, Webex, Slack, Teams, Voov, Discord." )
[Source freie-messenger.de](https://www.freie-messenger.de/systemvergleich/)

## Element comme tchat personnel
On peut reprocher la complexité de l'interface utilisateur Element par rapport à une appli de tchat personnel telle WhatsApp ou Signal. En effet, le client Element, expose un nombre important de fonctionnalités qui concernent le tchat d'équipe ou qui n'intéressent que des administrateurs ou experts de Matrix. Il existe des clients alternatifs qui proposent une expérience proche de celle de l'app WhatsApp ou Signal, tel SchildiChat, Fluffychat ou Cinny. La nouvelle mouture ElementX devrait également améliorer l'expérience Element sur un certain nombre de points. C'est une solution **TECH**. Une solution **EDUCPOP** est de conseiller le bon client et aider à sa bonne configuration lors d'une install party. 

## Matrix pour l'animation de collectifs

Matrix comme tchat d'équipe est différent, et souvent mieux que WhatsApp. Un adhérent denih nous explique son expérience :


> Matrix est made in France, cocorico !!
> Un collectif peut être organisé au sein d'un "Espace" pouvant regrouper des salons publics ou privés. Une organisation peut entrer en relation avec des utilisateurs Element invités (n'ayant pas de compte), mais aussi des utilisateurs de Whatsapp et Signal grâce aux passerelles. De plus, un salon peut être commun à plusieurs espaces.
> Exemple d'un groupe local Extinction Rebellion : création d'un Espace avec un salon public d'Accueil des nouvelleaux bridgé à whatsapp & signal, etc. Puis dans le processus de formation et d'implication des militant.e.s sur des actions concrètes de désobéissance civile, ces personnes migrent de Whatsapp à Element pour accéder à des salons privés et chiffrés (adhérant au passage à un CHATONS ;)
> Par cette configuration (Espace/salons), on retrouve l'organisation d'un Discord, Slack ou Mattermost, tout en protégeant mieux la vie privée des utilisateurices Element. Enfin c'est la possibilité de communiquer facilement avec tout un ensemble de communautés, surtout si l'interopérabilités avec d'autres platformes se développe (IRC, XMPP, Slack, Telegram, Facebook Messenger peut-être, etc.).

## Tableau comparatif de l'expérience utilisateur.ice

| -   | WhatsApp ![Logo de WhatsApp](WhatsApp.svg.webp "Logo de WhatsApp") | Signal<br><br> ![Logo de Signal](Signal-Logo.svg.png "Logo de Signal")
 | Matrix: Synapse et Element ![Logo de Matrix](Matrix_logo.svg.png "Logo de Matrix")
 ![Logo de Element](element.png "Logo de Element")
 | Rocketchat ![Logo de Rocket.Chat](rocketchat.png "Logo de Rocket.Chat")
 | Nextcloud Talk ![Logo de Nextcloud](nextcloud_talk.png "Logo de Nextcloud")
 |
| --- | --- | --- | --- | --- | --- |
| **Securité** |     |     |     |     |     |
| End to end encryption | ✅/❌ | ✅   | ✅, optionnel | beta | ❌   |
| Nombre de CVEs (plus, c'est mieux) |     |     | 33  | 17  | 11  |
| Sécurité de Gestion des clés | ✅/❌ Obscure | ✅ Transparent | ✅ |     |     |
| **Facilité d'usage, fonctionnalités : Tchat personnel** |     |     |     |     |     |
| Facilité Inscription | ✅ Numéro téléphone | ✅ Numéro téléphone | ✅/❌ Pseudo+MDP+Instance | ❌ Pseudo+MDP sur chaque instance | Fédération ? |
| Découverte contact par Numéro de téléphone | ✅   | ✅   | ✅/❌ via serveur d'identité | ❌   | ❌   |
| Découverte contact par email | ❌   | ❌   | ✅/❌ via serveur d'identité | ❌   | ❌   |
| Facilité de Gestion des clés | ✅ Obscure | ✅ Transparent | ✅/❌ Gestion sauvegarde clés chiffrement |     |     |
| **Facilité d'usage, fonctionnalités : Tchat d'équipe** |     |     |     |     |     |
| Annuaire des canaux publics | ❌   | ❌   | ✅   | ✅   | ❌   |
| Annuaire des membres | ❌ Contacts Android | ❌ Contacts Android | ✅/❌ Suggestions uniquement | ✅   | ❌   |
| Groupement de canaux de discussions | [✅/❌](https://www.theverge.com/2022/4/14/23024703/whatsapp-communities-admin-group-messaging-feature) | ❌   | ✅ (Espaces) | ✅ (Teams) | ❌   |
| Personnalisation icône de canal | ✅   | ✅   | ✅   | ✅   | ❌   |
| Réponse dans Fil de discussion | ❌   | ❌   | ✅ | ✅   | ❌   |
| **Visioconférence** |     |     |     |     |     |
| Logiciel |     |     | Element Call, Jitsi Meet (ou P2P en 1:1) | Jitsi Meet | [Spreed WebRTC](https://github.com/strukturag/spreed-webrtc) |
| Nombre de contributeurices sur le dernier mois |     |     | 21  | 21  | 2   |
| Qualité appels mobiles | ✅   | ✅   | ✅/❌ dépend serveur | ?   | ?   |
| Qualité visio mobiles | ✅   | ✅/❌ | ✅/❌ dépend serveur | ?   | ?   |
| **Interopérabilité** |     |     |     |     |     |
| Fédération | ❌   | ❌   | ✅   | alpha | ❌   |
| Import/Export | ❌   | ❌   | beta | ✅   | ❌   |
| **Santé communauté open source** | ❌   |     |     |     |     |
| Nombre de ⭐️ sur Github | ❌   | 23K (Android), 13K (Desktop), 8K (Server) | 9,6K⭐️ (synapse) 8,4K⭐️ (element) | 32,5K⭐️ | 1,3K⭐️ |
| Activité | ❌   | [🔗 GitHub](https://github.com/signalapp/Signal-Android/pulse/monthly) | [🔗 GitHub](https://github.com/vector-im/element-web/pulse/monthly) | [🔗 GitHub](https://github.com/RocketChat/Rocket.Chat/pulse/monthly) | [🔗 GitHub](https://github.com/nextcloud/spreed/) |
| Nombre de contributeurices sur le dernier mois | ❌   | 9   | 61  | 51  | 9   |
| Nombre de tickets ouverts / fermés sur le dernier mois (16 mai>juin) | ❌   | 23/50 (02/2023) | 80/111 (synapse) et 152/491 (element) | 61/46 | 19/15 |
| Qualité du code (L1 à L5, L5 meilleur)[source](https://selfhosted.libhunt.com/) | ❌   |     | L3 (Synapse) | L3  | Non évalué |
| **Applications** |     |     |     |     |     |
| Bureau | ✅/❌ QR code mobile | ✅ QR code mobile ou Signald | ✅ Natif | ✅ Natif | ❌   |
| Web | ✅/❌ QR code mobile | ❌   | ✅   | ✅   | ✅   |
| Mobile AppStore et PlayStore | ✅   | ✅   | ✅   | ✅   | ✅   |
| Note / Download PlayStore | 4.3 / 5000M | 4.3 / 100M | 4.1 / 500K | 4.3 | 3.7 |
| Mobile F-Droid | ❌   | ✅ Langis, MollyIM | ✅   | ❌   | ✅   |
| Notifications Push Mobile | ✅   | ✅   | ✅   | ✅ (avec subscription pro, ou custom deployment) | ❌   |
| **Documentation** |     |     |     |     |     |
| Centre de doc Météo |     |     | ❌   | ✅   | ❌   |
| Documentation officielle |     |     | [Doc user, Doc admin et doc hébergeur](https://matrix.org/docs/guides) | [Doc user, Doc admin, Doc hébergeur](https://docs.rocket.chat/) | [Doc user](https://docs.nextcloud.com/server/latest/user_manual/en/talk/index.html) |
| **Eco-conception** |     |     |     |     |     |
| Empreinte écologique du langage de programation du serveur (basé sur[cette analyse](https://torbjornzetterlund.com/how-environmental-friendly-is-programming-languages/)) |     |     | élevée (python) | moyenne (js) | élevée (php) personnelles |
| Smartphone non obligatoire | ❌ VM Android | ❌/✅ via Signald | ✅   | ✅   | ✅   |
| Version Minimale Android |     | 5.0+ | 5.0+ |     |     |

* * *

👉 Comparatif inspiré/adapté de [cet article sur le blog d'IndieHosters](https://indiehosters.net/blog/2022/06/21/matrix-vs-rocketchat-vs-talk.html)

* * *

# Pistes pour encourager la "transition" vers Matrix

- Parler de Matrix autour de soi, en expliquant avantages & inconvénients
- Sensibilisation à l'autodéfense numérique (éduc'pop) -> avoir à dispo des brochures, tuto, wiki, posters, jeux, ateliers hacking, formation à travers des organismes locaux, etc.
- Encourager l'adhésion à un CHATONS hébergeant Matrix plutôt que de créer un compte sur matrix.org. Cela permet un accompagnement par une communauté locale pour la prise-en-main d'une suite d'outils alternatifs complémentaires (mail, RSS, Mumble, OpenSondage, NextCloud, OnlyOffice, etc.)
- Organisation d'ateliers de prise-en-main de Matrix. Configuration pour une utilisatoin en tchat personnel ou d'équipe. Visites guidées des instances selon les envies et centre d'intérêt de chacun (maparrainages)
- Se rapprocher de collectifs (locaux) cherchant à améliorer leurs usages, à se dégafamiser, à utiliser les services des CHATONS - quelle manière de faire vous avez habituellement ?
- Participer à l'amélioration de Matrix comme commun numérique en s'impliquant auprès d'Alsace Réseau Neutre **EDUCPOP** ou du collectif **CHATONS**, ou en contribuant aux logiciels **TECH**. 

Pour réagir à cet article :
* [Forum des CHATONS](https://forum.arn-fai.net/t/libre-communication-trilogie-matrix-episode-2-en-quoi-matrix-est-il-une-alternative-grand-public-a-whatsapp-et-discord/7884)

Pour approfondir et contribuer :
* [Forum des CHATONS](https://forum.chatons.org/t/matrix-donner-plus-de-visibilite-a-ce-service-supporte-par-des-chatons/1434)
* [Forum d'Alsace Réseau Neutre](https://forum.arn-fai.net/t/amelioration-de-lexperience-utilisateur-element-matrix/7533)
* [En français](https://wikilibriste.fr/fr/debutant/communications#messagerie)
* [En Allemand](https://www.freie-messenger.de/)