---
title: 'Nos réponses à la consultation au sujet de la vidéo surveillance automatisée'
visible: false
media_order: monitoring-1305045_960_720.jpg
date: '24-03-2022 13:03'
---

La CNIL ainsi que La Quadrature du net ont organisé une consultation sur les dispositifs de vidéo-surveillances automatisées, c’est à dire des systèmes qui analysent les images vidéos pour en extraire des éléments qui s’y trouvent. Nous publions ici nos réponses qui ont été transmises à La Quadrature du net.

![Image de 2 caméras de vidéo surveillance](monitoring-1305045_960_720.jpg)


## 1. Pourquoi vous opposez-vous au déploiement de la vidéosurveillance automatisée ?

Ces systèmes de vidéo surveillance automatisées présentent un risque trop grand de dérives liberticides. Ils rendent possible une surveillance généralisée de la population qui a toutes les chances d’étouffer la démocratie, les libertés fondamentales et l'état de droit. Les gouvernements actuels, si l'on considère leurs intentions nobles et pures, ne peuvent pas prédire quels seront les objectifs des gouvernements futurs, l'arrivée des talibans en Afghanistan a bien montré comment une population peut se piéger elle-même avec les technologies trop bavardes, des opposant⋅es et des minorités l'ont payé de leurs vies. 

Si l'on se penche sur les raisons ayant amené à l'utilisation de tels systèmes, plusieurs études pointent la relative inefficacité de la surveillance vidéo. La surveillance automatisée, en plus des dérives futures potentielles, ne permet pas non plus de régler le problème à la source.

Une étude suisse conclut son étude sur l'efficacité préventive des caméras en expliquant que les personnes visées par les caméras dans des quartiers habitués aux trafics de drogue s'adaptaient à leurs emplacements et ajustaient leur fonctionnement en conséquence. Même le rôle dissuasif des caméras n'est pas présent pour les personnes qu'il cherche justement à mettre en garde.

Banaliser ces technologies, c'est aussi banaliser leur emplois dans les pires conditions à travers le monde, par exemple contre la population Ouïghours en chine ou contre les minorités LGBT en Russie.

Nous pourrions arguer que "je n'ai rien à cacher, je n'ai pas peur d'être filmé" mais cela revient à citer Edward Snowden, qui rétorque que " Dire que votre droit à la vie privée importe peu car vous n’avez rien à cacher revient à dire que votre liberté d’expression importe peu, car vous n’avez rien à dire. Car même si vous n’utilisez pas vos droits aujourd’hui, d’autres en ont besoin. Cela revient à dire : les autres ne m’intéressent pas ".

Si on peut être tenté de mettre en place ces système en les encadrant, il n'y aura qu'un pas pour étendre leur champs d'action et finalement basculer dans la génération de meta-données pour l'ensemble de la population.

Pour nous l'existence de ces technologies remettent en cause le principe même de vidéo surveillance qu'elle soit automatisée ou non, car nous constatons qu'il n'y a qu'un pas pour passer de l'une à l'autre. C'est d'autant plus vrai qu'aujourd'hui il n'est pas possible d'avoir accès à une liste fiable des structures privées ou publics employant ces systèmes.

Pour résumer, nous sommes opposés aux dérives actuelles et futurs de ces technologies, et réclamons une véritable politique de marche arrière sur les moyens employés pour surveiller la population.

## 2. Savez-vous si des systèmes de vidéosurveillance automatisée sont déployés dans votre région ? Savez-vous si des actions militantes ont été entreprises contre ce déploiement (documentation, contact d'élus, action en justice, autre) ? 

Nous savons d'après la brochure Pixel 2020 de l'association nationale de la vidéoprotection que Strasbourg dispose d'un système d'aide au visionnage permettant d'analyser les images à l'aide d'un logiciel.

Des réunions d'informations du public ont été organisé dans le but de décrire ce qui est connu et les demandes CADA en cours. Nous savons également que certains syndicalistes de la région ont porté plaintes pour des faits de surveillances des lieux de travail, mais rien n'a été fait à notre connaissance sur la question de la vidéo surveillance automatisée.

## 3. Que pensez-vous de la façon dont le droit encadre actuellement ce sujet ? Qu'attendez-vous des autorités sur ce sujet (CNIL, Parlement, police, justice) ?

Nous pensons que tout projet de loi qui vise à introduire l'autorisation de nouvelles techniques est voué comme les autres dispositifs déjà mis en place à être étendue. Nous attendons donc des futurs projets de lois, des politiques de désarmement de la surveillance, notamment sur le volet de l'espace publique et privée et donc de la vidéo surveillance. Nous pensons que le conseil constitutionnel à des interprétations trop large et qu'il ne fait pas assez barrage.

Par ailleurs, les CNIL européennes sont bien en peine pour faire respecter l'esprit du RGPD censé nous protéger des dérives du secteur privé et de certaines partie du secteur public. Nous réclamons que les entités dédiés aux contrôle du respect du RGPD et au contrôle de la surveillance étatique soient fortement renforcées. Nous constatons que des DPO sont placardisés et démissionnent dans leurs entreprises.

Enfin, concernant l'éducation et la formation, nous considérons que les outils de contrôle parental proposant le suivi GPS de son enfant ou la surveillance d'examen par visio-conférence sont des outils qui habituent les nouvelles générations à des dispositifs à la limite de la légalité, et que la question de l'accoutumance des nouvelles générations aux dispositifs de surveillance devraient être traitées afin d'éviter l'avènement d'un crédit social français auxquels pourrait sans nul doute participer les systèmes de vidéo surveillance automatisée.

*Les bénévoles des associations Alsace Réseau Neutre, FAI et Hébergeur associatif à but non lucratif et Hackstub, rassemblant des usager'e's de l'informatique.*


