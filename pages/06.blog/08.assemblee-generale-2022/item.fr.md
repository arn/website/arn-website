---
title: '✨ Assemblée générale 2022 ✨'
visible: false
date: '20-03-2022 13:01'
---

L'association Alsace Réseau Neutre vous invite à son assemblée générale annuelle, le 9 avril 2022 à 14h, qui se déroulera à la Fabrique, au 91 Route des romains à Strasbourg. 

Ce moment d'échange nous permettra ensemble de faire le bilan de l'année écoulée, proposer les grandes direction de l'année à venir et élire le nouveau conseil d'administration.

Si vous souhaitez y assister, veuillez nous précisez si vous souhaitez participer en ligne ou en présentiel via le lien ci-dessous 🙂

[Renseignez votre mode de participation →](https://sondage.sans-nuage.fr/assembl%C3%A9e-g%C3%A9n%C3%A9rale-alsace-r%C3%A9seau-neutre-2022-1647884662)

## Pour voter durant l'assemblée générale 🗳️
Veuillez vérifier que votre cotisation est à jour sur l'espace membre afin de valider votre droit de vote 😉

[Espace membre →](https://adherents.arn-fai.net/members/login/?next=/members/invoices/)

## Le programme 📅 
🦴 En chair et en os, directement à la Fabrique, située au 91 Route des romains à Strasbourg.
🌐 Simultanément, en ligne, sur le salon dédié à cette adresse : BigBlueButton Alsace Réseau Neutre

Nous organiserons ces 2 heures en abordant ces points :
* Bilan moral de l'association
* Bilan financier de l'association
* Élection du Conseil d'administration
* Choix des personnes nous représentant au CA de la FFDN et des CHATONS
* Les orientations pour l'année 2022/2023

## Pour finir ensemble 🥳
Alsace Réseau Neutre organisera un atelier pour accueillir les personnes motivées à nous aider à maintenir nos services en état et préparer l'évolution de votre association !
Nous avons besoin d’aide à la fois sur les missions techniques mais aussi sur le reste, nous espérons que ce moment convivial permettra de mieux faire connaissance et présenter l'envers du décor à toute personne souhaitant s'investir avec nous !
