---
title: 'Notre participation au décret sur le contrôle parental'
visible: false
date: '19-10-2022 00:00'
published: true
---

Le 5 octobre nous avons envoyé une contribution concernant le [décret sur le renforcement du contrôle parental sur les accès internet](https://www.entreprises.gouv.fr/fr/consultations-publiques/consultation-publique-renforcement-du-controle-parental-sur-acces-internet).

Nous poursuivions au travers de cette participation plusieurs buts, tout en nous cloisonnant à ce qu'il nous semblait possible de demander dans le cadre qu'impose la loi du 2 mars 2022.

Malheureusement, le 14 octobre 2022, la [nouvelle version du décret notifiée sur europa.eu](https://ec.europa.eu/growth/tools-databases/tris/fr/search/?trisaction=search.detail&year=2022&num=694&mLang=fr&CFID=14024435&CFTOKEN=41a81d8cbd342d3f-CE54F491-06DF-E767-016FA65089665ED5) ne nous semble pas concluante.

Ci-dessous, vous trouverez les réponses qu'apportent le nouveau décret à nos remarques.

## Assurer la non-divulgation d'informations privées sur le ou la mineur à ses parents telles que son orientation sexuelle

Aucune disposition n'a été prise pour éviter l'outing par ces logiciels, si ce n'est que le filtrage des siteweb ne semble plus une fonctionnalité obligatoire ! Seul le blocage des contenus mis à disposition par des boutiques d'applications est conservé.

Cela dit, ça ne change pas le problème puisque cette fonctionnalitée peut être incluse de façon optionnelle par les fabricants.

Par ailleurs, nous pouvons légitimement nous demander qui de l'industrie du numérique à rédigé une contribution de sorte que le filtrage de siteweb ne soit plus dans les fonctionnalités obligatoires ?


## Interdire le détournement de ces outils pour collecter des données

C'est raté. Si les fonctionnalités non obligatoires "ne peuvent donner lieu à une collecte de données sur l’utilisateur mineur à des fins commerciales", les fonctionnalités obligatoires elles:

> * Sont mises en œuvre localement sans entraîner une remontée de données à caractère personnel de l’utilisateur mineur vers des serveurs et sans que la configuration du contrôle parental nécessite de créer un compte sur un serveur, **sauf en cas d’accord express de l’utilisateur majeur ou  lorsque cela est techniquement impossible** ;  
>
> * Ne donnent pas lieu à un traitement de données à caractère personnel de l’utilisateur mineur **sauf en cas d’accord express de l’utilisateur majeur ou lorsque cela est techniquement impossible**.

## Alerter sur le détournement possible de ces outils dans le cadre de violence conjuguale

Nous proposions notamment que ces outils bénéficient d'un indicateur clair et directement visible lorsqu'ils sont en fonctionnement.

Mais non, les stalkerwares ont encore de beau jour devant eux.

## Alerter sur l'imperfection des systèmes de filtrage par liste noire
*... et mettre en place de la transparence, des recours et des contre-pouvoirs vis-à-vis de l'édition de ces listes noires*

Nada, il n'y a rien sur le sujet des modalités d'éditions, de recours ou d'audit du code source par la société civile.

## Éviter les solutions à base de DNS menteurs et demander une exemption pour les FAI non concernés
 *Par exemple, les FAI qui fournissent quasi-exclusivement à des publics majeurs (FAI étudiants ou pour entreprise) ou qui ne fournissent pas de box.*

Petite pirouette: finalement le blocage par les FAI a été repoussé à l'écriture d'un autre texte.

## Proposer des solutions alternatives au contrôle parental

Victoire ! Notre proposition de proposer des documentations lors de la vente de smartphone pour mener une politique de prévention sur les risques de l'usage du numérique pour les jeunes a été incluse. 

Reste à voir ce qu'en feront les fabricants.

> Les fabricants de terminaux mettent à la disposition des utilisateurs finals, de manière accessible et compréhensible, quel que soit le support, les informations suivantes :
> [...]
> 2° Des contenus informatifs en matière d’identification et de prévention des risques liés à l’exposition des mineurs aux services de communication au public en ligne, notamment en matière de pratiques addictives, d'harcèlement en ligne ou d’exposition à des contenus inappropriés ;

## Conclusion

Si notre contribution semble effectivement avoir fait évoluer le décret, les forces en présences semblent empécher l'adoption de nos mesures qui nous semblent pourtant relever du bon sens.

Par ailleurs, nous avons oublié de demander d'encadrer le recours à la géolocalisation des jeunes.

Si le sujet vous intérresse, vous pouvez lire l'[intégralité de notre contribution ainsi que des commentaires plus détaillés](https://forum.arn-fai.net/t/consultation-publique-renforcement-du-controle-parental-sur-les-acces-internet/6657).
