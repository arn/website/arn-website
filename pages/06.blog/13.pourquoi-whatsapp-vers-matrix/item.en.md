---
title: "Install any software on any device"
visible: false
date: '23-11-2022 17:00'
published: true
---

![The 4th logo of FSFE campaign](logo-upcycling.jpg)
*Logo created by FSFE*

**It's [European Waste Reduction Week](https://ewwr.eu/). Like Alsace Réseau Neutre, claim the right to install the software of your choice on your devices.**

<a class="action-btn" href="https://fsfe.org/activities/upcyclingandroid/openletter.html#sign-the-letter" target="_BLANK" role="button">Sign the open letter</a>

## Why *we signed*
The creation of such a right would be entirely consistent with our demands for [living free](/claims#living-free) and [creating sustainable technologies](/en/asso/claims#creating-sustainable-technologies). More specifically, this right is a necessary condition to "make our digital equipment work for more than 10 years". In addition, it would greatly promote "our reappropriation of technologies."

At the end of 2021, we were holding an outreach conference on [ways to decrease our digital footprint](https://tube.fdn.fr/w/eQSYgM9gEWETcJEEhLuZhb). SPOILER: increasing the lifespan of devices, i.e. televisions, computers, smartphones, etc. is THE priority. So yes, if you still had doubts, it's better to keep using your old cuckoo clock that consumes a little more electricity than to be responsible for the manufacturing of a new equipment and the creation of a waste.

Proud of our observation, a destitute person called us at the end of this conference explaining that despite all his good will not to renew his phone, it simply did not work anymore because of the lack of manufacturer's update.

Moreover, during our workshops "[Stop Obsolescence and Tracking](/agenda)", we are regularly confronted with smartphones that are impossible to reinstall or configure, as well as with application stores that forbid the installation in their full version of tools to avoid tracking like Blokada.

![Cartoon bubble where a person from the HOP workshop announces to a participant "Sorry, we won't be able to reinstall this smartphone"](le-drame-de-atelier-hop.png)
*"Sorry, we won't be able to reinstall this smartphone"*

Finally, if ecosystems were more open, we would be able to improve reuse by finding new uses for equipment. Smartphones with a broken screen could for example make very good small self-hosting servers, provided they can be easily reinstalled.

## How do we *guarantee* this right?

FSFE details in its letter that to guarantee this right it is necessary that:
 * users have the right to freely choose the operating systems and software running on their devices
 * users have the right to freely choose between service providers to connect their devices with 
 * devices are interoperable and compatible with open standards
 * source code of drivers, tools, and interfaces are published under a free license 

## It's up to you!
But, this right will not come into being if no one demands it, and that's where you can get in on the action.

<a class="action-btn" href="https://fsfe.org/activities/upcyclingandroid/openletter.html#sign-the-letter" target="_BLANK" role="button">Sign the open letter</a>
