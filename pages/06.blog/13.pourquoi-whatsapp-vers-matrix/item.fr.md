---
title: 'Pourquoi quitter WhatsApp et migrer vers Element ? (Matrix 1/3)'
visible: false
date: '10-12-2022 17:00'
feed:
    limit: 10
media_order: '2022-12-06_Header_Plan de travail 1.jpg,comment-gafam-generent-des-milliards.jpg,Edward_Snowden-2.jpg,microsoft_graph.png'
---

**L'application WhatsApp vient de se faire [dérober 500 millions de numéros de téléphone dont 20 millions en France](https://www.journaldugeek.com/2022/11/25/whatsapp-laisse-fuiter-20-millions-de-numeros-francais-comment-savoir-si-vous-etes-concernes/).
Alsace Réseau Neutre vous explique pourquoi vous devriez songer à migrer vers notre messagerie instantannée alternative Element+Matrix, proposée par de nombreux hébergeurs éthiques du collectif CHATONS.**

![Logo de WhatsApp versus logo de Element/Matrix stylisés avec la charte graphique ARN](2022-12-06_Header_Plan%20de%20travail%201.jpg "Logo de WhatsApp versus logo de Element/Matrix")
*Logo de WhatsApp versus logo de Element/Matrix*

## Communication et réseaux sociaux aux mains des monopoles des géants de la Tech

WhatsApp est l’application mobile de discussion instantanée comptant la plus vaste base d’utilisateurs au monde. Avec 2 milliards d'utilisateurs, elle se place devant Messenger (Facebook/META, États-Unis), Wechat (Tencent, Chine), QQ mobile (Tencent, Chine), Telegram (Telegram Messenger, Dubaï) et Snapchat (Snap Inc., États-Unis). Le marché est donc maîtrisé par quelques géants du net, souvent en monopole dans un pays donné. WhatsApp a été rachetée 19 milliards d'euros par Facebook en 2014, qui est donc en situation de monopole à l’échelle mondiale.

![Graphique préseantant les principales sources de revenu des GAFAM en 2017. Par exemple Facebook réalise 98% de son chiffre d'affaires par la publicité. Google 86%. Microsoft, Amazon, Apple respectivement par la vente de logiciel, de biens et services et de hardware](comment-gafam-generent-des-milliards.jpg "Comment les GAFAM gagnent des milliards?")
*Comment les GAFAM gagnent des milliards?*

D'un point de vue juridique, Facebook est une entreprise soumise au [Cloud Act](https://fr.wikipedia.org/wiki/CLOUD_Act), une loi qui force les entreprises américaines à partager leurs données avec les services de renseignements américains. Par ailleurs, l'entreprise a été sanctionnée à de multiples reprises par la Commission nationale de l'informatique et des libertés (CNIL).

D'un point de vue technique, la plupart des applications de discussions instantanées sont [chiffrées de bout en bout](https://fr.wikipedia.org/wiki/Chiffrement_de_bout_en_bout), le serveur et ses administrateurs ne voient donc pas le contenu des messages (textes et médias). Dès lors, nous pourrions nous arrêter à cet unique fait et déclarer que notre vie privée est protégée, ce qui serait une erreur.

Pour commencer, Whatsapp est une solution «centralisée», c'est à dire qu'une seule structure (Facebook/Meta) possède et gère l'ensemble des serveurs qui font fonctionner ladite solution. Autrement dit, la société est reine et son actionnariat peut décider de l'avenir de ce service comme il le souhaite. Les utilisateurices sont captives et n'ont pas leur mot à dire.

A celà, on peut ajouter que la promesse de chiffrement de bout-en-bout est contournée lorsqu'un des destinataires signale un message comme abusif à Whatsapp. En effet, les 4 messages précédents sont alors envoyés à la firme pour vérification. 
De plus, Whatsapp étant un outil propriétaire et privé, les méthodes de chiffrement sont confidentielles. Il est impossible d'auditer le code afin de l'analyser et vérifier les dires de l'entreprise, et par là même, impossible de prouver que des messages ne sont pas affichés en clair, sans chiffrement, sous d'autres circonstances.

En 2013 avant son rachat, WhatsApp perdait 138 millions de dollars pour un revenu de 10 millions. Elle avait 350 millions d'utilisateur.ices et gagnait 1 million d'utilisateur.ices par jour [[1]](https://www.institutionalinvestor.com/article/b14zbh419wytf2/deals-of-the-year-2014-facebooks-data-driven-takeover-of-whatsapp). Le modèle d'affaire de Facebook repose sur l'exploitation et la revente des profils de ses utilisateurs. WhatsApp a accès aux métadonnées, numéros de téléphone, carnets d’adresses, et Facebook ne se gêne pas pour les agréger aux données de son réseau social [[2]](https://www.android-mt.com/news/whatsapp-la-fuite-des-donnees-vers-facebook-ne-concerne-pas-les-francais/110798/). Les métadonnées (qui envoie le message à qui, à quelle heure, etc.) permettent de construire un graphe social qui constitue une mine d'informations pour le profilage comportemental des individus, comme très bien expliqué par Microsoft [[3]](https://support.microsoft.com/en-us/office/how-does-delve-know-what-s-relevant-to-me-048d502e-80a7-4f77-ac5c-f9d81733c385). La véritable valeur de WhatsApp qui justifie l'investissement de Facebook de 19 milliards, c'est de pouvoir lier le profil comportemental à un numéro de téléphone qui peut/doit dans la plupart des pays être lié à l'identité civile de la personne abonnée. Le numéro de téléphone est le candidat parfait pour un numéro unique d'identification à travers tout l’internet.

![Delve, le graphe social de Microsoft 365. Les noeuds du réseau sont représentés par des avatars et des applications de Micosoft 365. Les branches du réseau indiquent les métadonnées échangées.](microsoft_graph.png "Delve : le graphe social de Microsoft 365")
*Delve : le graphe social de Microsoft 365 [[source]](https://support.microsoft.com/en-us/office/how-does-delve-know-what-s-relevant-to-me-048d502e-80a7-4f77-ac5c-f9d81733c385)*

De leur côté, les applications Facebook et Instagram créent un profil extrêmement détaillé du comportement de l'individu et lui donne un identifiant unique (compte Facebook) qui peut théoriquement rester anonyme. WhatsApp met un numéro de téléphone, donc un nom, une identité sur l'identifiant unique, levant donc son anonymat. L'alliance de WhatsApp et Facebook/Instagram est donc diaboliquement performante dans le profilage du comportement des individus. Facebook utilise ces données comportementales pour améliorer l'expérience utilisateur et ses algorithmes de modération des contenus. Le surplus restant est appelé surplus comportemental, ces données supplémentaires collectées volontairement, sont utilisées pour alimenter des modèles de prédictions comportementales (que vais-je acheter demain) qui seront monétisés, par exemple pour la publicité ciblée. Parmi les clients de ce marché et ceux qui achètent ou utilisent les algorithmes de Facebook, on trouve les services secrets, les gouvernements, ou encore des entreprises comme Cambridge Analytica ou Nation Builder, qui promettent aux plus offrant une victoire à l'élection à laquelle il participe. Le surplus comportemental est finalement la marchandise que s'échangent les entreprises à l'ère du capitalisme de surveillance, et qui justifie leur valorisation boursière plus élevée que les géants du pétrole du 20ème siècle.

## Et Signal alors ?

Signal, fondé par un repenti de WhatsApp, a longtemps proposé une alternative intéressante, et même une référence pour les activistes. En effet, elle propose le chiffrement d'une partie des métadonnées et les serveurs étaient localisés en Suisse, perçue comme ayant une législation des plus protectrices de la vie privée. L'inscription se fait néanmoins avec un numéro de téléphone, et même si ce numéro est dit chiffré, l'anonymat ne semble pas possible/garanti [4](https://www.ouest-france.fr/high-tech/telephonie/faut-il-quitter-whatsapp-apres-le-changement-de-ses-conditions-generales-d-utilisation-7127154). Imaginons par exemple qu’une personne malveillante s’introduise dans un large groupe Signal d'activiste. Même si elle n’a aucun contact dans son carnet d’adresses, elle pourra voir tous les numéros de téléphone présents. 

Le financement de Signal ne semble pas en voie de pérennisation, le modèle par don et financement philantropique basculant vers l'intégration de fonctionnalités monétisées. D'abord un système de payement type blockchain, puis une orientation réseau social questionnable, avec l'intégration de stories. Nous partons du principe qu'au bout d'un certain temps le dicton “si c'est gratuit c'est toi le produit” finira par s'appliquer, et donc que tôt ou tard Signal finira par devoir concéder l'un ou l'autre aspect éthique de sa solution pour sa survie économique. On peut enfin évoquer le risque important qu'une solution centralisée de cette ampleur soit victime d'un piratage de grande ampleur, comme cela a été le cas pour WhatsApp. Ou encore qu'elle soit forcée par l'un ou l'autre gouvernement à permettre l'accès au graphe social ou au contenu des messages sans décision judiciaire, via des lois anti-terroristes dévoyées.

## Notre solution avec Element+Matrix est-elle meilleure ? A vous de juger !

La messagerie instantanée est un moyen de communication fondamental, à travers lequel l'individu exerce plusieurs de ses besoins et droits fondamentaux. Etant donnée la criticité des données personnelles qui y sont échangées, il faut une protection élevée qui est complexe sur internet. 
Retenons que le chiffrement de bout-en-bout des messages est certes important pour un individu, mais que ce sont plutôt les métadonnées décrivant les interactions entre les individus qui sont la ressource clé alimentant le capitalisme de surveillance. De part l'effet de réseau, et l'expérience utilisateur.ice fluide de WhatsApp, le coût à payer par un individu pour mettre à l'abris ses métadonnées est immense. Ainsi la lutte contre le profilage comportemental, fondamentale pour notre souveraineté et démocratie, nécessite une action coordonnée plutôt qu'individuelle. 

Pour cela, nous avons développé une stratégie et une solution technique d'échapée de WhatsApp que nous détaillerons dans un 2ème article : ARN-Messager. Elle permet de contourner l'[effet de réseau](https://fr.wikipedia.org/wiki/Effet_de_r%C3%A9seau) "plus y'a de gens sur WhatsApp, plus WhatsApp est attractif". Nous avons fait le choix de [l'application de messagerie instantanée Element, reposant sur le protocole de communication Open Source Matrix](https://usbeketrica.com/fr/article/matrix-le-protocole-decentralise-made-in-france-qui-cartonne-en-ukraine). Son caractère fédéré lui donne un avantage face aux géants centralisés comme WhatsApp et Signal. Dans un 3ème article nous évoquerons l'expérience utilisateur.ice offerte par Element+Matrix et ARN-Messager.

Nous proposons ici un tableau résumant les protections offertes par WhatsApp, Signal et Element+Matrix.


|                                                                          | Element+Matrix                                                    | Signal                                                                                                                                                | Facebook/WhatsApp                                                      |
|--------------------------------------------------------------------------|-------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| Financement                                           | ✅ Hébergement par de multiples structures comme ARN /<br>[Matrix Foundation](https://matrix.org/foundation/)  /<br>[New Vector Ltd](https://element.io/)                                                     | ✅ Dons                                                                                                                                                  | ❌ Profilage et revente des données personnelles                          |
| Juridiction                                       | Selon l'instance              | structure aux USA                                                                                                                                 | structure aux USA, collab NSA ❌                                     |
| Décentralisé                                       | ✅<br><small>copie entre serveurs fédérés</small>              | ❌                                                                                                                                 | ❌                                      |
| Chiffrement des messages et médias de bout-en-bout                       | ✅ audité récemment                                          | ✅ audité en 2014                                                                                                                                | ✅/❌<br><small>Logiciel propriétaire donc non auditable et messages lus par Facebook si indésirables</small> |
| Chiffrement des Métadonnées                                              | ❌                                                               | ✅/❌<br><small>Sealed Sender : rédacteur du message généralement chiffré. Nom, description et membres du groupe chiffré sur le serveur. Seuls participants ont accès</small> | ❌                                                                    |
| Suppression des métadonnées                                                | ❌<br><small>Mais non utilisées d'après CGU ✅</small>                                                               | ✅<br><small>supprimé une fois le message acheminé. Conserve la date d'enregistrement et de dernière utilisation</small>                                                   | ❌<br><small>Et tout est utilisé</small>                         |
| Inscription pseudonymisée                                                | ✅<br><small>par défaut chez ARN</small>                                          | ❌<br><small>téléphone et Accès contacts Android, hashage</small>                                                                                                     | ❌<br><small>téléphone et Accès contacts Android nécessaire à l’usage</small>          |
| Protection des numéros de téléphone d’un groupe en cas d’intrusion       | ✅ <br><small>même si le numéro est optionnellement renseigné                                                               | ❌                                                                                                                                                   | ❌                                                                    |
| Audit du code serveur et client par un expert sécurité indépendant       | ❌ <br><small>mais code Open Source ✅ et utilisé par état français (tchap) => ANSSI? | ✅ en 2014                                                                                                                                          | ❌                                                                    |
| Possibilité d’ajouter un contact sans passer par un serveur d’adresses ? | ✅ <br><small>via son Matrix ID de la forme @pseudo:serveur.fr                                                                | ❌ <br><small>et serveur d'adresses centralisé                                                                                                                                                   | ❌ <br><small>et serveur d'adresses centralisé                                                                    |

### Pour réagir à cet article

**Rendez-vous sur le** [**sujet dédié de notre forum**](https://forum.arn-fai.net/t/libre-communication-trilogie-matrix-episode-1-pourquoi-quitter-whatsapp-et-migrer-vers-element-matrix/7095)
    
### Sources :
* [1] https://www.institutionalinvestor.com/article/b14zbh419wytf2/deals-of-the-year-2014-facebooks-data-driven-takeover-of-whatsapp
* [2] Amende pour échange de données entre WhatsApp et la maison mère Meta violant la RGPD https://www.android-mt.com/news/whatsapp-la-fuite-des-donnees-vers-facebook-ne-concerne-pas-les-francais/110798/
* [3] [Delve : le graphe social de Microsoft 365](https://support.microsoft.com/en-us/office/how-does-delve-know-what-s-relevant-to-me-048d502e-80a7-4f77-ac5c-f9d81733c385)
* https://www.generation-nt.com/actualites/rgpd-fuite-donnees-meta-facebook-amende-dpc-1998984
* Fuite de 500 millions de téléphones dont 20 millions en France https://www.journaldugeek.com/2022/11/25/whatsapp-laisse-fuiter-20-millions-de-numeros-francais-comment-savoir-si-vous-etes-concernes/
* https://arstechnica.com/gadgets/2021/09/whatsapp-end-to-end-encrypted-messages-arent-that-private-after-all/
* [4][Interview de la Quadrature du Net ](https://www.ouest-france.fr/high-tech/telephonie/faut-il-quitter-whatsapp-apres-le-changement-de-ses-conditions-generales-d-utilisation-7127154)
* https://www.securemessagingapps.com/
* https://www.businessofapps.com/data/whatsapp-statistics/
* https://www.statista.com/statistics/258749/most-popular-global-mobile-messenger-apps/
* http://www.1mtb.com/whatsapp-leads-the-global-mobile-messenger-wars-with-44-pc-market-share/
* https://fr.wikipedia.org/wiki/Snapchat
* https://en.wikipedia.org/wiki/WeChat
* https://en.wikipedia.org/wiki/Tencent_QQ
* https://fr.wikipedia.org/wiki/Telegram_(application)
* https://alain-michel.canoprof.fr/eleve/culture-numerique/education-numerique-internet-responsable
* https://matrix.org/legal/identity-server-privacy-notice-1