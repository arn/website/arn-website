---
title: 'Communiqué de presse : Les Noces d Étain - 10 ans de hacking'
visible: false
date: '31-05-2023 15:30'
media_order: 'nocesetain.jpg'
---

![10 ans de hacking - Noces d'étain - 2 juin atelier à la semencerie - 3 juin soirée au molodoï](nocesetain.jpg)

**Ces 2 et 3 juin, Alsace Réseau Neutre, Hackstub et YunoHost, célébreront leurs 10 ans d'existence à la Semencerie et au Molodoï, l'occasion de plonger le temps d'un week-end dans l'univers du hacking strasbourgeois.**

Au menu de ces « Noces d'Étain », une programmation d'ateliers variés en après-midi : réparation d'équipements, découvertes de logiciels libres, escape game, chasse aux trackers des GAFAM\*, maquillage pour déjouer les IA, expo interactive, jeu de rôle Network & Magic, atelier VJing, initiation au livecoding,  etc.

Le samedi soir au Molodoï, la triade pirate Hackstub/ARN/Yunohost en collaboration avec [Ultratech Records](https://www.ultratechrecords.com) regroupera sur scène des artistes de live-coding venant de toute la France : [Papírové Houby](https://soundcloud.com/ph-livecoding), [Azertype](https://www.instagram.com/azertype_/), [Bisou Magique](https://www.instagram.com/bis0u.magiqu3/), Strass, Evil Pony, [Crash Server](https://www.instagram.com/crashserver/) et [Veine](https://soundcloud.com/veinedevenus).

[Programmation détaillée](https://hackstub.eu/nocesdetain)

*Google Apple Facebook Amazon Microsoft

## A propos de la Hackstub, d'Alsace Réseau Neutre et de YunoHost
Leurs membres militent et défendent un numérique soutenable, accessible et hors des logiques du capitalisme de surveillance qui menacent nos libertés fondamentales et la démocratie. Iels s'opposent aux dérives des technologies du numérique mues par les géants du net, et leur utilisation grandissante à des fins de contrôle social ou de répression.

[La Hackstub](https://hackstub.eu), "foyer du hacking" en alsacien,  est un lieu de convivialité autour de la culture et de l'informatique libres. En tant qu'association, elle mène avec ARN un travail d'éducation populaire dans une perspective critique et émancipatrice des technologies, à travers des animations telles que des ateliers, des conférences, des projections, de l'autoformation, etc. Par exemple via les ateliers Halte à l'Obsolescence et au Pistage ou d'accessibilité numérique. [En savoir plus](https://hackstub.eu/home/fr/about)

[Alsace Réseau Neutre](https://arn-fai.net) est une association à but non lucratif qui peut être vue comme une AMAP du numérique qui œuvre ensemble à la construction d'un bout d'internet utopique. ARN propose de nombreux [services en ligne alternatifs](https://sans-nuage.fr), mais aussi des accès internet par fibre optique. En particulier, elle accompagne les particuliers et les associations locales à quitter les services des géants du net. [Espace presse](https://arn-fai.net/fr/asso/espace-presse)

[YunoHost](https://yunohost.org) est un logiciel libre qui propose de jardiner soi-même ses propres données et services en ligne (mail, site web, cloud, chat, réseau social libre, etc.). Il est installable par exemple sur un ancien ordinateur recyclé, ainsi transformé en serveur informatique. YunoHost entend simplifier et démocratiser cette activité longtemps réservée à une élite technicienne. Bien que ce logiciel soit désormais utilisé dans le monde entier, de nombreuses lignes de code de ce dernier ont été écrites en Alsace.

