---
title: 'Nos offres'
body_classes: modular
---
## __Combien *ça coûte* ?__


---

### Groupe sans-nuage

__*51€* / an\*__

<b>40 Go pour vos services</b><br>
<b>Jusqu'à 10 personnes</b><br>
Accès complet aux services pour chaque personne<br><br>

<a href="/asso/adhérer" role="button">Adhérer avec mon asso</a>

---

*\*Adhésion incluse dans le prix. Prix en franchise de TVA (art 293B du CGI), c'est à dire TTC.  Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction.*


