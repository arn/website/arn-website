---
title: 'Qu''est-ce que sans-nuage.fr pour mon asso ?'
body_classes: modular
image_align: left
---

## __Une solution complète *pour votre asso*__

*Sans-nuage.fr pour association* vous propose:
 * un espace Nextcloud de **40Go** pour:
   * **synchroniser vos fichiers** à partager avec vos bénévoles
   * **éditer vos documents** à plusieurs en temps réels
   * **partager des agendas ou des contacts**
   * **gérer vos projets** avec des tableaux kanban
   * **partager vos frais** lors d'évènements
 * une **messagerie instantannée** qui facilite la migration depuis un groupe whatsapp existant
 * des** boites mails**
 * un outil de **création de questionnaire en ligne**
 * des comptes Mobilizon pour **promouvoir vos évènements sur le Fedivers**

Vous pouvez demander **jusqu'à 10 comptes pour vos bénévoles**. Chaque compte donne accès à l'ensemble des services sans-nuage.fr.
