---
title: 'Comment procéder'
body_classes: modular
---
## __Comment *procéder* ?__

---
### 1. Créez un compte sur l’espace membre et demandez le service

Comme la plupart de nos services, celui-ci est réservé aux membres de l'association, il faut donc créer un compte sur l'[espace membre](https://adherents.arn-fai.net/members/register/), puis « [Demander un nouvel abonnement](https://adherents.arn-fai.net/members/login/?next=/members/request_subscriptions/step1) ».

---
### 2. Validation et mise en route

L’équipe bénévole vérifie qu’il reste les ressources nécessaires pour provisionner le service, valide votre demande et met en route le service.

---
### 3. Accès au service et paiement

Une fois votre service mis en place, vous pouvez accéder aux informations et à la documentation le concernant sur la page « [Mes abonnements](https://adherents.arn-fai.net/members/subscriptions/) ». Merci de procéder ensuite au règlement de l’adhésion si ce n’est pas déjà fait, et à la mise en place d’un virement permanent.

---

