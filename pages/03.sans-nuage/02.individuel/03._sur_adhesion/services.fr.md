---
title: 'Éligibilité'
body_classes: modular
size: 'sm'
---

## __Services *sur adhésion*__

L'adhésion permet d'obtenir **1 compte sans-nuage.fr** donnant accès aux services ci-dessous.

!!!! Besoin de plus de compte ? [Découvrir sans-nuage.fr pour les associations](/sans-nuage/group)

---

### Suite Nextcloud
Nextcloud

**Un drive pour vos fichiers**

 * synchronisable avec vos appareils
 * édition bureautique onlyoffice
 * agendas et contacts
 * gestion de tâche kanban
 * partage de frais pour colocation

---

### Chat Matrix
Element et ARN-messager

**Messagerie instantannée**

 * compatible **smartphone ou ordi**
 * conservez vos **groupes Whatsapp ou Signal**
 * chiffrement E2E (sauf avec WA et Signal)

---

### Boites mail
Roundcube

**Une boite mail @sans-nuage.fr**

 * accessible via **webmail**
 * support SMTP, IMAP et POP3
 * filtres SIEVE
 * jusqu'à 100 alias
 * sélecteur + disponible

---

### Sondages
Yakforms

Création de questionnaires en ligne

---

### Agenda public
Mobilizon

Promotion d'évènements via un agenda décentralisé et fédéré

---

### Flux RSS
Tiny tiny RSS

Suivie des nouvelles publications sur les sites web

---


<!--

{#
 * alternative à Gmail
 * capacité **entre 1 et 5go**
 * accessible via **webmail**, IMAP et POP3
 * jusqu'à 100 alias et le sélecteur +
#}

---


### Chat Matrix
Element et ARN-messager

Messagerie instantannée **interopérable**
{#
 * alternative à Whatsapp et Telegram
 * compatible **smartphone ou ordi**
 * conservez vos **groupes Whatsapp ou Signal**
 * chiffrement E2E (sauf avec WA et Signal)
#}

---


### Drive
Nextcloud File + onlyoffice

**Un espace de stockage** pour vos fichiers

{#
 * alternative à Google Drive et Office 365
 * entre 1 et 15Go
 * synchronisable avec vos appareils
 * édition bureautique collaborative
#}

---


### Agenda + contact
Nextcloud Agenda et Contacts

Des agendas synchronisables entre vos appareils

{#
 * alternative à Google Agenda
 * partageable avec vos contacts
 * compatible avec votre app préférée
#}

---


### Partage de frais
Nextcloud Cospend

Pour les **colocations** ou l'organisation d'évènements avec vos proches

{#
 * alternative à Tricount
 * accessible via navigateur ou smartphone
 * chargement de justificatifs possibles
 * répartition automatique des frais
#}

---


### Gestion de tâches
Nextcloud Deck

Suivie des tâches d'un projet avec la **méthode Kanban**
{#
 * alternative à Trello
 * accessible via navigateur ou smartphone
 * piéces jointes possibles
 * sous-tâches cochables
#}

---


### Sondages
Yakforms

Création de questionnaires en ligne

{#
 * alternative à Google forms
 * Publication par liens
 * Création par glisser/déposer
 * Conservation du sondage **pendant 2 ans**
#}

---


### Agenda public
Mobilizon

Promotion d'évènements via un agenda décentralisé et fédéré
{#
 * alternative à Facebook event
 * évènements locaux et en ligne
 * création de page de groupe
#}

---

### Flux RSS
Tiny tiny RSS

Suivie des nouvelles publications sur les sites web

{#
 * alternative à ???
 * abonnements flux RSS et Atom
 * suivie dans un seul endroits
 * organisation en catégorie
#}

-->
