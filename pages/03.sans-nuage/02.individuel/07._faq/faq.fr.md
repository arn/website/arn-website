---
title: 'Foire aux questions'
body_classes: modular
---
## __Foire aux *questions*__

---
### Pourquoi dois-je adhérer ?
Chez Alsace Réseau Neutre, association à but non lucratif sans salariés, vous n’êtes pas client⋅es d’un fournisseur. Vous êtres membre d’un collectif qui s’organise pour construire un bout d’internet utopique et influencer les politiques numériques vers des schémas plus vertueux. Les adhésions nous permettent de proposer ses services et de les financer.

Par ailleurs, nous souhaitons qu’au travers de l’assemblée générale, les personnes abonnées aient un droit de vote concernant l’avenir de leurs services.

---
### Que signifie « Prix en franchise de TVA » ?

La franchise de TVA signifie qu’à ce jour ARN n’est pas soumis à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».

---

### Comment payer ?

Tous les 1er du mois, une facture est éditée et il est attendu que vous la régliez sous 15 jours.

Si vous le souhaitez vous avez la possibilité de régler jusqu'à 12 mois d'avance en une seule fois.

Le règlement est à effectuer par virement bancaire. Les références bancaires se trouvent dans l'espace membre. **N'oubliez pas d'indiquer votre identifiant « ID » + le numéro d'identifiant qui vous a été attribué.**

Si vous n'avez pas le choix, nous acceptons les paiements en liquide à transmettre aux personnes en charges de la trésorerie d'ARN.


---

### Où se trouve le datacenter qui héberge les serveurs d'ARN ?

Il s'agit du datacenter Cogent, situé 46 route de Bischwiller à Schiltigheim, Alsace, France.

---
### Quelle est la politique de sauvegarde des données de connexions (logs) d’ARN pour ce service ?

TODO

Toutefois, nous vous invitons à lire la clause « [Mésusage des services](/asso/cgs) » de nos CGS. Notez que dans le cadre d’une enquête, nous pourrions être contraints légalement de permettre une mise sur écoute de votre VPN.

---
### Comment obtenir de l'aide en cas de problème ?
L'aide peut être obtenue en nous contactant sur le formulaire de contact, le forum, le chat d'entraide et même en présentiel lors des permanences du vendredi soir au hackstub. Il est impératif que vous preniez le temps de décrire votre problème.

Les bénévoles d'ARN essaierons de résoudre votre problème en faisant un diagnostique et potentiellement en vous accompagnant par mail ou chat (exceptionnellement téléphone). 

---

### Comment reproduire ce service ?

Ce service fonctionne grâce à [YunoHost](https://yunohost.org).

---

