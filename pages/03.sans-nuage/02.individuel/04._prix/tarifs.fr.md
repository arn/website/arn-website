---
title: 'Nos offres'
body_classes: modular
---
## __Combien *ça coûte* ?__

---

### Compte individuel

__Inclus avec<br>*l’adhésion à 15€*__

5 Go pour l'ensemble des services*<br><br>

<a href="/asso/adhérer" role="button">Adhérer</a>

---

### A venir en 2023 ###    {.second-box}

__<br>*+ 1€* / an / Go__

_Dans la limite de l'espace disponible pour sans-nuage.fr_

---

_\* Sur demande: nous pouvons répartir les quotas mail et drive comme vous le souhaitez.<br>Prix en franchise de TVA (art 293B du CGI), c'est à dire TTC.  Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction._


