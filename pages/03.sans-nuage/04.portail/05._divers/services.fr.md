---
title: services
body_classes: modular
class: sans-nuage-public
size: 'sm'
---

## Divers

---
### [RSS Bridge](https://rss-bridge.sans-nuage.fr)
RSS Bridge

Créez des flux RSS pour des sites web qui n'en ont pas.

---
### [Raccourcisseur de liens](https://lacontrevoie.fr/services/liens/) ###    {.not-arn}
Rs-short **chez La Contre-voie**

Raccourcir des liens

---
### [Générateur de QRcode](https://qrcode.sans-nuage.fr/)
LibreQR

Générer des QR codes

---

!!!! Devenir membre pour bénéficier du **[compte @sans-nuage.fr](/sans-nuage/individuel)** ou de **[services pour son asso](/sans-nuage/group)**
