---
title: services
body_classes: modular
class: sans-nuage-public
size: 'sm'
---

## Création collaborative

---
### [Pads](https://pad.sans-nuage.fr/)
Etherpad

Prenez des notes à plusieurs.

---
### [Calc](https://pad.exarius.org/sheet/) ###    {.not-arn}
CryptPad Tableur **chez Exarius**

Tableur à plusieurs.

---
### [Slides](https://slides.sans-nuage.fr)
Strut

Créez des slides ou des animations pour votre site.

---
### [Dessin](https://pad.exarius.org/whiteboard/) ###    {.not-arn}
CryptPad Dessin **chez Exarius**

Dessinez à plusieurs.

---
### [Questionnaire](https://pad.exarius.org/form/) ###    {.not-arn}
CryptPad Form **chez Exarius**

Créer des questionnaires en ligne

---
### [Wiki](https://libreto.sans-nuage.fr)
Libreto

Rassemblez vos pads sous la forme d'un Wiki.


---
