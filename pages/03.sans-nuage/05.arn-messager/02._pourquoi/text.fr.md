---
title: 'Pourquoi ARN-Messager'
body_classes: modular
image_align: left
---

## __Communication aux mains du *capitalisme de surveillance*__
L'immense problème WhatsApp by Facebook : 
 * 1er réseau social et tchat avec 2 milliards d'utilisateurs
  * 2ème Facebook Messenger
  * monopole dans de nombreux pays
 * Serveurs centralisés et entreprise siégeant aux Etats-Unis
  * Patriot Act permet aux renseignements américains d'accéder aux données personnelles
 * Facebook créé un profil de votre comportement
  * WhatsApp y ajoute votre identité civile via votre numéro de téléphone
  * Le profil est revendu sur le marché des données personnelles
  * Facebook rentabilise son achat de WhatsApp pour 19 milliards d'Euros
 * Le profil comportemental est ce qui explique la valorisation boursière des entreprises du capitalisme de surveillance

La solution ARN-Messager : 
 * facilite l'échappée de WhatsApp et Facebook
 * limite les metadonnées qu'ils peuvent récupérer
 * propose une stratégie de migration en luttant contre
  * le "lock-in"
  * l'effet réseau
 * s'adresse au grand public

## __Discussion sur la *sécurité d'ARN-Messager*__
A clarifier : Can you add a contact without needing to trust a directory server?

WhatsApp est chiffré de bout-en-bout, mes messages ne sont pas lisibles, ma vie privée est protégée, où est le problème?
 * le code est propriétaire donc il faut faire confiance à WhatsApp et ses employés
 * le code, y compris celui de chiffrement n'a pas été audité récemment par des spécialistes de la sécurité indépendants
 * messages lus par Facebook si "indésirables"
 * métadonnées non-chiffrées
 * appartient à Facebook qui fait commerce des (méta)données
 * WhatsApp a été révélé par Snowden comme collaborant avec la NSA

Signal, une solution pour une sécurité maximale?
 * chiffrement des métadonnées comme Threema, Amazon Wickr Me, Session et partiellement Wire
 * Hashage "mostly" du numéro de téléphone et du carnet d'adresse??

Signal a un avenir incertain : 
 * fondé par un repenti de WhatsApp
 * localisé initialement en Suisse, maintenant USA
 * numéro de téléphone envoyé à un tiers ; obligatoire pour l'enregistrement et la restauration
 * l'application a obligatoirement? accès au carnet d'adresses
  * notons ici que WhatsApp fait "mieux" car l'accès au carnet d'adresse est optionnel, même si l'app devient alors quasi-inutilisable

Matrix est déconseillé par certains experts pour les raisons suivantes => qu'en penser? : 
 * le code n'a pas été audité récemment par des spécialistes de la sécurité indépendants (Signal en 10/2014)
  * => Le code des applications serveur (Synapse) et client (Element) est Open Source, donc on peut vouloir faire confiance à la communauté pour identifier les failles, d'éventuelles Backdoor
  * => Le code de la librairie de chiffrement a été audité par un spécialiste indépendant
 * Le serveur principal est localisé au Royaume-Uni, toutes les juridictions où un serveur fédéré est localisé s'appliquent potentiellement 
  * => une grande majorité des serveurs des CHATONS avec lesquels nous souhaitons approfondir la fédération sont sous juridiction européenne.
 * pas de rapport de transparence (par l'entreprise New Vector? Par la Fondation Matrix.org?)
  * => Nous jugeons plus par les actes et le code que par les mots
 * Informations de contact (Contact info / contacts / identifiers / diagnostics / user content) transmises au [serveur d'identité central ](https://matrix.org/legal/identity-server-privacy-notice-1)
  * => seulement dans le cas où la recherche de contact se fait par le téléphone ou l'email
  * => infos hashées
  * => par défaut les comptes Matrix de sans-nuage sont pseudonimisés, seul un pseudo est renseigné, ni adresse email, ni numéro de téléphone.
  * => nous devrions regarder avec les CHATONS du côté des [serveurs d'identité fédérés](https://matrix.org/docs/projects/other/mxisd)

Source : https://www.securemessagingapps.com/