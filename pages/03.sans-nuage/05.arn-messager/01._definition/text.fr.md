---
title: 'Qu''est-ce que ARN-Messager ?'
body_classes: modular
image_align: left
---

## __Garder contact *en quittant WhatsApp*__

*ARN-Messager* c'est une solution pour :
 * supprimer son compte WhatsApp
 * rester en contact avec WhatsApp
 * migrer en douceur de WhatsApp vers Element
 * aller vers une solution de tchat plus libre et respectueuse de la vie privée
 * agir pour notre souveraineté et notre démocratie

## __Une passerelle entre *réseaux de discussion instantanée*__

*ARN-Messager* c'est un logiciel de passerelle :
 * qui synchronise les messages, images, etc.
 * entre un groupe WhatsApp et un salon Matrix
 * à travers un compte WhatsApp détenu par ARN
 * pour forcer l'interopérabilité des réseaux en situation de monopole
 * parce-que WhatsApp est un réseau verrouillé "lock-in"

## __Une interopérabilité *limitée*__

Les limitations d'*ARN-Messager* :
 * La solution ne peut être chiffrée de bout-en-bout
 * Pas de synchronisation des discussions 1 à 1