---
title: 'Foire aux questions'
body_classes: modular
---
## __Foire aux *questions*__

---
### Si c'est gratuit, c'est moi le produit ?
Ces services en accès libres vous sont offert par notre association financée exclusivement par ses membres. La maintenance est bénévole et les coûts financiers sont offert par l'association sur décision de ses membres.

N'hésitez pas à [faire un don](https://don.arn-fai.net) ou à [adhérer](/asso/adhérer) pour donner votre avis sur les futures évolutions de ces services et bénéficier des [services réservés aux membres](/sans-nuage/individuel).

---
### Pourquoi « sans-nuage » ?
Parce que le Cloud c'est l'ordinateur d'un autre, on ne sait où, géré par on ne sait qui ! A l'inverse, sans-nuage.fr vous encourage à devenir membre de l'association et ainsi d'avoir une part de contrôle sur le serveur que vous utilisez.

Avec sans-nuage.fr, vous savez où se trouve exactement vos données, quelles sont les personnes ayant des accès administrateur et vous avez la possibilité de participer à l'amélioration des services proposés.

---

### Où se trouve le datacenter qui héberge les serveurs d'ARN ?

Il s'agit du datacenter Cogent, situé 46 route de Bischwiller à Schiltigheim, Alsace, France.

---
### Quelle est la politique de sauvegarde de ces services ?

Les données sont sauvegardées de façon chiffrées à Strasbourg chez 2 à 3 membres du conseil d'administration de l'association.

Nous vous encourageons à sauvegarder également de votre côté.

---
### Quelle est la politique de rétention des données de connexions (logs) pour ces services ?

Nous conservons 2 semaines de logs techniques pour les services qui n'ajoutent, ne modifient ou ne suppriment d'informations, soit:
 * Le service de recherche web basé sur searx (les logs ne contiennent pas les mots-clés cherchés sauf si vous utilisez le paramètre `?q=` dans l'adresse du service)
 * Le service d'audio-conférence
 * Le service de slide basé sur Strut (qui fonctionne en local dans le navigateur)
 * Le service RSS-Bridge
 * Le service Portail

Les autres services sont susceptibles d'être loggués plus longtemps selon l'évolution du droit et des jurisprudences sur la rétention des données de connexions. Ce droit étant mouvant nous préférons ne rien indiquer ici de peur d'oublier de mettre à jour l'information.

Par ailleurs, nous vous invitons à lire la clause « [Mésusage des services](/asso/cgs) » de nos Conditions Générales de Services.

---
### Comment obtenir de l'aide en cas de problème ?

Pour commencer, pensez à vérifier le [statut de fonctionnement des services](https://forum.arn-fai.net/t/suivi-des-incidents-et-maintenances-majeures/371/9999).

L'aide peut être obtenue en nous contactant sur le formulaire de contact, le forum, le chat d'entraide et même en présentiel lors des permanences du vendredi soir au hackstub. Il est impératif que vous preniez le temps de décrire votre problème.

Les bénévoles d'ARN essaierons de résoudre votre problème en faisant un diagnostique et potentiellement en vous accompagnant par mail ou chat (exceptionnellement téléphone). 

---

### Comment reproduire ces services ?

Ces services fonctionnent grâce à [YunoHost](https://yunohost.org).

---

