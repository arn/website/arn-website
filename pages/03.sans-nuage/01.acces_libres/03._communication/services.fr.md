---
title: services
body_classes: modular
class: sans-nuage-public
size: 'sm'
media_order: 'SANS-NUAGE 02.svg,ARN 01.png,CHATONS.svg'
---

## Communication


---
### [Votes](https://date.sans-nuage.fr)
Framadate

Organisez des rendez-vous ou des votes.

---
### [Transfert de fichier](https://drop.sans-nuage.fr/)
Lufi

Partagez vos fichiers lourds à l'aide d'un lien.

---
### [Traduction](https://traduire.sans-nuage.fr/?source=fr&target=en)
LibreTranslate

Traduisez un texte

---
### [Audio-conférences](https://audio.sans-nuage.fr)
Mumble

Discutez en audio dans plusieurs salles.

---
