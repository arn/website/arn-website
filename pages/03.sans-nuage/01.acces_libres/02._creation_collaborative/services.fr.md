---
title: services
body_classes: modular
class: sans-nuage-public
size: 'sm'
---

## Création collaborative

---
### [Pads](https://pad.sans-nuage.fr/)
Etherpad

Prenez des notes à plusieurs.

---
### [Wiki](https://libreto.sans-nuage.fr)
Libreto

Rassemblez vos pads sous la forme d'un Wiki.

---
### [Slides](https://slide.sans-nuage.fr)
Strut

Créez des slides ou des animations pour votre site.

---
