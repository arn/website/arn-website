---
title: 'Comment procéder'
body_classes: modular
---
## __Comment *procéder* ?__

---
### 1. Créez un compte sur l’espace membre et demandez le service

Comme la plupart de nos services, celui-ci est réservé aux membres de l'association, il faut donc créer un compte sur l'[espace membre](https://adherents.arn-fai.net/members/register/), puis « [Demander un nouvel abonnement](https://adherents.arn-fai.net/members/login/?next=/members/request_subscriptions/step1) ». Vous êtes libre de ne régler l’adhésion qu’à la réception du service.

---
### 2. Validation et mise en route

L’équipe bénévole vous appelle ou vous contacte par mail pour vérifier la faisabilité du projet, et valide ensuite votre demande d'abonnement.

---

### 3. Pré-configuration

Une fois votre service validé, vous pouvez accéder aux informations et à la documentation le concernant depuis la page « Mes abonnements ». Vous devez dès lors pré-configurer votre machine pour faciliter sa configuration dans notre datacenter.

---

### 4. Pose sur place

L’équipe bénévole convient avec vous d'une date de rendez-vous pour la pose sur place.

---

### 5. Vérification du bon fonctionnement et règlement

Une fois votre service mis en place, vous pouvez accéder aux informations et à la documentation le concernant sur la page « [Mes abonnements](https://adherents.arn-fai.net/members/subscriptions/) ». Merci de procéder ensuite au règlement de l’adhésion si ce n’est pas déjà fait, et à la mise en place d’un virement permanent, ou à défaut de régler plusieurs mois d’avance en une fois.



---

