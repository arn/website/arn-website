---
title: 'Les fonctionnalités'
body_classes: modular
size: sm
---

## __Les *fonctionnalités*__

---
### Terminé le bruit dans votre salon

Votre serveur fera du bruit dans la baie d’ARN, plutôt que chez vous !

---
### Format non standard

En plus des machines rackable au format U, nous acceptons aussi les petites machines non rackables.

---
### Électricité redondée</h3>

Si votre machine le supporte, vous pouvez avoir jusqu'à deux arrivées électriques différentes.

---
### Redémarrage électrique</h3>

Il est possible d'effectuer un **reboot électrique** de votre serveur en le demandant à l'un de nos bénévols, qui le redémarrera entièrement à distance !

---
### Reverse DNS</h3>

Pour l'acheminement de vos mails, il est possible de **personnaliser le reverse DNS en IPv4 et en IPV6**.

---
### Transit IP ou annonce possible</h3>

ARN vous aidera à utiliser vos ressources IP à travers l’annonce de vos IP, ou grâce à un transit IP.



---
