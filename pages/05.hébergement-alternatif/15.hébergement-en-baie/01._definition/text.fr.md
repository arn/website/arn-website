---
title: 'Qu''est ce que l''hébergement en baie ?'
body_classes: modular
image_align: left
---

## __Qu'est-ce que *l'hébergement en baie* ?__

Alsace Réseau Neutre **loue une baie serveur** (_une armoire métallique faite pour stocker des serveurs_) au datacenter Cogent de Schiltigheim. Ce que nous vous proposons, c’est de **sous-louer un espace de notre baie**, pour que vous puissiez y placer votre serveur physique.