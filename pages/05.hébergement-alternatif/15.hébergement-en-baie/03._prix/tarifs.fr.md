---
title: 'Nos offres'
body_classes: modular
---

## __Combien *ça coûte* ?__
---
### Équipement rackable

__*65€* / mois / U__

*Rack 19"*

<b>1 IPv4</b> publique<br>
<b>1 préfixe IPv6</b> publique<br>
<b>Reverse DNS</b> personnalisable<br>
<b>2 arrivées</b> électriques

---
### NUC ou équivalent

__*20€* / mois__

*NUC, HP prodesk mini, etc*

<b>1 IPv4</b> publique<br>
<b>1 préfixe IPv6</b> publique<br>
<b>Reverse DNS</b> personnalisable<br>
<b>2 arrivées</b> électriques

---
### Petite carte ARM

__*6€* / mois__

*Olinuxino, Raspberry pi, etc.*

<b>1 IPv4</b> publique<br>
<b>1 préfixe IPv6</b> publique<br>
<b>Reverse DNS</b> personnalisable<br>
<b>1 arrivée</b> électrique

---

!!! Les offres ci-dessus n'incluent pas de location de serveur informatique. Nous hébergeons votre matériel dans la baie de l'association. Toutefois, il arrive parfois que nous ayons des serveurs à proposer en don ou location, nous contacter pour plus d'infos.

*Prix en franchise de TVA (art 293B du CGI), c'est à dire TTC. Sans engagement. Requiert une [adhésion à l’association](/asso/adhérer). Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction.*


