---
title: 'Foire aux questions'
body_classes: modular
---

## __Foire aux *questions*__

---
### Pourquoi dois-je adhérer ?
Chez Alsace Réseau Neutre, association à but non lucratif sans salariés, vous n’êtes pas client⋅es d’un fournisseur. Vous êres membre d’un collectif qui s’organise pour construire un bout d’internet utopique et influencer les politiques numériques vers des schémas plus vertueux. Les adhésions nous permettent de proposer ses services et de les financer.

Par ailleurs, nous souhaitons qu’au travers de l’assemblée générale, les personnes abonnées aient un droit de vote concernant l’avenir de leurs services.

---
### Que signifie « Prix en franchise de TVA » ?

La franchise de TVA signifie qu’à ce jour ARN n’est pas soumis à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».

---
### Quelle est la politique de sauvegarde des données de connexions (logs) d’ARN pour ce service ?

Nous enregistrons :

 * votre identité
 * les IPs ARN qui vous sont attribuées
 * la période d’attribution

Toutefois, nous vous invitons à lire la clause « Mésusage des services » de nos CGS. Notez que dans le cadre d’une enquête, nous pourrions être contraints légalement de permettre une mise sur écoute de votre VPN.

---
### Puis-je miner des crypto-monnaies consommatrices d’énergie sur le serveur installé ?
Non.

---
### Comment obtenir de l'aide en cas de problème ?
L'aide peut être obtenue en nous contactant sur le formulaire de contact, le forum, le chat d'entraide et même en présentiel lors des permanences du vendredi soir au hackstub. Il est impératif que vous preniez le temps de décrire votre problème.

Les bénévoles d'ARN essaierons de résoudre votre problème en faisant un diagnostique et potentiellement en vous accompagnant par mail ou chat (exceptionnellement téléphone). 

---

### Comment dépanner ou maintenir mon serveur ?
**Redémarrage à distance :** si votre machine plante ou que vous avez commis une erreur qui vous empêche d'avoir un accès distant à celle-ci (mauvaise configuration du pare-feu, machine qui ne redémarre pas comme prévu, etc.), l'association peut la redémarrer électriquement à distance grâce à ses <a href="https://wiki.arn-fai.net/technique:pdu">PDU</a> (multiprises programmables). Il vous suffit simplement de faire votre demande de redémarrage par email aux admins.


**Accès physique :** nous ne distribuons pas de badge permettant d'accéder en toute autonomie à votre machine. Vous serez donc  accompagnés d'un membre de l'équipe technique de l'association lors de chacune de vos interventions sur site.

---
### Puis-je héberger mon propre serveur mail sur cette connexion ?
Oui. La connexion étant neutre, les ports ne sont pas bridés (même le port 25) et vous pouvez définir un reverse DNS. En plus, nous ne listons pas nos IPs comme résidentiels, contrairement à de nombreux FAI.

Attention tout de même à ne pas ternir la réputation des IPs.

---

### Puis-je personnaliser le reverses DNS pour mes IPs ?

Oui en nous envoyant un mail.

Pour l'IPv4, il faut simplement nous indiquer le nom désiré.

Pour l'IPv6, soit vous nous indiquez le nom désiré pour une ou plusieurs adresses, soit nous vous déléguons la zone qui correspond à votre /56, afin que vous gériez vous-même les reverses associés à vos adresses. La deuxième méthode suppose que vous disposez d'au moins un serveur de noms qui fait autorité.

---
### L'IPv6 est-elle disponible ?
Oui.


---

### Quel est le débit ?

Capacité max. : 100 Mbps mutualisés entre tous les services et les membres de l’association, sans aucune limite de volume. <a href="https://arn-fai.net/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-">Est-ce que je pénalise l'association si je télécharge beaucoup de fichiers ?</a>



---
### A quoi ressemble notre demie baie ?


[Une petite photo de notre demie-baie par ici ](https://wiki.arn-fai.net/_detail/benevoles:technique:arn-baie-face-2015-12.jpg?id=benevoles%3Atechnique%3Amatos)



### Comment ça marche ? (Technologies utilisées )

Redondance : nous avons deux routeurs vers/depuis Internet et nous utilisons le protocole CARP (VRRP-like). En cas de panne matérielle ou de maintenance planifiée sur l'un de nos routeurs, votre machine conserve un accès à Internet sans interruption.

Isolation : nous allouons un VLAN à chaque abonné-e. Seuls vos machines et nos routeurs se trouvent dans ce VLAN.

---

### Comment reproduire ce service ?

Ce service nécessite une baie, de la connectivité, la possibilité d'annoncer des IPs, de configurer des prises programmables, des switchs, des routeurs BGP, le protocole Ucarp.

Une part de ces informations sont expliqués sur les liens suivants:

* [Présentation de notre matériel](https://wiki.arn-fai.net/benevoles:technique:matos)
* [Obtention de blocs d'IP et plan d'adressage](https://wiki.arn-fai.net/benevoles:technique:adressage)
* [Configuration du switch](https://wiki.arn-fai.net/benevoles:technique:l2)
* [Routeurs BGP](https://wiki.arn-fai.net/benevoles:technique:routage)
* [Redondance des passerelles réseaux/routeurs avec Ucarp](https://wiki.arn-fai.net/benevoles:technique:carp)
* [A propos des prises électriques programmables (PDU)](https://wiki.arn-fai.net/benevoles:technique:pdu)

Les documentations d'autres FAI de la Fédération des FAI associatifs sont également intérressantes sur ce sujet:
* [la-mezzanine-histoire-dun-centre-dhébergement-associatif-au-format-libre](https://www.aquilenet.fr/actualit%C3%A9s/la-mezzanine-histoire-dun-centre-dh%C3%A9bergement-associatif-au-format-libre/)
* [Configuration réseau de la mezzanine](https://atelier.aquilenet.fr/projects/services/wiki/Librehosting) {lang=en}

---
