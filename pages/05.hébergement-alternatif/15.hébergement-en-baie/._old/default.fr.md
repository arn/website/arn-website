---
title: Hébergement en baie
body_classes: infra
visible: true
routable: true
published: true
page-toc:
  active: false
---

<div class="logo-infra">
        <img src="../../images/INFRA_Titre.svg" alt="Infra hébergement alternatif" class="logo-infra-img"/>
</div>
<div class="infra-title-image">
            <h1>Qu'est ce que <span class="violet">l'hébergement en baie</span>?</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            Alsace Réseau Neutre <strong>loue une baie</strong> (= une armoire métallique faite pour stocker des serveurs) au datacenter Cogent de Schiltigheim. Ce que nous vous proposons, c’est de <strong>sous-louer un espace de notre baie</strong>, pour que vous puissiez y placer votre serveur physique.
            <a href="https://wiki.arn-fai.net/_detail/benevoles:technique:arn-baie-face-2015-12.jpg?id=benevoles%3Atechnique%3Amatos">En savoir plus</a>
        </p>
</div>
<div class="infra-title-image">
            <h2><span class="violet">Fonctionnalités</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <h3>Fini le bruit dans le salon</h3>
            <p>
            Votre serveur fera du bruit dans la baie d’ARN, plutôt que chez vous !
            </p>
            <h3>Format non standard</h3>
            <p>
            En plus des machines rackable au format U, nous acceptons aussi les petites machines non rackables.
            </p>
            <h3>Électricité redondée</h3>
            <p>
            Si votre machine le supporte, vous pouvez avoir jusqu'à deux arrivées électriques différentes.
            </p>
            <h3>Redémarrage électrique</h3>
            <p>
            Il est possible d'effectuer un <strong>reboot électrique</strong> de votre serveur depuis chez vous, et ainsi de le redémarrer entièrement à distance !
            </p>
            <h3>Reverse DNS</h3>
            <p>
            Pour l'acheminement de vos mails, il est possible de <strong>personnaliser le reverse DNS en IPv4 et en IPV6</strong>.
            </p>
            <h3>Transit IP ou annonce possible</h3>
            <p>
            ARN vous aidera à utiliser vos ressources IP à travers l’annonce de vos IP, ou grâce à un transit IP.
            </p>
</div>
<div class="infra-title-image">
            <h2>Nos <span class="violet">offres </span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Équipement rackable 19"</h3>
            <div class="offre-para">
                <p class="offre-title">50€/mois par U</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">NUC ou équivalent</h3>
            <div class="offre-para">
                <p class="offre-title">20€/mois</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                NUC, PC barebone, HP prodesk, etc
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Petite carte ARM</h3>
            <div class="offre-para">
                <p class="offre-title">6€/mois</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                Olinuxino, Raspberry pi, etc.
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
</div>
<p>
Prix en franchise de TVA (art 293B du CGI) et sans engagement.  Requiert une adhésion à l’association (15€/an). Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction.
</p>
<div class="infra-title-image">
            <h2>Comment <span class="violet"> souscrire </span> ?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="timeline">
<div class="timeline-back"></div>
    <div class="timeline-item">
        <h3>1. Créez un compte sur l’espace membre et demandez l'accès au service d'hébergement en baie</h3>
        Comme la plupart de nos services, celui-ci est réservé aux membres de l'association. Il faut donc créer un compte sur <a href="https://adherents.arn-fai.net/members/login/">l’espace membre</a>, puis « Demander un nouvel abonnement ». Vous êtes libre de ne régler le montant de l’adhésion qu'à la réception du service.
    </div> 
    <div class="timeline-item">
        <h3>2. Validation et mise en route</h3>
        L’équipe bénévole vous appelle ou vous contacte par mail pour vérifier la faisabilité du projet, et valide ensuite votre demande d'abonnement.
    </div>
    <div class="timeline-item">
        <h3>3. Pré-configuration</h3>
        Une fois votre service validé, vous pouvez accéder aux informations et à la documentation le concernant depuis la page « Mes abonnements ». Vous devez dès lors pré-configurer votre machine pour faciliter sa configuration dans notre datacenter.
    </div>
    <div class="timeline-item">
        <h3>4. Pose sur place</h3>
        L’équipe bénévole convient avec vous d'une date de rendez-vous pour la pose sur place.
    </div>
    <div class="timeline-item">
        <h3>5. Vérification du bon fonctionnement et règlement</h3>
        Une fois votre service mis en place, vous pouvez accéder aux informations et à la documentation le concernant sur la page « <a href="https://adherents.arn-fai.net/members/subscriptions/">Mes abonnements </a> ». Merci de procéder ensuite au règlement  de l’adhésion si ce n’est pas déjà fait, et à la mise en place d’un virement permanent, ou à défaut de régler plusieurs mois d’avance en une fois.
    </div>
</div>
<div class="infra-title-image">
            <h2>Foire aux <span class="violet">questions</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="infra-question-container">
    <div class="infra-question-box">
        <h3>Pourquoi dois-je adhérer ?</h3>
        <p>
            Chez Alsace Réseau Neutre, association à but non lucratif sans  salariés, vous n’êtes pas client⋅es d’un fournisseur… Vous devenez  membre d’un collectif qui s’organise pour construire un bout d’internet utopique et influencer les politiques numériques vers des schémas plus vertueux. <br><br>
            Par ailleurs, nous souhaitons que les personnes abonnées aient un pouvoir sur l’avenir de leur service.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Que signifie « Prix en franchise de TVA » ?</h3>
        <p>
            La franchise de TVA signifie qu’à ce jour, ARN n’est pas soumise à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».
        </p>
    </div><div class="infra-question-box">
        <h3>Y-a-t’il un engagement ?</h3>
        <p>
            Non. En revanche, les services sont renouvelés tacitement pour une durée de 30 jours chaque 1er du mois.<br><br>
            Si vous ne vous servez plus du service, n’oubliez pas de sauvegarder vos données et de résilier l’abonnement.
        </p>
    </div><div class="infra-question-box">
        <h3>Comment régler le service d'hébergement en baie ?</h3>
        <p>
            Vous bénéficiez d'une période d'essai d'un mois, prenant fin le 1er du mois suivant votre inscription. Après ce mois d'essai, une facturation aura lieu chaque 1er du mois. Le premier règlement est donc attendu au plus tard au courant de la première quinzaine du mois suivant l'inscription, avec possibilité de régler jusqu'à 12 mois d'avance en une seule fois.
            <br><br>
            Le règlement est à effectuer par virement bancaire. Les références bancaires se trouvent dans l'espace membre. N'oubliez pas d'indiquer votre identifiant « ID » + le numéro d'identifiant qui vous a été attribué.
        </p>
    </div><div class="infra-question-box">
        <h3>Où se trouve le datacenter qui héberge les serveurs d'ARN ?</h3>
        <p>
            Il s'agit du datacenter Cogent, situé 46 route de Bischwiller à Schiltigheim, Alsace, France.
        </p>
    </div><div class="infra-question-box">
        <h3>Comment puis-je intervenir sur le serveur ?</h3>
        <p>
            <strong>Redémarrage à distance :</strong> si votre machine plante ou que vous avez commis une erreur qui vous empêche d'avoir un accès distant à celle-ci (mauvaise configuration du pare-feu, machine qui ne redémarre pas comme prévu, etc.), l'association peut la redémarrer électriquement à distance grâce à ses <a href="https://wiki.arn-fai.net/technique:pdu">PDU</a> (multiprises programmables). Il vous suffit simplement de faire votre demande de redémarrage par email aux admins.<br><br>
            <strong>Accès physique :</strong> nous ne distribuons pas de badge permettant d'accéder en toute autonomie à votre machine. Vous serez donc  accompagnés d'un membre de l'équipe technique de l'association lors de chacune de vos interventions sur site.
        </p>
    </div><div class="infra-question-box">
        <h3>Quel est le débit ?</h3>
        <p>
            Capacité max. : 100 Mbps mutualisés entre tous les services et les membres de l’association, sans aucune limite de volume. <br><a href="https://arn-fai.net/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-">Est-ce que je pénalise l'association si je télécharge beaucoup de fichiers ?</a>
        </p>
    </div><div class="infra-question-box">
        <h3>Est-ce que l’association conserve des informations / traces / logs ?</h3>
        <p>
        Nous enregistrons :
        </p>
            <ul>
                <li>votre identité</li>
                <li>les IPs ARN qui vous sont attribuées</li>
                <li>la période d’attribution</li>
            </ul>
            <p>Toutefois, nous vous invitons à lire la clause « Mésusage des services » de nos CGS. Notez que dans le cadre d’une enquête, nous pourrions être contraints légalement de permettre une mise sur écoute de votre IP.
            </p>
    </div><div class="infra-question-box">
        <h3>Puis-je miner des crypto-monnaies consommatrices d’énergie sur le serveur installé ?</h3>
        <p>
            Non.
        </p>
    </div>
</div>
<div class="natta-help" style="text-align:center">
            <p class="aidez_nous">Nos actions vous plaisent ? Aidez-nous :)</p>
            <p><b>Venez à la prochaine réunion d’accueil des bénévoles</b></p>
</div>