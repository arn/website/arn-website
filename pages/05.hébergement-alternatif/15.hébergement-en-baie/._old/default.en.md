---
title: Rack hosting
body_classes: infra
visible: true
routable: true
published: true
page-toc:
  active: false
---

<div class="logo-infra">
        <img src="../../images/INFRA_Titre.svg" alt="Infra alternative hosting services" class="logo-infra-img"/>
</div>
<div class="infra-title-image">
            <h1>What is <span class="violet">rack hosting</span>?</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            Alsace Réseau Neutre <strong>rents a rack</strong> (= a metal cabinet that stores servers) at the Cogent data center, in the city of Schiltigheim. We offer you to <strong>sublet a place in this rack</strong>, to host your physical server.
            <a href="https://wiki.arn-fai.net/_detail/benevoles:technique:arn-baie-face-2015-12.jpg?id=benevoles%3Atechnique%3Amatos">Learn more</a>
        </p>
</div>
<div class="infra-title-image">
            <h2><span class="violet">Functionalities</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <h3>No more noise in the living room</h3>
            <p>
            Your server will make noise in ARN's rack rather than in your home!
            </p>
            <h3>Non-standard format</h3>
            <p>
            We accept U-shaped rackmount machines as well as small non-rackmount machines.
            </p>
            <h3>Redundant electricity</h3>
            <p>
            If your machine is adapted, it can have up to two power supplies.
            </p>
            <h3>Electrical reboot</h3>
            <p>
            It is possible to <strong>reboot your server remotely</strong>.
            </p>
            <h3>Reverse DNS</h3>
            <p>
            To route your emails more easily, you can <strong>customize the reverse DNS in IPv4 and in IPV6</strong>.
            </p>
            <h3>IP transit or announcement possible</h3>
            <p>
            ARN can help you use your IP resources through the announcement of your IPs, or thanks to an IP transit.
        </p>
</div>
<div class="infra-title-image">
            <h2>Our <span class="violet">deals and prices</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-infra">
            <h3 class="offre-title">19" rackmount equipment</h3>
            <div class="offre-para">
                <p class="offre-title">€50/month per U</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">NUC or equivalent</h3>
            <div class="offre-para">
                <p class="offre-title">€20/month</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                NUC, barebone PC, HP prodesk, etc.
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Small ARM card</h3>
            <div class="offre-para">
                <p class="offre-title">€6/month</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                Olinuxino, Raspberry pi, etc.
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
</div>
<p>
Our prices are VAT free (art 293B of the Tax Code) and do not imply any form of commitment. However, this service requires a membership to the association (€15 per year). Your subscription and your annual membership will be renewed by tacit agreement.
</p>
<div class="infra-title-image">
            <h2>How to <span class="violet">subscribe</span>?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="timeline">
<div class="timeline-back"></div>
    <div class="timeline-item">
        <h3>1. Create an account in the member area and request access to the service.</h3>
        Like most of our services, our rack hosting service is reserved for our members. Therefore, you need to create an account in the <a href="https://adherents.arn-fai.net/members/login/">member area</a>, then to "Request a new subscription". You are free to pay the subscription upon receipt of the service.
    </div> 
    <div class="timeline-item">
        <h3>2. Validation</h3>
        Our team of volunteers will then call you, or contact you by email to check the feasibility of the project and validate your subscription request. 
    </div>
    <div class="timeline-item">
        <h3>3. Pre-configuration</h3>
        Once your request has been validated, you can access information and documentation about your service on the "My Subscriptions" page. You must then pre-configure your machine to make its configuration easier in the datacenter.
    </div>
    <div class="timeline-item">
        <h3>4. On-site installation</h3>
        Our team of volunteers will schedule an appointment with you to proceed to the on-site installation.
    </div>
    <div class="timeline-item">
        <h3>5. Verification of correct operation and payment</h3>
        Please proceed to the payment of the membership if you have not already done so. Also make sure you have set up a standing order, or have paid for several months in advance in one go.
    </div>
</div>
<div class="infra-title-image">
            <h2>Frequently asked <span class="violet">questions</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="infra-question-container">
    <div class="infra-question-box">
        <h3>Why should I join ARN?</h3>
        <p>
            At Alsace Réseau Neutre, a non-profit association with no paid employees, you are not considered as a customer of a service provider. You become a member of a collective that is getting everything organized to create a utopian space on the Internet, and influence digital policies towards more virtuous schemes.  <br><br>
            Furthermore, our goal is also to help our subscribers take power over the future of their services!
        </p>
    </div>
    <div class="infra-question-box">
        <h3>What does "VAT-free pricing" mean?</h3>
        <p>
            "VAT-free" means that ARN is currently not subject to VAT. The "pre-tax" price is therefore equal to the price "including all taxes".
        </p>
    </div><div class="infra-question-box">
        <h3>Is there a commitment?</h3>
        <p>
            No, but our services are renewed tacitly for one month on the 1st of each month. <br><br>
            If you are no longer using a service, don't forget to save your data and cancel your subscription.
        </p>
    </div><div class="infra-question-box">
        <h3>How to pay for the rack hosting service?</h3>
        <p>
            You will have a free one month trial. This trial will end on the first day of the following month. From then, an invoice will be issued every first of the month.<br><br>The first payment is therefore expected no later than the first two weeks of the month following your free trial, with the possibility of paying up to 12 months in advance in one go.<br><br>
            Payment is to be made by bank transfer. Bank details can be found in the member area. Do not forget to indicate your "ID" + the identification number that has been assigned to you.
        </p>
    </div><div class="infra-question-box">
        <h3>Where is the datacenter that hosts ARN's servers?</h3>
        <p>
            The Cogent data center is located at 46 Bischwiller Street in Schiltigheim, Alsace, France.
        </p>
    </div><div class="infra-question-box">
        <h3>How can I intervene on the server?</h3>
        <p>
            <strong>Remote reboot:</strong> if your machine crashes or if an error occurs and prevents you from having remote access to your server (incorrect firewall configuration, machine not rebooting as expected, etc.), the association can reboot it remotely thanks to its  <a href="https://wiki.arn-fai.net/technique:pdu">PDUs</a> (programmable power strips). This reboot must be requested by email to the admins.<br><br>
            <strong>Physical access:</strong> we do not provide a badge that allows you to access your machine independently. Therefore, you will be accompanied by a member of the association's technical team each time you visit the site.
        </p>
    </div><div class="infra-question-box">
        <h3>What is the throughput?</h3>
        <p>
            The maximum capacity is 100 Mbps. It is shared between all the services and members of the association, with no limit. <br><a href="https://arn-fai.net/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-">Do I penalize the association if I download a lot of files?</a>
        </p>
    </div><div class="infra-question-box">
        <h3>Does the association keep my information / logs?</h3>
        <p>
        We log:
        </p>
            <ul>
                <li>your identity</li>
                <li>the ARN IPs that are assigned to you</li>
                <li>the allocation period</li>
            </ul>
            <p>However, we invite you to read the "Misuse of Services" clause in our TOS. Please note that in the case of an investigation, we may be legally obliged to allow a tap on your IP.</p>
    </div><div class="infra-question-box">
        <h3>Can I mine energy-consuming crypto-currencies on the installed server?</h3>
        <p>
            No, you can not.
        </p>
    </div>
</div>
<div class="natta-help" style="text-align:center">
            <p class="aidez_nous">Did you find this helpful? You can help us too :)</p>
            <p><b>Come to our next volunteer meeting</b></p>
</div>