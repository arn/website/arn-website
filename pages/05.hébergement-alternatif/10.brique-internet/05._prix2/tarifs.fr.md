---
title: 'Avec ou sans VPN ?'
body_classes: modular
---

## __Avec quelle *connexion internet*  ?__

---
### Sans VPN ###    {.second-box}

__pas de frais__

[Plus difficile d'envoyer des mails](#title_pourquoi-je-ne-peux-pas-faire-de-mail-sans-vpn-arn)

[Box à configurer manuellement](#title_comment-configurer-ma-box-si-je-n-ai-pas-de-vpn-et-que-je-souhaite-exposer-mes-services-sur-internet)

Blocage possible par votre FAI

---
### Avec un VPN ###

__*+4€* / mois__

IP propre pour le mail

Plug & Play

Facile à déplacer<br><br>

<a href="/internet-alternatif/tunnel-vpn" role="button">En savoir plus</a>

---

*Prix en franchise de TVA (art 293B du CGI), c'est à dire TTC. La souscription d'un VPN requiert une [adhésion à l’association](/asso/adhérer). Pour le VPN : renouvellement de l’abonnement vpn et de l’adhésion annuelle par tacite reconduction. Abonnement vpn payable en G1 (4DU/mois).*

!! La plupart des VPN commerciaux ne conviennent pas pour l'auto-hébergement, les VPN ARN sont spécifiques car chaque VPN est lié une IP publique dédiée et respecte la neutralité du net, en outre ARN fournit un fichier de configuration prêt à l'emploi avec l'application VPN Client de YunoHost. 
!!
!! Pour information: le site de YunoHost liste d'[autres structures suceptibles de fournir ce genre de Tunnel VPN](https://yunohost.org/fr/providers/vpn).

!!!! Alternativement, vous pouvez aussi décider de mettre votre machine dans la [baie d'ARN](/hébergement-alternatif/hébergement-en-baie). De même, si l'aspect matériel n'est pas important pour vous, une autre solution peut-être de vous héberger sur un [serveur privé virtuel](/hébergement-alternatif/vps) fournit par notre association et ainsi bénéficier de sa connectivité.
