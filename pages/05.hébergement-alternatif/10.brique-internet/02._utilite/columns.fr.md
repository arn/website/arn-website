---
title: 'Pourquoi faire ?'
body_classes: modular
image_align: left
size: lg
---

## __Pourquoi *faire* ?__

---

### Garder le contrôle sur ses données
Grâce à l'auto-hébergement, plus besoin d'accepter les conditions d'hébergeurs tiers qui souvent impliquent de céder des droits d'utilisations sur vos données. Vous pouvez ainsi protéger vos données mais également celles de vos proches.

---

### Créer son bout d'internet libre
Dans un internet centralisé, les entités privées et les gouvernements peuvent espionner, analyser et influencer les personnes en filtrant les contenus. Nous faisons parti d'une communauté qui croit en un internet ouvert et décentralisé. Nous espérons que vous aussi.

---

### Apprendre et comprendre internet
Vous souhaitez apprendre comment fonctionnent les ordinateurs et Internet ? Rien de tel que de jardiner son serveur pour apprendre les mécanismes de base au cœur des systèmes d'exploitation (OS) et d'Internet.

---

### Explorer de nouvelles possibilités
De [nombreux outils libres auto-hébergeables](https://yunohost.org/fr/apps) existent! Avec votre propre serveur, vous pouvez manuellement installer et faire tourner n'importe quel programme et personnaliser chaque morceau si besoin.

---

!! Pourquoi ne pas s'auto-héberger ?
!! * Vous n'avez pas le temps, la patience ou n'êtes pas du tout à l'aise avec le numérique
!! * Vous doutez de votre rigueur pour sauvegarder vos données
!! * Vous avez besoin de services les plus fiables possibles
!!
!! Dans ce cas, de façon alternative, vous pouvez regardez nos services [sans-nuage.fr](https://sans-nuage.fr) ou ceux des [autres CHATONS](https://chatons.org).

