---
title: Internet Access
body_classes: natta 
visible: true
published: true
login: {  }
content:
    items: '@self.modular'
routable: true
page-toc:
  active: false
---

<div class="logo-natta">
        <img src="../../images/NATTA_Titre.svg" alt="Natta accès internet alternatif" class="logo-natta-img"/>
</div>
<div class="natta-title-image">
            <h1>Our <span class="orange">Internet access </span>services</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
        <div class="acces-intro-text">
            <p>
            <iframe id="acces-eligible" title="eligibility optical fiber and antennas" src="https://arn-fai.net/eligibilité/">
            </iframe>
            <br>
            Try here your eligibility to ARN's optical fiber or antenna Internet connection.
            </p>
        </div>
</div>
<div class="natta-title-image">
            <h2>Characteristics of our <span class="orange">Internet connection</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
           <h3>IPv4 & IPv6 fixed public</h3>
           <p>
             If the connection is not natively IPv6, we can provide a <strong>VPN compatible with IPv6</strong>.
            </p>
            <h3>Net neutrality</h3>
            <p>
            In respect with the concept of <strong>Net Neutrality</strong>,  we commit ourselves to not close any port. Also, we will not change your DNS resolver and we do not monitor nor modify your Internet traffic.
            </p>
            <h3>With or without a router</h3>
            <p>
           The subscriptions we offer can either include a router or not. So you can use the right equipment for your home and to meet your needs. We also offer recycled routers to reduce your digital environmental footprint :)
            </p>
            <h3>Self hosting</h3>
            <p>
            No ports are closed and you can also <strong> customize the reverse DNS in IPv4 and in IPv6</strong>.
            </p>
            <h3>Connection sharing</h3>
            <p>
            Our contract allows you to share your Internet connection with your neighbors. Connection sharing is possible through <strong>ARN's VPN</strong> to separate the legal liability.
            </p>
            <h3>What about BBC?</h3>
            <p>
            We do not offer TV services. Your "human brain time" management is up to you, after all :)
            </p>
</div>
<div class="natta-title-image">
            <h2>Our <span class="orange"> deals</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-acces">
            <h3 class="offre-title">Optical fiber (via ROSACE)</h3>
            <div class="offre-para">
                <p class="offre-title">€30/ month</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p class="condition-acces">
                Service access fee: €100<br>
                Non-binding offer
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
        <div class="offre-box offre-acces">
            <h3 class="offre-title">Optical fiber or xDSL (via FDN)</h3>
            <div class="offre-para">
                <p class="offre-title">from €42/month </p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p class="condition-acces">
                Service access fee: between €66 and €156<br>
                12 month commitment
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
          <div class="offre-box offre-acces">
            <h3 class="offre-title">Radio antennas</h3>
            <div class="offre-para">
                <p class="offre-title">€15/month</p>
                <img src="../../images/Traitjaune.svg" alt=" " class="trait"/>
                <p class="condition-acces">
                Service access fee: depending on the connection<br>
                Non-binding offer
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
</div>
<p>Tax free price (art 293B of the CGI). Annual membership required (€15/month). Subscription renewal and annual membership renewal is done automatically. 
</p>
<div class="natta-title-image">
            <h2>How to <span class="orange">subscribe</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="timeline">
        <div class="timeline-back-natta"></div>
                <div class="timeline-item">
                    <h3>1. Create an account on the member space and request the service you desire</h3>
                    <p>
                    Most of our services require a membership subscription. You first need to create an account on the <a href="https://adherents.arn-fai.net/members/register/">member space</a> (page in French), then to "<a href="https://adherents.arn-fai.net/members/login/?next=/members/request_subscriptions/step1">Ask for a new subscription</a>". You can pay your membership fee when you have access to the service of your choice. 
                    </p>
                </div>
                <div class="timeline-item">
                    <h3>2. Validation, service access fee payment, and grid connection</h3>
                    <p>
                    The volunteers will phone you or contact you by email to ensure the connection is possible (in some cases, an appointment on site may be necessary) and validate your request. You can then pay the service access fee and we will set up an appointment to establish the network connection.
                    </p>
                </div>
            <div class="timeline-item">
                    <h3>3. Verification of proper operation and payment</h3>
                    <p>
                    Once your server has been set up, you can access information and documentation about this service directly on the "<a href="https://adherents.arn-fai.net/members/subscriptions/">My subscriptions</a>" page (French). 
                    Please proceed to the payment of the membership if you have not already done so. Also make sure you have set up a standing order, or have paid for several months in advance in one go.
                    </p>
                </div>
</div>
<div class="natta-title-image">
            <h2>Frequently asked <span class="orange">questions</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="natta-sous-lignage-gris"/>
</div>
<div class="natta-question-container">
    <div class="natta-question-box">
        <h3>Why should I join ARN?</h3>
        <p>
            At Alsace Réseau Neutre, a non-profit association with no paid employees, you are not considered as a customer of a service provider. You become a member of a collective that is getting everything organized to create a utopian space on the Internet, and influence digital policies towards more virtuous schemes. 
            <br><br>
            Furthermore, our goal is also to help our subscribers take power over the future of their services!
        </p>
    </div>
    <div class="natta-question-box">
        <h3>What does "VAT-free pricing" mean?</h3>
        <p>
            "VAT-free" means that ARN is currently not subject to VAT. The "pre-tax" price is therefore equal to the price "including all taxes".
        </p>
    </div><div class="natta-question-box">
        <h3>What is the Internet speed?</h3>
        <p>
            The download speed depends on the chosen service. To give you an idea:
            <ul>
                <li>Optical fiber via ROSACE: up to 1Gbps</li>
                <li>Optical fiber via FDN: up to 1Gbps for download and 500Mbps for upload (depends on the chosen deal)</li>
                <li>VDSL via FDN: up to 18 Mbps (upload up to 1 Mbps)</li>
                <li>ADSL via FDN: up to 50 Mbps (upload up to 8 Mbps)</li>
                <li>Radio antenna network: maximum 90Mbps symmetric</li>
            </ul>
            The maximum capacity is 90 Mbps if you use ARN’s VPN. More information on: <a href="https://arn-fai.net/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-">Do I penalize the association if I download a lot of files? (Page in French)</a>
        </p>
    </div><div class="natta-question-box">
        <h3>Is the optical fiber connected to my house?</h3>
        <p>
            Yes, it is a FTTH service "Fiber To The Home". In other words, the fiber will be connected to your home.  
        </p>
    </div><div class="natta-question-box">
        <h3>What is ARN's logging policy for the Internet connection services?</h3>
        <p>
           For the radio network, the logging policy is the same as the VPN one.<br>
           For optic fiber, ADSL and VDSL, we are currently discussing with FDN and SCANI to know more about their policy. We will keep you informed once it is defined.
        </p>
    </div><div class="natta-question-box">
        <h3>Do you have a 4G subscriptions?</h3>
        <p>
            We do not offer this service!
        </p>
    </div><div class="natta-question-box">
        <h3>Can I choose my router?</h3>
        <p>
            Yes, subscriptions are provided without a box (router). You will have to buy the router yourself, unless you want to connect to a single computer using an ethernet cable. You will have control over the features and energy consumption of your equipment.
            <br><br>
            If you were to find a router, you can seek support in ARN to make your choice. We will help you find second hand and cheap routers (Emmaus Strasbourg). It is also a positive action for the environment!
            <br>
            If you want to learn more about server features, do not hesitate to come to our <a href="https://mobilizon.sans-nuage.fr/@arnfai/events?future=true">monthly workshop on self-hosting</a>.
        </p>
    </div>
    <div class="natta-question-box">
        <h3>Is there a television or phone service included?</h3>
        <p>
           No, but...
            <br>
            Regarding televisions, you can have a free access to TNT via radio signal. You need a compatible television set or a STB. There are also IPTV deals.
            <br><br>
            In France, you can benefit from IPTV (connected with a router) landline phone services for less than €2 TTC/month - with OVH for exemple.
        </p>
    </div>
     <div class="natta-question-box">
        <h3>How can I get help if I have connection issues?</h3>
        <p>
           ARN volunteers will try to solve your problem by making a connection diagnosis and provide phone assistance if needed. If that is not enough, the information will be passed on to SCANI or FDN, who will take a look or will contact their sub-contractor.
            <br>
            You can contact us on the <a href="../../nous-contacter">contact form</a>, the forum, the support chat, and even in person at the AUBE office.
        </p>
    </div>
     <div class="natta-question-box">
        <h3>Can I host my own mail server on this connection?</h3>
        <p>
           Yes. As the connection is neutral (Net Neutrality), ports are not restricted (even port 25) and you can set up a reverse DNS. In addition, we do not list our IPs as residential unlike many ISPs.
           <br>
            However, be careful not to tarnish the reputation of your IPs.
        </p>
    </div>
     <div class="natta-question-box">
        <h3>Can my IP change? Is it shared?</h3>
        <p>
           IPs are fixed and we do not plan to share port ranges in order to promote self-hosting. 
           <br>
            However, regarding the fiber optic connection, SCANI or FDN IPs will be assigned for the moment. It is therefore possible that one day, the association will decide to renumber with ARN IPs.
        </p>
    </div>
     <div class="natta-question-box">
        <h3>Is ipv6 available?</h3>
        <p>
          Yes (at least with a VPN included).
        </p>
    </div>
     <div class="natta-question-box">
        <h3>How to replicate this service?</h3>
        <p>
           For fiber and xDSL are SCANI or FDN "white label" services. 
           <br>
            For radio antennas, everything is explained on the <a href="https://wiki.arn-fai.net/benevoles:technique:natta">volunteer wiki page</a> in the radio antennas section.
        </p>
    </div>
</div>
<div class="natta-help" style="text-align:center">
            <p class="aidez_nous">Did you find this helpful? You can help us too :)</p>
            <p><b>Come to our next volunteer meeting</b></p>
</div>