---
title: Internet Cube
published: true
body_classes: infra
routable: true
visible: true
page-toc:
  active: false
---

<div class="logo-infra">
        <img src="../../images/INFRA_Titre.svg" alt="Infra alternative hosting services" class="logo-infra-img"/>
</div>
<div class="infra-title-image">
            <h1>What is an <span class="violet">Internet Cube</span>?</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            An Internet Cube is a single-board computer the size of a credit card, with
            minimum energy consumption. This system works with <strong>free and open source tools and hardware</strong>.
        </p>
</div>
<div class="infra-title-image">
            <h2>What is an <span class="violet">Internet Cube</span> used for?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" "  class="infra-sous-lignage-gris"/>
            <p>
            An Internet Cube is mainly used by people who want to <strong>remove voluntary restrictions and limitations</strong> of their existing Internet access. It can also be used to <strong>host data and personal services at home</strong> instead of using proprietary services.
        </p>
</div>
<div class="infra-title-image">
            <h2>Why does ARN recommend <span class="violet">Internet Cubes</span>?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            The goal of the project is to provide a <strong>ready-to-use Internet Cubes</strong>: you power it to your Internet router, answer a few questions and that's it. However, we strongly recommend you to participate in one of our workshops to get a grip on your Internet Cube. We are currently working on making them easier to use.
        </p>
</div>
<div class="infra-title-image">
            <h2>How does <span class="violet">Internet Cube</span> work?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            Our Internet Cube is designed to be used with a VPN for self-hosting (having your data and services at home). The VPN simplifies the Internet Cube installation and gives more useful properties.
        </p>
</div>
<div class="infra-title-image">
            <h2>Our <span class="violet">deals</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Wi-fi brick</h3>
            <div class="offre-para">
                <p class="offre-title">88€ + VPN</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                Wi-fi brick<br>
                Features:<br>
                An OrangePi card<br>
                A Wi-fi antenna (8188Gu Chipset)<br>
                Rooter, Charger and battery<br>
                32 GB microSD card
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Wi-fi brick and Auto-hosting</h3>
            <div class="offre-para">
                <p class="offre-title">88€ + VPN</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                Wi-fi brick and self-hosting<br>
                Features:<br>
                An Olimex LIME2 card<br>
                A Wi-fi antenna (MOD-WIFI-R5370-ANT)<br>
                Rooter with hard drive slot, Charger and battery<br>
                32 GB microSD card
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">The custom brick</h3>
            <div class="offre-para">
                <p class="offre-title">Price is determined according to the features you want</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                Customized brick<br>
                Features:<br>
                If you want to create an Internet Cube with your hardware, come see us to determine the right elements and installation together!
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
</div>
<div class="infra-title-image">
            <h2 class="title-infra">How to <span class="violet">proceed</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="timeline">
    <div class="timeline-back"></div>
    <div class="timeline-item">
        <h3>1. Send us an email</h3>
        Send an email at <a href="mailto:contact@arn-fai.net">contact@arn-fai.net</a> and tell us that you want to have access to this service.
    </div> 
    <div class="timeline-item">
        <h3>2. Come pick it up</h3>
        You can come and pick up your Internet Cube at the AUBE association office - 97 avenue de Colmar, 67000 Strasbourg. We do not send the Brick by post.
    </div>
    <div class="timeline-item">
        <h3>3. Need help?</h3>
        We can help you configure and prepare the system, install the services you would like to use, and the VPN for effective operation.
    </div>
    <div class="timeline-item">
        <h3>4. You are now ready!</h3>
        You can proceed to the connection of your Internet Cube. Do not hesitate to ask us for help.
    </div>
</div>
