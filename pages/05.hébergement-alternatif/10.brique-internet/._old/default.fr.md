---
title: Brique Internet
published: true
body_classes: infra
routable: true
visible: true
page-toc:
  active: false
---


<div class="logo-infra">
        <img src="../../images/INFRA_Titre.svg" alt="Infra hébergement alternatif" class="logo-infra-img"/>
</div>
<div class="infra-title-image">
            <h1>Une <span class="violet">Brique Internet</span>, c'est quoi ?</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
                <p>
                Une Brique Internet est un ordinateur monocarte de la taille d’une carte de crédit, à faible consommation électrique. Ce système fonctionne grâce à des <strong>  outils et du matériel libres et ouverts</strong>.
                </p>
</div>
<div class="infra-title-image">
            <h2>À quoi sert une <span class="violet">Brique Internet</span> ?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            Une Brique Internet est principalement utilisée par des personnes désireuses de nettoyer leur accès à Internet existant, pour en <strong>supprimer les restrictions et les limitations volontaires</strong>. Il est également possible de l’utiliser pour <strong>héberger facilement des données et des services personnels</strong> à domicile, au lieu d'utiliser des services propriétaires.
        </p>
</div>
<div class="infra-title-image">
            <h2>Pourquoi ARN propose des <span class="violet">Briques Internet</span> ?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            L'objectif du projet est de fournir une <strong>Brique Internet prête à l'emploi</strong> : vous la branchez électriquement à votre Box Internet, vous répondez à quelques questions simples et c'est tout ! Toutefois, nous vous recommandons fortement de participer à un ou plusieurs de nos ateliers afin de prendre en main votre Brique Internet. Actuellement, nous esseyons de rendre leur utilisation plus simple.
        </p>
</div>
<div class="infra-title-image">
            <h2>Comment fonctionne une <span class="violet">Brique Internet</span> ?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            La Brique est conçue pour être utilisée (de préférence) avec un VPN permettant l'auto-hébergement (= le fait d'avoir ses données et ses services chez soi). C'est le cas des VPN fournis par ARN et les autres membres de la FFDN. Le VPN permet de simplifier l'installation de la Brique et lui confère quelques-unes de ses propriétés les plus intéressantes.
        </p>
</div>
<div class="infra-title-image">
            <h2>Nos <span class="violet">offres</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-infra">
            <h3 class="offre-title">La brique Wi-fi</h3>
            <div class="offre-para">
                <p class="offre-title">88€ + VPN</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                La brique Wi-fi<br>
                Caractéristiques :<br>
                Une carte OrangePi<br>
                Une antenne Wi-fi (Chipset 8188Gu)<br>
                Boîtier, Chargeur et batterie<br>
                Carte microSD 32 Go
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">Brique Wi-fi et auto-hébergement</h3>
            <div class="offre-para">
                <p class="offre-title">88€ + VPN</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                Brique Wi-fi et auto-hébergement<br>
                Caractéristiques :<br>
                Une carte Olimex LIME2<br>
                Une antenne Wi-fi (MOD-WIFI-R5370-ANT)<br>
                Boîtier avec emplacement disque dur, Chargeur et batterie<br>
                Carte microSD 32 Go
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
        <div class="offre-box offre-infra">
            <h3 class="offre-title">La brique sur mesure</h3>
            <div class="offre-para">
                <p class="offre-title">Prix déterminé suivant les fonctionnalités que vous souhaitez</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>
                La brique sur mesure<br>
                Caractéristiques :<br>
                Si vous souhaitez créer une brique Internet avec votre matériel, contactez-nous afin de déterminer les bons éléments et l’installation ensemble !
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
</div>
<div class="infra-title-image">
            <h2>Comment <span class="violet">procéder</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="timeline">
    <div class="timeline-back"></div>
    <div class="timeline-item">
        <h3>1. Envoyez-nous un mail</h3>
        Envoyez-nous un mail à <a href="mailto:contact@arn-fai.net">contact@arn-fai.net</a> en précisant que vous souhaitez avoir accès à ce service.
    </div> 
    <div class="timeline-item">
        <h3>2. Cherchez votre brique</h3>
        Vous pouvez venir chercher votre Brique Internet au local de l’association AUBE,  97 avenue de Colmar 67000 Strasbourg. La remise ne peut se faire qu’en main propre. Nous n'envoyons pas la Brique par courrier postal ou par transporteur.
    </div>
    <div class="timeline-item">
        <h3>3. Besoin d'aide ?</h3>
        Nous pouvons vous aider à configurer et préparer avec vous le système. Ceci, en installant les services que vous souhaiteriez utiliser et le VPN pour le bon fonctionnement.
    </div>
    <div class="timeline-item">
        <h3>4. Vous êtes maintenant parés !</h3>
        Vous pouvez maintenant procéder au branchement de votre Brique Internet. N’hésitez pas à nous demander de l’aide.
    </div>
</div>