---
title: 'Quelles solutions à quels coûts ?'
body_classes: modular
size: sm
---

## __Quelles *solutions*, à quels coûts ?__

Voici quelques solutions existantes et pertinentes déjà mis en œuvre par nos membres. Nous vous invitons à vous renseigner sur la solution la plus adaptée à votre usage et à venir [échanger avec nous](/contact) si vous avez des questions.

---
### Laptop de récup'

__gratuit et *écolo*__

Usage à évaluer selon les caractèristiques de la machine

_Important: Les formats tours sont déconseillés à cause de la consomation électrique_

---
### La Brique Internet

__à partir de *70€*__

Pour des services personnels peu gourmands

_Exemple: des boites mails, un petit cloud sans bureautique, un siteweb, un disque réseau, etc._

---
### La Brique Internet +

__environ *125€*__

Pour des usages plus gourmands ou en collectif

_[Nanopi R5S 4Go](https://fr.aliexpress.com/item/1005004309192673.html)_<br>
_[128Go SSD NVMe](https://fr.aliexpress.com/item/32870033933.html)_<br>
_Chargeur USB C non inclus_

---

!!! Nous avons en stock des cartes Olimex Lime1 et des Orange Pi PC + pour faire des Briques Internet classiques. Il nous manque toutefois des cartes microSD class 10.
