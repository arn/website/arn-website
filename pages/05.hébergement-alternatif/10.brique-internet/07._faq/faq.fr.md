---
title: 'Foire aux questions'
body_classes: modular
---

## __Foire aux *questions*__

---
### Que signifie « Prix en franchise de TVA » ?

La franchise de TVA signifie qu’à ce jour ARN n’est pas soumis à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».

---
### Comment obtenir de l'aide en cas de problème ?
Si vous êtes proches de Strasbourg, le plus simple est de venir [nous rencontrer le vendredi soir](/contact) en prévenant en avance via le forum ou notre formulaire de contact que vous venez pour de l'aide sur l'auto-hébergement.

Vous pouvez également tenter de demander de l'aide sur les chats ou les [forums d'ARN](https://forum.arn-fai.net) ou de [YunoHost](https://forum.yunohost.org).

---
### Pourquoi je ne peux pas faire de mail sans VPN ARN ?
Les FAI classiques ne permettent pas d'envoyer correctement des mails pour que ces derniers soient acceptés par Google, Microsoft ou encore Free.

Le VPN ARN permet de contourner les limitations de votre connexion internet (Reverse DNS personnalisable, IP non listée comme résidentiel, port 25 ouvert, etc.).

---
### Comment configurer ma box si je n'ai pas de VPN et que je souhaite exposer mes services sur internet ?
Si l'UPNP n'a pas réussi à ouvrir les ports de votre box, vous devez le faire manuellement en vous connectant à votre box.

TODO

---
### Est-il possible techniquement que mon accès internet via mon FAI commercial soit bridé et m'empèche de m'auto-héberger sans VPN ?
Malheureusement, oui et certains témoignages sur le forum de YunoHost indique que ça peut également arriver lors d'une mise à jour de votre Box.

Les raisons peuvent être multiples:

 * Votre FAI bloque le port 25 (mail) voire d'autres ports vous empéchant d'auto-héberger en autonomie ces services
 * Votre FAI n'a plus d'IP et commence à partager les IP entre plusieurs logements abonnés
 * L'interface de votre box ne permet plus de régler la redirection de port
 * Les fonctionnalités de configuration de votre box (typiquement du ReverseDNS) ne fonctionnent plus
 * Votre IP est inscrite comme IP résidentielle

 Pour tous ces problèmes, un VPN neutre et avec une ip publique dédiée est à même de régler la situation.

---
### Pourquoi conseiller YunoHost ou une version dérivée ?

YunoHost est un des systèmes les plus avancés dans la démocratisation de l'auto-hébergement. Par ailleurs, plusieurs membres d'ARN contribuent sur ce projet. Nous l'utilisons également en interne pour nos mails, siteweb et sans-nuage.fr.

Il existe d'autres projets similaires à YunoHost:
* [OpenMediaVault](https://www.openmediavault.org/)
* [FreeDombox](https://www.freedombox.org/)
* [FreeNAS](https://www.freenas.org/)
* [Umbrel](https://getumbrel.com)
* [LibreServer](https://freedombone.net/)

Par ailleurs, si vous réfléchissez à utiliser docker, kubernetes, debian ou openBSD dans un objectif d'apprentissage Let's Go. Si en revanche l'idée est de transmettre ensuite ce serveur à quelqu'un d'autres, réfléchissez bien à ce que celà implique. 

---
### Qu'est-ce qu'un nom de domaine ? Dois-je en avoir mon propre nom de domaine ?

Sur internet les serveurs sont identifiés par des IP uniques par exemple wikipedia peut être joint via l'ip `185.15.58.224`. Cependant, retenir une suite de nombre n'est pas le fort des êtres humains, c'est pouruqoi on associe à une ip un ou plusieurs nom de domaine, c'est à dire nom d'un site, par exemple `wikipedia.org` ou d'un mail, par exemple `protonmail.com`.

Un nom de domaine se loue à un registrar (ARN n'est pas registrar), et il est en général préférable de louer son propre nom de domaine. Toutefois, si l'objectif est d'essayer ou que vous avez peu de moyen, vous pouvez commencer avec un nom de domaine gratuit se terminant par noho.st, nohost.me ou ynh.fr, celui-ci vous sera fournit lors de l'installation de votre YunoHost.

---
### Comment contribuer à CLIC, à la Brique Internet ou à YunoHost ?
TODO

---

