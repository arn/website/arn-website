---
title: 'L''auto-hébergement ?'
body_classes: modular
image_align: left
---

## __Qu'est-ce que l'*auto-hébergement* ?__

L'auto-hébergement numérique c'est le fait de placer ses services numériques (par exemple: son blog, son cloud ou sa boite mail) sur un ordinateur chez soi plutôt que de les laisser chez un hébergeur tiers.

Il s'agit donc de transformer cet ordinateur en serveur, c'est à dire une machine, généralement allumée et connectée à internet 24h sur 24, dont le rôle est de répondre à des demandes: `Donne moi telle page web`, `Transmet ce mail à telle personne`, etc.
