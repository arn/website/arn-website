---
title: 'Comment se lancer ?'
body_classes: modular
size: lg
---

## __Comment *se lancer* ?__

Le plus simple est de venir [nous rencontrer au Hackstub](/contact). 

Si vous souhaitez essayez en autonomie, vous pouvez vous référer à la documentation d'[installation de YunoHost](https://yunohost.org/install).

---
