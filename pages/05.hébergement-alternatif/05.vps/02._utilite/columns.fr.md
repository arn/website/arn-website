---
title: 'Quand utiliser ces VPS ?'
body_classes: modular
size: sm
---

## __Les *fonctionnalités*__


---

### Redondance
Le VPS est **répliqué en temps réel** sur un autre serveur dans la même baie. Il est prêt à être redémarré par un⋅e bénévole en cas d’incident.

---

### Choix de l'OS
Système pré-installé au choix **Debian, Ubuntu, YunoHost**. Un⋅e bénévole peut aussi lancer l’ISO de votre choix, puis à vous de le configurer.

---

### IPv4 & IPv6
Chaque VPS inclut une IPv4 et une IPv6. Vous pouvez demander un **préfixe IPv6 sans surcoût** et/ou un IPv4 supplémentaire pour 4€/mois.

---

### Redémarrage électrique
Il est possible d'effectuer un reboot électrique de votre serveur depuis chez vous, et ainsi de le redémarrer entièrement à distance !

---

### Reverse DNS
Pour vous aider à acheminer vos mails, il est possible de **personnaliser le reverse DNS en IPv4 et en IPv6**.

---

### Entraide
**Les sauvegardes sont à faire de votre côté**. En cas de soucis, une console VNC, ainsi que le support bénévole et les autres membres sont là pour vous guider.


---

!!! Nous ne proposons pas d'infogérance de votre VPS, vous êtes donc en charge de la gestion de votre machine. Voir FAQ: [Je ne suis pas sûr de ma capacité à gérer un VPS ?](#je-ne-suis-pas-s-r-de-ma-capacit-g-rer-un-vps)
