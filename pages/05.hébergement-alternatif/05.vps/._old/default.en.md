---
title: Virtual Private Servers (VPS)
body_classes: infra
visible: true
routable: true
published: true
page-toc:
  active: false
---

<div class="logo-infra">
        <img src="../../images/INFRA_Titre.svg" alt="Infra alternative hosting services" class="logo-infra-img"/>
</div>
<div class="infra-title-image">
            <h1>What is a<span class="violet"> VPS</span>?</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            A server is a machine, generally running 24 hours a day, which responds to requests made by users online. To illustrate, it allows the user to access a web page or to send a message to a mailbox. To mutualize costs, it is possible to <strong>simulate several Virtual Private Servers (VPS)</strong> within one physical server. <br>
You can, for example, use a VPS to "self-host" a drive or emails, for your family or your association. 
        </p>
</div>
<div class="infra-title-image">
            <h2><span class="violet">Functionalities</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <h3>Redundancy</h3>
            <p>
            The VPS is replicated in real time to another server from the same rack. It is ready to be restarted by a volunteer if an incident occurs.
            </p>
            <h3>Choice of system</h3>
            <p>
            <strong>Debian, Ubuntu and YunoHost</strong> are pre-installed on the server. Upon request, a volunteer can launch the ISO of your choice and let you take care of its configuration.
            </p>
            <h3>IPv4 & IPv6</h3>
            <p>
          Each VPS includes an IPv4 and an IPv6. You can request an IPv6 prefix at no extra cost, and/or one additional IPv4 for €4 per month.
          </p>
            <h3>Electrical reboot</h3>
        <p>
            It is possible to execute an electrica
             reboot from anywhere and hence to reboot your VPS.
        </p>
            <h3>Reverse DNS</h3>
            <p>
           To help you route your mails, it is possible to <strong>customize the reverse DNS in IPv4 and in IPV6</strong>.
           </p>
            <h3>Support</h3>
            <p>
           The backups are to be done by yourself. In case of an issue, a VNC console, as well as a volunteer's technical support and other members are here to help you.
           </p>
             <h3>Coming soon</h3>
             <p>
           The association plans to migrate to Proxmox, to offer snapshots, and more autonomy on reboot and reinstallation operations.
           </p>
</div>
<div class="infra-title-image">
            <h2> Our<span class="violet"> deals</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-4-col">
            <h3 class="offre-title">VPS "SSD-1G-50"</h3>
            <div class="offre-para">
                <p class="offre-title">€10/month</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p> Features:<br/>
                    1 Virtual Core<br/>
                    1 GB RAM<br/>
                    50 GB SSD storage
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
        <div class="offre-box offre-4-col">
            <h3 class="offre-title">VPS "SSD-2G-50"</h3>
            <div class="offre-para">
                <p class="offre-title">€12/month</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>Features:<br/>
                    1 Virtual Core<br/>
                    2 GB RAM<br/>
                    50 GB SSD storage
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
       <div class="offre-box offre-4-col">
            <h3 class="offre-title">VPS "STO-1G-200"</h3>
            <div class="offre-para">
                <p class="offre-title">€10/month</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>Features:<br/>
                    1 Virtual Core<br/>
                    1 GB RAM<br/>
                    15 GB SSD storage<br/>
                    200 GB HDD storage
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
    <div class="offre-box offre-4-col">
            <h3 class="offre-title">VPS "STO-2G-200"</h3>
            <div class="offre-para">
                <p class="offre-title">€12/month</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>Features:<br/>
                    1 Virtual Core<br/>
                    2 GB RAM<br/>
                    15 GB SSD storage<br/>
                    200 GB HDD storage
                </p>
            </div>
            <a href="#" class="offre-button">Choose this option</a>
        </div>
</div>
<p>
Our prices are VAT free (art 293B of the Tax Code) and do not imply any form of commitment. However, this service requires a membership to the association (15€ per year). Your subscription and your annual membership will be renewed by tacit agreement.
</p>
<div class="infra-title-image">
            <h2>How to <span class="violet">subscribe </span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="timeline">
    <div class="timeline-back"></div>
    <div class="timeline-item">
        <h3>1. Create an account on the member space and request access to the service</h3>
        Like most of our services, our rack hosting service is reserved for our members. Therefore, you need to create an account in the member area, then to "Request a new subscription". You are free to pay the subscription upon receipt of the service.
    </div> 
    <div class="timeline-item">
        <h3>2. Validation and launch</h3>
        Our team of volunteers will check whether there are enough resources left to provide the requested service. If so, they will then validate your request and run the service.
    </div>
    <div class="timeline-item">
        <h3>3. Verification of correct operation and payment</h3>
        Once your server has been set up, you can access information and documentation about this service on the "My subscriptions" page. 
        Please proceed to the payment of the membership if you have not already done so. Also make sure you have set up a standing order, or have paid for several months in advance in one go.
    </div>
</div>
<div class="infra-title-image">
            <h2><span class="violet">Frequently asked </span>questions</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="infra-question-container">
    <div class="infra-question-box">
        <h3>Why should I join ARN? ?</h3>
        <p>
           At Alsace Réseau Neutre, a non-profit association with no paid employees, you are not considered as a customer of a service provider. You become a member of a collective that is getting everything organized to create a utopian space on the Internet, and influence digital policies towards more virtuous schemes.  <br><br>
           Furthermore, our goal is also to help our subscribers take power over the future of their services!
        </p>
    </div>
    <div class="infra-question-box">
        <h3>What does "VAT-free pricing" mean?</h3>
        <p>
           "VAT-free" means that ARN is currently not subject to VAT. The "pre-tax" price is therefore equal to the price "including all taxes".
        </p>
    </div><div class="infra-question-box">
        <h3>I am not confident in my ability to manage a VPS</h3>
        <p>
            Our VPS can be <strong>pre-installed with the YunoHost system</strong>, which requires less skills to achieve a result. However, self-hosting is a bit like gardening; keep in mind that you will have to spend time working on it, and that it will always take longer than having your services hosted with a third party. The best practice is to proceed step by step. Indeed, VPSs are non-binding and you can cancel them at any time.<br/>
            If you are still hesitating, the association organizes a monthly "self-hosting" session, during its "Free ourselves from tracking" workshop. You can also ask your questions directly to the other members on the forum, or on the chat.<br/>
            If you feel like this is too complex, please have a look at <a href="../../sans-nuage">sans-nuage.fr</a> and the suggestions of the other <a href="https://chatons.org">chatons members.</a> 
        </p>
    </div><div class="infra-question-box">
        <h3>Does the association automatically back up my VPS?</h3>
        <p>
            NO. The backup of your data and configurations on your VPS is entirely up to you. However, if you are looking for a backup space, do not hesitate to ask other members on the forum.
        </p>
    </div><div class="infra-question-box">
        <h3>Is it possible to upgrade my VPS?</h3>
        <p>
            It is possible to upgrade your package as long as you stay in the same range (STO or SSD). On the contrary, it is not possible to downgrade your VPS to a lower package in terms of SSD or HDD quantity.<br/>
Our resources (storage, RAM, CPU) are limited. We advocate an optimal and responsible use of resources, and solidarity between the members of the association. Hence we are against wastefulness and the monopolization of all resources by a few members. Therefore, if you already know that you will not use one of the resources included in your package, you can specify this when creating your VPS. The administrator can then resize your VPS accordingly.<br/>
You can request an additional IPv4 address for 4€/month regardless of your package. You can also request one or more IPv6 prefixes at no extra cost.
        </p>
    </div><div class="infra-question-box">
        <h3>Which operating systems are available?</h3>
        <p>
            We offer the following pre-installed systems:<br/>
Debian: stable and oldstable versions<br/>
Ubuntu: LTS versions still supported + the 2 latest intermediate versions released<br/>
Yunohost: stable version (and testing at certain times of the year)<br/><br/>
If you choose YunoHost, please tell us the main domain name to configure (if you already have one).<br/>
Upon request, any free system on x86/x86-64 architecture, just ask us to add the iso you want. For example: Debian, Ubuntu Server, CentOS, OpenBSD, FreeBSD, etc. The installation is then entirely up to you (it is neither automatic nor "turnkey").
Si vous choisissez YunoHost, merci de nous indiquer le nom de domaine principal à configurer si vous en avez déjà un.
À votre demande, tout système libre sur architecture x86/x86-64, il suffit de nous demander d'ajouter l'iso que vous souhaitez. Exemples : Debian, Ubuntu Server, CentOS, OpenBSD, FreeBSD, etc. L'installation est alors entièrement à votre charge (elle n'est ni automatique, ni clé en main).
        </p>
    </div><div class="infra-question-box">
        <h3>Is it possible to do a custom installation of the operating system?</h3>
        <p>
            Yes, upon request. This would allow you, for example, to activate the encryption of your hard disk. Otherwise, the configuration is that of the standard installation (a single unencrypted partition, a system in English, etc.).
        </p>
    </div><div class="infra-question-box">
        <h3>How do I access my VPS?</h3>
        <p>
        Nous enregistrons :
        </p>
            <ul>
            <li>By SSH, command line / text access. We recommend key authentication to avoid brute force attacks (systematic and automatic testing of all possible login + password combinations to find the one you have chosen). Password authentication is still possible, but not recommended.</li>
            <li>By VNC, remote graphic display. This allows you to troubleshoot an error that prevents you from having SSH access (incorrect firewall configuration, VPS that does not restart as expected, etc.). Also, this feature allows you to perform a custom installation of the operating system and to enter your passphrase to start an encrypted system.</li>
            <li>Via the web administration interface, if you have chosen YunoHost.</li>
            </ul>
    </div>
    <div class="infra-question-box">
        <h3>Can I customize the reverse DNS for my IPs?</h3>
        <p>
            Yes, by sending us an email. <br/>
For IPv4, you have to tell us which name you have chosen.<br/>
For IPv6, either you tell us the desired name for one or more addresses, or we delegate the zone that corresponds to your /56, so that you manage the reverse DNS associated with your addresses yourself. This method assumes that you have at least one authoritative name server.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Is there a commitment?</h3>
        <p>
           No, but our services are renewed tacitly for one month on the 1st of each month.<br/>
If you are no longer using a service, don't forget to save your data and cancel your subscription.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>How to pay for this service?</h3>
        <p>
          You will have a free one month trial. This trial will end on the first day of the following month. From then, an invoice will be issued every first of the month.<br/>
The first payment is therefore expected no later than the first two weeks of the month following your free trial, with the possibility of paying up to 12 months in advance in one go.<br/>
            Payment is to be made by bank transfer. Bank details can be found in the member area. Do not forget to indicate your "ID" + the identification number that has been assigned to you.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Where is the datacenter where ARN's servers are located?</h3>
        <p>
          The Cogent data center is located at 46 Bischwiller Street in Schiltigheim, Alsace, France.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>What is the debit speed?</h3>
        <p>
            The maximum capacity is 100 Mbps. It is shared between all the services and members of the association, with no limit. <a href="https://arn-fai.net/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-">Do I penalize the association if I download a lot of files?</a>
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Does the association keep my personal information?</h3>
        <p>
           Yes, as required by French law, we must record your identity and the IP addresses assigned by ARN to your VPS. No more, no less. All this information is kept for one year. ARN also has the right to verify your identity.<br/>
Please note that: ARN is an association located in France, and we are under the French legislation in force. Thus, we will comply with any request for identification or termination of the service justified by the law. In other words, if your aim is to hide from ARCOM (the authority in charge of fighting against the distribution of works without the consent of their authors) or to run a website that contains illegal content, do not subscribe to our VPS. These are just a few examples, all French and European legislation is applicable to you and ARN.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>How it works (Technologies we use)</h3>
        <p>
           KVM for the hypervisor, Ganeti for VM orchestration, and DRBD for redundancy: in case of hardware failure on one of our servers, your VPS will be transferred to another server in the same rack. In theory, there is no interruption of any service on our side during maintenance. But in practice, in certain cases, your machine may have to be restarted.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>How to reproduce this service?</h3>
        <p>
            The main part of the information on this service is documented on the page dedicated to <a href="https://wiki.arn-fai.net/benevoles:technique:ganeti">Ganeti</a>. However, we are planning to migrate to Proxmox, and if you are considering deploying a VM cluster, we think this solution is easier to handle.
        </p>
    </div>
</div>
<div class="natta-help" style="text-align:center">
            <p class="aidez_nous">Did you find this helpful? You can help us too :)</p>
            <p><b>Come to our next volunteer meeting</b></p>
</div>