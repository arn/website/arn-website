---
title: Serveurs privés virtuels (VPS)
body_classes: infra
visible: true
routable: true
published: true
page-toc:
  active: false
---

<div class="logo-infra">
        <img src="../../images/INFRA_Titre.svg" alt="Infra hébergement alternatif" class="logo-infra-img"/>
</div>
<div class="infra-title-image">
            <h1>Qu’est-ce qu’un <span class="violet">VPS</span> ?</h1>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <p>
            Un serveur est une machine, en général allumée 24h/24, qui répond à des demandes  faites par les utilisateurs en ligne. Par exemple, elle permet d'accéder à  une page web ou de transmettre un message dans une boîte mail. Pour mutualiser les coûts, il est possible de <strong>simuler plusieurs serveurs privés virtuels (VPS)</strong> à l’intérieur d’un serveur physique. <br>
           Vous pouvez, par exemple, vous en servir pour « auto-héberger » un drive ou des mails, pour votre famille ou votre association.
        </p>
</div>
<div class="infra-title-image">
            <h2><span class="violet">Fonctionnalités</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
            <h3>Redondance</h3>
            <p>
            Le VPS est répliqué en temps réel sur un autre serveur dans la même baie. Il est prêt à être redémarré par un⋅e bénévole en cas d’incident.
            </p>
            <h3>Choix du système d'exploitation</h3>
            <p>
            Les systèmes pré-installés sur notre serveur sont <strong>Debian, Ubuntu et YunoHost</strong>. Sur demande, un⋅e bénévole peut également lancer l’ISO de votre choix et vous laisser le configurer.
            </p>
            <h3>IPv4 & IPv6</h3>
            <p>
           Chaque VPS inclut une IPv4 et une IPv6. Vous pouvez demander un préfixe IPv6 sans surcoût et/ou un IPv4 supplémentaire pour 4€/mois.
           </p>
            <h3>Redémarrage électrique</h3>
            <p>
            Il est possible d'effectuer un reboot électrique de votre serveur depuis chez vous, et ainsi de le redémarrer entièrement à distance !
            </p>
            <h3>Reverse DNS</h3>
            <p>
            Pour vous aider à acheminer vos mails, il est possible de <strong>personnaliser le reverse DNS en IPv4 et en IPV6</strong>.
            </p>
            <h3>Entraide</h3>
            <p>
            Les sauvegardes sont à faire de votre côté. En cas de soucis, une console VNC, ainsi que le support bénévole et les autres membres sont là pour vous guider.
            </p>
             <h3>À venir</h3>
             <p>
            L’association souhaite migrer vers Proxmox afin de proposer des snapshots et plus d’autonomie sur les opérations de redémarrage et de réinstallation.
            </p>
</div>
<div class="infra-title-image">
            <h2>Nos <span class="violet">offres</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="offres-container">
        <div class="offre-box offre-4-col">
            <h3 class="offre-title">VPS "SSD-1G-50"</h3>
            <div class="offre-para">
                <p class="offre-title">10€/mois</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>caractéristiques :<br/>
                    1 Cœur virtuel<br/>
                    1 Go RAM<br/>
                    50 Go de stockage SSD
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
        <div class="offre-box offre-4-col">
            <h3 class="offre-title">VPS "SSD-2G-50"</h3>
            <div class="offre-para">
                <p class="offre-title">12€/mois</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>caractéristiques :<br/>
                    1 Cœur virtuel<br/>
                    2 Go RAM<br/>
                    50 Go de stockage SSD
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
       <div class="offre-box offre-4-col">
            <h3 class="offre-title">VPS "STO-1G-200"</h3>
            <div class="offre-para">
                <p class="offre-title">10€/mois</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>caractéristiques :<br/>
                    1 Cœur virtuel<br/>
                    1 Go RAM<br/>
                    15 Go de stockage SSD<br/>
                    200 Go de stockage HDD
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
    <div class="offre-box offre-4-col">
            <h3 class="offre-title">VPS "STO-2G-200"</h3>
            <div class="offre-para">
                <p class="offre-title">12€/mois</p>
                <img src="../../images/Traitviolet.svg" alt=" " class="trait"/>
                <p>caractéristiques :<br/>
                    1 Cœur virtuel<br/>
                    2 Go RAM<br/>
                    15 Go de stockage SSD<br/>
                    200 Go de stockage HDD
                </p>
            </div>
            <a href="#" class="offre-button">Choisir cette offre</a>
        </div>
</div>
<p>
Prix en franchise de TVA (art 293B du CGI) et sans engagement. Requiert une adhésion à l’association (15€/an). Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction.
</p>

<div class="infra-title-image">
            <h2>Comment <span class="violet">souscrire </span>?</h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="timeline">
    <div class="timeline-back"></div>
    <div class="timeline-item">
        <h3>1. Créez un compte sur l’espace membre et demandez l'accès au service d'hébergement en baie</h3>
        Comme la plupart de nos services, celui-ci est réservé aux membres de l'association. Il faut donc créer un compte sur <a href="https://adherents.arn-fai.net/members/login/">l’espace membre</a>, puis « Demander un nouvel abonnement ». Vous êtes libre de ne régler le montant de l’adhésion qu'à la réception du service.
    </div> 
    <div class="timeline-item">
        <h3>2. Validation et mise en route</h3>
        L’équipe bénévole vous appelle ou vous contacte par mail pour vérifier la faisabilité du projet, et valide ensuite votre demande d'abonnement.
    </div>
    <div class="timeline-item">
        <h3>3. Vérification du bon fonctionnement et règlement</h3>
        Une fois votre service mis en place, vous pouvez accéder aux informations et à la documentation le concernant sur la page « <a href="https://adherents.arn-fai.net/members/subscriptions/">Mes abonnements </a> ». Merci de procéder ensuite au règlement  de l’adhésion si ce n’est pas déjà fait, et à la mise en place d’un virement permanent, ou à défaut de régler plusieurs mois d’avance en une fois.
    </div>
</div>


<div class="infra-title-image">
            <h2>Foire aux <span class="violet">questions</span></h2>
            <img src="../../images/sous-lignage-gris.svg" alt=" " class="infra-sous-lignage-gris"/>
</div>
<div class="infra-question-container">
    <div class="infra-question-box">
        <h3>Pourquoi dois-je adhérer ?</h3>
        <p>
            Chez Alsace Réseau Neutre, association à but non lucratif sans  salariés, vous n’êtes pas client⋅es d’un fournisseur… Vous devenez  membre d’un collectif qui s’organise pour construire un bout d’internet utopique et influencer les politiques numériques vers des schémas plus vertueux. <br><br>
            Par ailleurs, nous souhaitons que les personnes abonnées aient un pouvoir sur l’avenir de leur service.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Que signifie « Prix en franchise de TVA » ?</h3>
        <p>
            La franchise de TVA signifie qu’à ce jour, ARN n’est pas soumise à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».
        </p>
    </div><div class="infra-question-box">
        <h3>Je ne suis pas sûr de ma capacité à gérer un VPS ? ?</h3>
        <p>
            Nos VPS peuvent être <strong>pré-installés avec le système d'exploitation YunoHost</strong>, qui réduit les compétences nécessaires pour parvenir à un résultat. Cependant, la pratique de l’auto-hébergement, c’est un peu comme le jardinage : il faut s’attendre à devoir y passer un peu de temps, et ce sera toujours plus long que d’avoir ses services chez un tiers. L’idéal est d’y aller pas à pas. En effet, les services VPS étant sans engagement, vous pouvez les résilier à tout moment.<br/>
            Si vous hésitez, l’association organise chaque mois  une table « auto-hébergement » lors de l’atelier « Libérons-nous du pistage ». Vous pouvez aussi vous renseigner auprès des  autres membres directement sur le forum ou le chat.<br>
<b>Attention tout de même</b> : l’équipe support d’ARN ne pourra pas être considérée comme étant en charge de l’infogérance du système à l’intérieur de votre VPS.<br/>
Si vous estimez que tout cela est trop complexe, pensez à consulter <a href="../../sans-nuage">sans-nuage.fr</a>et les propositions des autres <a href="https://chatons.org">CHATONS.</a>
        </p>
    </div><div class="infra-question-box">
        <h3>Est-ce que l’association sauvegarde automatiquement mon VPS ?</h3>
        <p>
            NON. La sauvegarde de vos données et de vos configurations présentes sur votre VPS est entièrement à votre charge. Toutefois, si vous cherchez un espace de sauvegarde, n’hésitez pas à solliciter les membres sur le forum.
        </p>
    </div><div class="infra-question-box">
        <h3>Est-il possible de faire évoluer les caractéristiques de mon VPS ? </h3>
        <p>
            Il est possible de passer à une offre supérieure tant que vous restez dans la même gamme (STO ou SSD). Le passage à une offre inférieure n'est pas possible en ce qui concerne la quantité de SSD ou de HDD.<br/>
            Nos ressources (stockage, RAM, CPU) sont limitées. Nous prônons un usage optimal et responsable des ressources, ainsi que la solidarité entre les membres de l'association. Nous nous opposons donc à l'accaparation de toutes les ressources par quelques un-e-s   et au gaspillage. Dans ce contexte, si vous savez que vous n'utiliserez pas une des ressources de votre offre, vous pouvez l'indiquer lors de la création de votre VPS. L'administrateur dimensionnera le VPS en fonction de cela.<br/><br/>
            Vous pouvez demander une adresse IPv4 supplémentaire pour 4€/mois, quelle que soit votre offre. Vous pouvez également demander un ou plusieurs préfixes IPv6 sans surcoût.
</p>
    </div><div class="infra-question-box">
        <h3>Quels systèmes d'exploitation sont disponibles ?</h3>
        <p>
            Nous proposons les systèmes pré-installés suivants:<br/>
            Debian : versions stable et oldstable<br/>
            Ubuntu : versions  LTS encore supportées + les 2 dernières versions intermédiaires publiées<br/>
            Yunohost : version stable  (et testing à certains moments de l’année)<br/><br/>
            Si vous choisissez YunoHost, merci de nous indiquer le nom de domaine principal à configurer si vous en avez déjà un.
            À votre demande, tout système libre sur architecture x86/x86-64, il suffit de nous demander d'ajouter l'iso que vous souhaitez. Exemples : Debian, Ubuntu Server, CentOS, OpenBSD, FreeBSD, etc. L'installation est alors entièrement à votre charge (elle n'est ni automatique, ni clé en main).
        </p>
    </div><div class="infra-question-box">
        <h3>Est-il possible de faire une installation personnalisée du système d'exploitation ?</h3>
        <p>
            Oui, à votre demande. Cela vous permet par exemple d'activer le chiffrement de votre disque dur. À défaut, la configuration est celle de l'installation standard (une seule partition non chiffrée, système en anglais, etc.).
        </p>
    </div><div class="infra-question-box">
        <h3>Comment accéder à mon VPS ?</h3>
        Nous enregistrons :
            <ul>
            <li>Par SSH, accès en ligne de commande / texte. Nous préférons une authentification par clé afin d'éviter des attaques par bruteforce (force brute en français, essai systématique et automatique de toutes les combinaisons identifiant + mot de passe possibles afin de trouver celle que vous avez choisie). Une authentification par mot de passe reste possible, mais est vivement déconseillée.</li>
            <li>Par VNC, affichage graphique à distance. Cela permet de se dépanner soi-même si vous commettez une erreur qui vous empêche d'avoir un accès SSH (mauvaise configuration du pare-feu, VPS qui ne redémarre pas comme prévu, etc.). C'est également cette fonctionnalité qui rend possible une installation personnalisée du système d'exploitation et la saisie de votre phrase de passe pour démarrer un système chiffré.</li>
            <li>Via l’interface d’administration web si vous avez choisi YunoHost</li>
            </ul>
    </div>
    <div class="infra-question-box">
        <h3>Puis-je personnaliser le reverses DNS pour mes IPs ?</h3>
        <p>
            Oui en nous envoyant un mail.<br/>
Pour l'IPv4, il faut simplement nous indiquer le nom désiré.<br/>
Pour l'IPv6, soit vous nous indiquez le nom désiré pour une ou plusieurs adresses, soit nous vous déléguons la zone qui correspond à votre /56, afin que vous gériez vous-même les reverses associés à vos adresses. La deuxième méthode suppose que vous disposez d'au moins un serveur de noms qui fait autorité.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Y-a-t’il un engagement ?</h3>
        <p>
           Non, en revanche les services sont renouvelés tacitement pour une durée de 30 jours chaque 1er du mois.<br/>
Si vous ne vous servez plus du service, n’oubliez pas de sauvegarder vos données et de résilier l’abonnement.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Comment payer ?</h3>
        <p>
          Vous bénéficiez d'une période d'essai d'un mois, prenant fin le 1er du mois suivant votre inscription. Après ce mois d'essai, une facturation aura lieu chaque 1er du mois. Le premier règlement est donc attendu au plus tard au courant de la première quinzaine du mois suivant l'inscription, avec possibilité de régler jusqu'à 12 mois d'avance en une seule fois.<br/>
Le règlement est à effectuer par virement bancaire. Les références bancaires se trouvent dans l'espace membre. N'oubliez pas d'indiquer votre identifiant « ID » + le numéro d'identifiant qui vous a été attribué.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Où se trouve le datacenter qui héberge les serveurs d'ARN ?</h3>
        <p>
           Il s'agit du datacenter Cogent, situé 46 route de Bischwiller à Schiltigheim, Alsace, France.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Quel est le débit ?</h3>
        <p>
          Capacité max. : 100 Mbps mutualisés entre tous les services et les membres de l’association, sans aucune limite de volume. <a href="https://arn-fai.net/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-">Est-ce que je pénalise l'association si je télécharge beaucoup de fichiers ?</a>
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Est-ce que l’association conserve des informations / traces / logs ?</h3>
        <p>
           Oui, tel que requis par la législation française, nous conservons  votre identité + les adresses IP assignées par ARN à votre VPS. Pas plus, pas moins. Tout cela est conservé pendant un an. ARN se réserve le droit de vérifier la véracité de votre identité.<br/>
<b>Attention</b> : ARN est une association située en France et nous respectons  la législation en vigueur. Ainsi, nous donnerons suite à toute demande d'identification ou arrêt du service justifiée par la loi. Autrement dit, si votre objectif  est de vous cacher de l’ARCOM (autorité chargée de lutter contre la diffusion d'œuvres sans l'accord de leurs auteur-ice-s) ou de  mettre en place un site web présentant un contenu interdit par la loi  française, ne prenez pas un VPS chez ARN. <br>
Ce sont là des exemples parmi d'autres, toute la législation française et européenne s'impose à vous et à ARN.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Comment ça marche ? (Technologies utilisées )</h3>
        <p>
           KVM pour l'hyperviseur, Ganeti pour l’orchestration des VM, et DRBD pour la redondance : en cas de panne matérielle sur l'un de nos serveurs, votre VPS sera transféré sur un autre serveur dans la même baie. En théorie, aucune interruption de service à prévoir lors d'une maintenance de notre côté. Mais en  pratique, dans certains cas, votre machine peut être redémarrée.
        </p>
    </div>
    <div class="infra-question-box">
        <h3>Comment reproduire ce service ?</h3>
        <p>
           La partie principale est documentée sur la page dédiée à <a href="https://wiki.arn-fai.net/benevoles:technique:ganeti">Ganeti</a>. Toutefois, nous prévoyons de migrer vers Proxmox. Et si vous envisagez de déployer un cluster de VM, nous pensons également que cette solution est plus facile à prendre en main.
        </p>
    </div>
</div>
<div class="natta-help" style="text-align:center">
            <p class="aidez_nous">Nos actions vous plaisent ? Aidez-nous :)</p>
            <p><b>Venez à la prochaine réunion d’accueil des bénévoles</b></p>
</div>