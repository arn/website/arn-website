---
title: 'Nos offres'
body_classes: modular
---
## __Nos *offres*__
---
### Serveur de base

__*7€* / mois__

1 vCPU<br>
1 à 2Go de ram<br>
15Go SSD raid5 ou 6
1 IPv4 publique<br>
1 préfixe IPv6 publique<br>
Reverse DNS personnalisable<br>
100 Mbps symétrique<br>
sans engagement<br>

---
### En supplément ###    {.second-box}

__*+1€* / mois__

+1 Go de ram<br>
**OU** <br>
+15 Go SSD<br>
**OU**<br>
+64 Go HDD<br>

---
*IPv4 supplémentaire : 4€ / mois - vCPU supplémentaire: 5€ / mois. Prix en franchise de TVA (art 293B du CGI), c'est à dire TTC. Sans engagement. Requiert une [adhésion à l’association](/asso/adhérer). Renouvellement de l’abonnement et de l’adhésion annuelle par tacite reconduction.*


