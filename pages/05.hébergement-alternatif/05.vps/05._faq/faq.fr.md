---
title: 'Foire aux questions'
body_classes: modular
---

## __Foire aux *questions*__

---
### Pourquoi dois-je adhérer ?
Chez Alsace Réseau Neutre, association à but non lucratif sans salariés, vous n’êtes pas client⋅es d’un fournisseur. Vous êtes membre d’un collectif qui s’organise pour construire un bout d’internet utopique et influencer les politiques numériques vers des schémas plus vertueux. Les adhésions nous permettent de proposer ces services et de les financer.

Par ailleurs, nous souhaitons qu’au travers de l’assemblée générale, les personnes abonnées aient un droit de vote concernant l’avenir de leurs services.

---
### Que signifie « Prix en franchise de TVA » ?

La franchise de TVA signifie qu’à ce jour ARN n’est pas soumis à la TVA. Le montant « Hors Taxe » est donc égal au montant « Toutes Taxes Comprises ».


---

### Y-a-t’il un engagement ?

Non, en revanche les services sont renouvelés tacitement pour une durée de 30 jours chaque 1er du mois.

Si vous ne vous servez plus du service, n’oubliez pas de sauvegarder vos données et de résilier l’abonnement.


---

### Comment payer ?

Tous les 1er du mois, une facture est éditée et il est attendu que vous la régliez sous 15 jours.

Si vous le souhaitez vous avez la possibilité de régler jusqu'à 12 mois d'avance en une seule fois.

Le règlement est à effectuer par virement bancaire. Les références bancaires se trouvent dans l'espace membre. **N'oubliez pas d'indiquer votre identifiant « ID » + le numéro d'identifiant qui vous a été attribué.**

Si vous n'avez pas le choix, nous acceptons les paiements en liquide à transmettre aux personnes en charges de la trésorerie d'ARN.

---

### Où se trouve le datacenter qui héberge les serveurs d'ARN ?

Il s'agit du datacenter Cogent, situé 46 route de Bischwiller à Schiltigheim, Alsace, France.

---
### Quelle est la politique de sauvegarde des données de connexions (logs) d’ARN pour ce service ?

Nous enregistrons :

 * votre identité
 * les IPs ARN qui vous sont attribuées
 * la période d’attribution

Toutefois, nous vous invitons à lire la clause « [Mésusage des services](/asso/cgs) » de nos CGS. Notez que dans le cadre d’une enquête, nous pourrions être contraints légalement de permettre une mise sur écoute de votre VPS ou même de couper votre service d'hébergement sur un simple signalement LCEN.

---
### Comment obtenir de l'aide en cas de problème ?
L'aide peut être obtenue en nous contactant sur le formulaire de contact, le forum, le chat d'entraide et même en présentiel lors des permanences du vendredi soir au hackstub. Il est impératif que vous preniez le temps de décrire votre problème.

Les bénévoles d'ARN essaierons de résoudre votre problème en faisant un diagnostique et potentiellement en vous accompagnant par mail ou chat (exceptionnellement téléphone). 

! IMPORTANT: vous êtes aux commandes de votre VPS, nous ne nous chargeons pas de l'infogérance de votre serveur. La sollicitation de l'aide de l'équipe de support par mail est donc réservé aux problématiques du ressort de l'association, pas aux déploiement et à la maintenance de service sur votre VPS. Vous pouvez toutefois demander de l'aide aux restes des membres de l'asso ou sur des forums dédiés comme celui de YunoHost.

---

### Je ne suis pas sûr de ma capacité à gérer un VPS ?

Nos VPS peuvent être **pré-installés avec le système d'exploitation YunoHost**, qui réduit les compétences nécessaires pour parvenir à un résultat. Cependant, la pratique de l’auto-hébergement, c’est un peu comme le jardinage : il faut s’attendre à devoir y passer un peu de temps, et ce sera toujours plus long que d’avoir ses services chez un tiers. L’idéal est d’y aller pas à pas. En effet, les services VPS étant sans engagement, vous pouvez les résilier à tout moment.

Si vous hésitez, l’association organise chaque mois  une table « auto-hébergement » lors de l’atelier « Libérons-nous du pistage ». Vous pouvez aussi vous renseigner auprès des  autres membres directement sur le forum ou le chat.

! Attention tout de même : l’équipe support d’ARN ne pourra pas être considérée comme étant en charge de l’infogérance du système à l’intérieur de votre VPS.

Si vous estimez que tout cela est trop complexe, pensez à consulter [sans-nuage.fr](https://sans-nuage.fr) et les propositions des autres <a href="https://chatons.org" target="_blank">CHATONS.</a>

---
### Puis-je héberger mon propre serveur mail sur cette connexion ?
Oui. La connexion étant neutre, les ports ne sont pas bridés (même le port 25) et vous pouvez définir un reverse DNS. En plus, nous ne listons pas nos IPs comme résidentiels, contrairement à de nombreux FAI.

Attention tout de même à ne pas ternir la réputation des IPs.

---

### Puis-je personnaliser le reverses DNS pour mes IPs ?

Oui en nous envoyant un mail.

Pour l'IPv4, il faut simplement nous indiquer le nom désiré.

Pour l'IPv6, soit vous nous indiquez le nom désiré pour une ou plusieurs adresses, soit nous vous déléguons la zone qui correspond à votre /56, afin que vous gériez vous-même les reverses associés à vos adresses. La deuxième méthode suppose que vous disposez d'au moins un serveur de noms qui fait autorité.

---
### L'IPv6 est-elle disponible ?
Oui.


---

### Quel est le débit ?

Capacité max. : 100 Mbps mutualisés entre tous les services et les membres de l’association, sans aucune limite de volume. <a href="https://arn-fai.net/factu-opes#est-ce-qu-un-e-gros-sse-t-l-chargeur-se-p-nalise-l-association-">Est-ce que je pénalise l'association si je télécharge beaucoup de fichiers ?</a>

---

### Est-ce que l’association sauvegarde automatiquement mon VPS ?

NON. La sauvegarde de vos données et de vos configurations présentes sur votre VPS est entièrement à votre charge. Toutefois, si vous cherchez un espace de sauvegarde, n’hésitez pas à solliciter les membres sur le forum.


---

### Est-il possible de faire évoluer les caractéristiques de mon VPS ? 

Il est possible de passer à une offre supérieure tant que vous restez dans la même gamme (STO ou SSD). Le passage à une offre inférieure n'est pas possible en ce qui concerne la quantité de SSD ou de HDD.

Nos ressources (stockage, RAM, CPU) sont limitées. Nous prônons un usage optimal et responsable des ressources, ainsi que la solidarité entre les membres de l'association. Nous nous opposons donc à l'accaparation de toutes les ressources par quelques un-e-s   et au gaspillage. Dans ce contexte, si vous savez que vous n'utiliserez pas une des ressources de votre offre, vous pouvez l'indiquer lors de la création de votre VPS. L'administrateur dimensionnera le VPS en fonction de cela.

Vous pouvez demander une adresse IPv4 supplémentaire pour 4€/mois, quelle que soit votre offre. Vous pouvez également demander un ou plusieurs préfixes IPv6 sans surcoût.



---

### Quels systèmes d'exploitation sont disponibles ?

Nous proposons les systèmes pré-installés suivants:

 * Debian : versions stable et oldstable
 * Ubuntu : versions  LTS encore supportées + les 2 dernières versions intermédiaires publiées
 * Yunohost : version stable  (et testing à certains moments de l’année)

Si vous choisissez YunoHost, merci de nous indiquer le nom de domaine principal à configurer si vous en avez déjà un.

À votre demande, tout système libre sur architecture x86/x86-64, il suffit de nous demander d'ajouter l'iso que vous souhaitez. Exemples : Debian, Ubuntu Server, CentOS, OpenBSD, FreeBSD, etc. L'installation est alors entièrement à votre charge (elle n'est ni automatique, ni clé en main).


---

### Est-il possible de faire une installation personnalisée du système d'exploitation ?

Oui, à votre demande. Cela vous permet par exemple d'activer le chiffrement de votre disque dur. À défaut, la configuration est celle de l'installation standard (une seule partition non chiffrée, système en anglais, etc.).


---

### Comment accéder à mon VPS ?

Nous enregistrons :

 * Par SSH, accès en ligne de commande / texte. Nous préférons une authentification par clé afin d'éviter des attaques par bruteforce (force brute en français, essai systématique et automatique de toutes les combinaisons identifiant + mot de passe possibles afin de trouver celle que vous avez choisie). Une authentification par mot de passe reste possible, mais est vivement déconseillée.
 * Par VNC, affichage graphique à distance. Cela permet de se dépanner soi-même si vous commettez une erreur qui vous empêche d'avoir un accès SSH (mauvaise configuration du pare-feu, VPS qui ne redémarre pas comme prévu, etc.). C'est également cette fonctionnalité qui rend possible une installation personnalisée du système d'exploitation et la saisie de votre phrase de passe pour démarrer un système chiffré.
 * Via l’interface d’administration web si vous avez choisi YunoHost



---

### Comment ça marche ? (Technologies utilisées )

KVM pour l'hyperviseur, Ganeti pour l’orchestration des VM, et DRBD pour la redondance : en cas de panne matérielle sur l'un de nos serveurs, votre VPS sera transféré sur un autre serveur dans la même baie. En théorie, aucune interruption de service à prévoir lors d'une maintenance de notre côté. Mais en  pratique, dans certains cas, votre machine peut être redémarrée.


---

### Comment reproduire ce service ?

La partie principale est documentée sur la page dédiée à [Ganeti](https://wiki.arn-fai.net/benevoles:technique:ganeti). Toutefois, nous prévoyons de migrer vers Proxmox. Et si vous envisagez de déployer un cluster de VM, nous pensons également que cette solution est plus facile à prendre en main.


---
