---
title: 'Qu''est-ce qu''un VPS ?'
body_classes: modular
image_align: left
---

## __Qu'est-ce qu'un *VPS* ?__

VPS est l'abbréviation de virtual private serveur soit en français : serveur privé virtuel

Un **serveur** est une machine, en général allumée 24h/24, qui répond à des demandes faites par les utilisateurs en ligne. Par exemple, elle permet d'accéder à une page web ou de transmettre un message dans une boîte mail. Pour mutualiser les coûts, il est possible de simuler plusieurs **serveurs privés virtuels (VPS)** à l’intérieur d’un serveur physique.

Vous pouvez, par exemple, vous en servir pour « <a href="https://yunohost.org/fr/administrate/selfhosting" target="_blank">auto-héberger</a> » un drive ou des mails, pour votre famille ou votre association.

