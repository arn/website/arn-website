
var map;

function init() {
	map = L.map('map').setView([48.4128, 8.1656], 9);

	var mapnikUrl = 'http://b.www.toolserver.org/tiles/bw-mapnik/{z}/{x}/{y}.png';
	var vmfabsUrl = 'https://tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png';
	var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

	var minimal = L.tileLayer(mapnikUrl, { maxZoom: 18 });
	var vmfabs = L.tileLayer(vmfabsUrl,  { maxZoom: 18 });
	var osm = L.tileLayer(osmUrl, { maxZoom: 18, attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors' });

	var baseMaps = { 
		'HillShade': vmfabs,
		'Minimal': minimal,
		'OSM': osm,
	};

	itof_immeuble=new L.layerGroup();

	var allMapLayers = { 'minimal': minimal, 'vmfabs': vmfabs, 'osm': osm, 'itof_immeuble': itof_immeuble };

	var overlayMaps = {
		'Éligibilité bâtiment': itof_immeuble,
	}

	$("#sidebar_button").click(function() {
		$("#sidebar").toggle();
	}).tooltip({
		placement : 'left',
		title : 'Afficher/masquer la sidebar'
	});

	$('#torpedo').modal('show');

	osm.addTo(map);

	L.control.layers(baseMaps, overlayMaps).addTo(map);

	if (L.Browser.mobile) {
		map.removeControl(map.zoomControl);
		$('#sidebar').hide();
	}

	var geojsonMarkerOptions = {
		radius: 6,
		color: "#000",
		fill: 1,
		weight: 1,
		opacity: 1,
		fillOpacity: 1,
	};

	var geojsonPolyLineOptions = {
		weight: 3,
		fill: 1,
		opacity: 1,
	};

	var hash = new L.Hash(map);

	map.on('moveend', function() {
		zoom_based_layerchange();
	});
}

function zoom_based_layerchange() {
	var currentZoom = map.getZoom();

	if (currentZoom > 15) {
		load_itof();
		$("#alert").hide();
	} else {
		itof_immeuble.clearLayers();
		$("#alert").show();
	}
}

function load_itof() {
	console.log(map.getBounds());
	var geojsonMarkerOptions = {
		radius: 6,
		color: "#000",
		fill: 1,
		weight: 1,
		opacity: 1,
		zindex: 1,
		fillOpacity: 1,
		contextmenu: false
	};

	var geojsonPolyLineOptions = {
		weight: 3,
		fill: 1,
		zindex: 2,
		opacity: 0.8,
		contextmenu: false
	};

	// Lib d'icones : http://www.softicons.com/search?search=round
	var okProdMarker = L.icon({
		iconUrl: 'assets/images/green.png',
		iconAnchor: [ 8, 8 ],
	});

	var waitingMarker = L.icon({
		iconUrl: 'assets/images/yellow.png',
		iconAnchor: [ 8, 8 ],
	});

	var nokProdMarker = L.icon({
                iconUrl: 'assets/images/red.png',
                iconAnchor: [ 8, 8 ],
        });

	$.get("https://cooperateurs.scani.fr/api/eligibilite?bounds="+JSON.stringify(map.getBounds()), function(data) {
		var geojsonLayer = new L.GeoJSON(data, {
			style: function(feature) {
				return {fillColor: '#551111'};
			},
			pointToLayer: function(feature, latlng) {
				if (feature.properties.etatimmeuble == 3) {
					geojsonMarkerOptions.icon = okProdMarker;
				} else if (feature.properties.etatimmeuble == 2) {
					geojsonMarkerOptions.icon = waitingMarker;
				} else {
					geojsonMarkerOptions.icon = nokProdMarker;
				}

				var marker = L.marker(latlng, geojsonMarkerOptions);
				return marker;
			},
			onEachFeature: function(feature, layer) {
				createPopup(feature, layer);
			}

		} );

		itof_immeuble.clearLayers();
		itof_immeuble.addLayer(geojsonLayer);
		itof_immeuble.addTo(map);
	});
}

function createPopup (feature, layer) {
	{
		if (feature.geometry.type == 'Point') {
			var names = '';
//			names = names + '<div class="row"><div class="col-md-8">';
			// <h4 align="center">'+feature.properties.identifiantimmeuble+'</h4>';
			names = names+feature.properties.address + ' (' + feature.properties.oi + ')';
			//names = names + '</div><div class="col-md-4 align-middle" id="eligpart">';
			names = names + '<div id="eligpart">';
			if (feature.properties.etatimmeuble == 3) {
			names = names + '<h4>Vous êtes éligible !</h4>';
				names = names+"<p>Vous êtes éligible au raccordement fibre optique par le réseau <b>ROSACE</b> sous 3 mois maximum.<br>ARN peut sans doute vous raccorder :)</p>";
			} else if (feature.properties.etatimmeuble == 2) {
			names = names + '<h4>Vous êtes bientôt éligible !</h4>';
				names = names+"<p>Déploiement via le réseau ROSACE en cours sur la zone. Disponible dans 3 à 9 mois !<br>ARN pourra sans doute vous raccorder.</p>";
			} else {
				names = names+"<p>Non éligible pour l'instant."
                names = names + "<br> Nous pouvons toutefois peut-être faire quelques choses:<ul><li>Si vous connaissez un bâtiment voisin visible qui accepterait de nous laisser placer une antenne pour renvoyer le signal vers chez vous</li><li>Si vous êtes <a href='https://vador.fdn.fr/souscription/souscription.cgi' target='_parent'>éligible xDSL via le réseau FDN</a></li></ul>"
                names = names + "<p>NB: le réseau ROSACE est déployé uniquement sur les zones peu dense. </p>";
			}
			names = names + '</div>';
			// names = names + '</div>';
			layer.bindPopup(names, {maxWidth: 300, minWidth: 150, maxHeight: 250, closeButton: true } );
		}
	}
}

function test_radio(lon, lat) {
	$('#eligpart').html('<img src="https://cooperateurs.scani.fr/images/loading_wheel.gif" height="50" width="50">');
	$.get('https://cooperateurs.scani.fr/api/eligibilite?lat='+lat+'&lon='+lon+'&dist=1000', function(data) {
		if (data.scani.apcount == 0) {
			$('#eligpart').html("pas d'émetteur Alsace Réseau Neutre dans le secteur. Il va falloir travailler ça ensemble !");
		} else if (data.scani.apcount < 3) {
			$('#eligpart').html("On devrait pouvoir vous connecter en radio mais il faut venir vérifier sur place !");
		} else {
			$('#eligpart').html("Il y a tout un tas d'émetteurs SCANI dans votre secteur, ça devrait aller pour vous connecter en radio !");
		}
		console.log(data.scani.apcount);
	});

}

function map_cp(datum) {
	console.log("infunc");
	console.log(datum);
	$.get('https://cooperateurs.scani.fr/api/eligibilite?get_coord='+datum, function (data) {
		console.log(data);
		$('#info').html(data);
		map.setView([data[1], data[0]], 18);
	});
	$('#info').show()
};

