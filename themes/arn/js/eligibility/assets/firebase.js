const config = {
	apiKey: "AIzaSyCeiEZizS9dEKy4dPsVO0AS95APQAbCOms",
	authDomain: "scani-app.firebaseapp.com",
	databaseURL: "https://scani-app.firebaseio.com",
	projectId: "scani-app",
	storageBucket: "scani-app.appspot.com",
	messagingSenderId: "532869444065",
	appId: "1:532869444065:web:e19991dbb02d76ca1f22a0"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.onMessage(payload => {
	console.log("Message received. ", payload);
	var notification = JSON.parse(payload.data.notification);
	var text = '<b>'+atob(notification.title)+'</b><br/>'+atob(notification.body);
	if (notification.click_action) {
		text = text+'<br/><a href="'+notification.click_action+'">Cliquez ici</a>';
	}
	$.bootstrapGrowl(text, { delay: 0 });
});

