$('#return-to-top').hide();

$(window).scroll(function () {
    if ($(document).width() > 800) {
        if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
            $('#return-to-top').fadeIn(200);    // Fade in the arrow
        } else {
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
        }
        var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
        var footerHeight = $("#footer").height() - 80;
        if (scrollBottom <= footerHeight) {        // If page is scrolled more than 50px
            $('#return-to-top').css("bottom",footerHeight + "px");
        } else {
            $('#return-to-top').css("bottom","20px");
        }
    }
});
