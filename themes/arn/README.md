# Arn Theme

The **Arn** Theme is for [Grav CMS](http://github.com/getgrav/grav).  This README.md file should be modified to describe the features, installation, configuration, and general usage of this theme.

## Description

Theme pour la refonte du site web ARN par des élèves du master CAWEB

## Compilation du SCSS
```
sudo apt install node-node-sass
cd themes
sass arn/scss/theme.scss:arn/css-compiled/theme.min.css --style compressed
sass arn/scss/theme.scss:arn/css-compiled/theme.css
```
